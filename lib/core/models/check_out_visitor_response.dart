class CheckOutVisitorResponse {
  Data? data;
  String? message;
  String? errorMessage;
  bool? error;

  CheckOutVisitorResponse(
      {this.data, this.message, this.errorMessage, this.error});

  CheckOutVisitorResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    errorMessage = json['errorMessage'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['message'] = message;
    data['errorMessage'] = errorMessage;
    data['error'] = error;
    return data;
  }
}

class Data {
  String? id;
  String? visitorName;
  String? hostName;
  String? department;
  String? designation;
  String? noVisitors;
  String? documentNumber;
  String? documentType;
  String? plateNo;
  String? passId;
  String? visitReason;
  String? datetimeIn;
  String? datetimeOut;
  String? siteId;
  String? status;
  List<Items>? items;

  Data(
      {this.id,
      this.visitorName,
      this.hostName,
      this.department,
      this.designation,
      this.noVisitors,
      this.documentNumber,
      this.documentType,
      this.plateNo,
      this.passId,
      this.visitReason,
      this.datetimeIn,
      this.datetimeOut,
      this.siteId,
      this.status,
      this.items});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorName = json['visitor_name'];
    hostName = json['host_name'];
    department = json['department'];
    designation = json['designation'];
    noVisitors = json['no_visitors'];
    documentNumber = json['documentNumber'];
    documentType = json['document_type'];
    plateNo = json['plateNo'];
    passId = json['pass_id'];
    visitReason = json['visit_reason'];
    datetimeIn = json['datetime_in'];
    datetimeOut = json['datetime_out'];
    siteId = json['site_id'];
    status = json['status'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['visitor_name'] = visitorName;
    data['host_name'] = hostName;
    data['department'] = department;
    data['designation'] = designation;
    data['no_visitors'] = noVisitors;
    data['documentNumber'] = documentNumber;
    data['document_type'] = documentType;
    data['plateNo'] = plateNo;
    data['pass_id'] = passId;
    data['visit_reason'] = visitReason;
    data['datetime_in'] = datetimeIn;
    data['datetime_out'] = datetimeOut;
    data['site_id'] = siteId;
    data['status'] = status;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? id;
  String? visitorId;
  String? name;
  String? serialNo;

  Items({this.id, this.visitorId, this.name, this.serialNo});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorId = json['visitor_id'];
    name = json['name'];
    serialNo = json['serialNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['visitor_id'] = visitorId;
    data['name'] = name;
    data['serialNo'] = serialNo;
    return data;
  }
}
