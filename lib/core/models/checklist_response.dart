class CheckListResponse {
  List<CheckListData>? data;
  String? message;
  bool? errors;

  CheckListResponse({this.data, this.message, this.errors});

  CheckListResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <CheckListData>[];
      json['data'].forEach((v) {
        data!.add(CheckListData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class CheckListData {
  String? id;
  String? plannerId;
  String? name;
  String? questionType;
  String? type;
  String? category;
  String? status;

  CheckListData(
      {this.id,
      this.plannerId,
      this.name,
      this.questionType,
      this.type,
      this.category,
      this.status});

  CheckListData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    plannerId = json['planner_id'];
    name = json['name'];
    questionType = json['question_type'];
    type = json['type'];
    category = json['category'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['planner_id'] = plannerId;
    data['name'] = name;
    data['question_type'] = questionType;
    data['type'] = type;
    data['category'] = category;
    data['status'] = status;
    return data;
  }
}
