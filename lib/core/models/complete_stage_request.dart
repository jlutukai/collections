class CompleteStageRequest {
  String? id;
  int? formId;
  int? answeredBy;
  String? plannerId;
  String? shiftId;
  String? endDate;
  String? date;
  String? type;
  int? plannerStageId;
  int? typeId;
  String? userId;
  List<Responses>? items;

  CompleteStageRequest(
      {this.id,
        this.formId,
        this.answeredBy,
        this.plannerId,
        this.shiftId,
        this.endDate,
        this.date,
        this.type,
        this.plannerStageId,
        this.typeId,
        this.userId,
        this.items});

  CompleteStageRequest.fromJson(Map<String, dynamic> json) {
    formId = json['form_id'];
    answeredBy = json['answered_by'];
    plannerId = json['planner_id'];
    shiftId = json['shift_id'];
    endDate = json['end_date'];
    date = json['date'];
    type = json['type'];
    userId = json['user_id'];
    plannerStageId = json['planner_stage_id'];
    typeId = json['type_id'];
    if (json['items'] != null) {
      items = <Responses>[];
      json['items'].forEach((v) {
        items!.add(Responses.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['form_id'] = formId;
    data['answered_by'] = answeredBy;
    data['planner_id'] = plannerId;
    data['shift_id'] = shiftId;
    data['end_date'] = endDate;
    data['date'] = date;
    data['type'] = type;
    data['user_id'] = userId;
    data['planner_stage_id'] = plannerStageId;
    data['type_id'] = typeId;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Responses {
  String? formItemId;
  String? response;
  String? type;

  Responses({this.formItemId, this.response, this.type});

  Responses.fromJson(Map<String, dynamic> json) {
    formItemId = json['form_item_id'];
    response = json['response'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['form_item_id'] = formItemId;
    data['response'] = response;
    data['type'] = type;
    return data;
  }
}
