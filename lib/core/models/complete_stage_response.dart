class CompleteStageResponse {
  int? data;
  String? message;
  String? erroMessage;
  bool? error;

  CompleteStageResponse(
      {this.data, this.message, this.erroMessage, this.error});

  CompleteStageResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    message = json['message'];
    erroMessage = json['erroMessage'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['data'] = this.data;
    data['message'] = message;
    data['erroMessage'] = erroMessage;
    data['error'] = error;
    return data;
  }
}
