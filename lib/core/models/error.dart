enum AppError {
  NotFound,
  InternalServerError,
  UndefinedError,
  ValidationError,
  BadRequest,
  JsonParsing,
  DBError,
}

class CustomError {
  final AppError key;
  final String? message;

  const CustomError({
    required this.key,
    this.message,
  });
}