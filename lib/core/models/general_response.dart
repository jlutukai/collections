class GeneralResponse {
  String? id;
  String? message;
  String? erroMessage;
  bool? error;

  GeneralResponse({this.id, this.message, this.erroMessage, this.error});

  GeneralResponse.fromJson(Map<String, dynamic> json) {
    id = json['data']?.toString();
    message = json['message'];
    erroMessage = json['erroMessage'];
    error = json['error'] == null ? false : json["error"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['erroMessage'] = erroMessage;
    data['error'] = error;
    return data;
  }
}
