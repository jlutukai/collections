class GetAvailableLines {
  List<AvailableLinesData>? data;
  String? message;
  bool? errors;

  GetAvailableLines({this.data, this.message, this.errors});

  GetAvailableLines.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <AvailableLinesData>[];
      json['data'].forEach((v) {
        data!.add(AvailableLinesData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class AvailableLinesData {
  String? plannerId;
  String? plannerStageId;
  String? stageName;
  String? varietyName;
  String? pictureName;

  AvailableLinesData(
      {this.plannerId,
        this.plannerStageId,
        this.stageName,
        this.varietyName,
        this.pictureName});
  @override
  String toString() {
    return '${varietyName ?? 'undefined'} - $stageName';
  }

  AvailableLinesData.fromJson(Map<String, dynamic> json) {
    plannerId = json['planner_id'];
    plannerStageId = json['planner_stage_id'];
    stageName = json['stage_name'];
    varietyName = json['variety_name'];
    pictureName = json['picture_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['planner_id'] = plannerId;
    data['planner_stage_id'] = plannerStageId;
    data['stage_name'] = stageName;
    data['variety_name'] = varietyName;
    data['picture_name'] = pictureName;
    return data;
  }
}
