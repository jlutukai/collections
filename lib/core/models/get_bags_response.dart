import 'package:weight_collection/core/models/local/localModels.dart';

class GetBagResponse {
  List<CollectionData>? data;
  String? message;
  bool? errors;

  GetBagResponse({this.data, this.message, this.errors});

  GetBagResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <CollectionData>[];
      json['data'].forEach((v) {
        data!.add(CollectionData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}
