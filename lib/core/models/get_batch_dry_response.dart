class GetBatchDryResponse {
  List<BatchDryData>? data;
  String? message;
  bool? errors;

  GetBatchDryResponse({this.data, this.message, this.errors});

  GetBatchDryResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <BatchDryData>[];
      json['data'].forEach((v) {
        data!.add(BatchDryData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class BatchDryData {
  String? batchId;
  String? bagId;
  String? date;
  String? time;
  String? plannerStageId;
  String? dirtyWeight;
  String? speciesName;
  String? varietyName;
  String? pictureName;
  String? bagNo;
  String? lineId;

  BatchDryData(
      {this.batchId,
      this.bagId,
      this.bagNo,
      this.dirtyWeight,
      this.date,
      this.time,
      this.plannerStageId,
      this.speciesName,
      this.varietyName,
      this.pictureName,
      this.lineId});

  BatchDryData.fromJson(Map<String, dynamic> json) {
    batchId = json['batch_id'];
    bagId = json['bag_id'];
    date = json['date'];
    time = json['time'];
    plannerStageId = json['planner_stage_id'];
    dirtyWeight = json['dirty_weight'];
    speciesName = json['species_name'];
    varietyName = json['variety_name'];
    pictureName = json['picture_name'];
    lineId = json['line_id'];
    bagNo = json['bag_no'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['batch_id'] = batchId;
    data['bag_id'] = bagId;
    data['date'] = date;
    data['time'] = time;
    data['dirty_weight'] = dirtyWeight;
    data['planner_stage_id'] = plannerStageId;
    data['species_name'] = speciesName;
    data['variety_name'] = varietyName;
    data['picture_name'] = pictureName;
    data['line_id'] = lineId;
    data['bag_no'] = bagNo;
    return data;
  }
}
