class GetBatchWetResponse {
  List<WetBatchData>? data;
  String? message;
  bool? errors;

  GetBatchWetResponse({this.data, this.message, this.errors});

  GetBatchWetResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <WetBatchData>[];
      json['data'].forEach((v) {
        data!.add(WetBatchData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class WetBatchData {
  String? batchId;
  String? bagId;
  String? bagNo;
  String? date;
  String? time;
  String? plannerStageId;
  String? speciesName;
  String? varietyName;
  String? pictureName;
  String? lineId;
  String? totalWeight;

  WetBatchData(
      {this.batchId,
      this.bagId,
      this.bagNo,
      this.date,
      this.time,
      this.plannerStageId,
      this.speciesName,
      this.varietyName,
      this.pictureName,
      this.totalWeight,
      this.lineId});

  WetBatchData.fromJson(Map<String, dynamic> json) {
    batchId = json['batch_id'];
    bagId = json['bag_id'];
    date = json['date'];
    time = json['time'];
    plannerStageId = json['planner_stage_id'];
    speciesName = json['species_name'];
    varietyName = json['variety_name'];
    pictureName = json['picture_name'];
    lineId = json['line_id'];
    bagNo = json['bag_no'];
    totalWeight = json['total_weight'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['batch_id'] = batchId;
    data['bag_id'] = bagId;
    data['date'] = date;
    data['time'] = time;
    data['planner_stage_id'] = plannerStageId;
    data['species_name'] = speciesName;
    data['variety_name'] = varietyName;
    data['picture_name'] = pictureName;
    data['line_id'] = lineId;
    data['bag_no'] = bagNo;
    data['total_weight'] = totalWeight;
    return data;
  }
}
