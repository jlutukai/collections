
class GetBedLineItemsResponse {
  List<LineData>? data;
  String? message;
  bool? errors;

  GetBedLineItemsResponse({this.data, this.message, this.errors});

  GetBedLineItemsResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <LineData>[];
      json['data'].forEach((v) {
        data!.add(LineData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class LineData {
  String? plannerStageId;
  String? varietyName;
  String? formName;
  String? stageName;
  String? endDate;
  String? pictureName;
  String? type;
  List<Questions>? items;

  LineData(
      {this.plannerStageId,
        this.varietyName,
        this.formName,
        this.stageName,
        this.endDate,
        this.pictureName,
        this.type,
        this.items});


  @override
  String toString() {
    return '${varietyName ?? 'undefined'} - ${stageName??""}';
  }

  LineData.fromJson(Map<String, dynamic> json) {
    plannerStageId = json['planner_stage_id'];
    varietyName = json['variety_name'];
    formName = json['form_name'];
    stageName = json['stage_name'];
    endDate = json['end_date'];
    pictureName = json['picture_name'];
    type = json['type'];
    if (json['items'] != null) {
      items = <Questions>[];
      json['items'].forEach((v) {
        items!.add(Questions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['planner_stage_id'] = plannerStageId;
    data['variety_name'] = varietyName;
    data['form_name'] = formName;
    data['stage_name'] = stageName;
    data['end_date'] = endDate;
    data['picture_name'] = pictureName;
    data['type'] = type;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Questions {
  String? id;
  String? name;
  String? formId;
  String? type;
  String? typeCode;
  String? status;
  String? conditionalQuestion;
  String? expectedResponse;
  String? secondaryName;
  String? expectedResponseType;
  String? dbName;

  Questions(
      {this.id,
        this.name,
        this.formId,
        this.type,
        this.typeCode,
        this.status,
        this.conditionalQuestion,
        this.expectedResponse,
        this.secondaryName,
        this.expectedResponseType,
        this.dbName});

  Questions.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    formId = json['form_id'];
    type = json['type'];
    typeCode = json['type_code'];
    status = json['status'];
    conditionalQuestion = json['conditional_question'];
    expectedResponse = json['expected_response'];
    secondaryName = json['secondary_name'];
    expectedResponseType = json['expected_response_type'];
    dbName = json['db_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['form_id'] = formId;
    data['type'] = type;
    data['type_code'] = typeCode;
    data['status'] = status;
    data['conditional_question'] = conditionalQuestion;
    data['expected_response'] = expectedResponse;
    data['secondary_name'] = secondaryName;
    data['expected_response_type'] = expectedResponseType;
    data['db_name'] = dbName;
    return data;
  }
}
