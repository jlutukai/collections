class GetClerkPerformanceResponse {
  List<PerformanceData>? data;
  String? message;
  bool? errors;

  GetClerkPerformanceResponse({this.data, this.message, this.errors});

  GetClerkPerformanceResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <PerformanceData>[];
      json['data'].forEach((v) {
        data!.add(PerformanceData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class PerformanceData {
  String? totalWeight;
  String? contractNo;
  String? speciesName;
  String? varietyName;
  List<PerformanceItems>? items;

  PerformanceData(
      {this.totalWeight,
        this.contractNo,
        this.speciesName,
        this.varietyName,
        this.items});

  PerformanceData.fromJson(Map<String, dynamic> json) {
    totalWeight = json['total_weight'];
    contractNo = json['contract_no'];
    speciesName = json['species_name'];
    varietyName = json['variety_name'];
    if (json['items'] != null) {
      items = <PerformanceItems>[];
      json['items'].forEach((v) {
        items!.add( PerformanceItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['total_weight'] = totalWeight;
    data['contract_no'] = contractNo;
    data['species_name'] = speciesName;
    data['variety_name'] = varietyName;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PerformanceItems {
  String? batchId;
  String? batchDirtyWeight;
  String? bedName;
  String? batchCleanWeight;
  String? cleanWeight;
  int? contributionPercentage;
  String? batchDate;

  PerformanceItems(
      {this.batchId,
        this.batchDirtyWeight,
        this.bedName,
        this.batchCleanWeight,
        this.cleanWeight,
        this.contributionPercentage,
        this.batchDate});

  PerformanceItems.fromJson(Map<String, dynamic> json) {
    batchId = json['batch_id'];
    batchDirtyWeight = json['batch_dirty_weight'];
    bedName = json['bed_name'];
    batchCleanWeight = json['batch_clean_weight'];
    cleanWeight = json['clean_weight'];
    contributionPercentage = json['contribution_percentage'];
    batchDate = json['batch_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['batch_id'] = batchId;
    data['batch_dirty_weight'] = batchDirtyWeight;
    data['bed_name'] = bedName;
    data['batch_clean_weight'] = batchCleanWeight;
    data['clean_weight'] = cleanWeight;
    data['contribution_percentage'] = contributionPercentage;
    data['batch_date'] = batchDate;
    return data;
  }
}
