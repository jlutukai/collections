class GetDailyWetResponse {
  List<DailyWetData>? data;
  String? message;
  bool? errors;

  GetDailyWetResponse({this.data, this.message, this.errors});

  GetDailyWetResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <DailyWetData>[];
      json['data'].forEach((v) {
        data!.add(DailyWetData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class DailyWetData {
  String? bagId;
  String? bagNo;
  String? bedId;
  String? varietyName;
  String? bedName;
  String? speciesName;
  String? plannerStageId;
  String? pictureName;
  String? lineId;
  String? staffName;
  String? staffId;
  String? supervisorName;
  String? supervisorId;
  String? greenhouseId;

  DailyWetData(
      {this.bagNo,
      this.bagId,
      this.bedId,
      this.varietyName,
      this.bedName,
      this.speciesName,
      this.plannerStageId,
      this.pictureName,
      this.lineId,
      this.staffName,
      this.staffId,
      this.supervisorName,
      this.supervisorId,
      this.greenhouseId});

  DailyWetData.fromJson(Map<String, dynamic> json) {
    bagId = json['bag_id'];
    bedId = json['bed_id'];
    varietyName = json['variety_name'];
    plannerStageId = json['planner_stage_id'];
    bedName = json['bed_name'];
    speciesName = json['species_name'];
    pictureName = json['picture_name'];
    lineId = json['line_id'];
    staffName = json['staff_name'];
    staffId = json['staff_id'];
    supervisorName = json['supervisor_name'];
    supervisorId = json['supervisor_id'];
    greenhouseId = json['greenhouse_id'];
    bagNo = json['bag_no'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bag_id'] = bagId;
    data['bed_id'] = bedId;
    data['variety_name'] = varietyName;
    data['planner_stage_id'] = plannerStageId;
    data['bed_name'] = bedName;
    data['species_name'] = speciesName;
    data['picture_name'] = pictureName;
    data['line_id'] = lineId;
    data['staff_name'] = staffName;
    data['staff_id'] = staffId;
    data['supervisor_name'] = supervisorName;
    data['supervisor_id'] = supervisorId;
    data['greenhouse_id'] = greenhouseId;
    data['bag_no'] = bagNo;
    return data;
  }
}
