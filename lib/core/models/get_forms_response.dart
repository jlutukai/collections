class GetFormsResponse {
  List<FormsData>? data;
  String? message;
  bool? errors;

  GetFormsResponse({this.data, this.message, this.errors});

  GetFormsResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <FormsData>[];
      json['data'].forEach((v) {
        data!.add(FormsData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class FormsData {
  String? id;
  String? name;
  String? description;
  List<Items>? items;
  List<Items>? customItems;

  FormsData({this.id, this.name, this.description, this.items, this.customItems});

  FormsData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    description = json['description'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
    if (json['custom_items'] != null) {
      customItems = <Items>[];
      json['custom_items'].forEach((v) {
        customItems!.add(Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['description'] = description;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    if (customItems != null) {
      data['custom_items'] = customItems!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? id;
  String? name;
  String? formId;
  String? type;
  String? typeCode;
  String? status;
  String? conditionalQuestion;
  String? expectedResponse;
  String? secondaryName;
  String? expectedResponseType;
  String? dbName;

  Items(
      {this.id,
        this.name,
        this.formId,
        this.type,
        this.typeCode,
        this.status,
        this.conditionalQuestion,
        this.expectedResponse,
        this.secondaryName,
        this.expectedResponseType,
        this.dbName});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    formId = json['form_id'];
    type = json['type'];
    typeCode = json['type_code'];
    status = json['status'];
    conditionalQuestion = json['conditional_question'];
    expectedResponse = json['expected_response'];
    secondaryName = json['secondary_name'];
    expectedResponseType = json['expected_response_type'];
    dbName = json['db_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['form_id'] = formId;
    data['type'] = type;
    data['type_code'] = typeCode;
    data['status'] = status;
    data['conditional_question'] = conditionalQuestion;
    data['expected_response'] = expectedResponse;
    data['secondary_name'] = secondaryName;
    data['expected_response_type'] = expectedResponseType;
    data['db_name'] = dbName;
    return data;
  }
}

