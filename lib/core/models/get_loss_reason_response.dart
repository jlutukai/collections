class GetLossReasonResponse {
  List<ReasonData>? data;
  String? message;
  bool? errors;

  GetLossReasonResponse({this.data, this.message, this.errors});

  GetLossReasonResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <ReasonData>[];
      json['data'].forEach((v) {
        data!.add(ReasonData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class ReasonData {
  String? id;
  String? name;
  String? status;

  ReasonData({this.id, this.name, this.status});


  @override
  String toString() {
    return '$name';
  }

  ReasonData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['status'] = status;
    return data;
  }
}
