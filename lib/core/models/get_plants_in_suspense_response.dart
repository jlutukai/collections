class GetPlantsInSuspenseResponse {
  List<PlantData>? data;
  String? message;
  bool? errors;

  GetPlantsInSuspenseResponse({this.data, this.message, this.errors});

  GetPlantsInSuspenseResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <PlantData>[];
      json['data'].forEach((v) {
        data!.add(PlantData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class PlantData {
  String? id;
  String? plannerStageId;
  String? plannerId;
  String? fromBed;
  String? toBed;
  String? initiatedBy;
  String? receivedBy;
  String? plantsInitiated;
  String? plantsReceived;
  String? balance;
  String? dateReceived;
  String? timeReceived;
  String? status;
  String? dateInitiated;
  String? timeInitiated;
  String? initiatedByName;
  String? pictureName;
  String? stageName;
  String? varietyName;

  PlantData(
      {this.id,
      this.plannerStageId,
      this.plannerId,
      this.fromBed,
      this.toBed,
      this.initiatedBy,
      this.receivedBy,
      this.plantsInitiated,
      this.plantsReceived,
      this.balance,
      this.dateReceived,
      this.timeReceived,
      this.status,
      this.dateInitiated,
      this.timeInitiated,
      this.initiatedByName,
      this.pictureName,
      this.stageName,
      this.varietyName});


  @override
  String toString() {
    return '${varietyName ?? 'undefined'} - $stageName';
  }

  PlantData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    plannerStageId = json['planner_stage_id'];
    plannerId = json['planner_id'];
    fromBed = json['from_bed'];
    toBed = json['to_bed'];
    initiatedBy = json['initiated_by'];
    receivedBy = json['received_by'];
    plantsInitiated = json['plants_initiated'];
    plantsReceived = json['plants_received'];
    balance = json['balance'];
    dateReceived = json['date_received'];
    timeReceived = json['time_received'];
    status = json['status'];
    dateInitiated = json['date_initiated'];
    timeInitiated = json['time_initiated'];
    initiatedByName = json['initiated_by_name'];
    pictureName = json['picture_name'];
    varietyName = json['variety_name'];
    stageName = json['stage_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['planner_stage_id'] = plannerStageId;
    data['planner_id'] = plannerId;
    data['from_bed'] = fromBed;
    data['to_bed'] = toBed;
    data['initiated_by'] = initiatedBy;
    data['received_by'] = receivedBy;
    data['plants_initiated'] = plantsInitiated;
    data['plants_received'] = plantsReceived;
    data['balance'] = balance;
    data['date_received'] = dateReceived;
    data['time_received'] = timeReceived;
    data['status'] = status;
    data['date_initiated'] = dateInitiated;
    data['time_initiated'] = timeInitiated;
    data['initiated_by_name'] = initiatedByName;
    data['picture_name'] = pictureName;
    data['variety_name'] = varietyName;
    data['stage_name'] = stageName;
    return data;
  }
}
