import 'scan_bed_response.dart';

class GetGreenHouseData {
  List<GreenHouseData>? data;
  String? message;
  bool? errors;

  GetGreenHouseData({this.data, this.message, this.errors});

  GetGreenHouseData.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <GreenHouseData>[];
      json['data'].forEach((v) {
        data!.add(GreenHouseData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class GreenHouseData {
  String? greenhouseName;
  String? greenhouseArea;
  int? greenhouseBedsNo;
  double? totalUsedArea;
  int? usedBeds;
  double? usedAreaRate;
  double? usedBedsRate;
  List<LineDetails>? lineDetails;

  GreenHouseData(
      {this.greenhouseName,
        this.greenhouseArea,
        this.greenhouseBedsNo,
        this.totalUsedArea,
        this.usedBeds,
        this.usedAreaRate,
        this.usedBedsRate,
        this.lineDetails});

  GreenHouseData.fromJson(Map<String, dynamic> json) {
    greenhouseName = json['greenhouse_name'];
    greenhouseArea = json['greenhouse_area'];
    greenhouseBedsNo = json['greenhouse_beds_no'];
    totalUsedArea = json['total_used_area'];
    usedBeds = json['used_beds'];
    usedAreaRate = json['used_area_rate'];
    usedBedsRate = json['used_beds_rate'];
    if (json['line_details'] != null) {
      lineDetails = <LineDetails>[];
      json['line_details'].forEach((v) {
        lineDetails!.add(LineDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['greenhouse_name'] = greenhouseName;
    data['greenhouse_area'] = greenhouseArea;
    data['greenhouse_beds_no'] = greenhouseBedsNo;
    data['total_used_area'] = totalUsedArea;
    data['used_beds'] = usedBeds;
    data['used_area_rate'] = usedAreaRate;
    data['used_beds_rate'] = usedBedsRate;
    if (lineDetails != null) {
      data['line_details'] = lineDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

// String? contractNo;
// String? type;
// String? totalLostQuantity;
// String? currentStageLostQuantity;
// String? currentPlantNo;
// String? stageFemalePlantNo;
// String? stageMalePlantNo;
// String? endDate;
// String? pictureName;
// double? quotaProduced;//"quota_produced": 1192.140000000000100044417195022106170654296875,
// double? quotaProducedPercentage;//"quota_produced_percentage": 29.800000000000000710542735760100185871124267578125,
// String? uom;//"uom": "K"

// class LineDetails {
//   String? varietyName;
//   String? stageName;
//   int? femalePlantNo;
//   int? malePlantNo;
//   int? bedNo;
//
//   LineDetails(
//       {this.varietyName,
//         this.stageName,
//         this.femalePlantNo,
//         this.malePlantNo,
//         this.bedNo});
//
//   LineDetails.fromJson(Map<String, dynamic> json) {
//     varietyName = json['variety_name'];
//     stageName = json['stage_name'];
//     femalePlantNo = json['female_plant_no'];
//     malePlantNo = json['male_plant_no'];
//     bedNo = json['bed_no'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = <String, dynamic>{};
//     data['variety_name'] = varietyName;
//     data['stage_name'] = stageName;
//     data['female_plant_no'] = femalePlantNo;
//     data['male_plant_no'] = malePlantNo;
//     data['bed_no'] = bedNo;
//     return data;
//   }
// }
