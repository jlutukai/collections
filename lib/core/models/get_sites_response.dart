class GetSiteResponse {
  List<SiteData>? data;
  String? message;
  bool? errors;

  GetSiteResponse({this.data, this.message, this.errors});

  GetSiteResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <SiteData>[];
      json['data'].forEach((v) {
        data!.add(SiteData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class SiteData {
  String? id;
  String? name;
  String? longitude;
  String? latitude;
  String? status;

  SiteData({this.id, this.name, this.longitude, this.latitude, this.status});

  SiteData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['longitude'] = longitude;
    data['latitude'] = latitude;
    data['status'] = status;
    return data;
  }
}
