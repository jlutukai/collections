class GetSpeciesResponse {
  List<SpeciesData>? data;
  String? message;
  bool? errors;

  GetSpeciesResponse({this.data, this.message, this.errors});

  GetSpeciesResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <SpeciesData>[];
      json['data'].forEach((v) {
        data!.add(SpeciesData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class SpeciesData {
  String? id;
  String? name;
  String? type;
  String? speciesName;

  SpeciesData({this.id, this.name, this.type, this.speciesName});

  @override
  String toString() {
    return '${name ?? 'undefined'} - $type';
  }

  SpeciesData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    type = json['type'];
    speciesName = json['species_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['type'] = type;
    data['species_name'] = speciesName;
    return data;
  }
}
