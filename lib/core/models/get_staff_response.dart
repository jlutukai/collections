class GetStaffResponse {
  List<StaffData>? data;
  String? message;
  bool? errors;

  GetStaffResponse({this.data, this.message, this.errors});

  GetStaffResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <StaffData>[];
      json['data'].forEach((v) {
        data!.add(StaffData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class StaffData {
  String? id;
  String? name;
  String? existingStaffId;

  StaffData({this.id, this.name, this.existingStaffId});

  StaffData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    existingStaffId = json['existing_staff_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['existing_staff_id'] = existingStaffId;
    return data;
  }
}
