class GetTaskAppraisal {
  List<AppraisalData>? data;
  String? message;
  bool? errors;

  GetTaskAppraisal({this.data, this.message, this.errors});

  GetTaskAppraisal.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <AppraisalData>[];
      json['data'].forEach((v) {
        data!.add(AppraisalData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class AppraisalData {
  String? id;
  String? title;
  String? taskDescription;
  String? priority;
  String? staffName;
  String? progress;
  String? deadline;
  String? formId;

  AppraisalData(
      {this.id,
        this.title,
        this.taskDescription,
        this.priority,
        this.staffName,
        this.progress,
        this.deadline, this.formId});

  AppraisalData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    title = json['title'];
    taskDescription = json['task_description'];
    priority = json['priority'];
    staffName = json['staff_name'];
    progress = json['progress'];
    deadline = json['deadline'];
    formId = json['form_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['title'] = title;
    data['task_description'] = taskDescription;
    data['priority'] = priority;
    data['staff_name'] = staffName;
    data['progress'] = progress;
    data['deadline'] = deadline;
    data['form_id'] = formId;
    return data;
  }
}


class EventData {
  String? title;
  String? description;
  String? taskId;
  String? formId;

  EventData({this.title, this.description, this.taskId, this.formId});
}