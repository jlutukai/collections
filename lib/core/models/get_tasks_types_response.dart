
class TaskType {


  String? name;
  String? id;

  TaskType({
    this.name,
    this.id,
  });

  @override
  String toString() {
    return '$name';
  }

  TaskType.fromJson(Map<String, dynamic> json) {
    name = json["name"]?.toString();
    id = json["Id"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["name"] = name;
    data["Id"] = id;
    return data;
  }
}

class GetTaskTypeResponse {


  List<TaskType?>? data;
  String? message;
  bool? errors;

  GetTaskTypeResponse({
    this.data,
    this.message,
    this.errors,
  });
  GetTaskTypeResponse.fromJson(Map<String, dynamic> json) {
    if (json["data"] != null) {
      final v = json["data"];
      final arr0 = <TaskType>[];
      v.forEach((v) {
        arr0.add(TaskType.fromJson(v));
      });
      data = arr0;
    }
    message = json["message"]?.toString();
    errors = json["errors"];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data["data"] = arr0;
    }
    data["message"] = message;
    data["errors"] = errors;
    return data;
  }
}
