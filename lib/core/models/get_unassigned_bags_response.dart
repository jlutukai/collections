class GetUnassignedBagsResponse {
  List<UnassignedBagData>? data;
  String? message;
  bool? errors;

  GetUnassignedBagsResponse({this.data, this.message, this.errors});

  GetUnassignedBagsResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <UnassignedBagData>[];
      json['data'].forEach((v) {
        data!.add(UnassignedBagData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class UnassignedBagData {
  String? batchId;
  String? plannerId;
  String? batchDate;
  String? varietyName;
  String? varietyId;
  String? pictureName;

  UnassignedBagData(
      {this.batchId,
        this.plannerId,
        this.batchDate,
        this.varietyName,
        this.varietyId,
        this.pictureName});

  UnassignedBagData.fromJson(Map<String, dynamic> json) {
    batchId = json['batch_id'];
    plannerId = json['planner_id'];
    batchDate = json['batch_date'];
    varietyName = json['variety_name'];
    varietyId = json['variety_id'];
    pictureName = json['picture_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['batch_id'] = batchId;
    data['planner_id'] = plannerId;
    data['batch_date'] = batchDate;
    data['variety_name'] = varietyName;
    data['variety_id'] = varietyId;
    data['picture_name'] = pictureName;
    return data;
  }
}
