class GetVarietyResponse {
  List<VarietyData>? data;
  String? message;
  bool? errors;

  GetVarietyResponse({this.data, this.message, this.errors});

  GetVarietyResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <VarietyData>[];
      json['data'].forEach((v) {
        data!.add(VarietyData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class VarietyData {
  String? plannerStageId;
  String? plannerId;
  String? stageName;
  String? varietyName;
  String? pictureName;

  VarietyData(
      {this.plannerStageId,
        this.plannerId,
        this.stageName,
        this.varietyName,
        this.pictureName});

  @override
  String toString() {
    return '${varietyName ?? 'undefined'} - $stageName';
  }

  VarietyData.fromJson(Map<String, dynamic> json) {
    plannerStageId = json['planner_stage_id'];
    plannerId = json['planner_id'];
    stageName = json['stage_name'];
    varietyName = json['variety_name'];
    pictureName = json['picture_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['planner_stage_id'] = plannerStageId;
    data['planner_id'] = plannerId;
    data['stage_name'] = stageName;
    data['variety_name'] = varietyName;
    data['picture_name'] = pictureName;
    return data;
  }
}
