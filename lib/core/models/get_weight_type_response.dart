class GetWeightTypesResponse {
  List<WeightTypeData>? data;
  String? message;
  bool? errors;

  GetWeightTypesResponse({this.data, this.message, this.errors});

  GetWeightTypesResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <WeightTypeData>[];
      json['data'].forEach((v) {
        data!.add(WeightTypeData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class WeightTypeData {
  String? id;
  String? name;
  String? status;

  WeightTypeData({this.id, this.name, this.status});

  WeightTypeData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['status'] = status;
    return data;
  }
}
