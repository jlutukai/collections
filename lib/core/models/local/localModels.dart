import 'package:hive/hive.dart';

part 'localModels.g.dart';

@HiveType(typeId: 1)
class Visitor {
  @HiveField(1)
  String? id;
  @HiveField(2)
  String? visitorName;
  @HiveField(3)
  String? hostName;
  @HiveField(4)
  String? department;
  @HiveField(5)
  String? designation;
  @HiveField(6)
  String? noVisitors;
  @HiveField(7)
  String? documentNumber;
  @HiveField(8)
  String? documentType;
  @HiveField(9)
  String? plateNo;
  @HiveField(10)
  String? passId;
  @HiveField(11)
  String? visitReason;
  @HiveField(12)
  String? datetimeIn;
  @HiveField(13)
  String? datetimeOut;
  @HiveField(14)
  String? siteId;
  @HiveField(15)
  String? status;
  @HiveField(16)
  List<ItemObj>? items;
  @HiveField(17)
  String? houseNo;
  @HiveField(18)
  String? blockNo;
  @HiveField(19)
  String? vehiclePass;
  @HiveField(20)
  String? companyName;
  @HiveField(21)
  String? officeNo;

  Visitor(
      {this.id,
      this.visitorName,
      this.hostName,
      this.department,
      this.designation,
      this.noVisitors,
      this.documentNumber,
      this.documentType,
      this.plateNo,
      this.passId,
      this.visitReason,
      this.datetimeIn,
      this.datetimeOut,
      this.siteId,
      this.status,
      this.items,
      this.houseNo,
      this.blockNo,
      this.companyName,
      this.vehiclePass,
      this.officeNo});

  Visitor.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    visitorName = json['visitor_name'];
    hostName = json['host_name'];
    department = json['department'];
    designation = json['designation'];
    noVisitors = json['no_visitors'];
    documentNumber = json['documentNumber'];
    documentType = json['document_type'];
    plateNo = json['plateNo'];
    passId = json['pass_id'];
    visitReason = json['visit_reason'];
    datetimeIn = json['datetime_in'];
    datetimeOut = json['datetime_out'];
    siteId = json['site_id'];
    status = json['status'];
    if (json['items'] != null) {
      items = <ItemObj>[];
      json['items'].forEach((v) {
        items!.add(ItemObj.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['visitor_name'] = visitorName;
    data['host_name'] = hostName;
    data['department'] = department;
    data['designation'] = designation;
    data['no_visitors'] = noVisitors;
    data['documentNumber'] = documentNumber;
    data['document_type'] = documentType;
    data['plateNo'] = plateNo;
    data['pass_id'] = passId;
    data['visit_reason'] = visitReason;
    data['datetime_in'] = datetimeIn;
    data['datetime_out'] = datetimeOut;
    data['site_id'] = siteId;
    data['status'] = status;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 2)
class ItemObj {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? visitorId;
  @HiveField(2)
  String? name;
  @HiveField(3)
  String? serialNo;

  ItemObj({this.id, this.visitorId, this.name, this.serialNo});

  ItemObj.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorId = json['visitor_id'];
    name = json['name'];
    serialNo = json['serialNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['visitor_id'] = visitorId;
    data['name'] = name;
    data['serialNo'] = serialNo;
    return data;
  }
}

@HiveType(typeId: 3)
class TimeSheetData {
  @HiveField(0)
  String? status;
  @HiveField(1)
  String? date;
  @HiveField(2)
  String? shiftId;
  @HiveField(3)
  String? employeeId;
  @HiveField(4)
  String? id;
  @HiveField(5)
  String? staffPhone;
  @HiveField(6)
  String? name;

  @HiveField(7)
  bool? modified;
  @HiveField(8)
  String? reason;
  @HiveField(9)
  String? image;
  @HiveField(10)
  String? biometricId;
  @HiveField(11)
  String? timeIn;
  @HiveField(12)
  String? timeOut;

  @HiveField(13)
  String? fileName;
  @HiveField(14)
  String? commentIn;
  @HiveField(15)
  String? commentOut;

  TimeSheetData(
      {this.status,
      this.date,
      this.shiftId,
      this.employeeId,
      this.id,
      this.staffPhone,
      this.name,
      this.modified,
      this.reason,
      this.image,
      this.biometricId,
      this.timeIn,
      this.timeOut,
      this.commentIn,
      this.commentOut,
      this.fileName});

  TimeSheetData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    date = json['date'];
    shiftId = json['shift_id'];
    employeeId = json['employee_id'];
    id = json['Id'];
    staffPhone = json['staff_phone'];
    name = json['name'];
    reason = json['reason'];
    timeIn = json['time_in'];
    timeOut = json['time_out'];
    commentIn = json['comment_in'];
    commentOut = json['comment_out'];
    fileName = json['file_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['date'] = date;
    data['shift_id'] = shiftId;
    data['employee_id'] = employeeId;
    data['Id'] = id;
    data['staff_phone'] = staffPhone;
    data['name'] = name;
    data['reason'] = reason;
    data['time_in'] = timeIn;
    data['time_out'] = timeOut;
    data['comment_in'] = commentIn;
    data['comment_out'] = commentOut;
    data['file_name'] = fileName;
    return data;
  }
}

@HiveType(typeId: 4)
class TimeSheetDetails {
  @HiveField(0)
  String? timesheetId;
  @HiveField(1)
  String? status;
  @HiveField(2)
  String? timeIn;
  @HiveField(3)
  String? timeOut;
  @HiveField(4)
  String? reason;
  @HiveField(5)
  String? imageIn;
  @HiveField(6)
  String? bioMetric;
  @HiveField(7)
  String? imageOut;
  @HiveField(8)
  String? commentIn;
  @HiveField(9)
  String? commentOut;

  TimeSheetDetails(
      {this.timesheetId,
      this.status,
      this.timeIn,
      this.timeOut,
      this.reason,
      this.imageIn,
      this.bioMetric,
      this.imageOut,
      this.commentOut,
      this.commentIn});

  TimeSheetDetails.fromJson(Map<String, dynamic> json) {
    timesheetId = json['timesheet_id'];
    status = json['status'];
    timeIn = json['time_in'];
    timeOut = json['time_out'];
    reason = json['reason'];
    commentIn = json['comment_in'];
    commentOut = json['comment_out'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['timesheet_id'] = timesheetId;
    data['status'] = status;
    data['time_in'] = timeIn;
    data['time_out'] = timeOut;
    data['reason'] = reason;
    data['comment_in'] = commentIn;
    data['comment_out'] = commentOut;
    return data;
  }
}

@HiveType(typeId: 5)
class CheckListResult {
  @HiveField(0)
  String? responseId;
  @HiveField(1)
  String? questionId;
  @HiveField(2)
  String? response;
  @HiveField(3)
  String? date;
  @HiveField(4)
  String? answeredBy;
  @HiveField(5)
  String? defaultCustom;
  @HiveField(6)
  String? shiftId;
  @HiveField(7)
  String? categoryId;
  @HiveField(8)
  String? name;
  @HiveField(9)
  String? questionType;
  @HiveField(10)
  String? fileLocation;
  @HiveField(11)
  String? shiftType;
  @HiveField(12)
  bool? synced;
  @HiveField(13)
  String? status;
  @HiveField(14)
  String? plannerId;

  CheckListResult(
      {this.responseId,
      this.questionId,
      this.response,
      this.date,
      this.answeredBy,
      this.defaultCustom,
      this.shiftId,
      this.categoryId,
      this.name,
      this.questionType,
      this.fileLocation,
      this.shiftType,
      this.synced,
      this.status,
      this.plannerId});

  CheckListResult.fromJson(Map<String, dynamic> json) {
    responseId = json['response_id'];
    questionId = json['question_id'];
    response = json['response'];
    date = json['date'];
    answeredBy = json['answered_by'];
    defaultCustom = json['default_custom'];
    shiftId = json['shift_id'];
    categoryId = json['category_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['response_id'] = responseId;
    data['question_id'] = questionId;
    data['response'] = response;
    data['date'] = date;
    data['answered_by'] = answeredBy;
    data['default_custom'] = defaultCustom;
    data['shift_id'] = shiftId;
    data['category_id'] = categoryId;
    return data;
  }
}

@HiveType(typeId: 6)
class PlannerData {
  @HiveField(0)
  String? plannerId;
  @HiveField(1)
  String? plannerName;
  @HiveField(2)
  String? status;
  @HiveField(3)
  String? verificationMethod;
  @HiveField(4)
  List<Shifts>? shifts;

  PlannerData(
      {this.plannerId,
      this.plannerName,
      this.status,
      this.verificationMethod,
      this.shifts});

  PlannerData.fromJson(Map<String, dynamic> json) {
    plannerId = json['planner_id'];
    plannerName = json['planner_name'];
    status = json['status'];
    verificationMethod = json['verification_method'];
    if (json['shifts'] != null) {
      shifts = <Shifts>[];
      json['shifts'].forEach((v) {
        shifts!.add(Shifts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['planner_id'] = plannerId;
    data['planner_name'] = plannerName;
    data['status'] = status;
    data['verification_method'] = verificationMethod;
    if (shifts != null) {
      data['shifts'] = shifts!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 7)
class Shifts {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? plannerId;
  @HiveField(2)
  String? shiftName;
  @HiveField(3)
  String? shiftChangeFrom;
  @HiveField(4)
  String? shiftChangeTo;
  @HiveField(5)
  String? shiftColor;
  @HiveField(6)
  String? code;
  @HiveField(7)
  int? duration;

  Shifts(
      {this.id,
      this.plannerId,
      this.shiftName,
      this.shiftChangeFrom,
      this.shiftChangeTo,
      this.shiftColor,
      this.code,
      this.duration});

  Shifts.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    plannerId = json['planner_id'];
    shiftName = json['shift_name'];
    shiftChangeFrom = json['shift_change_from'];
    shiftChangeTo = json['shift_change_to'];
    shiftColor = json['shift_color'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['planner_id'] = plannerId;
    data['shift_name'] = shiftName;
    data['shift_change_from'] = shiftChangeFrom;
    data['shift_change_to'] = shiftChangeTo;
    data['shift_color'] = shiftColor;
    data['code'] = code;
    return data;
  }
}

@HiveType(typeId: 8)
class DeviceDetails {
  @HiveField(0)
  String? name;
  @HiveField(1)
  int? xOrigin;
  @HiveField(2)
  int? yOrigin;
  @HiveField(3)
  int? width;
  @HiveField(4)
  int? height;

  DeviceDetails(
      {this.name, this.xOrigin, this.yOrigin, this.width, this.height});
}

@HiveType(typeId: 9)
class UpdateFormData {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? formId;
  @HiveField(2)
  String? answeredBy;
  @HiveField(3)
  String? plannerId;
  @HiveField(4)
  String? shiftId;
  @HiveField(5)
  String? date;
  @HiveField(6)
  List<FormItems>? items;
  @HiveField(7)
  int? plannerStageId;
  @HiveField(8)
  String? type;
  @HiveField(9)
  int? typeId;
  @HiveField(10)
  int? taskId;

  UpdateFormData(
      {this.id,
      this.formId,
      this.answeredBy,
      this.plannerId,
      this.shiftId,
      this.date,
      this.items,
      this.plannerStageId,
      this.type,
      this.typeId,
      this.taskId});

  UpdateFormData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    formId = json['form_id'];
    answeredBy = json['answered_by'];
    plannerId = json['planner_id'];
    shiftId = json['shift_id'];
    date = json['date'];
    if (json['items'] != null) {
      items = <FormItems>[];
      json['items'].forEach((v) {
        items!.add(FormItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['form_id'] = formId;
    data['answered_by'] = answeredBy;
    data['planner_id'] = plannerId;
    data['shift_id'] = shiftId;
    data['date'] = date;
    data['type'] = type;
    data['planner_stage_id'] = plannerStageId;
    data['type_id'] = typeId;
    data['task_id'] = taskId;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 10)
class FormItems {
  @HiveField(0)
  String? formItemId;
  @HiveField(1)
  String? response;
  @HiveField(2)
  String? type;
  @HiveField(3)
  String? question;
  @HiveField(4)
  String? responseType;
  @HiveField(5)
  bool? required;
  @HiveField(6)
  bool? isFile;
  @HiveField(7)
  String? responseName;

  FormItems(
      {this.formItemId,
      this.response,
      this.type,
      this.question,
      this.responseType,
      this.required,
      this.isFile,
        this.responseName
      });

  FormItems.fromJson(Map<String, dynamic> json) {
    formItemId = json['form_item_id'];
    response = json['response'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['form_item_id'] = formItemId;
    data['response'] = response;
    data['type'] = type;
    return data;
  }
}

@HiveType(typeId: 11)
class CreateTaskData {
  @HiveField(0)
  String? assignedBy;
  @HiveField(1)
  String? taskDescription;
  @HiveField(2)
  String? deadline;
  @HiveField(3)
  List<AssignedTo>? assignTo;
  @HiveField(4)
  String? title;
  @HiveField(5)
  String? taskType;
  @HiveField(6)
  String? taskVisibility;
  @HiveField(7)
  String? priority;
  @HiveField(8)
  String? associatedBy;
  @HiveField(9)
  String? taskTypeName;
  @HiveField(10)
  List<SubTask?>? taskItem;
  @HiveField(11)
  String? associatedId;
  @HiveField(12)
  String? associatedType;

  CreateTaskData(
      {this.assignedBy,
      this.taskDescription,
      this.deadline,
      this.assignTo,
      this.title,
      this.taskType,
      this.taskVisibility,
      this.priority,
      this.associatedBy,
      this.taskTypeName,
      this.taskItem,
      this.associatedId,
      this.associatedType});

  CreateTaskData.fromJson(Map<String, dynamic> json) {
    assignedBy = json["assigned_by"]?.toString();
    taskDescription = json["task_description"]?.toString();
    deadline = json["deadline"]?.toString();
    if (json["assign_to"] != null) {
      final v = json["assign_to"];
      final arr0 = <AssignedTo>[];
      v.forEach((v) {
        arr0.add(AssignedTo.fromJson(v));
      });
      assignTo = arr0;
    }
    title = json["title"]?.toString();
    taskType = json["task_type"]?.toString();
    taskVisibility = json["task_visibility"]?.toString();
    priority = json["priority"]?.toString();
    associatedBy = json["associated_by"]?.toString();
    if (json["task_item"] != null) {
      final v = json["task_item"];
      final arr0 = <SubTask>[];
      v.forEach((v) {
        arr0.add(SubTask.fromJson(v));
      });
      taskItem = arr0;
    }
    associatedType = json["item_associated"]?.toString();
    associatedId = json["item_associated_id"]?.toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["assigned_by"] = assignedBy;
    data["task_description"] = taskDescription;
    data["deadline"] = deadline;
    if (assignTo != null) {
      final v = assignTo;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v.toJson());
      }
      data["assign_to"] = arr0;
    }
    data["title"] = title;
    data["task_type"] = taskType;
    data["task_visibility"] = taskVisibility;
    data["priority"] = priority;
    data["associated_by"] = associatedBy;
    if (taskItem != null) {
      final v = taskItem;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data["task_item"] = arr0;
    }
    data["item_associated"] = associatedType;
    data["item_associated_id"] = associatedId;

    return data;
  }
}

@HiveType(typeId: 12)
class AssignedTo {
  @HiveField(0)
  String? assignToId;
  @HiveField(1)
  String? type;
  @HiveField(2)
  String? name;

  AssignedTo({this.assignToId, this.type, this.name});

  AssignedTo.fromJson(Map<String, dynamic> json) {
    assignToId = json["assign_to_id"]?.toString();
    type = json["type"]?.toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["assign_to_id"] = assignToId;
    data["type"] = type;
    return data;
  }
}

@HiveType(typeId: 13)
class SubTask {
  @HiveField(0)
  String? description;

  SubTask({this.description});

  SubTask.fromJson(Map<String, dynamic> json) {
    description = json["description"]?.toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["description"] = description;
    return data;
  }
}

@HiveType(typeId: 14)
class CollectionData {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? batchId;
  @HiveField(2)
  String? bagId;
  @HiveField(3)
  String? status;
  @HiveField(4)
  String? date;
  @HiveField(5)
  String? varietyId;
  @HiveField(6)
  String? weightType;
  @HiveField(7)
  double? totalWeight;
  @HiveField(8)
  List<ItemBags>? bags;
  @HiveField(9)
  int? syncStatus;
  @HiveField(10)
  String? weightTypeId;
  @HiveField(11)
  String? batchType;
  @HiveField(12)
  double? cleanWeight;

  CollectionData(
      {this.id,
      this.batchId,
      this.bagId,
      this.status,
      this.date,
      this.varietyId,
      this.weightType,
      this.totalWeight,
      this.bags,
      this.syncStatus,
      this.weightTypeId,
      this.batchType,
      this.cleanWeight});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CollectionData &&
          runtimeType == other.runtimeType &&
          bagId == other.bagId &&
          status == other.status &&
          date == other.date &&
          varietyId == other.varietyId &&
          weightType == other.weightType &&
          totalWeight == other.totalWeight &&
          weightTypeId == other.weightTypeId;

  @override
  int get hashCode =>
      bagId.hashCode ^
      status.hashCode ^
      date.hashCode ^
      varietyId.hashCode ^
      weightType.hashCode ^
      totalWeight.hashCode ^
      weightTypeId.hashCode;

  CollectionData.fromJson(Map<String, dynamic> json) {
    batchId = json['batch_id'];
    bagId = json['bag_id'];
    status = json['status'];
    date = json['date'];
    varietyId = json['variety_id'];
    if (json['item_line'] != null) {
      bags = <ItemBags>[];
      json['item_line'].forEach((v) {
        bags!.add(ItemBags.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['batch_id'] = batchId;
    data['bag_id'] = bagId;
    data['status'] = status;
    data['date'] = date;
    data['variety_id'] = varietyId;
    if (bags != null) {
      data['item_line'] = bags!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 15)
class ItemBags {
  @HiveField(0)
  String? bagId;
  @HiveField(1)
  double? weight;
  @HiveField(2)
  String? clerkId;

  ItemBags({this.bagId, this.weight, this.clerkId});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ItemBags &&
          runtimeType == other.runtimeType &&
          bagId == other.bagId;

  @override
  int get hashCode => bagId.hashCode;

  ItemBags.fromJson(Map<String, dynamic> json) {
    weight =
        json['weight'] != null ? double.tryParse(json['weight']) ?? 0.0 : 0.0;
    clerkId = json['clerk_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['weight'] = weight;
    data['clerk_id'] = clerkId;
    return data;
  }
}


@HiveType(typeId: 16)
class UpdateBatchRequest {
  @HiveField(0)
  String? userId;
  @HiveField(1)
  String? plannerStageId;
  @HiveField(2)
  String? bagId;
  @HiveField(3)
  String? image;
  @HiveField(4)
  String? lineId;
  @HiveField(5)
  String? varietyName;
  @HiveField(6)
  String? stageNage;
  @HiveField(7)
  double? total;
  @HiveField(8)
  List<BagItems>? items;
  @HiveField(9)
  List<UnMarkedBags>? unMarkedBags;

  UpdateBatchRequest(
      {this.userId,
        this.plannerStageId,
        this.bagId,
        this.lineId,
        this.image,
        this.total,
        this.stageNage,
        this.varietyName,
        this.items,
        this.unMarkedBags});

  UpdateBatchRequest.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    plannerStageId = json['planner_stage_id'];
    bagId = json['bag_id'];
    if (json['items'] != null) {
      items = <BagItems>[];
      json['items'].forEach((v) {
        items!.add(BagItems.fromJson(v));
      });
    }
    if (json['un_marked_bags'] != null) {
      unMarkedBags = <UnMarkedBags>[];
      json['un_marked_bags'].forEach((v) {
        unMarkedBags!.add(UnMarkedBags.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['user_id'] = userId;
    data['planner_stage_id'] = plannerStageId;
    data['bag_id'] = bagId;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    if (unMarkedBags != null) {
      data['un_marked_bags'] =
          unMarkedBags!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 17)
class BagItems {
  @HiveField(0)
  String? bagId;
  @HiveField(1)
  String? weight;
  @HiveField(2)
  String? rating;

  BagItems({this.bagId, this.weight, this.rating});

  BagItems.fromJson(Map<String, dynamic> json) {
    bagId = json['bag_id'];
    weight = json['weight'];
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bag_id'] = bagId;
    data['weight'] = weight;
    data['rating'] = rating;
    return data;
  }
}

@HiveType(typeId: 18)
class UnMarkedBags {
  @HiveField(0)
  String? bagId;

  UnMarkedBags({this.bagId});

  UnMarkedBags.fromJson(Map<String, dynamic> json) {
    bagId = json['bag_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bag_id'] = bagId;
    return data;
  }
}
@HiveType(typeId: 19)
class CleanWeightRequest {
  @HiveField(0)
  String? batchId;
  @HiveField(1)
  String? cleanWeight;
  @HiveField(2)
  String? userId;
  @HiveField(3)
  String? bagId;
  @HiveField(4)
  String? image;
  @HiveField(5)
  String? lineId;
  @HiveField(6)
  String? dirtyWeight;

  CleanWeightRequest(
      {this.batchId,
        this.cleanWeight,
        this.userId,
        this.bagId,
        this.dirtyWeight,
        this.image,
        this.lineId});

  CleanWeightRequest.fromJson(Map<String, dynamic> json) {
    batchId = json['batch_id'];
    cleanWeight = json['clean_weight'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['batch_id'] = batchId;
    data['clean_weight'] = cleanWeight;
    data['user_id'] = userId;
    return data;
  }
}