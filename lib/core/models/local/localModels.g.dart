// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localModels.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class VisitorAdapter extends TypeAdapter<Visitor> {
  @override
  final int typeId = 1;

  @override
  Visitor read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Visitor(
      id: fields[1] as String?,
      visitorName: fields[2] as String?,
      hostName: fields[3] as String?,
      department: fields[4] as String?,
      designation: fields[5] as String?,
      noVisitors: fields[6] as String?,
      documentNumber: fields[7] as String?,
      documentType: fields[8] as String?,
      plateNo: fields[9] as String?,
      passId: fields[10] as String?,
      visitReason: fields[11] as String?,
      datetimeIn: fields[12] as String?,
      datetimeOut: fields[13] as String?,
      siteId: fields[14] as String?,
      status: fields[15] as String?,
      items: (fields[16] as List?)?.cast<ItemObj>(),
      houseNo: fields[17] as String?,
      blockNo: fields[18] as String?,
      companyName: fields[20] as String?,
      vehiclePass: fields[19] as String?,
      officeNo: fields[21] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, Visitor obj) {
    writer
      ..writeByte(21)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.visitorName)
      ..writeByte(3)
      ..write(obj.hostName)
      ..writeByte(4)
      ..write(obj.department)
      ..writeByte(5)
      ..write(obj.designation)
      ..writeByte(6)
      ..write(obj.noVisitors)
      ..writeByte(7)
      ..write(obj.documentNumber)
      ..writeByte(8)
      ..write(obj.documentType)
      ..writeByte(9)
      ..write(obj.plateNo)
      ..writeByte(10)
      ..write(obj.passId)
      ..writeByte(11)
      ..write(obj.visitReason)
      ..writeByte(12)
      ..write(obj.datetimeIn)
      ..writeByte(13)
      ..write(obj.datetimeOut)
      ..writeByte(14)
      ..write(obj.siteId)
      ..writeByte(15)
      ..write(obj.status)
      ..writeByte(16)
      ..write(obj.items)
      ..writeByte(17)
      ..write(obj.houseNo)
      ..writeByte(18)
      ..write(obj.blockNo)
      ..writeByte(19)
      ..write(obj.vehiclePass)
      ..writeByte(20)
      ..write(obj.companyName)
      ..writeByte(21)
      ..write(obj.officeNo);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VisitorAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class ItemObjAdapter extends TypeAdapter<ItemObj> {
  @override
  final int typeId = 2;

  @override
  ItemObj read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ItemObj(
      id: fields[0] as String?,
      visitorId: fields[1] as String?,
      name: fields[2] as String?,
      serialNo: fields[3] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, ItemObj obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.visitorId)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.serialNo);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ItemObjAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TimeSheetDataAdapter extends TypeAdapter<TimeSheetData> {
  @override
  final int typeId = 3;

  @override
  TimeSheetData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TimeSheetData(
      status: fields[0] as String?,
      date: fields[1] as String?,
      shiftId: fields[2] as String?,
      employeeId: fields[3] as String?,
      id: fields[4] as String?,
      staffPhone: fields[5] as String?,
      name: fields[6] as String?,
      modified: fields[7] as bool?,
      reason: fields[8] as String?,
      image: fields[9] as String?,
      biometricId: fields[10] as String?,
      timeIn: fields[11] as String?,
      timeOut: fields[12] as String?,
      commentIn: fields[14] as String?,
      commentOut: fields[15] as String?,
      fileName: fields[13] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, TimeSheetData obj) {
    writer
      ..writeByte(16)
      ..writeByte(0)
      ..write(obj.status)
      ..writeByte(1)
      ..write(obj.date)
      ..writeByte(2)
      ..write(obj.shiftId)
      ..writeByte(3)
      ..write(obj.employeeId)
      ..writeByte(4)
      ..write(obj.id)
      ..writeByte(5)
      ..write(obj.staffPhone)
      ..writeByte(6)
      ..write(obj.name)
      ..writeByte(7)
      ..write(obj.modified)
      ..writeByte(8)
      ..write(obj.reason)
      ..writeByte(9)
      ..write(obj.image)
      ..writeByte(10)
      ..write(obj.biometricId)
      ..writeByte(11)
      ..write(obj.timeIn)
      ..writeByte(12)
      ..write(obj.timeOut)
      ..writeByte(13)
      ..write(obj.fileName)
      ..writeByte(14)
      ..write(obj.commentIn)
      ..writeByte(15)
      ..write(obj.commentOut);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimeSheetDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TimeSheetDetailsAdapter extends TypeAdapter<TimeSheetDetails> {
  @override
  final int typeId = 4;

  @override
  TimeSheetDetails read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TimeSheetDetails(
      timesheetId: fields[0] as String?,
      status: fields[1] as String?,
      timeIn: fields[2] as String?,
      timeOut: fields[3] as String?,
      reason: fields[4] as String?,
      imageIn: fields[5] as String?,
      bioMetric: fields[6] as String?,
      imageOut: fields[7] as String?,
      commentOut: fields[9] as String?,
      commentIn: fields[8] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, TimeSheetDetails obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.timesheetId)
      ..writeByte(1)
      ..write(obj.status)
      ..writeByte(2)
      ..write(obj.timeIn)
      ..writeByte(3)
      ..write(obj.timeOut)
      ..writeByte(4)
      ..write(obj.reason)
      ..writeByte(5)
      ..write(obj.imageIn)
      ..writeByte(6)
      ..write(obj.bioMetric)
      ..writeByte(7)
      ..write(obj.imageOut)
      ..writeByte(8)
      ..write(obj.commentIn)
      ..writeByte(9)
      ..write(obj.commentOut);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimeSheetDetailsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CheckListResultAdapter extends TypeAdapter<CheckListResult> {
  @override
  final int typeId = 5;

  @override
  CheckListResult read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CheckListResult(
      responseId: fields[0] as String?,
      questionId: fields[1] as String?,
      response: fields[2] as String?,
      date: fields[3] as String?,
      answeredBy: fields[4] as String?,
      defaultCustom: fields[5] as String?,
      shiftId: fields[6] as String?,
      categoryId: fields[7] as String?,
      name: fields[8] as String?,
      questionType: fields[9] as String?,
      fileLocation: fields[10] as String?,
      shiftType: fields[11] as String?,
      synced: fields[12] as bool?,
      status: fields[13] as String?,
      plannerId: fields[14] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, CheckListResult obj) {
    writer
      ..writeByte(15)
      ..writeByte(0)
      ..write(obj.responseId)
      ..writeByte(1)
      ..write(obj.questionId)
      ..writeByte(2)
      ..write(obj.response)
      ..writeByte(3)
      ..write(obj.date)
      ..writeByte(4)
      ..write(obj.answeredBy)
      ..writeByte(5)
      ..write(obj.defaultCustom)
      ..writeByte(6)
      ..write(obj.shiftId)
      ..writeByte(7)
      ..write(obj.categoryId)
      ..writeByte(8)
      ..write(obj.name)
      ..writeByte(9)
      ..write(obj.questionType)
      ..writeByte(10)
      ..write(obj.fileLocation)
      ..writeByte(11)
      ..write(obj.shiftType)
      ..writeByte(12)
      ..write(obj.synced)
      ..writeByte(13)
      ..write(obj.status)
      ..writeByte(14)
      ..write(obj.plannerId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckListResultAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class PlannerDataAdapter extends TypeAdapter<PlannerData> {
  @override
  final int typeId = 6;

  @override
  PlannerData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PlannerData(
      plannerId: fields[0] as String?,
      plannerName: fields[1] as String?,
      status: fields[2] as String?,
      verificationMethod: fields[3] as String?,
      shifts: (fields[4] as List?)?.cast<Shifts>(),
    );
  }

  @override
  void write(BinaryWriter writer, PlannerData obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.plannerId)
      ..writeByte(1)
      ..write(obj.plannerName)
      ..writeByte(2)
      ..write(obj.status)
      ..writeByte(3)
      ..write(obj.verificationMethod)
      ..writeByte(4)
      ..write(obj.shifts);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PlannerDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class ShiftsAdapter extends TypeAdapter<Shifts> {
  @override
  final int typeId = 7;

  @override
  Shifts read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Shifts(
      id: fields[0] as String?,
      plannerId: fields[1] as String?,
      shiftName: fields[2] as String?,
      shiftChangeFrom: fields[3] as String?,
      shiftChangeTo: fields[4] as String?,
      shiftColor: fields[5] as String?,
      code: fields[6] as String?,
      duration: fields[7] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, Shifts obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.plannerId)
      ..writeByte(2)
      ..write(obj.shiftName)
      ..writeByte(3)
      ..write(obj.shiftChangeFrom)
      ..writeByte(4)
      ..write(obj.shiftChangeTo)
      ..writeByte(5)
      ..write(obj.shiftColor)
      ..writeByte(6)
      ..write(obj.code)
      ..writeByte(7)
      ..write(obj.duration);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ShiftsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class DeviceDetailsAdapter extends TypeAdapter<DeviceDetails> {
  @override
  final int typeId = 8;

  @override
  DeviceDetails read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DeviceDetails(
      name: fields[0] as String?,
      xOrigin: fields[1] as int?,
      yOrigin: fields[2] as int?,
      width: fields[3] as int?,
      height: fields[4] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, DeviceDetails obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.xOrigin)
      ..writeByte(2)
      ..write(obj.yOrigin)
      ..writeByte(3)
      ..write(obj.width)
      ..writeByte(4)
      ..write(obj.height);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DeviceDetailsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class UpdateFormDataAdapter extends TypeAdapter<UpdateFormData> {
  @override
  final int typeId = 9;

  @override
  UpdateFormData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UpdateFormData(
      id: fields[0] as String?,
      formId: fields[1] as String?,
      answeredBy: fields[2] as String?,
      plannerId: fields[3] as String?,
      shiftId: fields[4] as String?,
      date: fields[5] as String?,
      items: (fields[6] as List?)?.cast<FormItems>(),
      plannerStageId: fields[7] as int?,
      type: fields[8] as String?,
      typeId: fields[9] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, UpdateFormData obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.formId)
      ..writeByte(2)
      ..write(obj.answeredBy)
      ..writeByte(3)
      ..write(obj.plannerId)
      ..writeByte(4)
      ..write(obj.shiftId)
      ..writeByte(5)
      ..write(obj.date)
      ..writeByte(6)
      ..write(obj.items)
      ..writeByte(7)
      ..write(obj.plannerStageId)
      ..writeByte(8)
      ..write(obj.type)
      ..writeByte(9)
      ..write(obj.typeId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpdateFormDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class FormItemsAdapter extends TypeAdapter<FormItems> {
  @override
  final int typeId = 10;

  @override
  FormItems read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FormItems(
      formItemId: fields[0] as String?,
      response: fields[1] as String?,
      type: fields[2] as String?,
      question: fields[3] as String?,
      responseType: fields[4] as String?,
      required: fields[5] as bool?,
      isFile: fields[6] as bool?,
      responseName: fields[7] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, FormItems obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.formItemId)
      ..writeByte(1)
      ..write(obj.response)
      ..writeByte(2)
      ..write(obj.type)
      ..writeByte(3)
      ..write(obj.question)
      ..writeByte(4)
      ..write(obj.responseType)
      ..writeByte(5)
      ..write(obj.required)
      ..writeByte(6)
      ..write(obj.isFile)
      ..writeByte(7)
      ..write(obj.responseName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FormItemsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CreateTaskDataAdapter extends TypeAdapter<CreateTaskData> {
  @override
  final int typeId = 11;

  @override
  CreateTaskData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CreateTaskData(
      assignedBy: fields[0] as String?,
      taskDescription: fields[1] as String?,
      deadline: fields[2] as String?,
      assignTo: (fields[3] as List?)?.cast<AssignedTo>(),
      title: fields[4] as String?,
      taskType: fields[5] as String?,
      taskVisibility: fields[6] as String?,
      priority: fields[7] as String?,
      associatedBy: fields[8] as String?,
      taskTypeName: fields[9] as String?,
      taskItem: (fields[10] as List?)?.cast<SubTask?>(),
      associatedId: fields[11] as String?,
      associatedType: fields[12] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, CreateTaskData obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.assignedBy)
      ..writeByte(1)
      ..write(obj.taskDescription)
      ..writeByte(2)
      ..write(obj.deadline)
      ..writeByte(3)
      ..write(obj.assignTo)
      ..writeByte(4)
      ..write(obj.title)
      ..writeByte(5)
      ..write(obj.taskType)
      ..writeByte(6)
      ..write(obj.taskVisibility)
      ..writeByte(7)
      ..write(obj.priority)
      ..writeByte(8)
      ..write(obj.associatedBy)
      ..writeByte(9)
      ..write(obj.taskTypeName)
      ..writeByte(10)
      ..write(obj.taskItem)
      ..writeByte(11)
      ..write(obj.associatedId)
      ..writeByte(12)
      ..write(obj.associatedType);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CreateTaskDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AssignedToAdapter extends TypeAdapter<AssignedTo> {
  @override
  final int typeId = 12;

  @override
  AssignedTo read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AssignedTo(
      assignToId: fields[0] as String?,
      type: fields[1] as String?,
      name: fields[2] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, AssignedTo obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.assignToId)
      ..writeByte(1)
      ..write(obj.type)
      ..writeByte(2)
      ..write(obj.name);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AssignedToAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class SubTaskAdapter extends TypeAdapter<SubTask> {
  @override
  final int typeId = 13;

  @override
  SubTask read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SubTask(
      description: fields[0] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, SubTask obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.description);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SubTaskAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CollectionDataAdapter extends TypeAdapter<CollectionData> {
  @override
  final int typeId = 14;

  @override
  CollectionData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CollectionData(
      id: fields[0] as String?,
      batchId: fields[1] as String?,
      bagId: fields[2] as String?,
      status: fields[3] as String?,
      date: fields[4] as String?,
      varietyId: fields[5] as String?,
      weightType: fields[6] as String?,
      totalWeight: fields[7] as double?,
      bags: (fields[8] as List?)?.cast<ItemBags>(),
      syncStatus: fields[9] as int?,
      weightTypeId: fields[10] as String?,
      batchType: fields[11] as String?,
      cleanWeight: fields[12] as double?,
    );
  }

  @override
  void write(BinaryWriter writer, CollectionData obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.batchId)
      ..writeByte(2)
      ..write(obj.bagId)
      ..writeByte(3)
      ..write(obj.status)
      ..writeByte(4)
      ..write(obj.date)
      ..writeByte(5)
      ..write(obj.varietyId)
      ..writeByte(6)
      ..write(obj.weightType)
      ..writeByte(7)
      ..write(obj.totalWeight)
      ..writeByte(8)
      ..write(obj.bags)
      ..writeByte(9)
      ..write(obj.syncStatus)
      ..writeByte(10)
      ..write(obj.weightTypeId)
      ..writeByte(11)
      ..write(obj.batchType)
      ..writeByte(12)
      ..write(obj.cleanWeight);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CollectionDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class ItemBagsAdapter extends TypeAdapter<ItemBags> {
  @override
  final int typeId = 15;

  @override
  ItemBags read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ItemBags(
      bagId: fields[0] as String?,
      weight: fields[1] as double?,
      clerkId: fields[2] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, ItemBags obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.bagId)
      ..writeByte(1)
      ..write(obj.weight)
      ..writeByte(2)
      ..write(obj.clerkId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ItemBagsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class UpdateBatchRequestAdapter extends TypeAdapter<UpdateBatchRequest> {
  @override
  final int typeId = 16;

  @override
  UpdateBatchRequest read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UpdateBatchRequest(
      userId: fields[0] as String?,
      plannerStageId: fields[1] as String?,
      bagId: fields[2] as String?,
      lineId: fields[4] as String?,
      image: fields[3] as String?,
      total: fields[7] as double?,
      stageNage: fields[6] as String?,
      varietyName: fields[5] as String?,
      items: (fields[8] as List?)?.cast<BagItems>(),
      unMarkedBags: (fields[9] as List?)?.cast<UnMarkedBags>(),
    );
  }

  @override
  void write(BinaryWriter writer, UpdateBatchRequest obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.userId)
      ..writeByte(1)
      ..write(obj.plannerStageId)
      ..writeByte(2)
      ..write(obj.bagId)
      ..writeByte(3)
      ..write(obj.image)
      ..writeByte(4)
      ..write(obj.lineId)
      ..writeByte(5)
      ..write(obj.varietyName)
      ..writeByte(6)
      ..write(obj.stageNage)
      ..writeByte(7)
      ..write(obj.total)
      ..writeByte(8)
      ..write(obj.items)
      ..writeByte(9)
      ..write(obj.unMarkedBags);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpdateBatchRequestAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class BagItemsAdapter extends TypeAdapter<BagItems> {
  @override
  final int typeId = 17;

  @override
  BagItems read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BagItems(
      bagId: fields[0] as String?,
      weight: fields[1] as String?,
      rating: fields[2] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, BagItems obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.bagId)
      ..writeByte(1)
      ..write(obj.weight)
      ..writeByte(2)
      ..write(obj.rating);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BagItemsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class UnMarkedBagsAdapter extends TypeAdapter<UnMarkedBags> {
  @override
  final int typeId = 18;

  @override
  UnMarkedBags read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UnMarkedBags(
      bagId: fields[0] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, UnMarkedBags obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.bagId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UnMarkedBagsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CleanWeightRequestAdapter extends TypeAdapter<CleanWeightRequest> {
  @override
  final int typeId = 19;

  @override
  CleanWeightRequest read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CleanWeightRequest(
      batchId: fields[0] as String?,
      cleanWeight: fields[1] as String?,
      userId: fields[2] as String?,
      bagId: fields[3] as String?,
      dirtyWeight: fields[6] as String?,
      image: fields[4] as String?,
      lineId: fields[5] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, CleanWeightRequest obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.batchId)
      ..writeByte(1)
      ..write(obj.cleanWeight)
      ..writeByte(2)
      ..write(obj.userId)
      ..writeByte(3)
      ..write(obj.bagId)
      ..writeByte(4)
      ..write(obj.image)
      ..writeByte(5)
      ..write(obj.lineId)
      ..writeByte(6)
      ..write(obj.dirtyWeight);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CleanWeightRequestAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
