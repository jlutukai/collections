class PlannerReportResponse {
  List<PlannerReportData>? data;
  String? message;
  bool? errors;

  PlannerReportResponse({this.data, this.message, this.errors});

  PlannerReportResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <PlannerReportData>[];
      json['data'].forEach((v) {
        data!.add(PlannerReportData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class PlannerReportData {
  String? id;
  String? questionId;
  String? plannerId;
  String? categoryId;
  String? response;
  String? date;
  String? answeredBy;
  String? defaultCustom;
  String? shiftId;
  String? status;

  PlannerReportData(
      {this.id,
      this.questionId,
      this.plannerId,
      this.categoryId,
      this.response,
      this.date,
      this.answeredBy,
      this.defaultCustom,
      this.shiftId,
      this.status});

  PlannerReportData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    questionId = json['question_id'];
    plannerId = json['planner_id'];
    categoryId = json['category_id'];
    response = json['response'];
    date = json['date'];
    answeredBy = json['answered_by'];
    defaultCustom = json['default_custom'];
    shiftId = json['shift_id'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['question_id'] = questionId;
    data['planner_id'] = plannerId;
    data['category_id'] = categoryId;
    data['response'] = response;
    data['date'] = date;
    data['answered_by'] = answeredBy;
    data['default_custom'] = defaultCustom;
    data['shift_id'] = shiftId;
    data['status'] = status;
    return data;
  }
}
