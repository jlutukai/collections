class ReportingCategoriesResponse {
  List<CategoryData>? data;
  String? message;
  bool? errors;

  ReportingCategoriesResponse({this.data, this.message, this.errors});

  ReportingCategoriesResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <CategoryData>[];
      json['data'].forEach((v) {
        data!.add(CategoryData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class CategoryData {
  String? id;
  String? name;
  bool? isCompete = false;

  CategoryData({this.id, this.name, this.isCompete});

  CategoryData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    return data;
  }
}
