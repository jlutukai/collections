class ScanBagQRBody {
  String? type;
  String? bag;
  String? id;

  ScanBagQRBody({this.type, this.bag, this.id});

  ScanBagQRBody.fromJson(Map<String, dynamic> json) {
    type = json['type']?.toString();
    bag = json['bag'] != null ? json['bag'].toString() : "";
    id = json['id'] != null ? json['id'].toString() : "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['type'] = type;
    data['bag'] = bag;
    data['id'] = id;
    return data;
  }
}
