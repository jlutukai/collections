class ScanBagResponse {
  List<ScanBagData>? data;
  String? message;
  bool? errors;

  ScanBagResponse({this.data, this.message, this.errors});

  ScanBagResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <ScanBagData>[];
      json['data'].forEach((v) {
        data!.add(ScanBagData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class ScanBagData {
  String? id;
  String? bedId;
  String? bagId;
  String? plannerStageId;
  String? bedName;
  String? staffName;
  String? varietyName;
  String? stageName;
  String? pictureName;
  String? endDate;
  String? bagStatus;

  ScanBagData(
      {this.id,
      this.bedId,
      this.bagId,
      this.plannerStageId,
      this.bedName,
      this.staffName,
      this.varietyName,
      this.stageName,
      this.pictureName,
      this.endDate,
      this.bagStatus});

  ScanBagData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    bedId = json['bed_id'];
    bagId = json['bag_id'];
    plannerStageId = json['planner_stage_id'];
    bedName = json['bed_name'];
    staffName = json['staff_name'];
    varietyName = json['variety_name'];
    stageName = json['stage_name'];
    pictureName = json['picture_name'];
    endDate = json['end_date'];
    bagStatus = json['bag_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['bed_id'] = bedId;
    data['bag_id'] = bagId;
    data['planner_stage_id'] = plannerStageId;
    data['bed_name'] = bedName;
    data['staff_name'] = staffName;
    data['variety_name'] = varietyName;
    data['stage_name'] = stageName;
    data['picture_name'] = pictureName;
    data['end_date'] = endDate;
    data['bag_status'] = bagStatus;
    return data;
  }
}
