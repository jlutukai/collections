class ScanBedResponse {
  List<ScanBedData>? data;
  String? message;
  bool? errors;

  ScanBedResponse({this.data, this.message, this.errors});

  ScanBedResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <ScanBedData>[];
      json['data'].forEach((v) {
        data!.add(ScanBedData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class ScanBedData {
  String? code;
  String? sectionId;
  List<StaffDetails>? staffDetails;
  List<StaffDetails>? staffPollinatedDetails;
  List<LineDetails>? lineDetails;
  List<SectionDetails>? sectionDetails;
  String? bedName;

  ScanBedData(
      {this.code,
      this.sectionId,
      this.staffDetails,
      this.lineDetails,
      this.sectionDetails,
        this.staffPollinatedDetails,
      this.bedName});

  ScanBedData.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    sectionId = json['section_id'];
    if (json['staff_details'] != null) {
      staffDetails = <StaffDetails>[];
      json['staff_details'].forEach((v) {
        staffDetails!.add(StaffDetails.fromJson(v));
      });
    }
    if (json['staff_pollinated_details'] != null) {
      staffPollinatedDetails = <StaffDetails>[];
      json['staff_pollinated_details'].forEach((v) {
        staffPollinatedDetails!.add(StaffDetails.fromJson(v));
      });
    }
    if (json['line_details'] != null) {
      lineDetails = <LineDetails>[];
      json['line_details'].forEach((v) {
        lineDetails!.add(LineDetails.fromJson(v));
      });
    }
    if (json['section_details'] != null) {
      sectionDetails = <SectionDetails>[];
      json['section_details'].forEach((v) {
        sectionDetails!.add(SectionDetails.fromJson(v));
      });
    }
    bedName = json['bed_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['section_id'] = sectionId;
    if (staffDetails != null) {
      data['staff_details'] = staffDetails!.map((v) => v.toJson()).toList();
    }
    if (staffPollinatedDetails != null) {
      data['staff_pollinated_details'] = staffPollinatedDetails!.map((v) => v.toJson()).toList();
    }
    if (lineDetails != null) {
      data['line_details'] = lineDetails!.map((v) => v.toJson()).toList();
    }
    if (sectionDetails != null) {
      data['section_details'] = sectionDetails!.map((v) => v.toJson()).toList();
    }
    data['bed_name'] = bedName;
    return data;
  }
}

class StaffDetails {
  String? staffName;
  String? id;

  StaffDetails({this.staffName, this.id});

  @override
  String toString() {
    return '$staffName';
  }

  StaffDetails.fromJson(Map<String, dynamic> json) {
    staffName = json['staff_name'];
    id = json['staff_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['staff_name'] = staffName;
    data['staff_id'] = id;
    return data;
  }
}

class SectionDetails {
  String? sectionName;
  String? id;

  SectionDetails({this.sectionName, this.id});

  @override
  String toString() {
    return 'SectionDetails{sectionName: $sectionName}';
  }

  SectionDetails.fromJson(Map<String, dynamic> json) {
    sectionName = json['section_name'];
    id = json['section_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['section_name'] = sectionName;
    data['section_id'] = id;
    return data;
  }
}

class LineDetails {
  String? varietyName;
  String? stageName;
  String? contractNo;
  String? type;
  String? totalLostQuantity;
  String? currentStageLostQuantity;
  String? currentPlantNo;
  String? stageFemalePlantNo;
  String? stageMalePlantNo;
  String? endDate;
  String? pictureName;
  double?
      quotaProduced;
  double?
      quotaProducedPercentage;
  String? uom; //"uom": "K",
  int? bedNo;

  LineDetails(
      {this.varietyName,
      this.stageName,
      this.contractNo,
      this.type,
      this.totalLostQuantity,
      this.currentStageLostQuantity,
      this.currentPlantNo,
      this.stageFemalePlantNo,
      this.stageMalePlantNo,
      this.endDate,
      this.pictureName,
      this.quotaProduced,
      this.quotaProducedPercentage,
      this.uom, this.bedNo});

  LineDetails.fromJson(Map<String, dynamic> json) {
    varietyName = json['variety_name'];
    stageName = json['stage_name'];
    contractNo = json['contract_no'];
    type = json['type'];
    totalLostQuantity = json['total_lost_quantity']?.toString();
    currentStageLostQuantity = json['current_stage_lost_quantity']?.toString();
    currentPlantNo = json['current_plant_no']?.toString();
    stageFemalePlantNo = json['stage_female_plant_no']?.toString() ?? json["female_plant_no"]?.toString();
    stageMalePlantNo = json['stage_male_plant_no']?.toString() ??  json["male_plant_no"]?.toString() ;
    endDate = json['end_date'];
    pictureName = json['picture_name'];
    quotaProduced = json['quota_produced']?.toDouble();
    quotaProducedPercentage = json['quota_produced_percentage']?.toDouble();
    uom = json['uom'];
    bedNo = json['bed_no'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['variety_name'] = varietyName;
    data['stage_name'] = stageName;
    data['contract_no'] = contractNo;
    data['type'] = type;
    data['total_lost_quantity'] = totalLostQuantity;
    data['current_stage_lost_quantity'] = currentStageLostQuantity;
    data['current_plant_no'] = currentPlantNo;
    data['stage_female_plant_no'] = stageFemalePlantNo;
    data['stage_male_plant_no'] = stageMalePlantNo;
    data['end_date'] = endDate;
    data['picture_name'] = pictureName;
    data['quota_produced'] = quotaProduced;
    data['quota_produced_percentage'] = quotaProducedPercentage;
    data['uom'] = uom;
    return data;
  }
}
