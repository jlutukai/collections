// class GetScanGreenHouseResponse {
//   List<ScanGreenHouseData>? data;
//   String? message;
//   bool? errors;
//
//   GetScanGreenHouseResponse({this.data, this.message, this.errors});
//
//   GetScanGreenHouseResponse.fromJson(Map<String, dynamic> json) {
//     if (json['data'] != null) {
//       data = <ScanGreenHouseData>[];
//       json['data'].forEach((v) {
//         data!.add(ScanGreenHouseData.fromJson(v));
//       });
//     }
//     message = json['message'];
//     errors = json['errors'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = <String, dynamic>{};
//     if (this.data != null) {
//       data['data'] = this.data!.map((v) => v.toJson()).toList();
//     }
//     data['message'] = message;
//     data['errors'] = errors;
//     return data;
//   }
// }
//
// class ScanGreenHouseData {
//   String? supervisorId;
//   String? supervisorName;
//   String? staffPicture;
//   String? greenhouseArea;
//   int? greenhouseBedsNo;
//   String? totalUsedArea;
//   int? usedBeds;
//   String? usedAreaRate;
//   String? usedBedsRate;
//   List<LineDetails>? lineDetails;
//
//   ScanGreenHouseData(
//       {this.supervisorId,
//       this.supervisorName,
//       this.staffPicture,
//       this.greenhouseArea,
//       this.greenhouseBedsNo,
//       this.totalUsedArea,
//       this.usedBeds,
//       this.usedAreaRate,
//       this.usedBedsRate,
//       this.lineDetails});
//
//   ScanGreenHouseData.fromJson(Map<String, dynamic> json) {
//     supervisorId = json['supervisor_id'];
//     supervisorName = json['supervisor_name'];
//     staffPicture = json['staff_picture'];
//     greenhouseArea = json['greenhouse_area'];
//     greenhouseBedsNo = json['greenhouse_beds_no'];
//     totalUsedArea = json['total_used_area'];
//     usedBeds = json['used_beds'];
//     usedAreaRate = json['used_area_rate'];
//     usedBedsRate = json['used_beds_rate'];
//     if (json['line_details'] != null) {
//       lineDetails = <LineDetails>[];
//       json['line_details'].forEach((v) {
//         lineDetails!.add(LineDetails.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = <String, dynamic>{};
//     data['supervisor_id'] = supervisorId;
//     data['supervisor_name'] = supervisorName;
//     data['staff_picture'] = staffPicture;
//     data['greenhouse_area'] = greenhouseArea;
//     data['greenhouse_beds_no'] = greenhouseBedsNo;
//     data['total_used_area'] = totalUsedArea;
//     data['used_beds'] = usedBeds;
//     data['used_area_rate'] = usedAreaRate;
//     data['used_beds_rate'] = usedBedsRate;
//     if (lineDetails != null) {
//       data['line_details'] = lineDetails!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class LineDetails {
//   String? varietyName;
//   String? stageName;
//   String? stageEndDate;
//   int? stageBedsUsed;
//   String? endDate;
//   String? pictureName;
//
//   LineDetails(
//       {this.varietyName,
//       this.stageName,
//       this.stageEndDate,
//       this.stageBedsUsed,
//       this.endDate,
//       this.pictureName});
//
//   LineDetails.fromJson(Map<String, dynamic> json) {
//     varietyName = json['variety_name'];
//     stageName = json['stage_name'];
//     stageEndDate = json['stage_end_date'];
//     stageBedsUsed = json['stage_beds_used'];
//     endDate = json['end_date'];
//     pictureName = json['picture_name'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = <String, dynamic>{};
//     data['variety_name'] = varietyName;
//     data['stage_name'] = stageName;
//     data['stage_end_date'] = stageEndDate;
//     data['stage_beds_used'] = stageBedsUsed;
//     data['end_date'] = endDate;
//     data['picture_name'] = pictureName;
//     return data;
//   }
// }
