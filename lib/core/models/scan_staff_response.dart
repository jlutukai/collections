class ScanStaffResponse {
  List<StaffScanData>? data;
  String? message;
  bool? errors;

  ScanStaffResponse({this.data, this.message, this.errors});

  ScanStaffResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <StaffScanData>[];
      json['data'].forEach((v) {
        data!.add(StaffScanData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}

class StaffScanData {
  String? name;
  String? functionId;
  String? functionName;
  String? staffPicture;
  List<FunctionCompetency>? functionCompetency;
  List<BedDetails>? bedDetails;

  StaffScanData(
      {this.name,
      this.functionId,
      this.functionName,
      this.staffPicture,
      this.functionCompetency,
      this.bedDetails});

  StaffScanData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    functionId = json['function_id'];
    functionName = json['function_name'];
    staffPicture = json['staff_picture'];
    if (json['function_competency'] != null) {
      functionCompetency = <FunctionCompetency>[];
      json['function_competency'].forEach((v) {
        functionCompetency!.add(FunctionCompetency.fromJson(v));
      });
    }
    if (json['bed_details'] != null) {
      bedDetails = <BedDetails>[];
      json['bed_details'].forEach((v) {
        bedDetails!.add(BedDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['function_id'] = functionId;
    data['function_name'] = functionName;
    data['staff_picture'] = staffPicture;
    if (functionCompetency != null) {
      data['function_competency'] =
          functionCompetency!.map((v) => v.toJson()).toList();
    }
    if (bedDetails != null) {
      data['bed_details'] = bedDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FunctionCompetency {
  String? competencyName;

  FunctionCompetency({this.competencyName});

  FunctionCompetency.fromJson(Map<String, dynamic> json) {
    competencyName = json['competency_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['competency_name'] = competencyName;
    return data;
  }
}

class BedDetails {
  String? bedId;
  String? bedName;
  String? stageName;
  String? startDate;
  String? endDate;
  String? varietyName;
  String? pictureName;
  String? plannerStageId;

  BedDetails(
      {this.bedId,
      this.bedName,
      this.stageName,
      this.startDate,
      this.endDate,
      this.varietyName,
      this.pictureName,
      this.plannerStageId});


  @override
  String toString() {
    return '${bedName ?? 'undefined'} - $varietyName';
  }

  BedDetails.fromJson(Map<String, dynamic> json) {
    bedId = json['bed_id'];
    bedName = json['bed_name'];
    stageName = json['stage_name'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    varietyName = json['variety_name'];
    pictureName = json['picture_name'];
    plannerStageId = json['planner_stage_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bed_id'] = bedId;
    data['bed_name'] = bedName;
    data['stage_name'] = stageName;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['variety_name'] = varietyName;
    data['picture_name'] = pictureName;
    data['planner_stage_id'] = plannerStageId;
    return data;
  }
}
