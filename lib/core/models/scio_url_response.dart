class ScioUrlResponse {
  ScioUrl? scioUrl;
  String? message;

  ScioUrlResponse({this.scioUrl, this.message});

  ScioUrlResponse.fromJson(Map<String, dynamic> json) {
    scioUrl = json['scio_url'] != null
        ? ScioUrl.fromJson(json['scio_url'])
        : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (scioUrl != null) {
      data['scio_url'] = scioUrl!.toJson();
    }
    data['message'] = message;
    return data;
  }
}

class ScioUrl {
  Data? data;

  ScioUrl({this.data});

  ScioUrl.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? id;
  String? name;
  String? url;
  String? dropLat;
  String? dropLong;
  String? status;
  String? type;

  Data(
      {this.id,
        this.name,
        this.url,
        this.dropLat,
        this.dropLong,
        this.status,
        this.type});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    url = json['url'];
    dropLat = json['drop_lat'];
    dropLong = json['drop_long'];
    status = json['status'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['name'] = name;
    data['url'] = url;
    data['drop_lat'] = dropLat;
    data['drop_long'] = dropLong;
    data['status'] = status;
    data['type'] = type;
    return data;
  }
}
