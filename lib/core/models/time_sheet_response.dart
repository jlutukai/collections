import 'local/localModels.dart';

class TimeSheetResponse {
  List<TimeSheetData>? data;
  String? message;
  bool? errors;

  TimeSheetResponse({this.data, this.message, this.errors});

  TimeSheetResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <TimeSheetData>[];
      json['data'].forEach((v) {
        data!.add(TimeSheetData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['errors'] = errors;
    return data;
  }
}
