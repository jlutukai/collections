import 'dart:io';

import 'package:chucker_flutter/chucker_flutter.dart';
import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:flutter/foundation.dart';
import 'package:weight_collection/core/models/complete_stage_response.dart';
import 'package:weight_collection/core/models/get_clerk_performance_resonse.dart';
import 'package:weight_collection/core/models/get_unassigned_bags_response.dart';
import 'package:weight_collection/core/models/login_response.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';
import 'package:weight_collection/utils/useful.dart';

import '../models/error.dart';
import '../models/general_response.dart';
import '../models/get_available_lines.dart';
import '../models/get_scan_greenhouse_response.dart';
import '../models/get_task_appraisal.dart';
import '../models/get_staff_response.dart';
import '../models/get_tasks_response.dart';
import '../models/scio_url_response.dart';
import '../viewmodels/ReportFormModel.dart';

class Api {
  static const baseUrl = 'https://office.scioerp.net/app_api';
  late Dio _dio;
  String firebaseId = "";

  Api() {
    BaseOptions options =
        BaseOptions(receiveTimeout: const Duration(seconds: 180), connectTimeout: const Duration(seconds: 180));
    _dio = Dio(options);
    _dio.interceptors.add(LogInterceptor(
        request: true,
        requestBody: true,
        requestHeader: true,
        responseHeader: true,
        responseBody: true));
    _dio.interceptors.add(ChuckerDioInterceptor());

    _dio.options.headers["content-type"] = "application/json";
  }

  Future<Either<CustomError, Response>> safeCall(
      Future<Response> request) async {
    try {
      return Right(await request);
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response!.statusCode == 404) {
          return const Left(
            CustomError(
              key: AppError.NotFound,
              message: "Request not found",
            ),
          );
        } else {
          return Left(
            CustomError(
              key: AppError.NotFound,
              message: e.response?.data["message"],
            ),
          );
        }
      } else {
        // Error due to setting up or sending the request
        if (kDebugMode) {
          print('Error sending request!');
        }
        if (kDebugMode) {
          print(e.message);
        }
        return Left(
          CustomError(
            key: AppError.UndefinedError,
            message: e.message,
          ),
        );
      }
    }
  }


  Either<CustomError, Response> checkHttpStatus(Response response) {
    if (response.statusCode == 200) return Right(response);
    if (response.statusCode! >= 500) {
      return Left(CustomError(
          key: AppError.InternalServerError,
          message: "Server error with http status ${response.statusCode}"));
    }
    return Left(CustomError(
        key: AppError.BadRequest,
        message: "Bad http status ${response.statusCode}"));
  }

  Future<Either<CustomError, Map<String, dynamic>>> parseJson(
      Response response) async {
    try {
      return Right(response.data);
    } catch (e) {
      return const Left(CustomError(
          key: AppError.JsonParsing, message: 'failed on json parsing'));
    }
  }

  Future<Either<CustomError, ScioUrlResponse>> verifyUrl(FormData data,) {
    String endPoint = "$baseUrl/scio_address.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(ScioUrlResponse.fromJson);
  }

  Future<Either<CustomError, LoginResponse>> login(FormData data) async {
    String endPoint = "${getUrl()}/app_api/login.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(LoginResponse.fromJson);
  }

  Future<Either<CustomError, GeneralResponse>> updateToken(Map<String, String> data) async {
    String endPoint = "${getUrl()}/app_api/update_token.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GeneralResponse.fromJson);
  }

  Future<Either<CustomError, GeneralResponse>> removeToken(Map<String, String> data) async {
    String endPoint = "${getUrl()}/app_api/logout.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GeneralResponse.fromJson);
  }

  Future<Map<String, dynamic>> getSites() async {
    try {
      String endPoint = "${getUrl()}/app_api/get_site.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> createVisitor(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/create_visitor.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getVisitors(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/get_visitor.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> checkOutVisitor(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/check_out_visitor.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getPlanners(Map<String, String> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/attendance/get_planner_details.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTimeSheet(Map<String, String> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/attendance/get_timesheet_details.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTimeSheetEmployees(
      Map<String, String> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/attendance/get_timesheet_employees.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/attendance/update_status.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> uploadVerificationImage(File file) async {
    var endpoint = "${getUrl()}/app_api/attendance/upload_picture.php";

    String fileName = file.path.split('/').last;
    String mimeType = mime(fileName)!;
    String mimee = mimeType.split('/')[0];
    String type = mimeType.split('/')[1];
    FormData formData = FormData.fromMap({
      "image": await MultipartFile.fromFile(file.path,
          filename: fileName, contentType: MediaType(mimee, type)),
    });

    Dio dio = Dio();
    dio.options.headers["content-type"] = "multipart/form-data";
    // dio.interceptors.add(LoggingInterceptor());

    try {
      Response response = await dio.post(
        endpoint,
        data: formData,
        onSendProgress: (received, total) {
          if (total != -1) {
            print("${(received / total * 100).toStringAsFixed(0)}%");
          }
        },
      );
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getPlannerReport(
      Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_planner_report.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getCategories() async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_checklist_category.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getChecklist(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_planner_checklist.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getDefaultChecklist() async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_default_checklist.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateResponse(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/update_response.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }


  Future<Map<String, dynamic>> getReportsForm(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_form.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }




  Future<Map<String, dynamic>> getTaskTypes() async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_task_type.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getFunctions() async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_function.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getStaff() async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_staff.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> createTask(Map<String, dynamic> data) async {
    try {
      String? associatedId = data['item_associated_id'];
      String endPoint = (associatedId ?? '').isEmpty
          ? "${getUrl()}/app_api/task_new/create_task.php"
          : "${getUrl()}/app_api/collection/create_task.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }


  Future<Map<String, dynamic>> updateTaskProgress(
      Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/update_progress.php";
      Response response = await _dio.post(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> createTaskComment(
      Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/create_comment.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTaskComment(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_comment.php";
      Response response = await _dio.get(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> editTaskDate(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/edit_deadline.php";
      Response response = await _dio.post(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  // _handleError(error) {
  //   String errorDescription = "";
  //   if (error is DioError) {
  //     DioError dioError = error;
  //     switch (dioError.type) {
  //       case DioErrorType.CANCEL:
  //         errorDescription = "Request to API server was cancelled";
  //         break;
  //       case DioErrorType.CONNECT_TIMEOUT:
  //         errorDescription = "Connection timeout with API server";
  //         break;
  //       case DioErrorType.DEFAULT:
  //         errorDescription =
  //             "Connection to API server failed due to internet connection";
  //         break;
  //       case DioErrorType.RECEIVE_TIMEOUT:
  //         errorDescription = "Receive timeout in connection with API server";
  //         break;
  //       case DioErrorType.RESPONSE:
  //         errorDescription = "${dioError.response.data['error']} ";
  //         break;
  //       case DioErrorType.SEND_TIMEOUT:
  //         errorDescription = "Send timeout in connection with API server";
  //         break;
  //     }
  //   } else {
  //     errorDescription = "Unexpected error occurred";
  //   }
  //   return errorDescription;
  // }

  Future<Map<String, dynamic>> getWeightType() async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/get_weight_type.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getBags() async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/get_bag.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> openBatch(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/open_batch.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateBatchItem(
      Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/update_batch_item.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateBatchStatus(
      Map<String, dynamic> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/collection/update_batch_status.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getScanStaff(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/scan_staff_id.php";
      Response response = await _dio.get(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getScanBed(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/scan_bed_id.php";
      Response response = await _dio.get(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getScanBag(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/scan_bag_id.php";
      Response response = await _dio.get(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> assignStaffToBed(
      Map<String, dynamic> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/collection/assign_staff_to_bed.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> assignBagToBed(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/assign_bag_to_bed.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> clearBed(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/clear_bed.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> clearStaff(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/clear_staff.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> clearStaffItem(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/clear_staff_item.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> clearBag(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/clear_bag.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Either<CustomError, GetGreenHouseData>> scanGreenHouse(Map<String, dynamic> data) {
    String endPoint = "${getUrl()}/app_api/lists/scan_greenhouse_id.php";
    return safeCall(_dio.get(endPoint, queryParameters: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GetGreenHouseData.fromJson);
  }

  // Future<Map<String, dynamic>> scanGreenHouse(Map<String, dynamic> data) async {
  //   try {
  //     String endPoint = "${getUrl()}/app_api/lists/scan_greenhouse_id.php";
  //     Response response = await _dio.get(endPoint, queryParameters: data);
  //     return getErrorCode(response);
  //   } on SocketException {
  //     final jsonResponse = {
  //       'success': false,
  //       'response': "Check your Internet connection"
  //     };
  //     return jsonResponse;
  //   } on FormatException {
  //     final jsonResponse = {
  //       'success': false,
  //       'response': "Bad response format"
  //     };
  //     return jsonResponse;
  //   } on HttpException {
  //     final jsonResponse = {
  //       'success': false,
  //       'response': "Server is unreachable" //Server not responding
  //     };
  //     return jsonResponse;
  //   }
  // }

  Future<Map<String, dynamic>> updateMaintenance(
      Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/update_maintenance.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateStaffReport(
      Map<String, dynamic> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/collection/update_staff_report.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getBedLineItems(
      Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/get_bed_info.php";
      Response response = await _dio.get(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateBagWeighment(
      Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/mark_bag_weighment.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> reAssignBed(Map<String, dynamic> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/collection/reassign_staff_to_bed.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getLossReasons() async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/get_loss_drop.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getScoutingReasons() async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/get_scouting_drop.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateLoss(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/update_loss.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateScouting(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/update_scouting.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getDailyWet() async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/get_daily_wet.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateDailyWet(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/update_daily_wet.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getBatchWet() async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/get_batch_dry.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateBatchWet(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/update_batch_dry.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getBatchDry() async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/get_batch_clean.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateCleanWeight(
      Map<String, dynamic> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/collection/update_clean_weight.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> movePlant(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/move_plants.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getPlantsInSuspense() async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/get_plants.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getAvailablePlants() async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/get_available_line.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> receivePlant(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/receive_plants.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> addPlant(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/add_plant.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getForms() async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/get_form.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateStageStatus(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/mark_stage_complete.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Either<CustomError, CompleteStageResponse>> completeStage(Map<String, dynamic> data) {
    String endPoint = "${getUrl()}/app_api/collection/complete_stage.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(CompleteStageResponse.fromJson);
  }

  Future<Either<CustomError, GetAvailableLines>> getAvailableLines() {
    String endPoint = "${getUrl()}/app_api/collection/get_awaiting_line.php";
    return safeCall(_dio.get(endPoint))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GetAvailableLines.fromJson);
  }
  Future<Either<CustomError, GetStaffResponse>> getCollectionStaff() {
    String endPoint = "${getUrl()}/app_api/lists/get_staff.php";
    return safeCall(_dio.get(endPoint))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GetStaffResponse.fromJson);
  }
  Future<Either<CustomError, CompleteStageResponse>> startStage(Map<String, dynamic> data) {
    String endPoint = "${getUrl()}/app_api/collection/initiate_stage.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(CompleteStageResponse.fromJson);
  }

  Future<Either<CustomError, CompleteStageResponse>> addSection(Map<String, dynamic> data) {
    String endPoint = "${getUrl()}/app_api/collection/create_section.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(CompleteStageResponse.fromJson);
  }

  Future<Either<CustomError, CompleteStageResponse>> removeSection(Map<String, dynamic> data) {
    String endPoint = "${getUrl()}/app_api/collection/clear_section.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(CompleteStageResponse.fromJson);
  }

  Future<Map<String, dynamic>> updateEndDate(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/collection/ammend_stage_date.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getVarieties() async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/get_variety.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }
  Future<Map<String, dynamic>> getSpecies() async {
    try {
      String endPoint = "${getUrl()}/app_api/lists/get_species.php";
      Response response = await _dio.get(endPoint);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Either<CustomError, GetTasksResponse>> getTasks(Map<String, dynamic>  data) async {
    String endPoint = "${getUrl()}/app_api/task_new/get_task.php";
    return safeCall(_dio.get(endPoint, queryParameters: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GetTasksResponse.fromJson);
  }

  Future<Either<CustomError, GetTaskAppraisal>> getTasks2(Map<String, dynamic>  data) async {
    String endPoint = "${getUrl()}/app_api/appraisal/get_task.php";
    return safeCall(_dio.get(endPoint, queryParameters: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GetTaskAppraisal.fromJson);
  }

  Future<Either<CustomError, GeneralResponse>> updateFormResponse(Map<String, dynamic>  data) async {
    String endPoint = "${getUrl()}/app_api/collection/update_response.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GeneralResponse.fromJson);
  }

  Future<Either<CustomError, GeneralResponse>> updateFormTaskResponse(Map<String, dynamic>  data) async {
    String endPoint = "${getUrl()}/app_api/appraisal/update_task.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GeneralResponse.fromJson);
  }
  Future<Either<CustomError, GeneralResponse>> delayTask(Map<String, dynamic>  data) async {
    String endPoint = "${getUrl()}/app_api/appraisal/delay_task.php";
    return safeCall(_dio.post(endPoint, data: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GeneralResponse.fromJson);
  }

  Future<Either<CustomError, GeneralResponse>> uploadFormResponse(File file, Map<String, dynamic> request) async {

    print(request);

    String fileName = file.path.split('/').last;
    String mimeType = mime(fileName)!;
    String mimee = mimeType.split('/')[0];
    String type = mimeType.split('/')[1];
    FormData formData = FormData.fromMap({
      ...request,
      "image": await MultipartFile.fromFile(file.path,
          filename: fileName, contentType: MediaType(mimee, type)),
    });
    var endPoint = "${getUrl()}/app_api/collection/update_response_picture.php";
    return safeCall(_dio.post(endPoint, data: formData, options: Options(
      headers: {"content-type" : "multipart/form-data"}
    )))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GeneralResponse.fromJson);

  }

  Future<Either<CustomError, GetUnassignedBagsResponse>> getUnassignedBags(Map<String, dynamic>  data) async {
    String endPoint = "${getUrl()}/app_api/collection/get_unknown_bag.php";
    return safeCall(_dio.get(endPoint, queryParameters: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GetUnassignedBagsResponse.fromJson);
  }

  Future<Either<CustomError, GetClerkPerformanceResponse>> getStaffPerformance(Map<String, dynamic>  data) async {
    String endPoint = "${getUrl()}/app_api/collection/get_clerk_performance.php";
    return safeCall(_dio.get(endPoint, queryParameters: data))
        .thenRight(checkHttpStatus)
        .thenRight(parseJson)
        .mapRight(GetClerkPerformanceResponse.fromJson);
  }


  Map<String, dynamic> getErrorCode(Response response) {
    switch (response.statusCode) {
      case 200:
        final result = response.data;
        final jsonResponse = {'success': true, 'response': result};
        return jsonResponse;
      case 201:
        final result = response.data;
        final jsonResponse = {'success': true, 'response': result};
        return jsonResponse;
      case 400:
        final jsonResponse = {
          'success': false,
          'response': "Bad response ${response.statusCode}"
        };
        return jsonResponse;
      case 401:
        final jsonResponse = {
          'success': false,
          'response':
              "UnAuthorized Access, Login to continue ${response.statusCode}"
        };
        return jsonResponse;
      case 404:
        final jsonResponse = {
          'success': false,
          'response': "File Not Found! ${response.statusCode}"
        };
        return jsonResponse;
      case 500:
        final jsonResponse = {
          'success': false,
          'response': "Internal Server Error ${response.statusCode}"
        };
        return jsonResponse;
      case 501:
        final jsonResponse = {
          'success': false,
          'response': "Internal Server Error ${response.statusCode}"
        };
        return jsonResponse;
      case 502:
        final jsonResponse = {
          'success': false,
          'response': "Internal Server error ${response.statusCode}"
        };
        return jsonResponse;
      default:
        final jsonResponse = {
          'success': false,
          'response': "Something Went wrong ${response.statusCode}"
        };
        return jsonResponse;
    }
  }


}

//https://www.getpostman.com/collections/66e34611274b84543b43

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) {
        final isValidHost = ["https://${getUrl()}"]
            .contains(host); // <-- allow only hosts in array
        return true;
      });
  }
}
