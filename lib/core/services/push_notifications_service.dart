


import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../utils/useful.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;



  Future<void> initialise() async {
    NotificationSettings settings = await _fcm.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if(settings.authorizationStatus != AuthorizationStatus.authorized){
      Permission.notification.request();
    }

    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      if (kDebugMode) {
        print("message received");
      print(event.notification!.body);}
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      if (kDebugMode) {
        print('Message clicked!');
      }
    });
  }

  Future<String?> getFCMToken() async {
    var t = await _fcm.getToken();
    setTokenId(t??"");
    return t;
  }
}
