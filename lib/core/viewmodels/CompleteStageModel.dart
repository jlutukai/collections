import 'package:either_dart/either.dart';
import 'package:weight_collection/core/models/complete_stage_response.dart';
import 'package:weight_collection/core/models/error.dart';
import 'package:weight_collection/core/viewmodels/base_model.dart';

import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_bed_line_items_response.dart';
import '../models/get_species_response.dart';
import '../models/get_variety_response.dart';
import '../models/local/localModels.dart';
import '../services/api.dart';

class CompleteStageModel extends BaseModel {
  final Api _api = locator<Api>();
  List<LineData> lineData = [];
  List<FormItems> questions = [];
  List<VarietyData> varieties = [];
  List<SpeciesData> species = [];
  LineData? lineItem;
  String? formId;

  Future<Map<String, dynamic>> getLineItems(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBedLineItems(data);
      if (r['success']) {
        var response = GetBedLineItemsResponse.fromJson(r["response"]);
        if (response.data != null) {
          lineData = response.data!.cast<LineData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getVarieties() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getVarieties();
      if (r['success']) {
        var response = GetVarietyResponse.fromJson(r["response"]);
        if (response.data != null) {
          varieties = response.data!.cast<VarietyData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getSpecies() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getSpecies();
      if (r['success']) {
        var response = GetSpeciesResponse.fromJson(r["response"]);
        if (response.data != null) {
          species = response.data!.cast<SpeciesData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  void setLineData(LineData ld) {
    lineItem = ld;
    setUpQuestions(ld);
    notifyListeners();
  }

  void setUpQuestions(LineData ld) {
    questions.clear();
    for (var element in ld.items!) {
      formId = element.formId;
      FormItems formItem = FormItems()
        ..response = ""
        ..type = "default"
        ..formItemId = element.id
        ..question = element.name
        ..responseType = element.typeCode
        ..required = true;
      questions.add(formItem);
    }

    notifyListeners();
  }

  void setResponse(
      {required String? formItemId,
      required String? type,
      required String response,  bool? isFile,  String? responseName}) {
    for (var element in questions) {
      if (formItemId == element.formItemId && type == element.type) {
        element
          ..response = response
          ..responseName = responseName
          ..isFile = isFile;
        notifyListeners();
      }
    }
  }

  Future<Either<CustomError,CompleteStageResponse>> completeStage(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.completeStage(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
