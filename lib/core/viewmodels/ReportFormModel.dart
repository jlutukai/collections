import 'dart:io';

import 'package:either_dart/either.dart';
import 'package:hive/hive.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/error.dart';
import 'package:weight_collection/core/models/general_response.dart';
import 'package:weight_collection/core/services/api.dart';

import '../../locator.dart';
import '../../utils/useful.dart';
import '../models/get_forms_response.dart';
import '../models/get_species_response.dart';
import '../models/get_staff_response.dart';
import '../models/get_variety_response.dart';
import '../models/local/localModels.dart';
import 'base_model.dart';

class ReportFormsModel extends BaseModel {
  final Api _api = locator<Api>();
  List<FormsData> forms = [];
  List<VarietyData> varieties = [];
  List<SpeciesData> species = [];
  List<StaffData> staff = [];
  List<FormItems> questions = [];
  UpdateFormData? existingForm;

  Box offlineResponses = Hive.box("form_response");
  Box savedResponses = Hive.box("form_response_questions");

  Future<Map<String, dynamic>> getForms() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getForms();
      if (r['success']) {
        var d = GetFormsResponse.fromJson(r['response']);
        if (d.data != null) {
          forms = d.data!;
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError, GeneralResponse>> updateResponse(
      UpdateFormData data) async {
    setState(ViewState.Busy);
    if (data.taskId == null) {
      var r = await _api.updateFormResponse(data.toJson());
      setState(ViewState.Idle);
      return r;
    } else {
      var r = await _api.updateFormTaskResponse(data.toJson());
      setState(ViewState.Idle);
      return r;
    }
  }

  Future<Map<String, dynamic>> getVarieties() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getVarieties();
      if (r['success']) {
        var response = GetVarietyResponse.fromJson(r["response"]);
        if (response.data != null) {
          varieties = response.data!.cast<VarietyData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getSpecies() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getSpecies();
      if (r['success']) {
        var response = GetSpeciesResponse.fromJson(r["response"]);
        if (response.data != null) {
          species = response.data!.cast<SpeciesData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError, List<StaffData>>> getStaff() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getCollectionStaff();
      if (r.isRight) {
        staff = r.right.data ?? [];
      }
      setState(ViewState.Idle);
      return r.map((right) => right.data ?? []);
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  void initQuestions(
      {required String key,
      required String type,
      required int id,
      required FormsData formData,
      required int? plannerStageId,
      required int? taskId}) {
    questions.clear();

    existingForm = savedResponses.get(key);
    if (existingForm == null) {
      for (var element in (formData.items ?? [])) {
        FormItems formItem = FormItems()
          ..response = ""
          ..type = "default"
          ..formItemId = element.id
          ..question = element.name
          ..responseType = element.typeCode
          ..required = true;
        questions.add(formItem);
      }
      for (var element in (formData.customItems ?? [])) {
        FormItems formItem = FormItems()
          ..response = ""
          ..type = "custom"
          ..formItemId = element.id
          ..responseType = element.typeCode
          ..question = element.name
          ..required = true;
        questions.add(formItem);
      }
      UpdateFormData d = UpdateFormData()
        ..id = ""
        ..type = type
        ..typeId = id
        ..answeredBy = getUserId()
        ..formId = formData.id
        ..plannerStageId = plannerStageId
        ..date = df.format(DateTime.now())
        ..taskId = taskId
        ..items = questions;
      savedResponses.put(key, d);
    } else {
      questions.addAll(existingForm!.items!);
    }
    notifyListeners();
  }

  void clear({required String key}) {
    existingForm = null;
    savedResponses.delete(key);
  }

  void setInit(
      {required FormsData formData,
      required String key,
      required String type,
      required int id,
      required int? plannerStageId,
      required int? taskId}) {
    questions.clear();
    for (var element in formData.items!) {
      FormItems formItem = FormItems()
        ..response = ""
        ..type = "default"
        ..formItemId = element.id
        ..question = element.name
        ..responseType = element.typeCode
        ..required = true;
      questions.add(formItem);
    }
    for (var element in formData.customItems!) {
      FormItems formItem = FormItems()
        ..response = ""
        ..type = "custom"
        ..formItemId = element.id
        ..responseType = element.typeCode
        ..question = element.name
        ..required = true;
      questions.add(formItem);
    }
    UpdateFormData d = UpdateFormData()
      ..id = ""
      ..typeId = id
      ..answeredBy = getUserId()
      ..formId = formData.id
      ..date = df.format(DateTime.now())
      ..type = type
      ..plannerStageId = plannerStageId
      ..taskId = taskId
      ..items = questions;

    existingForm = d;
    savedResponses.put(key, d);
    notifyListeners();
  }

  void setSaved(
      {required FormsData formData,
      required String key,
      required String type,
      required int id,
      required int? plannerStageId,
      required int? taskId}) {
    if (existingForm != null) {
      UpdateFormData? d = existingForm!..items = questions;
      savedResponses.put(key, d);
    } else {
      UpdateFormData d = UpdateFormData()
        ..id = ""
        ..typeId = id
        ..answeredBy = getUserId()
        ..formId = formData.id
        ..date = df.format(DateTime.now())
        ..type = type
        ..taskId = taskId
        ..plannerStageId = plannerStageId
        ..items = questions;

      existingForm = d;
      savedResponses.put(key, d);
    }
    notifyListeners();
  }

  void setResponse(
      {required String? formItemId,
      required String? type,
      required String response,
      bool? isFile,
      String? responseName}) {
    for (var element in questions) {
      if (formItemId == element.formItemId && type == element.type) {
        element
          ..response = response
          ..responseName = responseName
          ..isFile = isFile;
        notifyListeners();
      }
    }
  }

  Future<void> uploadFiles(List<FormItems> list, String? responseId) async {
    for(var element in list){
      if((element.response??'').isNotEmpty) {
        File image = File(element.response!);
        await _api.uploadFormResponse(image, {"response_id": responseId, "question_id": element.formItemId});


      }
    }
  }
}


class UploadFormRequest{
  String? formItemId;
  String? itemType;
  String? id;
  String? formId;
  String? answeredBy;
  String? plannerId;
  String? shiftId;
  String? date;
  int? plannerStageId;
  String? type;
  int? typeId;
  int? taskId;

  UploadFormRequest({
      this.formItemId,
      this.itemType,
      this.id,
      this.formId,
      this.answeredBy,
      this.plannerId,
      this.shiftId,
      this.date,
      this.plannerStageId,
      this.type,
      this.typeId,
      this.taskId});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['form_id'] = formId;
    data['answered_by'] = answeredBy;
    data['planner_id'] = plannerId;
    data['shift_id'] = shiftId;
    data['date'] = date;
    data['type'] = type;
    data['planner_stage_id'] = plannerStageId;
    data['type_id'] = typeId;
    data['task_id'] = taskId;
    data['form_item_id'] = formItemId;
    data['type'] = type;
    return data;
  }
}