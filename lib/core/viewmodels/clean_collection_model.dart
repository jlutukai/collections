import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_batch_dry_response.dart';
import '../services/api.dart';
import 'base_model.dart';

class CleanCollectionModel extends BaseModel {
  final Api _api = locator<Api>();
  List<BatchDryData> bags = [];

  Future<Map<String, dynamic>> getBags() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBatchDry();
      if (r['success']) {
        var response = GetBatchDryResponse.fromJson(r["response"]);
        if (response.data != null) {
          bags = response.data!.cast<BatchDryData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
