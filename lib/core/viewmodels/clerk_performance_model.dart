import 'package:either_dart/either.dart';
import 'package:weight_collection/core/viewmodels/base_model.dart';

import '../../locator.dart';
import '../../utils/useful.dart';
import '../enums/view_state.dart';
import '../models/error.dart';
import '../models/get_clerk_performance_resonse.dart';
import '../models/get_unassigned_bags_response.dart';
import '../services/api.dart';

class ClerkPerformanceModel extends BaseModel{
  final Api _api = locator<Api>();


  List<PerformanceData> _performance = [];
  List<PerformanceData> get performance => _performance;


  String _startDate = df.format(DateTime.now());

  String get startDate => _startDate;

  set startDate(String value) {
    _startDate = value;
    notifyListeners();
  }

  String _endDate = df.format(DateTime.now());

  String get endDate => _endDate;

  set endDate(String value) {
    _endDate = value;
    notifyListeners();
  }



  Future<Either<CustomError, GetClerkPerformanceResponse>>  getStaffPerformance(int clerkId) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getStaffPerformance({
        "clerk_id": clerkId,
        'start_date': startDate,
        'end_date': endDate
      });
      if(r.isRight) {
        _performance = r.right.data ?? [];
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}