import 'package:hive/hive.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/get_bags_response.dart';
import 'package:weight_collection/core/models/get_weight_type_response.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/services/api.dart';
import 'package:weight_collection/core/viewmodels/base_model.dart';

import '../../locator.dart';

class CollectionModel extends BaseModel {
  final Api _api = locator<Api>();
  List<WeightTypeData>? weightTypes;
  List<CollectionData>? bags;
  final _collections = Hive.box('weight_collection_data');
  List<CollectionData> collections = [];

  Future<Map<String, dynamic>> getBags() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBags();
      if (r['success']) {
        var response = GetBagResponse.fromJson(r["response"]);
        if (response.data != null) {
          bags = response.data!.cast<CollectionData>();
          if (bags != null) {
            // bags!.map((e) => _collections.put(e.bagId, e));
            for (var element in bags!) {
              _collections.put("${element.bagId} ${element.batchId}", element);
            }
          }
        }
      }
      collections.clear();
      for (var element in _collections.values) {
        collections.add(element);
        notifyListeners();
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getWeightTypes() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getWeightType();
      if (r['success']) {
        var response = GetWeightTypesResponse.fromJson(r["response"]);
        if (response.data != null) {
          weightTypes = response.data!.cast<WeightTypeData>();
          if (weightTypes != null) {
            weightTypes!.sort((a, b) {
              var aName = a.name!.toLowerCase().trim();
              var bName = b.name!.toLowerCase().trim();
              return aName.compareTo(bName);
            });
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> openBatch(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.openBatch(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateBatchItem(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateBatchItem(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateBatchStatus(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateBatchStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
