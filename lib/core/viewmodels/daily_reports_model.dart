import 'dart:io';

import 'package:hive/hive.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/models/planner_report_response.dart';
import 'package:weight_collection/core/models/report_categories_response.dart';
import 'package:weight_collection/core/services/api.dart';
import '../../locator.dart';
import 'base_model.dart';

class DailyReportsModel extends BaseModel {
  final Api _api = locator<Api>();
  List<PlannerReportData>? reportsPrevious;
  List<PlannerReportData>? reportsCurrent;
  Box checkListAnswers = Hive.box("check_list_answer");

  List<CategoryData> categoriesCurrentShift = [];
  List<CategoryData> categoriesPreviousShift = [];

  Future<Map<String, dynamic>> getReports(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getPlannerReport(data);
      if (r['success']) {
        var d = PlannerReportResponse.fromJson(r['response']);
        initData(d.data!);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getCategories(
      Shifts currentShift, Shifts previousShift) async {
    // ReportingCategoriesResponse
    setState(ViewState.Busy);
    try {
      var r = await _api.getCategories();
      if (r['success']) {
        var d = ReportingCategoriesResponse.fromJson(r['response']);
        initializeCategories(d.data!, currentShift, previousShift);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  void initData(List<PlannerReportData> data) {
    for (var element in data) {
      CheckListResult c = CheckListResult(
          responseId: "${element.id}",
          questionId: "${element.questionId}",
          response: "${element.response}",
          date: "${element.date}",
          answeredBy: "${element.answeredBy}",
          defaultCustom: "${element.defaultCustom}",
          shiftId: "${element.shiftId}",
          categoryId: "${element.categoryId}",
          name: "",
          questionType: '',
          fileLocation: '',
          shiftType: "${element.shiftId}",
          synced: true,
          status: "${element.status}");
      checkListAnswers.put(
          "${element.shiftId}/${element.date!.substring(8)}/${element.questionId}/${element.categoryId}",
          c);
    }
  }

  Future<Map<String, dynamic>> updateResponse(Map<String, String> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api.updateResponse(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  void initializeCategories(
      List<CategoryData> data, Shifts currentShift, Shifts previousShift) {
    categoriesCurrentShift.clear();
    categoriesPreviousShift.clear();
    categoriesPreviousShift.addAll(data);
    categoriesCurrentShift.addAll(data);

    for (var element in checkListAnswers.values) {
      if (element.status == "Completed" && element.shiftId == currentShift.id) {
        for (var category in categoriesCurrentShift) {
          CategoryData c = category;
          c.isCompete = true;
          notifyListeners();
        }
      }
      if (element.status == "Completed" &&
          element.shiftId == previousShift.id) {
        for (var category in categoriesPreviousShift) {
          CategoryData c = category;
          c.isCompete = true;
          notifyListeners();
        }
      }
    }
  }
}
