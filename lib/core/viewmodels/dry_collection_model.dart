import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_batch_wet_response.dart';
import '../services/api.dart';
import 'base_model.dart';

class DryCollectionModel extends BaseModel {
  final Api _api = locator<Api>();
  List<WetBatchData> bags = [];

  Future<Map<String, dynamic>> getBags() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBatchWet();
      if (r['success']) {
        var response = GetBatchWetResponse.fromJson(r["response"]);
        if (response.data != null) {
          bags = response.data!.cast<WetBatchData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
