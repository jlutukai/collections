import 'dart:io';

import 'package:either_dart/either.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/models/planner_response.dart';
import 'package:weight_collection/core/services/api.dart';

import '../../locator.dart';
import '../models/error.dart';
import '../models/general_response.dart';
import 'base_model.dart';

class HomeModel extends BaseModel {
  final Api _api = locator<Api>();

  List<PlannerData>? planners;
  String? minDate;
  String? maxDate;

  late Shifts previousShift;
  Shifts? currentShift;
  late Shifts nextShift;

  final accountInfo = Hive.box<String>("accountInfo");

  Future<Map<String, dynamic>> getPlanners(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getPlanners(data);
      if (r['success']) {
        var d = PlannerResponse.fromJson(r['response']);
        planners = d.data;
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  String? getPlannerId() {
    return accountInfo.get("plannerId");
  }

  // ignore: missing_return
  Future<void> initialize(PlannerData element) async {
    /// TODO: Remove this
    if (element.shifts!.length == 1) {
      var i = element.shifts!.first;
      int from = int.tryParse(i.shiftChangeFrom!.substring(0, 2))!;
      int to = int.tryParse(i.shiftChangeTo!.substring(0, 2))!;

      if (from < to) {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute);
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close = DateTime(
            now.year, now.month, now.day, close.hour, close.minute);
        int d = open.difference(close).inHours;
        int duration;
        if (d < 0) {
          duration = d * -1;
        } else {
          duration = d;
        }
        i.duration = duration;
        if (now.isAfter(open) && now.isBefore(close)) {
          if (kDebugMode) {
            print("day from $open to $close");
          }
          currentShift = i;
          previousShift = i;
          nextShift = i;
          final f = DateFormat('yyyy-MM-dd');
          maxDate = f.format(now.add(const Duration(days: 1)));
          minDate = f.format(now.subtract(const Duration(days: 1)));
          notifyListeners();
        } else {
          previousShift = i;
          nextShift = i;
          final f = DateFormat('yyyy-MM-dd');
          maxDate = f.format(now.add(const Duration(days: 1)));
          minDate = f.format(now.subtract(const Duration(days: 1)));
          notifyListeners();
        }
      }
      if (from > to) {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute);
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close =
            DateTime(now.year, now.month, now.day, close.hour, close.minute)
                .add(const Duration(days: 1));
        int d = open.difference(close).inHours;
        int duration;
        if (d < 0) {
          duration = d * -1;
        } else {
          duration = d;
        }
        i.duration = duration;

        if (now.isAfter(open) && now.isBefore(close)) {
          currentShift = i;
          if (kDebugMode) {
            print("night from $open to $close  ${i.id}");
          }
          getPreviousShift(element, open);
          getNextShift(element, close, 1);
          if (kDebugMode) {
            print(close);
          }
        } else {
          getPreviousShift(element, now);
          getNextShift(element, now, 0);
        }
      }
    }
    for (var i in element.shifts!) {
      int from = int.tryParse(i.shiftChangeFrom!.substring(0, 2))!;
      int to = int.tryParse(i.shiftChangeTo!.substring(0, 2))!;

      if (from < to) {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute);
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close = DateTime(
            now.year, now.month, now.day, close.hour, close.minute);
        int d = open.difference(close).inHours;
        int duration;
        if (d < 0) {
          duration = d * -1;
        } else {
          duration = d;
        }
        i.duration = duration;
        if (now.isAfter(open) && now.isBefore(close)) {
          print("day from $open to $close");
          currentShift = i;
          getPreviousShift(element, open);
          getNextShift(element, close, 0);
        } else {
          getPreviousShift(element, now);
          getNextShift(element, now, 0);
        }
      }
      if (from > to) {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute);
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close =
            DateTime(now.year, now.month, now.day, close.hour, close.minute)
                .add(const Duration(days: 1));
        int d = open.difference(close).inHours;
        int duration;
        if (d < 0) {
          duration = d * -1;
        } else {
          duration = d;
        }
        i.duration = duration;

        if (now.isAfter(open) && now.isBefore(close)) {
          currentShift = i;
          print("night from $open to $close  ${i.id}");
          getPreviousShift(element, open);
          getNextShift(element, close, 1);
          print(close);
        } else {
          getPreviousShift(element, now);
          getNextShift(element, now, 0);
        }
      }
    }
  }

  void getNextShift(PlannerData element, DateTime c, int days) {
    for (var i in element.shifts!) {
      if (currentShift != null) {
        if (currentShift!.id != i.id) {
          if (days != 0) {
            DateFormat dateFormat = DateFormat.Hm();
            DateTime now = DateTime.now();
            DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
            open = DateTime(
                    now.year, now.month, now.day, open.hour, open.minute)
                .add(const Duration(days: 1));
            DateTime close = dateFormat.parse("${i.shiftChangeTo}");
            close = DateTime(
                    now.year, now.month, now.day, close.hour, close.minute)
                .add(const Duration(days: 1));
            if (open.isAfter(c) || c == open && currentShift!.id != i.id) {
              // print("######################errccccor $c $open    ##### ${i.id}");
              nextShift = i;
              final f = DateFormat('yyyy-MM-dd');
              maxDate = f.format(c);
            }
          } else {
            DateFormat dateFormat = DateFormat.Hm();
            DateTime now = DateTime.now();
            DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
            open = DateTime(
                now.year, now.month, now.day, open.hour, open.minute);
            DateTime close = dateFormat.parse("${i.shiftChangeTo}");
            close = DateTime(
                now.year, now.month, now.day, close.hour, close.minute);
            if (open.isAfter(c) || c == open && currentShift!.id != i.id) {
              print(
                  "######################errccccor $c $open    ##### ${i.id}");
              nextShift = i;
              final f = DateFormat('yyyy-MM-dd');
              maxDate = f.format(c);
            }
          }
        }
      } else {
        if (days != 0) {
          DateFormat dateFormat = DateFormat.Hm();
          DateTime now = DateTime.now();
          DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
          open =
              DateTime(now.year, now.month, now.day, open.hour, open.minute)
                  .add(const Duration(days: 1));
          DateTime close = dateFormat.parse("${i.shiftChangeTo}");
          close = DateTime(
                  now.year, now.month, now.day, close.hour, close.minute)
              .add(const Duration(days: 1));
          if (open.isAfter(c) || c == open && currentShift!.id != i.id) {
            // print("######################errccccor $c $open    ##### ${i.id}");
            nextShift = i;
            final f = DateFormat('yyyy-MM-dd');
            maxDate = f.format(c);
          }
        } else {
          DateFormat dateFormat = DateFormat.Hm();
          DateTime now = DateTime.now();
          DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
          open = DateTime(
              now.year, now.month, now.day, open.hour, open.minute);
          DateTime close = dateFormat.parse("${i.shiftChangeTo}");
          close = DateTime(
              now.year, now.month, now.day, close.hour, close.minute);
          if (open.isAfter(c) || c == open && currentShift!.id != i.id) {
            print("######################errccccor $c $open    ##### ${i.id}");
            nextShift = i;
            final f = DateFormat('yyyy-MM-dd');
            maxDate = f.format(c);
          }
        }
      }
    }
  }

  void getPreviousShift(PlannerData element, DateTime o) {
    for (var i in element.shifts!) {
      DateFormat dateFormat = DateFormat.Hm();
      DateTime now = DateTime.now();
      DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
      open = DateTime(now.year, now.month, now.day, open.hour, open.minute);
      DateTime close = dateFormat.parse("${i.shiftChangeTo}");
      // DateTime midTime = dateFormat
      //     .parse("${i.shiftChangeTo}")
      //     .add(Duration(hours: i.duration ~/ 2));
      close =
          DateTime(now.year, now.month, now.day, close.hour, close.minute);
      if (currentShift != null) {
        if (currentShift!.id != i.id) {
          if (close.isBefore(o) || o == close) {
            print(
                "######################errccccor $o $close    ##### ${i.id} ${currentShift!.id}");
            previousShift = i;
            notifyListeners();
            final f = DateFormat('yyyy-MM-dd');
            int d = open.difference(close).inHours;
            int duration;
            if (d < 0) {
              duration = d * -1;
            } else {
              duration = d;
            }
            minDate = f.format(o.subtract(Duration(hours: duration)));
          }
        }
      } else {
        if (close.isBefore(o) || o == close) {
          print(
              "######################errccccor $o $close    ##### ${i.id} ${currentShift?.id ?? ''}");
          previousShift = i;
          notifyListeners();
          final f = DateFormat('yyyy-MM-dd');
          int d = open.difference(close).inHours;
          int duration;
          if (d < 0) {
            duration = d * -1;
          } else {
            duration = d;
          }
          minDate = f.format(o.subtract(Duration(hours: duration)));
        }
      }
    }
  }

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError,GeneralResponse>> removeToken(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.removeToken(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
