import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/services/api.dart';

import '../../locator.dart';
import '../models/error.dart';
import '../models/general_response.dart';
import '../models/login_response.dart';
import 'base_model.dart';

class LoginModel extends BaseModel {
  final Api _api = locator<Api>();

  Future<Either<CustomError,LoginResponse>> login(FormData data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.login(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError,GeneralResponse>> updateToken(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateToken(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
