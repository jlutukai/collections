import 'package:hive/hive.dart';
import 'package:weight_collection/core/viewmodels/base_model.dart';

import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_variety_response.dart';
import '../models/local/localModels.dart';
import '../services/api.dart';

class ReadWeightModel extends BaseModel {
  final Api _api = locator<Api>();
  final cachedUpdateDailyWetRequest = Hive.box('batch_request');
  final cachedCleanRequest = Hive.box('clean_request');

  // UpdateBatchRequest? updateDailyWetRequest;
  // CleanWeightRequest? cleanWeightRequest;
  List<VarietyData> varieties = [];
  List<UnMarkedBags> unmarkedBags = [];

  bool hasData = false;

  UpdateBatchRequest? _updateDailyWetRequest;

  UpdateBatchRequest? get updateDailyWetRequest => _updateDailyWetRequest;

  void setUpdateDailyWetRequest(
      {required UpdateBatchRequest request, required String key}) {
    cachedUpdateDailyWetRequest.put(key, request);
    _updateDailyWetRequest = request;
    notifyListeners();
  }

  CleanWeightRequest? _cleanWeightRequest;

  CleanWeightRequest? get cleanWeightRequest => _cleanWeightRequest;

  void setCleanWeightRequest(
      {required CleanWeightRequest request, required String key}) {
    cachedCleanRequest.put(key, request);
    _cleanWeightRequest = request;
    notifyListeners();
  }

  setRequest({required UpdateBatchRequest request, required String key}) {
    setState(ViewState.Busy);
    if (request.items!.isNotEmpty) {
      hasData = true;
      request.total = request.items
          ?.map<double>((m) => double.parse(m.weight!))
          .reduce((a, b) => a + b);
    }
    setUpdateDailyWetRequest(request: request, key: key);
    setState(ViewState.Idle);
  }

  setCleanRequest({required CleanWeightRequest request, required String key}) {
    print(request.toJson());
    hasData = true;
    setCleanWeightRequest(request: request, key: key);
  }

  Future<Map<String, dynamic>> updateDailyWet() async {
    setState(ViewState.Busy);
    try {
      var request = updateDailyWetRequest!..unMarkedBags = unmarkedBags;
      var r = await _api.updateDailyWet(request.toJson());
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateBatchWet() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateBatchWet(updateDailyWetRequest!.toJson());
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateCleanWeight() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateCleanWeight(cleanWeightRequest!.toJson());
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getVarieties() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getVarieties();
      if (r['success']) {
        var response = GetVarietyResponse.fromJson(r["response"]);
        if (response.data != null) {
          varieties = response.data!.cast<VarietyData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  void addUnmarkedBagId(String id) {
    unmarkedBags.add(UnMarkedBags(bagId: id));
  }
}
