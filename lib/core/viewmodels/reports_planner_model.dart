import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/models/planner_response.dart';
import 'package:weight_collection/core/services/api.dart';
import '../../locator.dart';
import 'base_model.dart';

class ReportsPlannerModel extends BaseModel {
  final Api _api = locator<Api>();

  List<PlannerData>? planners;
  String? minDate;
  String? maxDate;

  Shifts? previousShift;
  Shifts? currentShift;
  Shifts? nextShift;

  final accountInfo = Hive.box<String>("accountInfo");

  Future<Map<String, dynamic>> getPlanners(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getPlanners(data);
      if (r['success']) {
        var d = PlannerResponse.fromJson(r['response']);
        planners = d.data;
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  String? getPlannerId() {
    return accountInfo.get("plannerId");
  }

  // ignore: missing_return
  Future<void> initialize(PlannerData element) async {
    /// TODO Remove this
    for (var i in element.shifts!) {
      int from = int.tryParse(i.shiftChangeFrom!.substring(0, 2))!;
      int to = int.tryParse(i.shiftChangeTo!.substring(0, 2))!;

      if (from < to) {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute);
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close = DateTime(
            now.year, now.month, now.day, close.hour, close.minute);
        if (now.isAfter(open) && now.isBefore(close)) {
          currentShift = i;
          print("day from $open to $close");
          getPreviousShift(element, open);
          getNextShift(element, close, 0);
        }
      }
      if (from > to) {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute);
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close =
            DateTime(now.year, now.month, now.day, close.hour, close.minute)
                .add(const Duration(days: 1));

        if (now.isAfter(open) && now.isBefore(close)) {
          currentShift = i;
          // print("night from $open to $close");
          getPreviousShift(element, open);
          getNextShift(element, close, 1);
          if (kDebugMode) {
            print(close);
          }
        }
      }
    }
  }

  void getNextShift(PlannerData element, DateTime c, int days) {
    for (var i in element.shifts!) {
      if (days != 0) {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute)
                .add(const Duration(days: 1));
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close =
            DateTime(now.year, now.month, now.day, close.hour, close.minute)
                .add(const Duration(days: 1));
        if (c == open) {
          nextShift = i;
          final f = DateFormat('yyyy-MM-dd');
          maxDate = f.format(c);
          print("close $open   max date $maxDate ");
        }
      } else {
        DateFormat dateFormat = DateFormat.Hm();
        DateTime now = DateTime.now();
        DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
        open =
            DateTime(now.year, now.month, now.day, open.hour, open.minute);
        DateTime close = dateFormat.parse("${i.shiftChangeTo}");
        close = DateTime(
            now.year, now.month, now.day, close.hour, close.minute);
        if (c == open) {
          nextShift = i;
          final f = DateFormat('yyyy-MM-dd');
          maxDate = f.format(c);
          print("close $open   max date $maxDate ");
        }
      }
    }
  }

  void getPreviousShift(PlannerData element, DateTime o) {
    for (var i in element.shifts!) {
      DateFormat dateFormat = DateFormat.Hm();
      DateTime now = DateTime.now();
      DateTime open = dateFormat.parse("${i.shiftChangeFrom}");
      open = DateTime(now.year, now.month, now.day, open.hour, open.minute);
      DateTime close = dateFormat.parse("${i.shiftChangeTo}");
      close =
          DateTime(now.year, now.month, now.day, close.hour, close.minute);
      if (o == close) {
        if (kDebugMode) {
          print("########################### $close  ################## $o");
        }
        previousShift = i;
        final f = DateFormat('yyyy-MM-dd');
        int d = open.difference(close).inHours;
        int duration;
        if (d < 0) {
          duration = d * -1;
        } else {
          duration = d;
        }
        minDate = f.format(o.subtract(Duration(hours: duration)));
        if (kDebugMode) {
          print("########################### $minDate  ################## ");
        }
      }
    }
  }
}
