import 'package:calendar_view/src/calendar_event_data.dart';
import 'package:either_dart/either.dart';
import 'package:weight_collection/core/viewmodels/base_model.dart';

import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/error.dart';
import '../models/general_response.dart';
import '../models/get_forms_response.dart';
import '../models/get_scan_greenhouse_response.dart';
import '../models/get_task_appraisal.dart';
import '../models/scan_greenhouse_response.dart';
import '../services/api.dart';
import 'package:collection/collection.dart';


class ScanGreenHouseModel extends BaseModel {
  final Api _api = locator<Api>();
  List<FormsData> forms = [];

  GreenHouseData? _scanResult;
  GreenHouseData? get scanResult => _scanResult;

  List<AppraisalData> _tasks = [];

  List<AppraisalData> get tasks => _tasks;

  final List<EventData> _events = [];

  List<EventData> get events => _events;

  DateTime? _selectedDate;

  DateTime? get selectedDate => _selectedDate;

  set selectedDate(DateTime? value) {
    _selectedDate = value;
    notifyListeners();
  }

  String? _searchKey;
  String? get searchKey => _searchKey;

  set searchKey(String? value) {
    _searchKey = value;
    _onSearchTasks();
    notifyListeners();
  }

  Future<Either<CustomError, GetGreenHouseData>> scanGreenHouse(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      setState(ViewState.Busy);
      var r = await _api.scanGreenHouse(data);
      if(r.isRight){
        if((r.right.data??[]).isNotEmpty){
          _scanResult = r.right.data?.first;
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateMaintenance(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateMaintenance(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError, GetTaskAppraisal>> getTasks(
      Map<String, String> data) async {
    setState(ViewState.Busy);
    var r = await _api.getTasks2(data);
    if (r.isRight) {
      _tasks = r.right.data ?? [];
      _tasks.sort((a, b) {
        var aName = a.deadline ?? "";
        var bName = b.deadline ?? "";
        return aName.compareTo(bName);
      });
    }
    setState(ViewState.Idle);
    return r;
  }

  void addEvent(CalendarEventData<Object?> event) {
    _events.clear();
    try {
      if (event.event is AppraisalData) {
        AppraisalData task = event.event as AppraisalData;
        _events.add(EventData(
            title: event.title,
            description: event.description,
            taskId: task.id,
            formId: task.formId));
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
    notifyListeners();
  }

  void addEvents(List<CalendarEventData<Object?>> cEvents) {
    _events.clear();
    try {
      for (var event in cEvents) {
        if (event.event is AppraisalData) {
          AppraisalData task = event.event as AppraisalData;
          _events.add(EventData(
              title: event.title,
              description: event.description,
              taskId: task.id,
              formId: task.formId));
        }
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
    notifyListeners();
  }

  void _onSearchTasks() {
    var keyword = (searchKey??"");
    if(keyword.length<3){
      _events.clear();
      notifyListeners();
      return;
    }
    _events.clear();
    _events.addAll(tasks
        .where(
            (element) => (element.title ?? "").toLowerCase().contains(keyword))
        .map((e) => EventData(
            title: e.title,
            description: e.taskDescription,
            taskId: e.id,
            formId: e.formId)));
    notifyListeners();
  }


  Future<Map<String, dynamic>> getForms() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getForms();
      if (r['success']) {
        var d = GetFormsResponse.fromJson(r['response']);
        if (d.data != null) {
          forms = d.data!;
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<FormsData?> getFormData(int? formId) async {
    return forms.firstWhereOrNull(
            (element) => element.id == formId.toString());
  }

  Future<Either<CustomError, GeneralResponse>> closeTask(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
      var r = await _api.updateFormTaskResponse(data);
      setState(ViewState.Idle);
      return r;
  }

  Future<Either<CustomError, GeneralResponse>> delayTask(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
      var r = await _api.delayTask(data);
      setState(ViewState.Idle);
      return r;
  }

  setEvents(DateTime date) {
    print("${date.year}-${date.month}-${date.day}");
    _events.clear();
    _events.addAll(tasks
        .where(
            (element) => element.deadline == "${date.year}-${date.month}-${date.day}")
        .map((e) => EventData(
        title: e.title,
        description: e.taskDescription,
        taskId: e.id,
        formId: e.formId)));
    notifyListeners();
  }
}
