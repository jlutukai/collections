import 'package:weight_collection/core/viewmodels/scan_type_bed.dart';

import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_bed_line_items_response.dart';
import '../models/scan_bag_response.dart';
import '../models/scan_bed_response.dart';
import '../services/api.dart';
import 'base_model.dart';

class ScanTypeBagModel extends BaseModel {
  final Api _api = locator<Api>();
  List<ScanBagData> scanResult = [];
  List<ScanBedData> scanBedResult = [];
  List<StaffDetails> staffDetails = [];
  List<StaffDetails> pollinatedStaff = [];
  List<LineData> lineData = [];
  AssociatedData? associatedData;

  Future<Map<String, dynamic>> getScanBag(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScanBag(data);
      if (r['success']) {
        var response = ScanBagResponse.fromJson(r["response"]);
        if (response.data != null) {
          scanResult = response.data!.cast<ScanBagData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getScanBed(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScanBed(data);
      if (r['success']) {
        var response = ScanBedResponse.fromJson(r["response"]);
        if (response.data != null) {
          scanBedResult = response.data!.cast<ScanBedData>();
          if (scanBedResult.isNotEmpty) {
            staffDetails = scanBedResult.first.staffDetails??[];
            pollinatedStaff = scanBedResult.first.staffPollinatedDetails??[];
            associatedData = AssociatedData(
                name:
                    "${scanBedResult.first.bedName} - ${scanBedResult.first.code}");
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getLineItems(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBedLineItems(data);
      if (r['success']) {
        var response = GetBedLineItemsResponse.fromJson(r["response"]);
        if (response.data != null) {
          lineData = response.data!.cast<LineData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> assignBagToBed(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.assignBagToBed(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateBagWeighment(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateBagWeighment(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> clearBag(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.clearBag(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
