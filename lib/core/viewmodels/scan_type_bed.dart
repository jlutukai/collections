import 'package:either_dart/either.dart';
import 'package:weight_collection/core/models/complete_stage_response.dart';
import 'package:weight_collection/core/models/error.dart';
import 'package:weight_collection/core/models/get_available_lines.dart';
import 'package:weight_collection/core/models/get_plants_in_suspense_response.dart';

import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_bed_line_items_response.dart';
import '../models/get_loss_reason_response.dart';
import '../models/scan_bag_response.dart';
import '../models/scan_bed_response.dart';
import '../models/scan_staff_response.dart';
import '../services/api.dart';
import 'base_model.dart';

class ScanTypeBedModel extends BaseModel {
  final Api _api = locator<Api>();
  List<ScanBedData> scanResult = [];
  List<SectionDetails> sectionDetails = [];
  List<StaffScanData> scanStaffResult = [];
  List<ScanBagData> scanBagResult = [];
  List<LineData> lineData = [];
  List<ReasonData> reasons = [];
  List<ReasonData> scoutingReasons = [];
  List<PlantData> plants = [];
  List<PlantData> availablePlants = [];
  List<AvailableLinesData> availableLines = [];
  AssociatedData? associatedData;

  Future<Map<String, dynamic>> getScanBed(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScanBed(data);
      if (r['success']) {
        var response = ScanBedResponse.fromJson(r["response"]);
        if (response.data != null) {
          scanResult = response.data!.cast<ScanBedData>();
          if(scanResult.isNotEmpty) {
            sectionDetails = scanResult.first.sectionDetails ?? [];
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getLineItems(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBedLineItems(data);
      if (r['success']) {
        var response = GetBedLineItemsResponse.fromJson(r["response"]);
        if (response.data != null) {
          lineData = response.data!.cast<LineData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getLossReasons() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getLossReasons();
      if (r['success']) {
        var response = GetLossReasonResponse.fromJson(r["response"]);
        if (response.data != null) {
          reasons = response.data!.cast<ReasonData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getScoutingReasons() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScoutingReasons();
      if (r['success']) {
        var response = GetLossReasonResponse.fromJson(r["response"]);
        if (response.data != null) {
          scoutingReasons = response.data!.cast<ReasonData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> assignBagToBed(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.assignBagToBed(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> assignStaffToBed(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.assignStaffToBed(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> clearBed(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.clearBed(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getScanStaff(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScanStaff(data);
      if (r['success']) {
        var response = ScanStaffResponse.fromJson(r["response"]);
        if (response.data != null) {
          scanStaffResult = response.data!.cast<StaffScanData>();
          if (scanStaffResult.isNotEmpty) {
            associatedData = AssociatedData(name: scanStaffResult.first.name);
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getScanBag(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScanBag(data);
      if (r['success']) {
        var response = ScanBagResponse.fromJson(r["response"]);
        if (response.data != null) {
          scanBagResult = response.data!.cast<ScanBagData>();
          if (scanBagResult.isNotEmpty) {
            associatedData = AssociatedData(
                name: scanBagResult.first.bagId, id: scanBagResult.first.id);
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateLoss(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateLoss(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateScouting(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateScouting(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> movePlant(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.movePlant(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getPlantsInSuspense() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getPlantsInSuspense();
      if (r['success']) {
        var response = GetPlantsInSuspenseResponse.fromJson(r["response"]);
        if (response.data != null) {
          plants = response.data!.cast<PlantData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> receivePlant(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.receivePlant(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> addPlant(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.addPlant(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateEndDate(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateEndDate(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> updateStatus(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateStageStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError, CompleteStageResponse>> startStage(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.startStage(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError, List<AvailableLinesData>>>
      getAvailableLines() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getAvailableLines();
      if (r.isRight) {
        availableLines = r.right.data ?? [];
      }
      setState(ViewState.Idle);
      return r.map((right) => right.data ?? []);
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getAvailablePlants() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getAvailablePlants();
      if (r['success']) {
        var response = GetPlantsInSuspenseResponse.fromJson(r["response"]);
        if (response.data != null) {
          availablePlants = response.data!.cast<PlantData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }


  Future<Map<String, dynamic>> clearStaff(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.clearStaffItem(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  addSection(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.addSection(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
  removeSection(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.removeSection(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}

class AssociatedData {
  String? id;
  String? name;

  AssociatedData({this.id, this.name});
}
