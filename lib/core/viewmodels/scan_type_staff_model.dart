import 'package:weight_collection/core/models/scan_staff_response.dart';
import 'package:weight_collection/core/viewmodels/base_model.dart';
import 'package:weight_collection/core/viewmodels/scan_type_bed.dart';

import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_bed_line_items_response.dart';
import '../models/scan_bed_response.dart';
import '../services/api.dart';

class ScanTypeStaffModel extends BaseModel {
  final Api _api = locator<Api>();
  List<StaffScanData> scanResult = [];
  List<ScanBedData> scanBedResult = [];
  List<LineData> lineData = [];
  AssociatedData? associatedData;

  Future<Map<String, dynamic>> getScanStaff(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScanStaff(data);
      if (r['success']) {
        var response = ScanStaffResponse.fromJson(r["response"]);
        if (response.data != null) {
          scanResult = response.data!.cast<StaffScanData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getLineItems(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBedLineItems(data);
      if (r['success']) {
        var response = GetBedLineItemsResponse.fromJson(r["response"]);
        if (response.data != null) {
          lineData = response.data!.cast<LineData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getScanBed(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getScanBed(data);
      if (r['success']) {
        var response = ScanBedResponse.fromJson(r["response"]);
        if (response.data != null) {
          scanBedResult = response.data!.cast<ScanBedData>();
          if (scanBedResult.isNotEmpty) {
            associatedData = AssociatedData(
                name:
                    "${scanBedResult.first.bedName} - ${scanBedResult.first.code}");
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> assignStaffToBed(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.assignStaffToBed(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> reAssignBed(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.reAssignBed(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> clearStaff(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.clearStaff(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  updateStaffReport(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateStaffReport(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
