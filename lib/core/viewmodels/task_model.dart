


import 'package:either_dart/either.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/get_function_response.dart';
import 'package:weight_collection/core/models/get_staff_task_response.dart';
import 'package:weight_collection/core/models/get_task_comments_reponse.dart';
import 'package:weight_collection/core/models/get_tasks_response.dart';
import 'package:weight_collection/core/models/get_tasks_types_response.dart';
import 'package:weight_collection/core/services/api.dart';

import '../../locator.dart';
import '../models/error.dart';
import 'base_model.dart';

class TaskModel extends BaseModel{
  final Api _api = locator<Api>();
   List<FunctionData>? functions;
   List<StaffTask>? staff;
   List<TaskType>? taskTypes;
   List<TaskData>? tasks;
  List<CommentData?>? comments;

  Future<Map<String, dynamic>> getFunctions() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getFunctions();
      if(r['success']){
        var response = GetFunctionResponse.fromJson(r["response"]);
        if(response.data!=null){
          functions = response.data!.cast<FunctionData>();
          if(functions!=null){
            functions!.sort((a, b) {
              var aName = a.jobTitle!.toLowerCase().trim();
              var bName = b.jobTitle!.toLowerCase().trim();
              return aName.compareTo(bName);
            });
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getStaff() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getStaff();
      if(r['success']){
        var response = GetStaffTaskResponse.fromJson(r["response"]);
        if(response.data!=null){
          staff = response.data!.cast<StaffTask>();
          if(staff!=null){
            staff!.sort((a, b) {
              var aName = a.name!.toLowerCase().trim();
              var bName = b.name!.toLowerCase().trim();
              return aName.compareTo(bName);
            });
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getTaskTypes() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getTaskTypes();
      if(r['success']){
        var response = GetTaskTypeResponse.fromJson(r["response"]);
        if(response.data!=null){
          taskTypes = response.data!.cast<TaskType>();
          if(taskTypes!=null){
            taskTypes!.sort((a, b) {
              var aName = a.name!.toLowerCase().trim();
              var bName = b.name!.toLowerCase().trim();
              return aName.compareTo(bName);
            });
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> createTask(Map<String, dynamic> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api.createTask(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Either<CustomError, GetTasksResponse>> getTasks(
      Map<String, String> data) async {
    setState(ViewState.Busy);
    var r = await _api.getTasks(data);
    if (r.isRight) {
      tasks = r.right.data ?? [];
      tasks!.sort((a, b) {
        var aName = a.deadline??"";
        var bName = b.deadline??"";
        return bName.compareTo(aName);
      });
    }
    setState(ViewState.Idle);
    return r;
  }


  Future<Map<String, dynamic>> updateTaskProgress(Map<String, String> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api.updateTaskProgress(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> createTaskComment(Map<String, String> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api.createTaskComment(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getTaskComment(Map<String, String> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api.getTaskComment(data);
      if(r['success']){
        var response = GetTaskCommentsResponse.fromJson(r["response"]);
        if(response.data!=null){
          comments = response.data!.cast<CommentData>();
          comments!.sort((a, b) {
            var aName = a!.date??"";
            var bName = b!.date??"";
            return bName.compareTo(aName);
          });
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }


  Future<Map<String, dynamic>> editTaskDate(Map<String, String> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api.editTaskDate(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }

}