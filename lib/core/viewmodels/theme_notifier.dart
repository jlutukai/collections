import 'package:flutter/material.dart';
import 'package:weight_collection/utils/useful.dart';

class ThemeNotifier with ChangeNotifier {
  bool _darkTheme = getTheme();

  bool get darkTheme => _darkTheme;

  set darkTheme(bool value) {
    _darkTheme = value;
    setTheme(value);
    notifyListeners();
  }
}
