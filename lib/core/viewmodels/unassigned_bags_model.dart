import 'package:either_dart/either.dart';
import 'package:weight_collection/core/models/get_unassigned_bags_response.dart';
import 'package:weight_collection/core/viewmodels/base_model.dart';

import '../../locator.dart';
import '../../utils/useful.dart';
import '../enums/view_state.dart';
import '../models/error.dart';
import '../services/api.dart';

class UnassignedBagsModel extends BaseModel{
  final Api _api = locator<Api>();


  List<UnassignedBagData> _unAssignedBags = [];
  List<UnassignedBagData> get unAssignedBags => _unAssignedBags;

  String _startDate = df.format(DateTime.now().subtract(Duration(days: 2)));

  String get startDate => _startDate;

  set startDate(String value) {
    _startDate = value;
    notifyListeners();
  }

  String _endDate = df.format(DateTime.now());

  String get endDate => _endDate;

  set endDate(String value) {
    _endDate = value;
    notifyListeners();
  }


  Future<Either<CustomError, GetUnassignedBagsResponse>>  getUnassignedBags(int bedId) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getUnassignedBags({
        "bed_id": bedId,
        'start_date': startDate,
        'end_date': endDate
      });
      if(r.isRight) {
        _unAssignedBags = r.right.data ?? [];
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
  
}