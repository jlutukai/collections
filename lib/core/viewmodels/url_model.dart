import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/scio_url_response.dart';
import 'package:weight_collection/core/services/api.dart';


import '../../locator.dart';
import '../models/error.dart';
import 'base_model.dart';

class UrlModel extends BaseModel {
  final Api _api = locator<Api>();

  Future<Either<CustomError, ScioUrlResponse>> veryUrl(
      FormData data,
      ) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.verifyUrl(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
