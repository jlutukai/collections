import 'package:weight_collection/core/viewmodels/base_model.dart';

import '../../locator.dart';
import '../enums/view_state.dart';
import '../models/get_daily_wet_response.dart';
import '../services/api.dart';

class WetCollectionModel extends BaseModel {
  final Api _api = locator<Api>();
  List<DailyWetData> bags = [];

  Future<Map<String, dynamic>> getBags() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getDailyWet();
      if (r['success']) {
        var response = GetDailyWetResponse.fromJson(r["response"]);
        if (response.data != null) {
          bags = response.data!.cast<DailyWetData>();
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      rethrow;
    }
  }
}
