import 'package:get_it/get_it.dart';
import 'package:weight_collection/core/viewmodels/unassigned_bags_model.dart';
import 'package:weight_collection/core/viewmodels/collection_model.dart';
import 'package:weight_collection/core/viewmodels/scan_greenhouse_model.dart';
import 'package:weight_collection/core/viewmodels/task_model.dart';

import 'core/services/NavigationService.dart';
import 'core/services/api.dart';
import 'core/services/push_notifications_service.dart';
import 'core/viewmodels/CompleteStageModel.dart';
import 'core/viewmodels/ReportFormModel.dart';
import 'core/viewmodels/clean_collection_model.dart';
import 'core/viewmodels/clerk_performance_model.dart';
import 'core/viewmodels/daily_reports_model.dart';
import 'core/viewmodels/dry_collection_model.dart';
import 'core/viewmodels/home_model.dart';
import 'core/viewmodels/login_model.dart';
import 'core/viewmodels/read_weight_model.dart';
import 'core/viewmodels/report_questions_model.dart';
import 'core/viewmodels/reports_planner_model.dart';
import 'core/viewmodels/scan_type_bag_model.dart';
import 'core/viewmodels/scan_type_bed.dart';
import 'core/viewmodels/scan_type_staff_model.dart';
import 'core/viewmodels/url_model.dart';
import 'core/viewmodels/wet_collection_model.dart';

GetIt locator = GetIt.instance;

void setUpLocator() {
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => PushNotificationService());

  locator.registerFactory(() => UrlModel());
  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => ReportsPlannerModel());
  locator.registerFactory(() => DailyReportsModel());
  locator.registerFactory(() => ReportsQuestionsModel());
  locator.registerFactory(() => HomeModel());
  locator.registerFactory(() => ReportFormsModel());
  locator.registerFactory(() => TaskModel());
  locator.registerFactory(() => CollectionModel());
  locator.registerFactory(() => ScanTypeStaffModel());
  locator.registerFactory(() => ScanTypeBedModel());
  locator.registerFactory(() => ScanTypeBagModel());
  locator.registerFactory(() => ScanGreenHouseModel());
  locator.registerFactory(() => WetCollectionModel());
  locator.registerFactory(() => ReadWeightModel());
  locator.registerFactory(() => DryCollectionModel());
  locator.registerFactory(() => CleanCollectionModel());
  locator.registerFactory(() => CompleteStageModel());
  locator.registerFactory(() => UnassignedBagsModel());
  locator.registerFactory(() => ClerkPerformanceModel());
}
