import 'dart:io';

// import 'package:firebase_core/firebase_core.dart';
import 'package:calendar_view/calendar_view.dart';
import 'package:chucker_flutter/chucker_flutter.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:weight_collection/core/viewmodels/theme_notifier.dart';
import 'package:weight_collection/ui/collections/collections_page.dart';
import 'package:weight_collection/ui/home_page.dart';
import 'package:weight_collection/ui/login_page.dart';
import 'package:weight_collection/ui/scanManagement/scan_bed/un_assigned_bags.dart';
import 'package:weight_collection/ui/scanManagement/scan_management.dart';
import 'package:weight_collection/ui/scanManagement/scan_staff/weight_collection_distribution.dart';
import 'package:weight_collection/ui/splash.dart';
import 'package:weight_collection/utils/QrScanner.dart';
import 'core/models/local/localModels.dart';
import 'core/models/scan_bag_qr_body.dart';
import 'firebase_options.dart';
import 'package:path_provider/path_provider.dart';

import 'locator.dart';
import 'ui/url_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseMessaging.onBackgroundMessage(_messageHandler);
  Directory document = await getApplicationDocumentsDirectory();
  Hive
    ..init(document.path)
    // ..registerAdapter(TimeSheetDataAdapter())
    // ..registerAdapter(CheckListResultAdapter())
    // ..registerAdapter(TimeSheetDetailsAdapter())
    // ..registerAdapter(VisitorAdapter())
    // ..registerAdapter(ItemObjAdapter())
    // ..registerAdapter(PlannerDataAdapter())
    // ..registerAdapter(DeviceDetailsAdapter())
    ..registerAdapter(UpdateFormDataAdapter())
    ..registerAdapter(FormItemsAdapter())
    ..registerAdapter(CreateTaskDataAdapter())
    ..registerAdapter(AssignedToAdapter())
    ..registerAdapter(SubTaskAdapter())
    ..registerAdapter(CollectionDataAdapter())
    ..registerAdapter(UpdateBatchRequestAdapter())
    ..registerAdapter(BagItemsAdapter())
    ..registerAdapter(UnMarkedBagsAdapter())
    ..registerAdapter(CleanWeightRequestAdapter())
    ..registerAdapter(ItemBagsAdapter());
  // ..registerAdapter(ShiftsAdapter());
  await Hive.openBox<String>("accountInfo");
  await Hive.openBox('form_response');
  await Hive.openBox('form_response_questions');
  await Hive.openBox('batch_request');
  await Hive.openBox('clean_request');
  await Hive.openBox('created_task_data');
  await Hive.openBox('collection_data');
  await Hive.openBox('weight_collection_data');
  setUpLocator();
  runApp(const MyApp());
  FlutterError.onError = (errorDetails) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
  };
  // Pass all uncaught asynchronous errors that aren't handled by the Flutter framework to Crashlytics
  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ThemeNotifier themeChangeProvider = ThemeNotifier();

  @override
  void initState() {
    super.initState();
    getCurrentAppTheme();
  }

  void getCurrentAppTheme() async {
    themeChangeProvider.darkTheme = themeChangeProvider.darkTheme;
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ThemeNotifier()),
      ],
      child: CalendarControllerProvider(
        controller: EventController(),
        child: MaterialApp(
          navigatorObservers: [ChuckerFlutter.navigatorObserver],
          debugShowCheckedModeBanner: false,
          title: 'Scio',
          theme: ThemeData(
            primaryColor: Colors.black,
            brightness: Brightness.light,
          ),
          darkTheme: ThemeData(
            primaryColor: Colors.black,
            brightness: Brightness.dark,
          ),
          themeMode: ThemeMode.dark,
          routes: {
            '/': (context) => const SplashScreen(),
            UrlPage.tag: (context) => const UrlPage(),
            LoginPage.tag: (context) => const LoginPage(),
            HomePage.tag: (context) => const HomePage(),
            ScanManagement.tag: (context) {
              var results = ModalRoute.of(context)!.settings.arguments as ScanBagQRBody;
              return ScanManagement(result: results,);
            },
            QRScanner.tag: (context) => const QRScanner(),
            CollectionsPage.tag: (context) => const CollectionsPage(),
            UnAssignedBagsPage.tag: (context) {
              var bedId = ModalRoute.of(context)!.settings.arguments as int;
              return UnAssignedBagsPage(bedId: bedId);
            },
            StaffCollectionPage.tag: (context) {
              var params = ModalRoute.of(context)!.settings.arguments as StaffPerformanceParams;
              return StaffCollectionPage(staffParams: params);
            }
          },
        ),
      ),
    );
  }
}

Future<void> _messageHandler(RemoteMessage message) async {
  if (kDebugMode) {
    print('background message ${message.notification?.body}');
  }
}

