import 'dart:async';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:path_provider/path_provider.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';
import 'package:weight_collection/core/enums/form_response_types.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/get_species_response.dart';
import 'package:weight_collection/core/models/get_staff_response.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/viewmodels/ReportFormModel.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';
import 'package:weight_collection/utils/handle_error.dart';
import 'package:weight_collection/utils/useful.dart';

import '../core/models/get_forms_response.dart';
import '../core/models/get_variety_response.dart';
import '../utils/loader.dart';
import 'base_view.dart';

class FormQuestions extends StatefulWidget {
  final FormsData formData;
  final int id;
  final int? plannerStageId;
  final int? taskId;
  final String type;

  const FormQuestions(
      {super.key,
      required this.formData,
      required this.id,
      required this.type,
        this.plannerStageId,
        this.taskId});

  @override
  _FormQuestionsResponseState createState() => _FormQuestionsResponseState();
}

class _FormQuestionsResponseState extends State<FormQuestions> {
  late ReportFormsModel _model;

  // List<FormItems> questions = [];
  //
  // Box offlineResponses = Hive.box("form_response");
  // Box savedResponses = Hive.box("form_response_questions");
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _picker = ImagePicker();
  final StopWatchTimer _stopWatchTimer = StopWatchTimer();

  @override
  void dispose() async {
    super.dispose();
    await _stopWatchTimer.dispose();
  }

  @override
  void initState() {
    _stopWatchTimer.rawTime.listen((value) {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportFormsModel>(
        onModelReady: (model) async {
          _model = model;
          bool isConnected = await checkConnection();
          if (!isConnected) {
            showToast('No connection, Check Internet Connection!');
          }
          model.initQuestions(
              key: getKey(),
              type: widget.type,
              formData: widget.formData,
              plannerStageId: widget.plannerStageId,
              taskId: widget.taskId,
              id: widget.id);
          if (model.offlineResponses.values.isNotEmpty) {
            sync(model);
          }
        },
        builder: (context, model, child) => Stack(
              children: [
                Scaffold(
                  key: _scaffoldKey,
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Forms',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                    actions: [
                      model.offlineResponses.values.isNotEmpty
                          ? IconButton(
                              onPressed: () async {
                                bool isConnected = await checkConnection();
                                if (!isConnected) {
                                  showToast(
                                      'No connection, Check Internet Connection!');
                                  return;
                                }
                                sync(model);
                              },
                              icon: const Icon(
                                Icons.sync_rounded,
                                color: Colors.white,
                              ))
                          : Container(),
                      model.existingForm != null
                          ? Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      model.clear(
                                          key:getKey());

                                      model.setInit(
                                          formData: widget.formData,
                                          key:getKey(),
                                          type: widget.type,
                                          id: widget.id,
                                          taskId: widget.taskId,
                                          plannerStageId:
                                              widget.plannerStageId);
                                    },
                                    child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 5),
                                        decoration: BoxDecoration(
                                            color: Colors.white10
                                                .withOpacity(0.07),
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(16))),
                                        child: const Text(
                                          'Clear',
                                          style: TextStyle(
                                              color: Colors.deepOrange),
                                        )),
                                  ),
                                ],
                              ),
                            )
                          : Container()
                    ],
                  ),
                  body: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: model.state == ViewState.Idle
                            ? _questionsList(model, context)
                            : Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(
                                        height: 200,
                                        width: 200,
                                        child: Lottie.asset(
                                            'assets/loading.json')),
                                    Text(
                                      'Please wait \n Loading...',
                                      style:
                                          TextStyle(color: fromHex(deepOrange)),
                                    )
                                  ],
                                ),
                              ),
                      ),
                      model.state == ViewState.Idle
                          ? Dismissible(
                              key: UniqueKey(),
                              direction: DismissDirection.startToEnd,
                              onDismissed: (DismissDirection direction) async {
                                bool hasConnection = await checkConnection();
                                bool filled =
                                    await (checkIfAllHaveBeenAnswered(model));
                                setState(() {});
                                if (!filled) {
                                  showToast("Provide all responses");
                                  return;
                                }

                                UpdateFormData data = UpdateFormData()
                                  ..id = ""
                                  ..type = widget.type
                                  ..typeId = widget.id
                                  ..answeredBy = getUserId()
                                  ..formId = widget.formData.id
                                  ..plannerStageId = widget.plannerStageId
                                  ..taskId = widget.taskId
                                  ..date = df.format(DateTime.now())
                                  ..items = model.questions.where((element) =>
                                    element.isFile == false || element.isFile == null
                                  ).toList();

                                if (hasConnection) {
                                  try {
                                    var r = await model
                                        .updateResponse(data);
                                    if (r.isRight) {
                                      if(r.right.error == false) {
                                        var responseId = r.right.id;
                                       await model.uploadFiles(model.questions.where((q) => q.isFile == true).toList(), responseId);
                                        showToast("Success");

                                        model.clear(
                                            key: getKey());
                                        model.setInit(
                                            formData: widget.formData,
                                            key: getKey(),
                                            type: widget.type,
                                            id: widget.id,
                                            taskId: widget.taskId,
                                            plannerStageId:
                                            widget.plannerStageId);
                                        if(data.taskId!=null) {
                                          Navigator.of(context).pop();
                                        }
                                      }else{
                                        showToast(r.right.erroMessage??"Failed to send response");
                                      }
                                    }
                                  } catch (e) {
                                    showToast(e.toString());
                                  }
                                } else {
                                  model.offlineResponses.add(data);
                                  showToast("Success");
                                  setState(() {
                                    model.clear(
                                        key:getKey());
                                  });
                                  model.setInit(
                                      formData: widget.formData,
                                      key:getKey(),
                                      type: widget.type,
                                      id: widget.id,
                                      taskId: widget.taskId,
                                      plannerStageId: widget.plannerStageId);
                                  if(data.taskId!=null) {
                                    Navigator.of(context).pop();
                                  }
                                }
                                setState(() {});
                              },
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 15),
                                decoration: BoxDecoration(
                                  color: Colors.white10.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(
                                      Icons.done,
                                      size: 36,
                                      color: fromHex(yellow),
                                    ),
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Text(
                                        'Submit Response',
                                        style: TextStyle(
                                          color: fromHex(yellow),
                                          fontWeight: FontWeight.w100,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_sharp,
                                      color: fromHex(yellow),
                                    )
                                  ],
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            ));
  }

  _questionsList(ReportFormsModel model, BuildContext context) {
    return model.questions.isNotEmpty
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemCount: model.questions.length,
            itemBuilder: (context, index) => Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Dismissible(
                  key: UniqueKey(),
                  direction: DismissDirection.startToEnd,
                  onDismissed: (DismissDirection direction) async {
                    _inputDialog(context, model.questions[index], model);
                    setState(() {});
                  },
                  child: InkWell(
                    onTap: () async {
                      _inputDialog(context, model.questions[index], model);
                      setState(() {});
                    },
                    child: Container(
                      color: Colors.white10.withOpacity(0.07),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 3),
                      child: Row(
                        children: [
                          Container(
                            color: model.questions[index].response!.isNotEmpty
                                ? Colors.green
                                : Colors.yellow,
                            width: 2,
                            height: 75,
                          ),
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              child: ExpansionTile(
                                leading: Text(
                                  "${index + 1}",
                                  style: TextStyle(color: fromHex(yellow)),
                                ),
                                title: Text(
                                  "${model.questions[index].question}",
                                  style: const TextStyle(color: Colors.white),
                                ),
                                children: [
                                  Text(
                                    (model.questions[index].responseName ??
                                        (model.questions[index].response ??
                                            "")),
                                    style: TextStyle(color: fromHex(yellow)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )),
            ),
          )
        : const Center(
            child: Text('Questions Not added yet'),
          );

  }

  void _inputDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    if (kDebugMode) {
      print("${checklist.responseType}");
    }
    if (checklist.responseType == null) {
      showToast("Question type is undefined");
      return;
    }
    if (checklist.responseType!.isEmpty) {
      showToast("Question type is empty");
      return;
    }
    if (checklist.responseType == FormResponseTypes.YES_NO_RESPONSE.name) {
      showYesNoDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.OPEN_TEXT_RESPONSE.name) {
      showOpenTextDialog(context, checklist, model);
    } else if (checklist.responseType == FormResponseTypes.NUMBER.name) {
      // Number
      showNumberResponseDialog(context, checklist, model);
    } else if (checklist.responseType == FormResponseTypes.RECORDING.name) {
      // Video
      showRecordingResponseDialog(context, checklist, model);
    } else if (checklist.responseType == FormResponseTypes.PICTURE.name) {
      //Picture
      showPictureResponseDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.SPECIES_DROPDOWN.name) {
      showSpeciesResponseDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.STAFF_DROPDOWN.name) {
      showStaffResponseDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.VARIETY_DROPDOWN.name) {
      showVarietyResponseDialog(context, checklist, model);
    } else {
      showToast("Unsupported response type");
    }

    // if (checklist.responseType!.contains("Audio")) {
    //   //Audio
    //   File? _audio;
    //   await showDialog(
    //       context: context,
    //       builder: (context) => Dialog(
    //           shape: RoundedRectangleBorder(
    //             borderRadius: BorderRadius.circular(20),
    //           ),
    //           elevation: 0.0,
    //           backgroundColor: Colors.grey[900],
    //           child: Container(
    //             color: Colors.transparent,
    //             padding:
    //                 const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
    //             child: Column(
    //               mainAxisSize: MainAxisSize.min,
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 ListTile(
    //                   title: Text(
    //                     "${checklist.question}",
    //                     style: const TextStyle(
    //                         fontWeight: FontWeight.bold, color: Colors.white),
    //                   ),
    //                 ),
    //                 Container(),
    //                 Padding(
    //                   padding: const EdgeInsets.symmetric(horizontal: 40),
    //                   child: ListTile(
    //                     onTap: () async {
    //                       // bool hasPermissions =
    //                       // await AudioRecorder.hasPermissions;
    //                       // if (!hasPermissions) {
    //                       //   Permission.microphone.request();
    //                       //   showToast("Allow Audio permissions to proceed!");
    //                       //   return;
    //                       // }
    //                       _audio = await getAudioRecording(context);
    //                       if (_audio == null) {
    //                         showToast("File not found try again!");
    //                         return;
    //                       }
    //                       for (var element in questions) {
    //                         if (checklist.formItemId == element.formItemId &&
    //                             checklist.type == element.type) {
    //                           setState(() {
    //                             element.response = _audio!.path;
    //                             element.isFile = true;
    //                           });
    //                         }
    //                       }
    //                       setSaved();
    //                       Navigator.of(context).pop();
    //                     },
    //                     leading: Icon(
    //                       Icons.settings_voice_outlined,
    //                       color: fromHex(yellow),
    //                     ),
    //                     title: Text(
    //                       'Start Recording',
    //                       style: TextStyle(color: fromHex(yellow)),
    //                     ),
    //                   ),
    //                 ),
    //                 const Divider(
    //                   thickness: 1,
    //                   color: Colors.grey,
    //                   indent: 40,
    //                   endIndent: 40,
    //                 ),
    //                 Padding(
    //                   padding: const EdgeInsets.symmetric(horizontal: 40),
    //                   child: ListTile(
    //                     onTap: () async {
    //                       _audio = await _getFile(["mp3", "amr", "aac"]);
    //                       if (_audio == null) {
    //                         showToast("File not found try again!");
    //                         return;
    //                       }
    //                       for (var element in questions) {
    //                         if (checklist.formItemId == element.formItemId &&
    //                             checklist.type == element.type) {
    //                           setState(() {
    //                             element.response = _audio!.path;
    //                             element.isFile = true;
    //                           });
    //                         }
    //                       }
    //                       setSaved();
    //                       Navigator.of(context).pop();
    //                     },
    //                     leading: Icon(
    //                       Icons.storage,
    //                       color: fromHex(yellow),
    //                     ),
    //                     title: Text(
    //                       'Pick From Storage',
    //                       style: TextStyle(color: fromHex(yellow)),
    //                     ),
    //                   ),
    //                 )
    //               ],
    //             ),
    //           )));
    // }
  }

  Future<File?> getVideo(ImageSource imageSource) async {
    var p = await _picker.pickVideo(source: imageSource);
    if (p != null) {
      File video = File(p.path);
      return video;
    } else {
      return null;
    }
  }

  Future<File?> getPicture(ImageSource imageSource) async {
    var p = await _picker.pickImage(source: imageSource, imageQuality: 40);
    if (p != null) {
      File picture = File(p.path);
      return picture;
    } else {
      return null;
    }
  }

  Future<File?> _getFile(List<String> s) async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: s);

    if (result != null) {
      return File(result.files.single.path!);
    } else {
      return null;
    }
  }

  Future<File?> getAudioRecording(BuildContext context) async {
    File? audioFile;
    int scanTime = 0;

    bool isRecording = false;
    var tempDir = await getTemporaryDirectory();
    String path = '${tempDir.path}/${DateTime.now()}.aac';
    await showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: _scaffoldKey.currentContext!,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, StateSetter setState) => SingleChildScrollView(
              child: Container(
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[900],
                    // color: Theme.of(context).canvasColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        SizedBox(
                          height: 100,
                          width: 100,
                          child: Stack(
                            children: [
                              Lottie.asset('assets/audio.json',
                                  animate: isRecording),
                              const Align(
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.keyboard_voice_rounded,
                                  color: Colors.black,
                                  size: 36,
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0),
                          child: StreamBuilder<int>(
                            stream: _stopWatchTimer.rawTime,
                            initialData: _stopWatchTimer.rawTime.value,
                            builder: (context, snap) {
                              final value = snap.data!;
                              final displayTime = StopWatchTimer.getDisplayTime(
                                  value,
                                  hours: true);
                              return Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      displayTime,
                                      style: TextStyle(
                                        fontSize: 26,
                                        fontFamily: 'Helvetica',
                                        color: fromHex(yellow),
                                      ),
                                    ),
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8),
                                  //   child: Text(
                                  //     value.toString(),
                                  //     style: const TextStyle(
                                  //         fontSize: 16,
                                  //         fontFamily: 'Helvetica',
                                  //         fontWeight: FontWeight.w400),
                                  //   ),
                                  // ),
                                ],
                              );
                            },
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                onTap: () async {
                                  print(path);
                                  // if (isRecording) {
                                  //   Recording recording =
                                  //       await AudioRecorder.stop();
                                  //   print(
                                  //       "Path : ${recording.path},  Format : ${recording.audioOutputFormat},  Duration : ${recording.duration},  Extension : ${recording.extension},");
                                  //   _stopWatchTimer.onExecute
                                  //       .add(StopWatchExecute.stop);
                                  //   setState(() {
                                  //     audioFile = File(recording.path);
                                  //     isRecording = false;
                                  //   });
                                  // } else {
                                  //   await AudioRecorder.start(
                                  //       path: "$path",
                                  //       audioOutputFormat:
                                  //           AudioOutputFormat.AAC);
                                  //   isRecording =
                                  //       await AudioRecorder.isRecording;
                                  //   _stopWatchTimer.onExecute
                                  //       .add(StopWatchExecute.start);
                                  //   setState(() {});
                                  // }
                                },
                                title: Center(
                                  child: Text(
                                    isRecording ? 'Pause' : 'Start',
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                            isRecording
                                ? Expanded(
                                    child: ListTile(
                                    onTap: () async {
                                      if (isRecording) {
                                        // Recording recording =
                                        //     await AudioRecorder.stop();
                                        // print(
                                        //     "Path : ${recording.path},  Format : ${recording.audioOutputFormat},  Duration : ${recording.duration},  Extension : ${recording.extension},");
                                        // _stopWatchTimer.onExecute
                                        //     .add(StopWatchExecute.stop);
                                        // setState(() {
                                        //   audioFile = File(recording.path);
                                        //   isRecording = false;
                                        // });
                                      }
                                    },
                                    title: Center(
                                      child: Text(
                                        isRecording ? 'Stop' : '',
                                        style: TextStyle(
                                            color: fromHex(deepOrange),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ))
                                : Container(),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        });
    return audioFile;
  }

  Future<bool> checkIfAllHaveBeenAnswered(ReportFormsModel model) async {
    print("###########");
    for (var element in model.questions) {
      print(element.response);
    }
    return model.questions.every((element) =>
        ((element.response != null) && element.response!.isNotEmpty));
  }

  Future<void> sync(ReportFormsModel model) async {
    for (var i = 0; i <= model.offlineResponses.values.length; i++) {
      UpdateFormData item = model.offlineResponses.getAt(i);
      await sendDataToDb(item, i, model);
    }
  }

  Future<void> sendDataToDb(
      UpdateFormData item, int i, ReportFormsModel model) async {
    try {
      var r = await _model.updateResponse(item);
      if (r.isRight && r.right.error == false) {
        model.offlineResponses.deleteAt(i);
      }
    } catch (e) {
      showToast(e.toString());
    }
  }

  void setSaved(ReportFormsModel model) {
    model.setSaved(
        formData: widget.formData,
        key: getKey(),
        type: widget.type,
        id: widget.id,
        plannerStageId: widget.plannerStageId,
      taskId: widget.taskId
    );
  }

  Future<void> showYesNoDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    String yes = 'yes';
    String no = 'no';
    String? ans = '';
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ListTile(
                  title: Text(
                    "${checklist.question}",
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          Radio(
                              value: no,
                              groupValue: ans,
                              activeColor: fromHex(yellow),
                              onChanged: (dynamic v) {
                                setState(() {
                                  ans = v;
                                });
                              }),
                          const Text('No',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      Row(
                        children: [
                          Radio(
                              value: yes,
                              activeColor: fromHex(yellow),
                              // toggleable: true,
                              groupValue: ans,
                              onChanged: (dynamic v) {
                                setState(() {
                                  ans = v;
                                });
                              }),
                          const Text('Yes',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                    ],
                  ),
                ),
                ListTile(
                  onTap: () async {
                    if (ans!.isEmpty) {
                      showToast('Please select a response');
                      return;
                    }

                    model.setResponse(
                        formItemId: checklist.formItemId,
                        type: checklist.type,
                        response: "$ans");
                    setSaved(model);
                    Navigator.of(context).pop();
                  },
                  title: const Center(
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> showOpenTextDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    TextEditingController comment = TextEditingController();
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  TextFormField(
                    controller: comment,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: 6,
                    textAlignVertical: TextAlignVertical.top,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Response";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "Enter your response here..",
                      labelText: "Response",
                      alignLabelWithHint: true,
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    onTap: () async {
                      if (comment.text.isEmpty) {
                        showToast("Please fill response");
                        return;
                      }

                      model.setResponse(
                          formItemId: checklist.formItemId,
                          type: checklist.type,
                          response: comment.text);
                      setSaved(model);

                      Navigator.of(context).pop();
                    },
                    title: const Center(
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.deepOrange),
                      ),
                    ),
                  ),
                ],
              ),
            )));
  }

  Future<void> showNumberResponseDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    TextEditingController number = TextEditingController();
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  TextFormField(
                    controller: number,
                    keyboardType: TextInputType.number,
                    textAlignVertical: TextAlignVertical.top,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Response";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "e.g. 1",
                      labelText: "Response",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    onTap: () async {
                      if (number.text.isEmpty) {
                        showToast("Please fill response");
                        return;
                      }
                      model.setResponse(
                          formItemId: checklist.formItemId,
                          type: checklist.type,
                          response: number.text);
                      setSaved(model);
                      Navigator.of(context).pop();
                    },
                    title: const Center(
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.deepOrange),
                      ),
                    ),
                  ),
                ],
              ),
            )));
  }

  Future<void> showRecordingResponseDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    File? video;
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  Container(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        video = await getVideo(ImageSource.camera);
                        if (video == null) {
                          showToast("File not found try again!");
                          return;
                        }

                        model.setResponse(
                            formItemId: checklist.formItemId,
                            type: checklist.type,
                            response: video!.path,
                            isFile: true);
                        setSaved(model);
                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.video_call_outlined,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Start Recording',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const Divider(
                    thickness: 1,
                    color: Colors.grey,
                    indent: 40,
                    endIndent: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        video = await _getFile(['mp4']);
                        if (video == null) {
                          showToast("File not found try again!");
                          return;
                        }
                        model.setResponse(
                            formItemId: checklist.formItemId,
                            type: checklist.type,
                            response: video!.path,
                            isFile: true);
                        setSaved(model);

                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.storage,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Pick From Storage',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }

  Future<void> showPictureResponseDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    File? image;
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  Container(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        image = await getPicture(ImageSource.camera);
                        if (image == null) {
                          showToast("File not found try again!");
                          return;
                        }
                        model.setResponse(
                            formItemId: checklist.formItemId,
                            type: checklist.type,
                            response: image!.path,
                            isFile: true);
                        setSaved(model);
                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.video_call_outlined,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Open Camera',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const Divider(
                    thickness: 1,
                    color: Colors.grey,
                    indent: 40,
                    endIndent: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        image = await getPicture(ImageSource.gallery);
                        if (image == null) {
                          showToast("File not found try again!");
                          return;
                        }
                        model.setResponse(
                            formItemId: checklist.formItemId,
                            type: checklist.type,
                            response: image!.path,
                            isFile: true);
                        setSaved(model);
                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.storage,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Pick From Gallery',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }

  Future<void> showSpeciesResponseDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    SpeciesData? species;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ReportFormsModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getSpecies();
          } catch (e) {
            showToast("Failed to get species");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose species Item",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSpeciesSearch(
                                items: model.species,
                                onChanged: (SpeciesData ld) => {
                                  setState(() {
                                    species = ld;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (species == null) {
                                    showToast("Please Specify Species");
                                    return;
                                  }

                                  _model.setResponse(
                                      formItemId: checklist.formItemId,
                                      type: checklist.type,
                                      response: species?.id ?? "",
                                      responseName: species?.name);
                                  setSaved(_model);
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> showVarietyResponseDialog(
      //showVarietyResponseDialog
      BuildContext context,
      FormItems checklist,
      ReportFormsModel model) async {
    VarietyData? variety;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ReportFormsModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getVarieties();
          } catch (e) {
            showToast("Failed to get varieties");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose Variety",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownVarietySearch(
                                items: model.varieties,
                                onChanged: (VarietyData ld) => {
                                  setState(() {
                                    variety = ld;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (variety == null) {
                                    showToast("Please Specify Variety");
                                    return;
                                  }

                                  _model.setResponse(
                                      formItemId: checklist.formItemId,
                                      type: checklist.type,
                                      response: variety?.plannerStageId ?? "",
                                      responseName: variety?.varietyName);
                                  setSaved(_model);
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> showStaffResponseDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    StaffData? staff;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ReportFormsModel>(
        onModelReady: (model) async {
          //get line items
          try {
            var r = await model.getStaff();
            if (r.isLeft) {
              handleError(r.left);
            }
          } catch (e) {
            showToast("Failed to get varieties");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose Staff ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonStaffDropDownSearch(
                                items: model.staff,
                                onChanged: (StaffData ld) => {
                                  setState(() {
                                    staff = ld;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (staff == null) {
                                    showToast("Please Specify Staff");
                                    return;
                                  }
                                  try {
                                    _model.setResponse(
                                        formItemId: checklist.formItemId,
                                        type: checklist.type,
                                        response: staff!.id!,
                                        responseName: staff!.name);
                                    setSaved(_model);
                                  } catch (e) {
                                    logToCrushlytics(e.toString());
                                  }
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

 String getKey() {
   return "${widget.type}_${widget.formData.id}_${widget.id}_${widget.plannerStageId}_${widget.taskId}";
 }
}
