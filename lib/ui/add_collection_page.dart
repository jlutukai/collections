
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/general_response.dart';
import 'package:weight_collection/core/models/get_weight_type_response.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/models/scan_bag_qr_body.dart';
import 'package:weight_collection/core/viewmodels/collection_model.dart';
import 'package:weight_collection/ui/capture_weights.dart';
import 'package:weight_collection/ui/collections/collections_page.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';
import 'package:weight_collection/utils/loader.dart';
import 'package:weight_collection/utils/useful.dart';

import 'base_view.dart';
import 'design_system/DropDownSearch.dart';

class AddCollectionPage extends StatefulWidget {
  const AddCollectionPage({super.key});

  @override
  _AddCollectionPageState createState() => _AddCollectionPageState();
}

class _AddCollectionPageState extends State<AddCollectionPage> {
  List<String> visibilityList = ['Dirty', 'Dry', 'Clean'];
  List<String> batchTypeList = ['Daily', 'Weekly'];
  String? visibility = "";
  String batchType = "";
  CollectionData? _collection;
  final _collectionData = Hive.box('collection_data');
  final collections = Hive.box('weight_collection_data');

  @override
  void initState() {
    _collection = _collectionData.get("current_collection");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<CollectionModel>(
      onModelReady: (model) async {
        await model.getWeightTypes();
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.black,
                    elevation: 0,
                    title: Text(
                      'Collections',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: Column(
                    children: [
                      Expanded(
                        child: Center(
                          child: _collection == null
                              ? Container()
                              : _collectionInfo(context),
                        ),
                      ),
                      _buttons(context, model)
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            )
          : const Loader(),
    );
  }

  _buttons(BuildContext context, CollectionModel model) {
    return Column(
      children: [
        _collection == null
            ? Column(
                children: [
                  Container(
                    margin:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                    child: CommonDropDownSearch<String>(
                      isRequired: true,
                      showAsterisk: true,
                      onChanged: (u) {
                        saveBatchType(u);
                      },
                      labelText: "Batch Type",
                      hint: "Select Batch Type",
                      items: batchTypeList,
                      validationText: "Please Select Batch Type",
                    ),
                  ),
                  Dismissible(
                    key: UniqueKey(),
                    direction: DismissDirection.startToEnd,
                    onDismissed: (DismissDirection direction) async {
                      setState(() {});
                      ScanBagQRBody? scanResponse = await getScanResult(context);
                      if (batchType.isEmpty) {
                        showToast("Specify Batch Type");
                        return;
                      }

                      if (scanResponse == null) {
                        showToast("Make sure the correct bag is scanned!");
                        return;
                      }

                      await openBatch(model, scanResponse);
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 15),
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 15),
                      decoration: BoxDecoration(
                        color: Colors.white10.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            Icons.qr_code,
                            size: 36,
                            color: fromHex(yellow),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Scan Bag',
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.w100,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward_ios_sharp,
                            color: fromHex(yellow),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : Container(),

        _collection != null
            ? _collection!.id!.isNotEmpty
                ? Container(
                    margin:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                    child: CommonDropDownSearch<WeightTypeData>(
                      isRequired: true,
                      showAsterisk: true,
                      onChanged: (u) {
                        saveWeightType(u);
                      },
                      labelText: "Weight Type",
                      hint: "Select Weight Type",
                      items: model.weightTypes??[],
                      validationText: "Please Select Weight Type",
                    ),
                  )
                : Container()
            : Container(),
        _collection != null
            ? _collection!.id!.isNotEmpty
                ? Dismissible(
                    key: UniqueKey(),
                    direction: DismissDirection.startToEnd,
                    onDismissed: (DismissDirection direction) async {
                      setState(() {});
                      ScanBagQRBody? scanResponse = await getScanResult(context);

                      if (_collection == null) {
                        showToast("Scan Bag first");
                        return;
                      }
                      if (_collection?.id == null) {
                        showToast("scan bag first");
                        return;
                      }
                      if (_collection!.id!.isEmpty) {
                        showToast("scan bag first");
                        return;
                      }
                      if (_collection!.weightType!.isEmpty) {
                        showToast("Select Weight Type");
                        return;
                      }

                      if (scanResponse == null) {
                        showToast("Make sure the correct bag is scanned!");
                        return;
                      }
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => CaptureWeightsPage(
                              _addItemBag,
                              scanResponse.bag ?? "",
                              false,
                              _setCleanWeight)));
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 15),
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 15),
                      decoration: BoxDecoration(
                        color: Colors.white10.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SvgPicture.asset(
                            "assets/weight.svg",
                            width: 36,
                            height: 36,
                            color: fromHex(yellow),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Capture Weight',
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.w100,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward_ios_sharp,
                            color: fromHex(yellow),
                          )
                        ],
                      ),
                    ),
                  )
                : Container()
            : Container(),
        _collection != null
            ? _collection!.batchType! == "Weekly"
                ? Dismissible(
                    key: UniqueKey(),
                    direction: DismissDirection.startToEnd,
                    onDismissed: (DismissDirection direction) async {
                      setState(() {});
                      ScanBagQRBody? scanResponse;

                      if (_collection == null) {
                        showToast("Scan Bag first");
                        return;
                      }
                      if (_collection?.id == null) {
                        showToast("scan bag first");
                        return;
                      }
                      if (_collection!.id!.isEmpty) {
                        showToast("scan bag first");
                        return;
                      }

                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => CaptureWeightsPage(
                              _addItemBag,
                              scanResponse?.bag ?? "",
                              true,
                              _setCleanWeight)));
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 15),
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 15),
                      decoration: BoxDecoration(
                        color: Colors.white10.withOpacity(0.07),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SvgPicture.asset(
                            "assets/weight.svg",
                            width: 36,
                            height: 36,
                            color: fromHex(yellow),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Text(
                              'Capture Clean Weight',
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.w100,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward_ios_sharp,
                            color: fromHex(yellow),
                          )
                        ],
                      ),
                    ),
                  )
                : Container()
            : Container(),
        _collection != null
            ? Dismissible(
                key: UniqueKey(),
                direction: DismissDirection.horizontal,
                onDismissed: (DismissDirection direction) async {
                  if (direction == DismissDirection.startToEnd) {
                    if (_collection == null) {
                      showToast("Provide above details");
                      return;
                    }

                    if (_collection!.bags!.isEmpty) {
                      showToast("Capture weights");
                      return;
                    }
                    for (var element in _collection!.bags!) {
                      await sendToserver(model, element);
                    }

                    await updateBatchStatus(model, context);
                  } else {
                    await clearData();
                    getTotal();
                  }
                },
                child: Container(
                  margin:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  decoration: BoxDecoration(
                    color: Colors.white10.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Icon(
                        Icons.arrow_back_ios,
                        size: 36,
                        color: fromHex(yellow),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Clear',
                        style: TextStyle(
                          color: fromHex(yellow),
                          fontWeight: FontWeight.w100,
                          fontSize: 20,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        'Add',
                        style: TextStyle(
                          color: fromHex(yellow),
                          fontWeight: FontWeight.w100,
                          fontSize: 20,
                        ),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 36,
                        color: fromHex(yellow),
                      )
                    ],
                  ),
                ),
              )
            : Container(),
        // _collection!= null? _collection!.id!.isNotEmpty?Dismissible(
        //   key: UniqueKey(),
        //   direction: DismissDirection.startToEnd,
        //   onDismissed: (DismissDirection direction) async {
        //     setState(() {
        //     });

        //   },
        //   child: Container(
        //     margin: const EdgeInsets.symmetric(
        //         vertical: 5, horizontal: 15),
        //     padding: const EdgeInsets.symmetric(
        //         vertical: 10, horizontal: 15),
        //     decoration: BoxDecoration(
        //       color: Colors.white10.withOpacity(0.07),
        //       borderRadius: BorderRadius.circular(10),
        //     ),
        //     child: Row(
        //       mainAxisAlignment:
        //       MainAxisAlignment.spaceAround,
        //       children: [
        //         Icon(
        //           Icons.done,
        //           size: 36,
        //           color: fromHex(yellow),
        //         ),
        //         const SizedBox(
        //           width: 20,
        //         ),
        //         Expanded(
        //           child: Text(
        //             'Submit',
        //             style: TextStyle(
        //               color: fromHex(yellow),
        //               fontWeight: FontWeight.w100,
        //               fontSize: 20,
        //             ),
        //           ),
        //         ),
        //         Icon(
        //           Icons.arrow_forward_ios_sharp,
        //           color: fromHex(yellow),
        //         )
        //       ],
        //     ),
        //   ),
        // ): Container(): Container(),

        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  _collectionInfo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Card(
        color: Colors.black,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Collection Details",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  Container(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: [
                        // Row(
                        //   children: [
                        //     _collection!.plantName!.isEmpty
                        //         ? Container()
                        //         : Expanded(
                        //             child: Column(
                        //               crossAxisAlignment:
                        //                   CrossAxisAlignment.start,
                        //               children: [
                        //                 Text(
                        //                   "${_collection!.plantName}",
                        //                   style: const TextStyle(
                        //                       fontSize: 15.6,
                        //                       fontWeight: FontWeight.bold,
                        //                       color: Colors.white),
                        //                 ),
                        //                 const Text(
                        //                   "Plant Name",
                        //                   style: TextStyle(
                        //                     fontSize: 11,
                        //                     fontStyle: FontStyle.italic,
                        //                     color: Colors.white54,
                        //                   ),
                        //                 ),
                        //               ],
                        //             ),
                        //           ),
                        //     _collection!.employeeName!.isEmpty
                        //         ? Container()
                        //         : Expanded(
                        //             child: Column(
                        //               crossAxisAlignment:
                        //                   CrossAxisAlignment.start,
                        //               children: [
                        //                 Text(
                        //                   "${_collection!.employeeName}",
                        //                   style: const TextStyle(
                        //                       fontSize: 15.6,
                        //                       fontWeight: FontWeight.bold,
                        //                       color: Colors.white),
                        //                 ),
                        //                 const Text(
                        //                   'Employee',
                        //                   style: TextStyle(
                        //                     fontSize: 11,
                        //                     fontStyle: FontStyle.italic,
                        //                     color: Colors.white54,
                        //                   ),
                        //                 ),
                        //               ],
                        //             ),
                        //           )
                        //   ],
                        // ),
                        Row(
                          children: [
                            _collection!.id!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_collection!.id}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Bag ID",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _collection!.weightType!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_collection!.weightType}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          'Weight Type',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                          ],
                        ),
                        Row(
                          children: [
                            _collection!.totalWeight! == 0.0
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_collection!.totalWeight}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Weight",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _collection!.batchType! == "Weekly"
                                ? _collection!.cleanWeight! == 0.0
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${_collection!.cleanWeight}",
                                              style: const TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            const Text(
                                              "Clean Weight",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                : Container(),
                          ],
                        ),
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Captured Weights",
                              style: TextStyle(
                                fontSize: 11,
                                fontStyle: FontStyle.italic,
                                color: Colors.white54,
                              ),
                            )
                          ],
                        ),
                        _collection!.bags!.isNotEmpty
                            ? _collectedWeightsList(context, _collection!.bags!)
                            : Container()
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _saveCollection(CollectionData data) {
    if (data.id == null) {
      showToast("scan bag first");
      return;
    }
    if (data.id!.isEmpty) {
      showToast("scan bag first");
      return;
    }
    _collectionData.put("current_collection", data);
    setState(() {
      _collection = data;
    });
  }

  Widget _visibilityDrop(BuildContext context, WeightTypeData? item) {
    return Text(
      visibility!.isNotEmpty ? "$visibility" : "Select Weight Type",
      style: TextStyle(
        color: visibility!.isEmpty ? fromHex(yellow) : Colors.white,
        fontSize: 17,
      ),
    );
  }

  Widget _visibilityPopUp(
      BuildContext context, WeightTypeData item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'Undefined',
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _batchTypeDrop(BuildContext context, String? item) {
    return Text(
      batchType.isNotEmpty ? batchType : "Select Batch Type",
      style: TextStyle(
        color: batchType.isEmpty ? fromHex(yellow) : Colors.white,
        fontSize: 17,
      ),
    );
  }

  Widget _batchTypePopUp(BuildContext context, String item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  _setCleanWeight(double w) {
    if (_collection != null) {
      CollectionData c = _collection!;
      c.cleanWeight = w;
      _saveCollection(c);
    }
  }

  _addItemBag(ItemBags i) {
    if (_collection != null) {
      if (_collection!.bags!.contains(i)) {
        if (kDebugMode) {
          print("#################### contains");
        }
        for (var element in _collection!.bags!) {
          if (i == element) {
            setState(() {
              element.weight = i.weight;
            });
          }
        }
      } else {
        if (kDebugMode) {
          print("#################### not contains");
        }
        setState(() {
          _collection!.bags!.add(i);
        });
      }
      _saveCollection(_collection!);
    }

    getTotal();
  }

  void getTotal() {
    var total = 0.0;
    _collection?.bags?.forEach((element) {
      total += element.weight ?? 0.0;
    });

    CollectionData? c = _collection?..totalWeight = total;
    if (c != null) {
      _saveCollection(c);
    }
  }

  _collectedWeightsList(BuildContext context, List<ItemBags> list) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: list.length,
      shrinkWrap: true,
      itemBuilder: (context, index) =>
          _collectionsListItem(context, list[index], index),
    );
  }

  _collectionsListItem(BuildContext context, ItemBags i, int index) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: BoxDecoration(
        color: Colors.white10.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Expanded(
              child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${index + 1}.   ",
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${i.bagId}",
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${i.weight} g(s)",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w100,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _collection!.bags!.removeAt(index);
                      _saveCollection(_collection!);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "remove",
                        style: TextStyle(
                            color: Colors.deepOrange.shade900,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ))
        ],
      ),
    );
  }

  Future<void> clearData() async {
    await _collectionData.clear();
    setState(() {
      _collection = null;
      visibility = '';
    });
  }

  Future<void> openBatch(
      CollectionModel model, ScanBagQRBody scanResponse) async {
    bool hasConnection = await checkConnection();
    if (hasConnection) {
      Map<String, dynamic> data = {
        "clerk_id": getUserId() ?? "33", //TODO
        "bag_id": _collection?.id ?? '',
        "type": _collection?.batchType
      };
      try {
        var r = await model.openBatch(data);
        if (r['success']) {
          var response = GeneralResponse.fromJson(r["response"]);
          if (!response.error!) {
            if (collections.keys.contains(scanResponse.bag)) {
              CollectionData collection = collections.get(scanResponse.bag);
              _saveCollection(collection);
            } else {
              CollectionData collection = CollectionData()
                ..id = scanResponse.bag
                ..totalWeight = 0.0
                ..weightType = ""
                ..bags = []
                ..batchId = response.id
                ..bagId = scanResponse.bag
                ..status = ""
                ..date = ""
                ..varietyId = ""
                ..weightType = ""
                ..totalWeight = 0.0
                ..cleanWeight = 0.0
                ..bags = []
                ..syncStatus = 0
                ..weightTypeId = ""
                ..batchType = batchType
                ..status = "";
              _saveCollection(collection);
            }
            showToast(response.message ?? 'Success');
          }
        } else {
          showToast("An error occurred");
        }
      } catch (e) {
        showToast(e.toString());
      }
    } else {
      // offlineResponses.add(data);
      showToast("Check your internet connection");
      // Navigator.of(context).pop();
    }
  }

  Future<void> sendToserver(
    CollectionModel model,
    ItemBags element,
  ) async {
    bool hasConnection = await checkConnection();
    if (hasConnection) {
      Map<String, dynamic> data = {
        "batch_id": _collection?.batchId,
        "weight": "${element.weight ?? 0.0}"
      };
      try {
        var r = await model.updateBatchItem(data);
        if (r['success']) {
          var response = GeneralResponse.fromJson(r["response"]);
          showToast(response.message ?? 'Success');
        } else {
          showToast("An error occurred");
        }
      } catch (e) {
        showToast(e.toString());
      }
    } else {
      // offlineResponses.add(data);
      showToast("Check your internet connection");
      // Navigator.of(context).pop();
    }
  }

  updateBatchStatus(CollectionModel model, BuildContext context) async {
    bool hasConnection = await checkConnection();
    if (hasConnection) {
      Map<String, dynamic> data = {
        "batch_id": _collection?.batchId,
        "clean_weight": "${_collection?.cleanWeight}"
      };
      try {
        var r = await model.updateBatchStatus(data);
        if (r['success']) {
          var response = GeneralResponse.fromJson(r["response"]);
          if (!response.error!) {
            showToast(response.message ?? 'Success');
            collections.put(
                "${_collection?.bagId ?? ''} ${_collection?.batchId ?? ''}",
                _collection);
            await clearData();
            Navigator.pushNamedAndRemoveUntil(
                context, CollectionsPage.tag, (Route<dynamic> route) => false);
          }
        } else {
          showToast("An error occurred");
        }
      } catch (e) {
        showToast(e.toString());
      }
    } else {
      // offlineResponses.add(data);
      showToast("Check your internet connection");
      // Navigator.of(context).pop();
    }
  }

  void saveBatchType(String? s) {
    setState(() {
      batchType = s ?? "";
    });
  }

  void saveWeightType(WeightTypeData? s) {
    if (_collection == null) {
      showToast("Scan Bag first");
      return;
    }
    if (_collection?.id == null) {
      showToast("scan bag first");
      return;
    }
    if (_collection!.id!.isEmpty) {
      showToast("scan bag first");
      return;
    }

    setState(() {
      visibility = s?.name ?? '';
    });
    CollectionData collection = _collection!
      ..weightTypeId = s?.id ?? ''
      ..weightType = s?.name ?? '';
    _saveCollection(collection);
  }
}
