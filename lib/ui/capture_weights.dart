import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter_svg/svg.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/utils/useful.dart';


class CaptureWeightsPage extends StatefulWidget {
  static const tag = 'capture';
  final Function(ItemBags i) addItemBag;
  final String qrCode;
  final bool isCleanWeight;
  final Function(double i) setCleanWeight;
  const CaptureWeightsPage(
      this.addItemBag, this.qrCode, this.isCleanWeight, this.setCleanWeight,
      {super.key});

  @override
  _CaptureWeightsPageState createState() => _CaptureWeightsPageState();
}

class _CaptureWeightsPageState extends State<CaptureWeightsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final List<String> _list = [];
  final List<bool> _stability = [];
  BluetoothConnection? connection;
  bool isConnected = false;
  bool isConnecting = false;
  String macAddress = "81:23:7A:71:B4:B8";
  String stableReading = "";

  @override
  void initState() {
    _list.add("0.00");
    _initializeScale();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Text(
              'Get Weight Reading',
              style: TextStyle(
                  color: fromHex(deepOrange), fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            automaticallyImplyLeading: false,
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    macAddress.isEmpty
                        ? Container()
                        : InkWell(
                            onTap: () {
                              setState(() {
                                macAddress = "";
                              }); // 98:da:20:02:50:e4
                            },
                            child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5),
                                decoration: BoxDecoration(
                                    color: Colors.white10.withOpacity(0.07),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(16))),
                                child: const Text(
                                  'Clear',
                                  style: TextStyle(color: Colors.deepOrange),
                                )),
                          ),
                  ],
                ),
              )
            ],
          ),
          body: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height - 100,
              child: Column(
                children: [
                  Expanded(
                      child: Column(
                    children: [
                      const SizedBox(
                        height: 40,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            _list.last,
                            style: TextStyle(
                                fontSize: 50,
                                color: _stability.length > 5
                                    ? fromHex(green)
                                    : Colors.white),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(bottom: 25),
                            child: Text(
                              "  g(s)",
                              style: TextStyle(fontSize: 26),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                width: 5.0,
                                height: 5.0,
                                decoration: BoxDecoration(
                                  color: _stability.isNotEmpty
                                      ? Colors.green
                                      : Colors.red,
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 5.0,
                                height: 5.0,
                                decoration: BoxDecoration(
                                  color: _stability.length >= 2
                                      ? Colors.green
                                      : Colors.red,
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 5.0,
                                height: 5.0,
                                decoration: BoxDecoration(
                                  color: _stability.length >= 3
                                      ? Colors.green
                                      : Colors.red,
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 5.0,
                                height: 5.0,
                                decoration: BoxDecoration(
                                  color: _stability.length >= 4
                                      ? Colors.green
                                      : Colors.red,
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 5.0,
                                height: 5.0,
                                decoration: BoxDecoration(
                                  color: _stability.length >= 5
                                      ? Colors.green
                                      : Colors.red,
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                    ],
                  )),
                  Container(
                    margin: const EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 15,
                    ),
                    child: Row(
                      children: [
                        Container(
                          color: isConnected ? Colors.green : Colors.red,
                          width: 2,
                          height: 50,
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () async {
                              if (macAddress.isNotEmpty) {
                                _initializeScale();
                              } else {
                                _showInputDialog();
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.only(top: 10),
                              decoration: BoxDecoration(
                                color: Colors.white10.withOpacity(0.07),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                children: [
                                  SvgPicture.asset(
                                    "assets/scale.svg",
                                    width: 36,
                                    height: 36,
                                    color: fromHex(yellow),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        isConnecting
                            ? CircularProgressIndicator(
                                backgroundColor: fromHex(deepOrange),
                                valueColor: AlwaysStoppedAnimation<Color>(
                                  fromHex(yellow),
                                ),
                              )
                            : Container(),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ),
                  _stability.length > 5
                      ? InkWell(
                          onTap: () async {
                            if (connection != null) {
                              connection!.dispose();
                              await connection!.close();
                            }
                            ItemBags i = ItemBags()
                              ..bagId = widget.qrCode
                              ..weight = double.tryParse(_list.last);

                            if (widget.isCleanWeight) {
                              widget.setCleanWeight(
                                  double.tryParse(_list.last) ?? 0.0);
                            } else {
                              widget.addItemBag(i);
                            }
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            margin: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 15),
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 15),
                            decoration: BoxDecoration(
                              color: Colors.white10.withOpacity(0.07),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                SvgPicture.asset(
                                  "assets/scale.svg",
                                  width: 36,
                                  height: 36,
                                  color: fromHex(yellow),
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  child: Text(
                                    'Capture',
                                    style: TextStyle(
                                      color: fromHex(yellow),
                                      fontWeight: FontWeight.w100,
                                      fontSize: 20,
                                    ),
                                  ),
                                ),
                                Icon(
                                  Icons.done,
                                  color: fromHex(yellow),
                                )
                              ],
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          left: 15,
          top: 27,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: Colors.white10.withOpacity(0.07),
            onPressed: () async {
              if (connection != null) {
                connection!.dispose();
                await connection!.close();
              }
              Navigator.pop(context);
            },
            mini: true,
            tooltip: "go back home",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(deepOrange),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _initializeScale() async {
    showToast("Please Wait Connecting to Scale...");
    setState(() {
      isConnecting = true;
    });
    try {
      connection = await BluetoothConnection.toAddress(macAddress);
      setState(() {
        if (connection != null) {
          isConnected = connection!.isConnected;
          isConnecting = false;
        }
      });
      if (isConnected) {
        showToast("Connection to Scale Successful...");
        showToast("Please Wait Getting Readings...");
      }
      connection!.input!.listen((Uint8List data) {
        String reading = '';
        if (ascii.decode(data).trim().startsWith("+") ||
            ascii.decode(data).trim().endsWith("g")) {
          if (kDebugMode) {
            print(
                'Data incoming: ${ascii.decode(data)} lenght - ${ascii.decode(data).length}');
          }
          String raw = ascii
              .decode(data)
              .trim()
              .replaceAll(" ", '')
              .replaceAll("+", '')
              .replaceAll("g", '')
              .replaceAll("\n", '');
          if (raw.length == 7) {
            reading = raw; //US,GS,+0002.1kg
          }
        }
        if (kDebugMode) {
          print(reading);
        }
        if (reading.isNotEmpty) {
          setState(() {
            if (_list.last.contains(reading)) {
              _stability.add(true);
            } else {
              _stability.clear();
            }
            setContent(reading);
            _list.add(reading);
            if (_stability.length > 10) {
              _disconnect();
            }
          });
        }

        connection!.output.add(data); // Sending data

        if (ascii.decode(data).contains('!')) {
          connection!.finish(); // Closing connection
          showToast('Disconnecting from scale');
        }
      }).onDone(() {
        showToast('Disconnected from Scale');
      });
    } catch (exception) {
      setState(() {
        isConnecting = false;
      });

      showToast(
          "Cannot connect, to scale \n Please check if scale is turned on");
    }
  }

  void setContent(String reading) {
    setState(() {
      stableReading = reading;
    });
  }

  Future<void> _disconnect() async {
    if (connection != null) {
      connection!.dispose();
      await connection!.close();
    }
  }

  Future<void> _showInputDialog() async {
    TextEditingController comment = TextEditingController();
    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (BuildContext context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: StatefulBuilder(builder: (context, StateSetter setState) {
              return Container(
                color: Colors.transparent,
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: comment,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        textAlignVertical: TextAlignVertical.top,
                        style: const TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please Enter Verification Code";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white10,
                          isDense: true,
                          hintText: "eg AB:1*:**:**",
                          labelText: "Mac Address",
                          alignLabelWithHint: true,
                          labelStyle: TextStyle(color: fromHex(yellow)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      ListTile(
                        onTap: () async {
                          if (comment.text.isNotEmpty) {
                            setState(() {
                              macAddress = comment.text.toString();
                            });
                          }
                          Navigator.pop(context);
                        },
                        title: const Center(
                          child: Text(
                            'confirm',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            })));
  }
}
