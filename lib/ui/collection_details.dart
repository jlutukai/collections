import 'package:flutter/material.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/utils/useful.dart';

class CollectionDetails extends StatefulWidget {
  final CollectionData collectionData;
  const CollectionDetails(this.collectionData, {super.key});

  @override
  _CollectionDetailsState createState() => _CollectionDetailsState();
}

class _CollectionDetailsState extends State<CollectionDetails> {
  CollectionData? _collection;

  @override
  void initState() {
    _collection = widget.collectionData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.black,
            elevation: 0,
            title: Text(
              'Collection Details',
              style: TextStyle(
                  color: fromHex(deepOrange), fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            automaticallyImplyLeading: false,
          ),
          body: Column(
            children: [
              Expanded(
                child: Center(
                  child: _collection == null
                      ? Container()
                      : _collectionInfo(context),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 15,
          top: 27,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: Colors.white10.withOpacity(0.07),
            onPressed: () {
              Navigator.pop(context);
            },
            mini: true,
            tooltip: "go back home",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(deepOrange),
            ),
          ),
        ),
      ],
    );
  }

  _collectionInfo(BuildContext context) {
    double w = 0.0;
    _collection?.bags?.forEach((element) {
      w += element.weight ?? 0.0;
    });
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Card(
        color: Colors.black,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Collection Details",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  Container(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: [
                        // Row(
                        //   children: [
                        //     _collection!.plantName!.isEmpty
                        //         ? Container()
                        //         : Expanded(
                        //       child: Column(
                        //         crossAxisAlignment:
                        //         CrossAxisAlignment.start,
                        //         children: [
                        //           Text(
                        //             "${_collection!.plantName}",
                        //             style: const TextStyle(
                        //                 fontSize: 15.6,
                        //                 fontWeight: FontWeight.bold,
                        //                 color: Colors.white),
                        //           ),
                        //           const Text(
                        //             "Plant Name",
                        //             style: TextStyle(
                        //               fontSize: 11,
                        //               fontStyle: FontStyle.italic,
                        //               color: Colors.white54,
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //     _collection!.employeeName!.isEmpty
                        //         ? Container()
                        //         : Expanded(
                        //       child: Column(
                        //         crossAxisAlignment:
                        //         CrossAxisAlignment.start,
                        //         children: [
                        //           Text(
                        //             "${_collection!.employeeName}",
                        //             style: const TextStyle(
                        //                 fontSize: 15.6,
                        //                 fontWeight: FontWeight.bold,
                        //                 color: Colors.white),
                        //           ),
                        //           const Text(
                        //             'Employee',
                        //             style: TextStyle(
                        //               fontSize: 11,
                        //               fontStyle: FontStyle.italic,
                        //               color: Colors.white54,
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     )
                        //   ],
                        // ),
                        Row(
                          children: [
                            _collection?.bagId == null
                                ? Container()
                                : _collection!.bagId!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${_collection!.bagId}",
                                              style: const TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            const Text(
                                              "Bag ID",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                            _collection?.status == null
                                ? Container()
                                : _collection!.status!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${_collection!.status}",
                                              style: const TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            const Text(
                                              'Weight Type',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                          ],
                        ),
                        Row(
                          children: [
                            w == 0.0
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "$w",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Weight",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                          ],
                        ),

                        const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Captured Weights",
                              style: TextStyle(
                                fontSize: 11,
                                fontStyle: FontStyle.italic,
                                color: Colors.white54,
                              ),
                            )
                          ],
                        ),
                        _collection?.bags == null
                            ? Container()
                            : _collection!.bags!.isNotEmpty
                                ? _collectedWeightsList(
                                    context, _collection!.bags!)
                                : Container()
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _collectedWeightsList(BuildContext context, List<ItemBags> list) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: list.length,
      shrinkWrap: true,
      itemBuilder: (context, index) =>
          _collectionsListItem(context, list[index], index),
    );
  }

  _collectionsListItem(BuildContext context, ItemBags i, int index) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: BoxDecoration(
        color: Colors.white10.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Expanded(
              child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${index + 1}.   ",
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${i.bagId}",
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${i.weight} g(s)",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w100,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ))
        ],
      ),
    );
  }
}
