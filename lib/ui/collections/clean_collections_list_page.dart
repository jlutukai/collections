
import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/get_batch_dry_response.dart';
import 'package:weight_collection/ui/collections/read_weight_page.dart';

import '../../core/enums/view_state.dart';
import '../../core/models/scan_bag_qr_body.dart';
import '../../core/viewmodels/clean_collection_model.dart';
import '../../utils/loader.dart';
import '../../utils/useful.dart';
import '../base_view.dart';
import '../scanManagement/utils.dart';
import 'constants.dart';

class CleanCollectionsList extends StatefulWidget {
  const CleanCollectionsList({super.key});

  @override
  State<CleanCollectionsList> createState() => _CleanCollectionsListState();
}

class _CleanCollectionsListState extends State<CleanCollectionsList> {
  List<BatchDryData> collections = [];
  late CleanCollectionModel _model;

  @override
  Widget build(BuildContext context) {
    return BaseView<CleanCollectionModel>(
        onModelReady: (model) async {
          _model = model;
          refreshData();
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Scaffold(
                backgroundColor: Colors.black,
                body: Column(
                  children: [
                    // SizedBox(
                    //   height: MediaQuery.of(context).size.height * 0.40,
                    //   child: _collectionsList(context),
                    // ),
                    Expanded(
                      child: _collectionsList1(context),
                    ),
                    _buttons(context)
                  ],
                ),
              )
            : const Loader());
  }

  _buttons(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) async {
            setState(() {});
            ScanBagQRBody? scanResponse = await getScanResult(context);

            if (scanResponse == null) {
              showToast("Make sure the correct bag is scanned!");
              return;
            }
            var existingWet = collections.firstWhereOrNull(
                (element) => element.bagId == scanResponse.id!);
            if (existingWet == null) {
              showToast("Please scan correct bag");
              return;
            }
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => ReadWeightPage(
                  scanResponse.id!,
                  type: clean,
                  dryBatch: existingWet,
                  onRefresh: refreshData,
                ),
              ),
            );
          },
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white10.withOpacity(0.07),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(
                  Icons.qr_code_2_outlined,
                  size: 36,
                  color: fromHex(yellow),
                ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    'Start Scanning',
                    style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.w100,
                      fontSize: 20,
                    ),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 2,
        ),
      ],
    );
  }


  _collectionItem(BatchDryData bag, int index) {
    return Container(
      // margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: BoxDecoration(
        color: Colors.grey[900],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(bag.pictureName != null
                        ? "${getUrl()}/CUSTOM/Variety/picture/${bag.pictureName}"
                        : noImage),
                  ),
                ),
              ),
              Expanded(
                child: Column(children: [
                  Text(
                    'harvested on',
                    style: normalStyle(),
                  ),
                  Text(
                    Jiffy.parse("${bag.date} ${bag.time}").yMMMEd,
                    style: titleStyle(color: Colors.white),
                  ),
                ]),
              ),
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            "This Sack Contains",
            style: normalStyle(),
          ),
          Text(
            bag.speciesName ?? "",
            style: titleStyle(),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            "line ",
            style: normalStyle(),
          ),
          Text(
            bag.lineId ?? "",
            style: titleStyle(),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            "Variety",
            style: normalStyle(),
          ),
          Text(
            bag.varietyName ?? "",
            style: titleStyle(),
          ),
          // const SizedBox(
          //   height: 5,
          // ),
          // Text(
          //   "Supervised By",
          //   style: normalStyle(),
          // ),
          // Text(
          //   bag.supervisorName ?? "",
          //   style: titleStyle(),
          // ),
          const SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  _collectionsList1(BuildContext context) {
    return collections.isNotEmpty
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemCount: collections.length,
            shrinkWrap: true,
            itemBuilder: (context, index) =>
                _colectionListItem1(context, collections[index], index),
          )
        : const Center(
            child: Text("No Bags added yet"),
          );
  }

  _colectionListItem1(BuildContext context, BatchDryData bag, int index) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: BoxDecoration(
        color: Colors.white10.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          varietyImage(pictureName: bag.pictureName, h: 50, w: 50),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "This Sack Contains",
                          style: normalStyle(),
                        ),
                        Text(
                          bag.speciesName ?? "",
                          style: titleStyle(),
                        ),
                        Text(
                          "line ",
                          style: normalStyle(),
                        ),
                        Text(
                          bag.lineId ?? "",
                          style: titleStyle(),
                        ),
                        Text(
                          "Variety",
                          style: normalStyle(),
                        ),
                        Text(
                          bag.varietyName ?? "",
                          style: titleStyle(),
                        ),
                        Text(
                          "Bag No.",
                          style: normalStyle(),
                        ),
                        Text(
                          bag.bagNo ?? "",
                          style: titleStyle(),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        Jiffy.now().yMMMEd,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w100,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ))
        ],
      ),
    );
  }

  Future<void> refreshData() async {
    try {
      await _model.getBags();
      collections.clear();
      collections.addAll(_model.bags);
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
    }
  }
}
