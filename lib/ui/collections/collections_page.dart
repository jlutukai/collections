import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';
import 'package:weight_collection/ui/collections/clean_collections_list_page.dart';
import 'package:weight_collection/ui/collections/daily_collections_list_page.dart';
import 'package:weight_collection/utils/useful.dart';

import '../../utils/collection_icons_icons.dart';
import 'dry_collections_list_page.dart';

class CollectionsPage extends StatefulWidget {
  static const tag = 'collections';

  const CollectionsPage({super.key});

  @override
  _CollectionsPageState createState() => _CollectionsPageState();
}

class _CollectionsPageState extends State<CollectionsPage> {
  int currentIndex = 0;
  PageController pageController = PageController();
  late CircularBottomNavigationController _navigationController;
  List<TabItem> tabItems = List.of([
    TabItem(
      CollectionIcons.tray_daily_wet,
      "Daily Wet",
      Colors.white12,
      labelStyle: TextStyle(
        color: fromHex(deepOrange),
      ),
    ),
    TabItem(
      CollectionIcons.tray_batch_dry,
      "Batch Dry",
      Colors.white12,
      labelStyle: TextStyle(
        color: fromHex(deepOrange),
      ),
    ),
    TabItem(
      CollectionIcons.tray_batch_clean,
      "Batch Clean",
      Colors.white12,
      labelStyle: TextStyle(
        color: fromHex(deepOrange),
      ),
    ),
  ]);

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _navigationController = CircularBottomNavigationController(currentIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.black,
            appBar: AppBar(
              backgroundColor: Colors.black,
              elevation: 0,
              title: Text(
                'Collections',
                style: TextStyle(
                    color: fromHex(deepOrange), fontWeight: FontWeight.bold),
              ),
              centerTitle: true,
              automaticallyImplyLeading: false,
            ),
            body: Column(
              children: [
                Expanded(
                  child: PageView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: pageController,
                    onPageChanged: (index) {
                      setState(() {
                        currentIndex = index;
                      });
                    },
                    children: const <Widget>[
                      DailyCollectionsPage(),
                      DryCollectionsListPage(),
                      CleanCollectionsList(),
                    ],
                  ),
                )
              ],
            ),
            bottomNavigationBar: CircularBottomNavigation(
              tabItems,
              controller: _navigationController,
              barBackgroundColor: Colors.black,
              selectedPos: currentIndex,
              iconsSize: 20,
              selectedIconColor: fromHex(deepOrange),
              normalIconColor: fromHex(grey),
              selectedCallback: (int? index) {
                setState(() {
                  currentIndex = index ?? 0;
                });
                navigateToScreens(index ?? 0);
              },
            ),
          ),
          Positioned(
            left: 15,
            top: 2,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: Colors.white10.withOpacity(0.07),
              onPressed: () {
                Navigator.pop(context);
              },
              mini: true,
              tooltip: "go back home",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(deepOrange),
              ),
            ),
          ),
        ],
      ),
    );
  }

  navigateToScreens(int index) {
    setState(() {
      currentIndex = index;
      pageController.animateToPage(index,
          duration: const Duration(milliseconds: 100), curve: Curves.ease);
    });
  }
}
