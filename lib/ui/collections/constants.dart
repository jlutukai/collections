import 'package:flutter/material.dart';

import '../../utils/useful.dart';

const String wet = 'wet';
const String dry = 'dry';
const String clean = 'clean';

titleStyle({Color? color}) {
  return TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16,
      color: color ?? fromHex(yellow));
}

normalStyle() {
  return TextStyle(
      fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
}
