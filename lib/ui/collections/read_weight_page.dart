import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:weight_collection/core/models/get_batch_dry_response.dart';
import 'package:weight_collection/core/models/get_batch_wet_response.dart';
import 'package:weight_collection/ui/base_view.dart';
import 'package:weight_collection/utils/loader.dart';

import '../../core/enums/view_state.dart';
import '../../core/models/general_response.dart';
import '../../core/models/get_daily_wet_response.dart';
import '../../core/models/get_variety_response.dart';
import '../../core/models/local/localModels.dart';
import '../../core/viewmodels/read_weight_model.dart';
import '../../utils/useful.dart';
import '../scanManagement/utils.dart';
import 'constants.dart';

class ReadWeightPage extends StatefulWidget {
  final String? bagId;
  final String type;
  final List<DailyWetData>? bags;
  final List<WetBatchData>? wetBatch;
  final BatchDryData? dryBatch;
  final DailyWetData? dailyWet;
  final Future<void> Function() onRefresh;

  const ReadWeightPage(this.bagId,
      {super.key,
      this.bags,
      this.wetBatch,
      this.dryBatch,
      required this.type,
      required this.onRefresh,
      this.dailyWet});

  @override
  State<ReadWeightPage> createState() => _ReadWeightPageState();
}

class _ReadWeightPageState extends State<ReadWeightPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final List<String> _list = [];
  final List<bool> _stability = [];
  BluetoothConnection? connection;
  bool isConnected = false;
  bool isConnecting = false;

  String macAddress = "81:23:7A:71:B4:B8"; //81-23-7A-B4-B8
  // String macAddress = "98:DA:20:02:50:E4";
  String stableReading = "";
  String plannerStageId = "";
  BagItems? bag;
  VarietyData? varietyData;

  // late ReadWeightModel _model;

  @override
  void initState() {
    _list.add("0.00");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ReadWeightModel>(
      onModelReady: (model) {
        initializeRequestState(model);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? PopScope(
              onPopInvoked: (b) {
                if (b) {
                  showExitPopup(model);
                }
              },
              child: SafeArea(
                child: Stack(
                  children: [
                    Scaffold(
                      key: _scaffoldKey,
                      backgroundColor: Colors.black,
                      appBar: AppBar(
                        backgroundColor: Colors.transparent,
                        elevation: 0,
                        title: Text(
                          'Get Weight Reading',
                          style: TextStyle(
                              color: fromHex(deepOrange),
                              fontWeight: FontWeight.bold),
                        ),
                        centerTitle: true,
                        automaticallyImplyLeading: false,
                        actions: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                hasBeenInitialized(model)
                                    ? Container()
                                    : InkWell(
                                        onTap: () {
                                          initializeRequestState(
                                              model); // 98:da:20:02:50:e4
                                        },
                                        child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 5),
                                            decoration: BoxDecoration(
                                                color: Colors.white10
                                                    .withOpacity(0.07),
                                                borderRadius:
                                                    const BorderRadius.all(
                                                        Radius.circular(16))),
                                            child: const Text(
                                              'Clear',
                                              style: TextStyle(
                                                  color: Colors.deepOrange),
                                            )),
                                      ),
                              ],
                            ),
                          )
                        ],
                      ),
                      body: SizedBox(
                        height: MediaQuery.of(context).size.height - 100,
                        child: SingleChildScrollView(
                          physics: const BouncingScrollPhysics(),
                          child: Column(
                            children: [
                              Column(
                                children: [
                                  model.updateDailyWetRequest != null
                                      ? _collectionInfo(
                                          context: context,
                                          request: model.updateDailyWetRequest!,
                                          model: model)
                                      : Container(),
                                  model.cleanWeightRequest != null
                                      ? _collectionInfo(
                                          context: context,
                                          cleanRequest:
                                              model.cleanWeightRequest!,
                                          model: model)
                                      : Container(),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  _readingInfo(context, model)
                                ],
                              ),
                              widget.type == dry
                                  ? _sliderButton("Specify Variety", () async {
                                      await _dialogChooseVariety(
                                          (variety) async {
                                        model.setRequest(
                                            request: model
                                                .updateDailyWetRequest!
                                              ..image = variety.pictureName
                                              ..plannerStageId =
                                                  variety.plannerStageId
                                              ..varietyName =
                                                  variety.varietyName
                                              ..stageNage = variety.stageName,
                                            key:
                                                "${widget.type}-${widget.bagId}");
                                        print(
                                            "@@@@@@@@@@@@@@@@@@@@@@@@@@ ${model.updateDailyWetRequest}");
                                        setState(() {
                                          varietyData = variety;
                                        });
                                      });
                                    }, "home_collection")
                                  : Container(),
                              widget.type == clean
                                  ? Container()
                                  : (bag == null)
                                      ? _sliderButton(getTitle(widget.type),
                                          () async {
                                          if (widget.type == dry &&
                                              varietyData == null) {
                                            showToast("Choose variety first");
                                            return;
                                          }
                                          var r = await getScanResult(context);
                                          if (r == null) {
                                            showToast(
                                                "Make sure the correct QR is scanned!");
                                            return;
                                          }
                                          if (r.type != "bag") {
                                            showToast("Scan bag Id to proceed");
                                            return;
                                          }

                                          getBag(r.id!, model);
                                        }, "scale_add_day_sack")
                                      : Container(),
                              _sliderButton(
                                  widget.type == clean
                                      ? "Submit Clean Weight"
                                      : widget.type == wet
                                          ? "Submit batch wet Weight"
                                          : "Complete Batch", () async {
                                if (widget.type == wet &&
                                    model.updateDailyWetRequest!.items!
                                        .isEmpty) {
                                  showToast("Capture Weight to proceed");
                                  return;
                                }
                                await confirmBatchSubmission((() async {
                                  await updateBatch(model, context);
                                }));
                              }, "scale_batch_completed"),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 15,
                      top: 5,
                      child: FloatingActionButton(
                        foregroundColor: Colors.grey,
                        backgroundColor: Colors.white10.withOpacity(0.07),
                        onPressed: () async {
                          var canGoBack = await showExitPopup(model);
                          if (canGoBack) {
                            if (connection != null) {
                              connection!.dispose();
                              await connection!.close();
                            }
                            Navigator.pop(context);
                          }
                        },
                        mini: true,
                        tooltip: "go back home",
                        child: Icon(
                          Icons.keyboard_backspace_sharp,
                          color: fromHex(deepOrange),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          : const Loader(),
    );
  }

  Future<void> confirmBatchSubmission(Future Function() onConfirm) async {
    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              insetPadding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Confirm",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      const Row(
                        children: [
                          Expanded(
                            child: Text(
                              "This action will complete the current batch. \nDo you wish to continue? ",
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                Navigator.of(context).pop();
                                onConfirm();
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  Future<void> _initializeScale() async {
    if (await Permission.bluetoothScan.request().isGranted &&
        await Permission.bluetoothConnect.request().isGranted &&
        await Permission.locationWhenInUse.request().isGranted) {
      showToast("Please Wait Connecting to Scale...");
      setState(() {
        isConnecting = true;
      });
      try {
        connection = await BluetoothConnection.toAddress(macAddress);
        if (connection == null) {
          showToast(
              "Cannot connect, to scale \n Please check if scale is turned on");
          return;
        }
        setState(() {
          if (connection != null) {
            isConnected = connection?.isConnected == true;
            isConnecting = false;
          }
        });
        if (isConnected) {
          showToast("Connection to Scale Successful...");
          showToast("Please Wait Getting Readings...");
        }
        connection?.input?.listen((Uint8List data) {
          String reading = '';
          if (ascii.decode(data).trim().startsWith("+") ||
              ascii.decode(data).trim().endsWith("g")) {
            if (kDebugMode) {
              print(
                  'Data incoming: ${ascii.decode(data)} lenght - ${ascii.decode(data).length}');
            }
            String raw = ascii
                .decode(data)
                .trim()
                .replaceAll(" ", '')
                .replaceAll("+", '')
                .replaceAll("g", '')
                .replaceAll("\n", '');
            if (raw.length == 7) {
              double? r = double.tryParse(raw);
              if (r != null) {
                reading = r.toStringAsFixed(1);
              } //US,GS,+0002.1kg
            }
          }
          if (kDebugMode) {
            print(reading);
          }
          if (reading.isNotEmpty) {
            setState(() {
              if (_list.last.contains(reading)) {
                _stability.add(true);
              } else {
                _stability.clear();
              }
              setContent(reading);
              _list.add(reading);
              if (_stability.length > 10) {
                _disconnect();
              }
            });
          }
          if (ascii.decode(data).contains('!')) {
            connection?.finish(); // Closing connection
            showToast('Disconnecting from scale');
          }
        }).onDone(() {
          showToast('Disconnected from Scale');
        });
      } catch (exception) {
        // showToast(exception.toString(), i: 1);
        setState(() {
          isConnecting = false;
        });
        if (kDebugMode) {
          print(exception.toString());
        }

        showToast(
            "Cannot connect, to scale \n Please check if scale is turned on");
      }
    } else {
      showToast("Allow Permissions to proceed");
    }
  }

  void setContent(String reading) {
    setState(() {
      stableReading = reading;
    });
  }

  Future<void> _disconnect() async {
    try {
      if (connection != null) {
        isConnected = false;
        connection!.dispose();
        await connection!.close();
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> _showInputDialog() async {
    TextEditingController comment = TextEditingController();
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (BuildContext context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: StatefulBuilder(
          builder: (context, StateSetter setState) {
            return Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: comment,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      textAlignVertical: TextAlignVertical.top,
                      style: const TextStyle(color: Colors.white),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please Enter Verification Code";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white10,
                        isDense: true,
                        hintText: "eg AB:1*:**:**",
                        labelText: "Mac Address",
                        alignLabelWithHint: true,
                        labelStyle: TextStyle(color: fromHex(yellow)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: fromHex(yellow)),
                        ),
                      ),
                    ),
                    ListTile(
                      onTap: () async {
                        if (comment.text.isNotEmpty) {
                          setState(() {
                            macAddress = comment.text.toString();
                          });
                        }
                        Navigator.pop(context);
                      },
                      title: const Center(
                        child: Text(
                          'confirm',
                          style: TextStyle(color: Colors.deepOrange),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  _sliderButton(
    String buttonName,
    Function() onClickAction,
    String assetName,
  ) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        onClickAction();
      },
      child: InkWell(
        onTap: () async {
          onClickAction();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Card(
            color: Colors.white10.withOpacity(0.07),
            child: Container(
              // color: fromHex("#c19d3d"),
              width: MediaQuery.of(context).size.width - 30,

              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets1/$assetName.svg",
                        width: 36,
                        height: 36,
                        color: fromHex(yellow),
                      ),
                      const SizedBox(
                        width: 28,
                      ),
                      Text(
                        buttonName,
                        style: TextStyle(
                            color: fromHex(yellow),
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      )
                    ],
                  ),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: fromHex(yellow),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void getBag(
    String id,
    ReadWeightModel model,
  ) {
    print("################# $id");
    print("################# ${widget.bags?.length}");
    if (widget.type == wet) {
      var existingWet =
          widget.bags?.firstWhereOrNull((element) => element.bagId == id);
      if (existingWet == null) {
        model.addUnmarkedBagId(id);
        showToast(
            "Please scan correct bag, Bag has not been marked to be weighed");
        return;
      }
      if (model.updateDailyWetRequest?.plannerStageId == null) {
        model.setRequest(
            request: model.updateDailyWetRequest!
              ..image = existingWet.pictureName
              ..varietyName = existingWet.varietyName
              ..plannerStageId = existingWet.plannerStageId
              ..lineId = existingWet.lineId,
            key: "${widget.type}-${widget.bagId}");
      }
      if (existingWet.plannerStageId !=
          model.updateDailyWetRequest?.plannerStageId) {
        showToast("Please scan correct bag");
        return;
      } else {
        setState(() {
          bag = BagItems(bagId: id);
        });
      }
    } else if (widget.type == dry) {
      model.setRequest(
          request: model.updateDailyWetRequest!
            ..image = varietyData!.pictureName
            ..plannerStageId = varietyData!.plannerStageId
            ..varietyName = varietyData!.varietyName
            ..stageNage = varietyData!.stageName,
          key: "${widget.type}-${widget.bagId}");

      setState(() {
        bag = BagItems(bagId: id);
      });
    } else if (widget.type == clean) {
      var existingWet = widget.dryBatch;
      if (existingWet == null) {
        showToast("Please scan correct bag");
        return;
      } else {
        model.setCleanRequest(
            request: model.cleanWeightRequest!
              ..image = existingWet.pictureName
              ..batchId = existingWet.batchId
              ..bagId = existingWet.bagId
              ..lineId = existingWet.lineId,
            key: "${widget.type}-${widget.bagId}");
        setState(() {
          bag = BagItems(bagId: id);
        });
      }
    }
  }

  Future<void> updateBatch(ReadWeightModel model, BuildContext context) async {
    if (widget.type == wet) {
      if (checkValidity(model.updateDailyWetRequest)) {
        try {
          var response = await model.updateDailyWet();
          GeneralResponse gr = GeneralResponse.fromJson(response['response']);
          if (gr.error == false) {
            showToast(gr.message ?? "Success", i: 1);
            Navigator.of(context).pop();
            widget.onRefresh();
          } else {
            showToast(gr.erroMessage ?? "Failed", i: 1);
          }
        } catch (e) {
          showToast(e.toString());
        }
      }
    } else if (widget.type == dry) {
      if (checkValidity(model.updateDailyWetRequest)) {
        try {
          var response = await model.updateBatchWet();
          GeneralResponse gr = GeneralResponse.fromJson(response['response']);
          if (gr.error == false) {
            showToast(gr.message ?? "Success", i: 1);
            Navigator.of(context).pop();
            widget.onRefresh();
          } else {
            showToast(gr.erroMessage ?? "Failed", i: 1);
          }
        } catch (e) {
          showToast(e.toString());
        }
      }
    } else if (widget.type == clean) {
      if (checkCleanValidity(model.cleanWeightRequest)) {
        try {
          var response = await model.updateCleanWeight();
          GeneralResponse gr = GeneralResponse.fromJson(response['response']);
          if (gr.error == false) {
            showToast(gr.message ?? "Success", i: 1);
            Navigator.of(context).pop();
            widget.onRefresh();
          } else {
            showToast(gr.erroMessage ?? "Failed", i: 1);
          }
        } catch (e) {
          showToast(e.toString());
        }
      }
    }
  }

  _readingInfo(BuildContext context, ReadWeightModel model) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            isConnecting
                ? CircularProgressIndicator(
                    backgroundColor: fromHex(deepOrange),
                    valueColor: AlwaysStoppedAnimation<Color>(
                      fromHex(yellow),
                    ),
                  )
                : IconButton(
                    onPressed: () async {
                      try {
                        await _initializeScale();
                      } catch (e) {
                        showToast(e.toString());
                      }
                    },
                    icon: Icon(
                      Icons.refresh,
                      color: isConnected ? Colors.green : Colors.red,
                      size: 40,
                    ),
                  ),
            const SizedBox(
              width: 15,
            ),
            Text(
              _list.last,
              style: TextStyle(
                  fontSize: 50,
                  color: _stability.length > 5 ? fromHex(green) : Colors.white),
            ),
            const Padding(
              padding: EdgeInsets.only(bottom: 25),
              child: Text(
                "  g(s)",
                style: TextStyle(fontSize: 26),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            bag?.rating != null
                ? IconButton(
                    onPressed: () {
                      _disconnect();
                      setState(() {
                        bag!.weight = _list.last;
                      });
                      addBag(model);
                      setState(() {
                        bag = null;
                        _list.clear();
                        _list.add("0.00");
                        _stability.clear();
                      });
                    },
                    icon: Icon(
                      Icons.arrow_forward_outlined,
                      color: fromHex(yellow),
                      size: 40,
                    ),
                  )
                : Container(),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        bag != null
            ? RatingBar.builder(
                initialRating: 0,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: false,
                itemCount: 5,
                itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: fromHex(yellow),
                ),
                onRatingUpdate: (rating) {
                  setState(() {
                    bag!.rating = rating.toString();
                  });
                },
              )
            : Container(),
      ],
    );
  }

  _collectionInfo(
      {required BuildContext context,
      UpdateBatchRequest? request,
      CleanWeightRequest? cleanRequest,
      required ReadWeightModel model}) {
    if (request != null) {
      return Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, right: 10, left: 10),
            child: Card(
              color: Colors.black,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 25, top: 25),
                    decoration: BoxDecoration(
                      color: Colors.white10,
                      border:
                          Border.all(color: fromHex(yellow).withOpacity(0.3)),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8.0, top: 60, bottom: 8.0),
                      child: Column(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Line',
                                style: titleStyle(),
                              ),
                              Text(
                                request.lineId ?? "Not Set",
                                style: normalStyle(),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Variety Name',
                                style: titleStyle(),
                              ),
                              Text(
                                request.varietyName ?? "Not Set",
                                style: normalStyle(),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Stage Name',
                                style: titleStyle(),
                              ),
                              Text(
                                request.stageNage ?? "Not Set",
                                style: normalStyle(),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Sack',
                                style: titleStyle(),
                              ),
                              Text(
                                "${request.bagId}",
                                style: normalStyle(),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Total',
                                style: titleStyle(),
                              ),
                              Text(
                                "${request.total ?? 0.0} g",
                                style: normalStyle(),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: _bagWeights(request.items ?? [], model),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: varietyImage(pictureName: request.image),
          )
        ],
      );
    } else if (cleanRequest != null) {
      return Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, right: 10, left: 10),
            child: Card(
              color: Colors.black,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 25, top: 25),
                    decoration: BoxDecoration(
                      color: Colors.white10,
                      border:
                          Border.all(color: fromHex(yellow).withOpacity(0.3)),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8.0, top: 60, bottom: 8.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Line',
                                    style: titleStyle(),
                                  ),
                                  Text(
                                    "${cleanRequest.lineId}",
                                    style: normalStyle(),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    'Sack',
                                    style: titleStyle(),
                                  ),
                                  Text(
                                    "${cleanRequest.bagId}",
                                    style: normalStyle(),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    'Dirty Weight',
                                    style: titleStyle(),
                                  ),
                                  Text(
                                    "${cleanRequest.dirtyWeight ?? 0} g",
                                    style: normalStyle(),
                                  ),
                                  Text(
                                    'Clean Weight',
                                    style: titleStyle(),
                                  ),
                                  Text(
                                    "${cleanRequest.cleanWeight ?? 0} g",
                                    style: normalStyle(),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: varietyImage(pictureName: cleanRequest.image),
          )
        ],
      );
    } else {
      return Container();
    }
  }

  _bagWeights(
    List<BagItems> list,
    ReadWeightModel model,
  ) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: list.length,
      shrinkWrap: true,
      itemBuilder: (context, index) => _bagItem(context, list[index], model),
    );
  }

  _bagItem(BuildContext context, BagItems i, ReadWeightModel model) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: BoxDecoration(
        color: Colors.white10.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Bag Id   ",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "${i.bagId}",
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "${i.weight} g(s)",
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w100,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          model.setRequest(
                              request: model.updateDailyWetRequest!
                                ..items!.remove(i),
                              key: "${widget.type}-${widget.bagId}");
                        },
                        child: const Text(
                          "Remove",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.deepOrangeAccent,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  void addBag(ReadWeightModel model) {
    if (widget.type != clean) {
      if (model.updateDailyWetRequest!.items!.isNotEmpty) {
        var existingBag = model.updateDailyWetRequest?.items
            ?.firstWhereOrNull((element) => element.bagId == bag!.bagId);
        if (existingBag == null) {
          model.setRequest(
              request: model.updateDailyWetRequest!..items!.add(bag!),
              key: "${widget.type}-${widget.bagId}");
        } else {
          model.setRequest(
              request: model.updateDailyWetRequest!..items!.remove(existingBag),
              key: "${widget.type}-${widget.bagId}");
          model.setRequest(
              request: model.updateDailyWetRequest!..items!.add(bag!),
              key: "${widget.type}-${widget.bagId}");
        }
      } else {
        model.setRequest(
            request: model.updateDailyWetRequest!..items!.add(bag!),
            key: "${widget.type}-${widget.bagId}");
      }
    } else {
      model.setCleanRequest(
          request: model.cleanWeightRequest!..cleanWeight = bag!.weight,
          key: "${widget.type}-${widget.bagId}");
    }
  }

  bool checkValidity(UpdateBatchRequest? request) {
    if (request == null) {
      showToast("Request not set");
      return false;
    }
    if (request.bagId == null) {
      showToast("Bag id not set");
      return false;
    }
    if (request.plannerStageId == null) {
      showToast("Planner Stage Not Defined");
      return false;
    }
    if (request.userId == null) {
      showToast("User Id Not Set");
      return false;
    }
    if (request.items == null) {
      showToast("Bags not set");
      return false;
    }
    if (request.items!.isEmpty) {
      showToast("Bags not set");
      return false;
    }

    return true;
  }

  bool checkCleanValidity(CleanWeightRequest? request) {
    if (request == null) {
      showToast("Request not set");
      return false;
    }
    if (request.batchId == null) {
      showToast("Batch id not set");
      return false;
    }
    if (request.cleanWeight == null) {
      showToast("Clean Weight Not Defined");
      return false;
    }
    if (request.userId == null) {
      showToast("User Id Not Set");
      return false;
    }
    return true;
  }

  String getTitle(String type) {
    if (type == wet) {
      return "Add Daily Wet";
    } else if (type == dry) {
      return "Add Daily Batch";
    } else if (type == clean) {
      return "Select Batch Dry";
    } else {
      return "Add Daily Wet";
    }
  }

  Future<bool> showExitPopup(ReadWeightModel model) async {
    if (!model.hasData) {
      return true;
    }
    return await showDialog(
          //show confirm dialogue
          //the return value will be from "Yes" or "No" options
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Warning'),
            content: const Text(
                'Leaving this page may lead to loss of captured weights, Are you sure you would like to proceed?'),
            actions: [
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(false),
                //return false when click on "NO"
                child: const Text('No'),
              ),
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(true),
                //return true when click on "Yes"
                child: const Text('Yes'),
              ),
            ],
          ),
        ) ??
        false; //if showDialouge had returned null, then return false
  }

  void initData(ReadWeightModel model) {
    if (widget.type == clean) {
      var existingWet = widget.dryBatch;
      if (existingWet == null) {
        showToast("Please scan correct bag");
        return;
      }
      print(existingWet.toJson());
      CleanWeightRequest request = CleanWeightRequest();
      model.setCleanRequest(
          request: request
            ..userId = getUserId()
            ..image = existingWet.pictureName
            ..batchId = existingWet.batchId
            ..bagId = existingWet.bagId
            ..dirtyWeight = existingWet.dirtyWeight
            ..lineId = existingWet.lineId,
          key: "${widget.type}-${widget.bagId}");
      bag = BagItems(bagId: existingWet.bagId);
      print(bag?.toJson());
    }
  }

  Future<void> _dialogChooseVariety(
      Future Function(VarietyData variety) onConfirm) async {
    VarietyData? lineData;

    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ReadWeightModel>(
        onModelReady: (model) async {
          var hasConnection = await checkConnection();
          if (!hasConnection) {
            showToast("No internet connection");
            Navigator.of(context).pop();
          }
          try {
            await model.getVarieties();
          } catch (e) {
            showToast("Failed to get varieties");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose Variety",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownVarietySearch(
                                items: model.varieties,
                                onChanged: (VarietyData ld) => {
                                  setState(() {
                                    lineData = ld;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineData == null) {
                                    showToast("Choose variety");
                                    return;
                                  }
                                  onConfirm(lineData!);
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  void initializeRequestState(ReadWeightModel model) {
    if (widget.type == wet) {
      model.setRequest(
          request: UpdateBatchRequest(
              bagId: widget.bagId, userId: getUserId(), items: []),
          key: "${widget.type}-${widget.bagId}");
      // bag = BagItems(bagId: widget.bagId);
    } else if (widget.type != clean) {
      model.setRequest(
          request: UpdateBatchRequest(
              bagId: widget.bagId,
              userId: getUserId(),
              plannerStageId: null,
              items: []),
          key: "${widget.type}-${widget.bagId}");
    } else {
      initData(model);
    }
  }

  bool hasBeenInitialized(ReadWeightModel model) {
    return (((model.updateDailyWetRequest?.items ?? []).isEmpty) ||
        ((model.cleanWeightRequest?.cleanWeight ?? "").isEmpty ||
            (model.cleanWeightRequest?.cleanWeight ?? "").isEmpty));
  }
}
