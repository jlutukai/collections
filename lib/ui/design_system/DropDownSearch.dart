import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

import '../../../utils/useful.dart';

class CommonDropDownSearch<T> extends StatelessWidget {
  final List<T> items;
  final T? selectedItem;
  final bool showSearchBox;
  final bool isRequired;
  final bool showAsterisk;
  final Function(T?) onChanged;
  final String labelText;
  final String hint;
  final String searchHint;
  final String? validationText;
  final Widget? Function(T?)? itemWidget;
  final MenuType menuType;

  const CommonDropDownSearch({super.key,
    required this.items,
    this.selectedItem,
    this.showSearchBox = false,
    this.isRequired = false,
    this.showAsterisk = false,
    required this.onChanged,
    required this.labelText,
    required this.hint,
    this.searchHint = "",
    this.itemWidget,
    this.validationText,
    this.menuType = MenuType.BottomSheet
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: DropdownSearch<T>(
        items: items,
        onChanged: (u) {
          onChanged(u);
        },
        itemAsString: (u) => u.toString(),
        selectedItem: selectedItem,
        validator: (u) =>
        (u == null && isRequired)
            ? validationText ?? "Required field"
            : null,
        dropdownDecoratorProps: DropDownDecoratorProps(
          baseStyle: const TextStyle(color: Colors.white),
          dropdownSearchDecoration: InputDecoration(
            label: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: labelText,
                      style: TextStyle(
                          backgroundColor: Colors.transparent,
                          fontWeight: FontWeight.w400,
                          color: fromHex(deepOrange),
                          fontSize: 12)),
                  TextSpan(
                      text: (showAsterisk) ? ' * ' : ' ',
                      style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                          backgroundColor: Colors.transparent,
                          color: Colors.red)),
                ],
              ),
            ),
            hintText: hint,
            labelStyle: TextStyle(color: fromHex(yellow)),
            border: InputBorder.none,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: fromHex(yellow)),
            ),
            filled: true,
            fillColor: Colors.white10,
            isDense: true,
          ),
        ),
        popupProps: PopupProps.modalBottomSheet(
            showSearchBox: showSearchBox,
            itemBuilder: (context, T? u, bool isSelected) {
              if (itemWidget != null) {
                return GestureDetector(
                    onTap: () {
                      onChanged(u);
                      Navigator.pop(context);
                    },
                    child: itemWidget!(u)
                );
              } else {
                return ListTile(
                  title:
                  Text(u.toString(), style: const TextStyle(color: Colors.white)),
                  onTap: () {
                    onChanged(u);
                    Navigator.pop(context);
                  },
                );
              }
            },
            searchFieldProps: TextFieldProps(
                style: const TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                        borderRadius: const BorderRadius.all(
                            Radius.circular(20))),
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                        borderRadius: const BorderRadius.all(
                            Radius.circular(20))),
                    filled: true,
                    fillColor: Colors.white10,
                    isDense: true,
                    hintText: searchHint,
                    hintStyle: const TextStyle(color: Colors.white))),
            modalBottomSheetProps:  ModalBottomSheetProps(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                backgroundColor: Colors.grey.shade900)),
      ),
    );
  }
}

enum MenuType { BottomSheet, Menu }