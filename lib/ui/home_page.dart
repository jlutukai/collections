
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/viewmodels/home_model.dart';
import 'package:weight_collection/ui/collections/collections_page.dart';
import 'package:weight_collection/ui/scanManagement/Green_house_task/green_house_task.dart';
import 'package:weight_collection/ui/scanManagement/scan_management.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';
import 'package:weight_collection/utils/useful.dart';

import '../core/models/scan_bag_qr_body.dart';
import '../utils/handle_error.dart';
import 'base_view.dart';
import 'login_page.dart';

class HomePage extends StatefulWidget {
  static const tag = 'home';

  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  List<PlannerData> planners = [];
  final GlobalKey<ScaffoldState> _scaffoldKey12 = GlobalKey<ScaffoldState>();
  Shifts? theShift;
  String? date;

  @override
  Widget build(BuildContext context) {
    return BaseView<HomeModel>(
      onModelReady: (model) async {
        if (kDebugMode) {
          print("@@@ ${getTokenId()}");
        }
        var id = accountInfo.get('userId');
        Map<String, String> data = {
          'supervisor_id': "$id",
        };
        if (kDebugMode) {
          print(data);
        }
        // bool isConnected = await checkConnection();
        // if (isConnected) {
        //   var r = await model.getPlanners(data);
        //   if (r['success']) {
        //   } else {
        //     showErrorDialog(r['response'], context, "get planners");
        //   }
        // } else {
        //   showToast("No internet connection");
        //   await model.getPlanners(data);
        // }
        // if (model.planners != null) {
        //   if (model.planners!.isNotEmpty && model.planners!.length == 1) {
        //     setPlanner(model.planners!.first);
        //     model.initialize(model.planners!.first);
        //   }
        // }
      },
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.black,
        key: _scaffoldKey12,
        resizeToAvoidBottomInset: true,
        // resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () async {
                      bool isConnected = await checkConnection();
                      if(!isConnected){
                        showToast("Check internet connection");
                        return;
                      }

                      onLogout(model, context);
                    },
                    child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 5),
                        decoration: const BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(16))),
                        child: Row(
                          children: [
                            Text(
                              'Log Out',
                              style: TextStyle(color: fromHex(yellow)),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.exit_to_app_outlined,
                              color: fromHex(yellow),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
        body: model.state == ViewState.Idle
            ? Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Welcome, ',
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontWeight: FontWeight.w100,
                                  fontSize: 20),
                            ),
                            Text(
                              accountInfo.get('userName') ?? '',
                              style: TextStyle(
                                  color: fromHex(deepOrange),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: CachedNetworkImage(
                        imageUrl: "${getUrl()}/CUSTOM/Logo/logo_app.png",
                      ),
                    ),
                  ),
                  Container(
                    // padding: const EdgeInsets.symmetric(vertical:25.0,),
                    decoration: const BoxDecoration(
                      // color: Colors.white10.withOpacity(0.07),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              setState(() {});
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      const CollectionsPage()));
                            },
                            child: InkWell(
                              onTap: () async {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        const CollectionsPage()));
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#c19d3d"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets1/home_collection.svg",
                                              width: 36,
                                              height: 36,
                                              color: fromHex(yellow),
                                            ),
                                            const SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Collection",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              setState(() {});
                              try {
                                var result = await getScanResult(context);
                                if (result == null) {
                                  showToast(
                                      "Make sure the correct QR is scanned!");
                                  return;
                                }
                                goToScanMangement(result, context);
                              }catch(e){
                                print(e);
                              }
                            },
                            child: InkWell(
                              onTap: () async {
                                try {
                                  var result = await getScanResult(context);
                                  if (result == null) {
                                    showToast(
                                        "Make sure the correct QR is scanned!");
                                    return;
                                  }
                                  goToScanMangement(result, context);
                                }catch(e){
                                  print(e);
                                }
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#c19d3d"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets1/home_management.svg",
                                              width: 36,
                                              height: 36,
                                              color: fromHex(yellow),
                                            ),
                                            const SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Management",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          _sliderButton("My Tasks", () async {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                const GreenHouseTask()));
                          }, Icons.auto_graph),
                          const SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SvgPicture.asset(
                    "assets/footer.svg",
                    width: MediaQuery.of(context).size.width,
                  ),
                ],
              )
            : Column(
                children: [
                  Expanded(
                      child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                            height: 200,
                            width: 200,
                            child: Lottie.asset('assets/loading.json')),
                        Text(
                          'Please wait \n Loading...',
                          style: TextStyle(color: fromHex(deepOrange)),
                        )
                      ],
                    ),
                  ))
                ],
              ),
      ),
    );
  }

  getShifts(HomeModel model, PlannerData planner) {
    model.initialize(planner);
  }


  Future<void> showShiftsDialog(PlannerData p, BuildContext context) async {
    final f = DateFormat('yyyy-MM-dd');

    List<String> reasons = ['None', 'Yesterday', 'Today', 'Tomorrow'];
    String? reason = 'None';

    List<String?> shifts = [];
    String? shift = 'None';
    shifts.add(shift);
    for (var element in p.shifts!) {
      shifts.add(
          "${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )");
    }
    await showDialog(
        context: _scaffoldKey12.currentContext!,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: StatefulBuilder(builder: (context, StateSetter setState) {
              return Container(
                color: Colors.transparent,
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 15,
                      ),
                      const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            child: Text(
                              'Choose Date',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 11,
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            color: Colors.white10,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0))),
                        child: DropdownButtonHideUnderline(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, bottom: 8.0, left: 5.0),
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                hint: const Text('Specify Date'),
                                isExpanded: true,
                                isDense: true,
                                value: reason,
                                dropdownColor: Colors.grey[800],
                                style: TextStyle(
                                  color: fromHex(yellow),
                                ),
                                onChanged: (String? val) {
                                  DateTime now = DateTime.now();

                                  if (val == 'Yesterday') {
                                    setState(() {
                                      DateTime y =
                                          now.subtract(const Duration(days: 1));
                                      date = f.format(y);
                                    });
                                  }
                                  if (val == 'Today') {
                                    setState(() {
                                      date = f.format(now);
                                    });
                                  }
                                  if (val == 'Tomorrow') {
                                    setState(() {
                                      DateTime t =
                                          now.add(const Duration(days: 1));
                                      date = f.format(t);
                                    });
                                  }
                                  if (kDebugMode) {
                                    print(date);
                                  }
                                  setState(() {
                                    reason = val;
                                  });
                                },
                                items: reasons.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            child: Text(
                              'Choose Shift',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 11,
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            color: Colors.white10,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0))),
                        child: DropdownButtonHideUnderline(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, bottom: 8.0, left: 5.0),
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                hint: const Text('Specify shift'),
                                isExpanded: true,
                                isDense: true,
                                value: shift,
                                dropdownColor: Colors.grey[800],
                                style: TextStyle(
                                  color: fromHex(yellow),
                                ),
                                onChanged: (String? val) {
                                  setState(() {
                                    shift = val;

                                    for (var element in p.shifts!) {
                                      if ("${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )" ==
                                          val) {
                                        theShift = element;
                                      }
                                    }
                                  });
                                  if (kDebugMode) {
                                    print(theShift!.id);
                                  }
                                },
                                items: shifts.map<DropdownMenuItem<String>>(
                                    (String? value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value!),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      ListTile(
                        onTap: () {
                          if (date == null) {
                            showToast("Please choose date");
                            return;
                          }
                          if (date == 'None') {
                            showToast("Please choose date");
                            return;
                          }
                          if (theShift == null) {
                            showToast("Please choose shift");
                            return;
                          }
                          print("$date  ${theShift!.id}");

                          Navigator.pop(context);
                        },
                        title: const Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            })));
  }

  _sliderButton(
      String buttonName,
      Function() onClickAction,
      IconData iconData,
      ) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        onClickAction();
      },
      child: InkWell(
        onTap: () async {
          onClickAction();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Card(
            color: Colors.white10.withOpacity(0.07),
            child: Container(
              // color: fromHex("#c19d3d"),
              height: 60,
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        iconData,
                        size: 36,
                        color: fromHex(yellow),
                      ),
                      const SizedBox(
                        width: 60,
                      ),
                      Text(
                        buttonName,
                        style: TextStyle(
                            color: fromHex(yellow),
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      )
                    ],
                  ),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: fromHex(yellow),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void goToScanMangement(ScanBagQRBody result, BuildContext context) {
    Navigator.of(context).pushNamed(ScanManagement.tag, arguments: result);
  }

  Future<void> onLogout(HomeModel model, BuildContext context) async {
    Map<String, String> data1 = {
      'token_id': "${getTokenId()}",
    };
    var result = await model
        .removeToken(data1);
    if (result.isRight) {
      if(result.right.error==false) {
        showToast("success");
        accountInfo.delete('userName');
        accountInfo.delete('userId');
        Navigator.pushNamedAndRemoveUntil(context, LoginPage.tag,
                (Route<dynamic> route) => false);
      }else{
        showToast(result.right.erroMessage??"Failed to remove token");
      }
    } else {
      handleError(result.left);
    }
  }
}
//line img
//assign worker
//remove treatment
