import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/viewmodels/login_model.dart';
import 'package:weight_collection/utils/buttons.dart';
import 'package:weight_collection/utils/useful.dart';

import '../core/services/push_notifications_service.dart';
import '../locator.dart';
import '../utils/handle_error.dart';
import 'base_view.dart';
import 'home_page.dart';

class LoginPage extends StatefulWidget {
  static const tag = 'login';

  const LoginPage({super.key});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isHidden = true;
  String pass = "";
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  PushNotificationService fcm = locator<PushNotificationService>();

  @override
  void initState() {
    // fcm.getFCMToken();
    checkPermissions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginModel>(
      builder: (context, model, child) => Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.black,
        body: Stack(
          children: [
            SingleChildScrollView(
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CachedNetworkImage(
                            imageUrl: "${getUrl()}/CUSTOM/Logo/logo_app.png",
                          )
                        ],
                      ),
                    ),
                    Expanded(
                        child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 25),
                            child: TextFormField(
                              controller: _userName,
                              keyboardType: TextInputType.text,
                              style: const TextStyle(color: Colors.white),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Please Enter user name";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white10,
                                isDense: true,
                                hintText: "John Doe",
                                labelText: "Username",
                                labelStyle: TextStyle(color: fromHex(yellow)),
                                hintStyle: TextStyle(
                                  color: Colors.blueGrey[400],
                                ),
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: fromHex(yellow)),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 25),
                            padding: const EdgeInsets.only(top: 15),
                            child: TextFormField(
                              controller: _password,
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: _isHidden,
                              style: const TextStyle(color: Colors.white),
                              onChanged: (val) {
                                pass = val;
                                setState(() {});
                              },
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Please your password";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white10,
                                isDense: true,
                                labelText: "Password",
                                labelStyle: TextStyle(color: fromHex(yellow)),
                                hintText: "Your password",
                                hintStyle: TextStyle(
                                  color: Colors.blueGrey[400],
                                ),
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: fromHex(yellow)),
                                ),
                                suffixIcon: pass.isNotEmpty
                                    ? IconButton(
                                        onPressed: _toggleVisibility,
                                        icon: _isHidden
                                            ? Icon(
                                                Icons.visibility_off,
                                                color: Colors.grey[400],
                                              )
                                            : Icon(
                                                Icons.visibility,
                                                color: fromHex(yellow),
                                              ),
                                      )
                                    : null,
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(
                                horizontal: 25, vertical: 20),
                            padding: const EdgeInsets.only(top: 20),
                            child: model.state == ViewState.Idle
                                ? commonButton(
                                    buttonName: "Login",
                                    onClickAction: () async {
                                      if (await checkConnection()) {
                                        onLogin(model, context);
                                      } else {
                                        showToast("check internet connection");
                                      }
                                    })
                                : const Center(
                                    child: CupertinoActivityIndicator(),
                                  ),
                          ),
                        ],
                      ),
                    )),
                    SvgPicture.asset(
                      "assets/footer.svg",
                      width: MediaQuery.of(context).size.width,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }


  Future<void> onLogin(LoginModel model, BuildContext context) async {
    await fcm.getFCMToken();
    if (_formKey.currentState!.validate()) {
      FormData data = FormData.fromMap({
        'username': _userName.text,
        'password': _password.text,
      });
      var result = await model.login(data);
      if(result.isRight){
        var r = result.right;
        if (r.loginData != null) {
          saveUser(r.loginData?.data?.id??"", r.loginData?.data?.name??"");
          await _updateToken(model, r.loginData?.data?.id??"", context);
        } else {
          showToast(r.message ?? "Failed");
        }
      } else {
        handleError(result.left);
      }
    }
  }

  Future<void> _updateToken(LoginModel model, String id, BuildContext context) async {
    Map<String, String> data1 = {
      'token_id': "${getTokenId()}",
      'user_id': id,
    };
    if (kDebugMode) {
      print("@@@@@ ${getTokenId()}");
    }
    var result = await model
        .updateToken(data1);
    if (result.isRight) {
      if(result.right.error==false) {
        showToast("success");
        Navigator.pushNamedAndRemoveUntil(
            context, HomePage.tag, (Route<dynamic> route) => false);
      }else{
        showToast(result.right.erroMessage??"Failed to update token");
      }
    } else {
      handleError(result.left);
    }
  }

  void checkPermissions() {
    Permission.notification.request();
  }
}
