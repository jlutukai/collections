import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/viewmodels/ReportFormModel.dart';
import 'package:weight_collection/utils/useful.dart';

import 'FormQuestionsResponse.dart';
import 'base_view.dart';

class ReportForms extends StatefulWidget {
  final int id;
  final int? taskId;
  final int? plannerStageId;
  final String type;

  const ReportForms(
      {super.key, required this.id, required this.type, this.plannerStageId, this.taskId});

  @override
  _ReportFormsState createState() => _ReportFormsState();
}

class _ReportFormsState extends State<ReportForms> {
  ReportFormsModel? _model;

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportFormsModel>(
        onModelReady: (model) async {
          _model = model;
          model.getForms();
        },
        builder: (context, model, child) => Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Forms',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: model.state == ViewState.Idle
                            ? _formsList(model, context)
                            : Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(
                                        height: 200,
                                        width: 200,
                                        child: Lottie.asset(
                                            'assets/loading.json')),
                                    Text(
                                      'Please wait \n Loading...',
                                      style:
                                          TextStyle(color: fromHex(deepOrange)),
                                    )
                                  ],
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            ));
  }

  _formsList(ReportFormsModel model, BuildContext context) {
    return model.forms.isNotEmpty
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemCount: model.forms.length,
            itemBuilder: (context, index) => InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => FormQuestions(
                        formData: model.forms[index],
                        id: widget.id,
                        type: widget.type,
                        plannerStageId: widget.plannerStageId ?? 0),
                  ),
                );
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.white10.withOpacity(0.07),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width - 120,
                                child: Text(
                                  "${index + 1} ${model.forms[index].name}",
                                  style: const TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width - 100,
                                  child: Text(
                                    model.forms[index].description ?? '',
                                    maxLines: 30,
                                    style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w100,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ))
                  ],
                ),
              ),
            ),
          )
        : const Center(
            child: Text('No Forms Added Yet'),
          );
  }
}
