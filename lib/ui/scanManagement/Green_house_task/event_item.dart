import 'package:flutter/material.dart';
import 'package:weight_collection/core/models/get_task_appraisal.dart';
import 'package:weight_collection/core/models/scan_bag_qr_body.dart';

import '../../../utils/useful.dart';
import '../utils.dart';

class EventItem extends StatelessWidget {
  final EventData event;
  final Future<void> Function(
      {int? formId, required ScanBagQRBody result, int? taskId}) onOpenReports;
  final Function(int? taskId) onCloseTask;
  final Function(int? taskId) onDelayTask;

  const EventItem(
      {super.key,
      required this.event,
      required this.onOpenReports,
      required this.onCloseTask,
      required this.onDelayTask});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (event.formId == null) {
          onCloseTask(int.tryParse(event.taskId ?? ""));
        } else {
          try {
            var result = await getScanResult(context);
            if (result == null) {
              showToast("Make sure the correct QR is scanned!");
              return;
            }
            onOpenReports(
                result: result,
                taskId: int.tryParse(event.taskId ?? ""),
                formId: int.tryParse(event.formId ?? ""));
          } catch (e) {
            print(e);
          }
        }
      },
      onLongPress: () {
        onDelayTask(int.tryParse(event.taskId ?? ""));
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 2, horizontal: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.grey[900],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              event.title ?? "",
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            Text(
              event.description ?? "",
              style: const TextStyle(
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
