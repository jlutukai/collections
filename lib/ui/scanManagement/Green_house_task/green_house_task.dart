import 'package:calendar_view/calendar_view.dart';
import 'package:cupertino_tabbar/cupertino_tabbar.dart' as CupertinoTabBar;
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dtp;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:weight_collection/core/models/get_bed_line_items_response.dart';
import 'package:weight_collection/core/models/get_task_appraisal.dart';
import 'package:weight_collection/core/models/scan_bag_qr_body.dart';
import 'package:weight_collection/utils/useful.dart';

import '../../../core/enums/view_state.dart';
import '../../../core/models/get_forms_response.dart';
import '../../../core/viewmodels/scan_greenhouse_model.dart';
import '../../../core/viewmodels/scan_type_bed.dart';
import '../../../utils/loader.dart';
import '../../FormQuestionsResponse.dart';
import '../../base_view.dart';
import '../utils.dart';
import 'event_item.dart';

class GreenHouseTask extends StatefulWidget {
  const GreenHouseTask({super.key});

  @override
  State<GreenHouseTask> createState() => _GreenHouseTaskState();
}

class _GreenHouseTaskState extends State<GreenHouseTask> {
  final _controller = EventController();
  final _monthViewKey = GlobalKey<MonthViewState>();
  final TextEditingController _search = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int cupertinoTabBarIValue = 0;

  int cupertinoTabBarIValueGetter() => cupertinoTabBarIValue;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ScanGreenHouseModel>(
      onModelReady: (model) async {
        await onGetData(model);
        // await model.scanGreenHouse({"greenhouse_id": widget.id});
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              appBar: AppBar(
                backgroundColor: Colors.black,
                automaticallyImplyLeading: false,
                centerTitle: true,
                leading: FloatingActionButton(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.white10.withOpacity(0.07),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  mini: true,
                  tooltip: "go back home",
                  child: Icon(
                    Icons.keyboard_backspace_sharp,
                    color: fromHex(deepOrange),
                  ),
                ),
                title: Text(
                  'Task / Actions',
                  style: TextStyle(color: fromHex(deepOrange)),
                ),
              ),
              body: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          const SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: _search,
                              keyboardType: TextInputType.text,
                              style: const TextStyle(color: Colors.white),
                              onChanged: (v) {
                                model.selectedDate = null;
                                model.searchKey = v;
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white10,
                                isDense: true,
                                hintText: "search here...",
                                labelText: "Search tasks by title",
                                labelStyle: TextStyle(color: fromHex(yellow)),
                                hintStyle: TextStyle(
                                  color: Colors.blueGrey[400],
                                ),
                                prefixIcon: const Icon(Icons.search),
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: fromHex(yellow)),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                        ],
                      ),
                      CupertinoTabBar.CupertinoTabBar(
                        Colors.black,
                        fromHex(yellow),
                        [
                          Text(
                            "Monthly",
                            style: TextStyle(
                              color: cupertinoTabBarIValue == 0
                                  ? Colors.black
                                  : Colors.white,
                              fontSize: 18.75,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SFProRounded",
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            "Weekly",
                            style: TextStyle(
                              color: cupertinoTabBarIValue == 1
                                  ? Colors.black
                                  : Colors.white,
                              fontSize: 18.75,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SFProRounded",
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                        cupertinoTabBarIValueGetter,
                        (int index) {
                          setState(() {
                            cupertinoTabBarIValue = index;
                          });
                        },
                        useSeparators: false,
                        // allowScrollable: true,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Expanded(
                        flex: 1,
                        child: cupertinoTabBarIValue == 0
                            ? _monthView(model)
                            : _weekView(model, context),
                      ),
                      Text(getTitle(model),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          )),
                      Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 100),
                            child: _eventList(
                                model.events, model.selectedDate, model),
                          )),
                    ],
                  ),
                ),
              ))
          : const Loader(),
    );
  }

  Future<void> onGetTasks(
      {required void Function() onSuccess,
      required ScanGreenHouseModel model,
      required void Function(dynamic title, dynamic message) onError}) async {
    try {
      Map<String, String> data = {'user_id': getUserId() ?? ""};
      var response = await model.getTasks(data);
      if (response.isRight) {
        onSuccess();
      } else {
        onError("Error Getting Tasks", response.left.message);
      }
    } catch (e) {
      onError("Error Getting Tasks", e.toString());
    }
  }

  void addEvent(List<AppraisalData> tasks) {
    for (var event in _controller.events) {
      CalendarControllerProvider.of(context).controller.remove(event);
      _controller.remove(event);
    }
    for (var element in tasks) {
      if ((element.deadline ?? "").trim().isNotEmpty) {
        final event = CalendarEventData(
          date: DateTime.tryParse((element.deadline ?? "01-01-2024").trim()) ??
              DateTime.now(),
          endDate:
              DateTime.tryParse((element.deadline ?? "01-01-2024").trim()) ??
                  DateTime.now(),
          event: element,
          startTime:
              DateTime.tryParse((element.deadline ?? "01-01-2024").trim()) ??
                  DateTime.now(),
          endTime: DateTime.tryParse((element.deadline ?? "01-01-2024").trim())
                  ?.add(const Duration(hours: 23)) ??
              DateTime.now().add(const Duration(hours: 8)),
          title: element.title ?? "",
          description: element.taskDescription ?? "",
          titleStyle: const TextStyle(color: Colors.black),
        );
        CalendarControllerProvider.of(context).controller.add(event);
        _controller.add(event);
      }
    }
  }

  _eventList(List<EventData> events, DateTime? selectedDate,
      ScanGreenHouseModel model) {
    return events.isEmpty
        ? Center(
            child: Text(_search.text.isNotEmpty
                ? "No Tasks Found Try Searching Using Another Keyword"
                : selectedDate == null
                    ? "No Task Today"
                    : "No Task on ${getMonth(selectedDate.month)} ${selectedDate.day}, ${selectedDate.year}"),
          )
        : ListView.builder(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            itemCount: events.length,
            itemBuilder: (context, index) => EventItem(
                event: events[index],
                onOpenReports: ({
                  required ScanBagQRBody result,
                  int? taskId,
                  int? formId,
                }) async {
                  FormsData? form = await model.getFormData(formId);
                  if (form == null) {
                    showToast("Form not found");
                  } else {
                    goToForms(
                        result: result,
                        taskId: taskId,
                        formId: formId,
                        form: form,
                        model: model);
                  }
                },
                onCloseTask: (int? taskId) {
                  onCloseTaskDialog(onConfirm: (String comments) async {
                    onCloseTask(
                        onSuccess: () async {
                          showToast("Task Closed Successfully");
                          await onGetData(model);
                        },
                        onError: (title, message) {
                          showErrorDialog(title, context, message);
                        },
                        model: model,
                        data: {
                          "comments": comments,
                          "taskId": taskId,
                          "answered_by": getUserId()
                        });
                  });
                },
                onDelayTask: (int? taskId) {
                  onDelayTaskDialog(
                      taskId: taskId,
                      onConfirm: (data) async {
                        onDelayTask(
                            model: model,
                            data: data,
                            onSuccess: () async {
                              showToast("Task Delayed Successfully");
                              await onGetData(model);
                            },
                            onError: (title, message) {
                              showErrorDialog(title, context, message);
                            });
                      });
                }));
  }

  Future<void> getPlannerStageIdDialog(
      {required Future<void> Function(int plannerStageId) onConfirm,
      required String bedId}) async {
    int? lineDataId;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": bedId});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose Line Item",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm(lineDataId!);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> onCloseTaskDialog(
      {required Future<void> Function(String comments) onConfirm}) async {
    final TextEditingController comments = TextEditingController();

    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              insetPadding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Close Task",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                controller: comments,
                                keyboardType: TextInputType.text,
                                style: const TextStyle(color: Colors.white),
                                minLines: 5,
                                maxLines: 5,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white10,
                                  isDense: true,
                                  hintText: "Enter Your Comments Here...",
                                  labelText: "Comments",
                                  labelStyle: TextStyle(color: fromHex(yellow)),
                                  hintStyle: TextStyle(
                                    color: Colors.blueGrey[400],
                                  ),
                                  border: InputBorder.none,
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: fromHex(yellow)),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                Navigator.of(context).pop();
                                onConfirm(comments.text);
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  Future<void> onDelayTaskDialog(
      {required Future<void> Function(Map<String, dynamic> data) onConfirm,
      required int? taskId}) async {
    final TextEditingController reason = TextEditingController();
    final TextEditingController deadline = TextEditingController();

    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              insetPadding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Close Task",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        controller: deadline,
                        keyboardType: TextInputType.number,
                        style: const TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please Enter task's deadline";
                          }
                          return null;
                        },
                        enableInteractiveSelection: false,
                        autofocus: false,
                        readOnly: true,
                        onTap: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime: DateTime.now(),
                              maxTime: DateTime.now()
                                  .add(const Duration(days: 94200)),
                              theme: dtp.DatePickerTheme(
                                headerColor: Colors.grey.shade900,
                                backgroundColor: Colors.grey.shade900,
                                itemStyle: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                                cancelStyle: const TextStyle(
                                  color: Colors.deepOrange,
                                  fontSize: 24,
                                ),
                                doneStyle: TextStyle(
                                  color: Colors.green.shade700,
                                  fontSize: 24,
                                ),
                              ), onChanged: (date) {
                            print('change $date in time zone ${date.timeZoneOffset.inHours}');
                          }, onConfirm: (date) {
                            setState(() {
                              deadline.text = df.format(date);
                            });
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.en);
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white10,
                          isDense: true,
                          hintText: "",
                          labelText: "Deadline",
                          labelStyle: TextStyle(color: fromHex(yellow)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        controller: reason,
                        keyboardType: TextInputType.text,
                        style: const TextStyle(color: Colors.white),
                        minLines: 5,
                        maxLines: 5,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white10,
                          isDense: true,
                          hintText: "Enter Your Comments Here...",
                          labelText: "Comments",
                          labelStyle: TextStyle(color: fromHex(yellow)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                if (deadline.text.isEmpty) {
                                  showToast("Please provide new deadline");
                                  return;
                                }
                                if (reason.text.isEmpty) {
                                  showToast("Please provide reason");
                                  return;
                                }
                                Navigator.of(context).pop();
                                onConfirm({
                                  "user_id": getUserId(),
                                  "task_id": taskId,
                                  "deadline": deadline.text,
                                  "reason": reason.text
                                });
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  getMonth(int month) {
    switch (month) {
      case 1:
        return "January";
      case 2:
        return "Febuary";
      case 3:
        return "March";
      case 4:
        return "April";
      case 5:
        return "May";
      case 6:
        return "June";
      case 7:
        return "July";
      case 8:
        return "August";
      case 9:
        return "Septemper";
      case 10:
        return "October";
      case 11:
        return "November";
      case 12:
        return "December";
    }
  }

  String getEventsCount(int length) {
    return length <= 0 ? "No " : "$length ";
  }

  String getTitle(ScanGreenHouseModel model) {
    return (model.searchKey ?? "").isNotEmpty
        ? "Searched Tasks"
        : model.selectedDate == null
            ? "${getEventsCount(model.events.length)}Tasks Today"
            : "${getEventsCount(model.events.length)}Tasks for ${getMonth(model.selectedDate!.month)} ${model.selectedDate!.day}, ${model.selectedDate!.year}";
  }

  Future<void> goToForms(
      {required ScanBagQRBody result,
      int? taskId,
      int? formId,
      required FormsData form,
      required ScanGreenHouseModel model}) async {
    BuildContext mContext = _scaffoldKey.currentContext ?? context;
    if (result.type == "bed") {
      await getPlannerStageIdDialog(
          onConfirm: (plannerStageId) async {
            var r = await Navigator.of(mContext).push(
              MaterialPageRoute(
                builder: (BuildContext context) => FormQuestions(
                  id: int.parse(result.id ?? ""),
                  type: "bed",
                  plannerStageId: plannerStageId,
                  taskId: taskId,
                  formData: form,
                ),
              ),
            );
          },
          bedId: result.id!);
      await onGetData(model);
    } else {
      var r = await Navigator.of(mContext).push(
        MaterialPageRoute(
          builder: (BuildContext context) => FormQuestions(
              id: int.parse(result.id ?? ""),
              type: result.type ?? "",
              taskId: taskId,
              formData: form),
        ),
      );
      await onGetData(model);
    }
  }

  Future<void> onCloseTask(
      {required ScanGreenHouseModel model,
      required Map<String, dynamic> data,
      required void Function() onSuccess,
      required void Function(dynamic title, dynamic message) onError}) async {
    try {
      var response = await model.closeTask(data);
      if (response.isRight) {
        onSuccess();
      } else {
        onError("Error Closing Tasks", response.left.message);
      }
    } catch (e) {
      onError("Error Closing Tasks", e.toString());
    }
  }

  Future<void> onDelayTask(
      {required ScanGreenHouseModel model,
      required Map<String, dynamic> data,
      required void Function() onSuccess,
      required void Function(dynamic title, dynamic message) onError}) async {
    try {
      var response = await model.delayTask(data);
      if (response.isRight) {
        onSuccess();
      } else {
        onError("Error Delaying Tasks", response.left.message);
      }
    } catch (e) {
      onError("Error Delaying Tasks", e.toString());
    }
  }

  Future<void> onGetData(ScanGreenHouseModel model) async {
    await model.getForms();
    await onGetTasks(
        onSuccess: () {},
        model: model,
        onError: (title, message) {
          showErrorDialog(title, context, message);
        });
    addEvent(model.tasks);
  }

  _monthView(ScanGreenHouseModel model) {
    return MonthView(
      key: _monthViewKey,
      controller: _controller,
      showBorder: false,
      minMonth: DateTime(1990),
      maxMonth: DateTime(2050),
      initialMonth: DateTime.now(),
      cellAspectRatio: 1,
      startDay: WeekDays.sunday,
      weekDayBuilder: (index) {
        return WeekDayTile(
          dayIndex: index,
          backgroundColor: Colors.black,
          displayBorder: false,
          textStyle: const TextStyle(color: Colors.white, fontSize: 17),
        );
      },
      cellBuilder: (date, events, isToday, isInMonth) {
        return Container(
          color: Colors.black,
          child: Column(
            children: [
              const SizedBox(
                height: 5.0,
              ),
              CircleAvatar(
                radius: 11,
                backgroundColor: isToday ? Colors.white : Colors.transparent,
                child: Text(
                  "${date.day}",
                  style: TextStyle(
                    color: isToday
                        ? Colors.black
                        : isInMonth
                            ? Colors.white
                            : Colors.white.withOpacity(0.4),
                    fontSize: 12,
                  ),
                ),
              ),
              if (events.isNotEmpty)
                Container(
                  decoration: BoxDecoration(
                    color: fromHex(deepOrange),
                    shape: BoxShape.circle,
                  ),
                  margin: const EdgeInsets.symmetric(
                      vertical: 3.0, horizontal: 3.0),
                  padding: const EdgeInsets.all(2.0),
                  alignment: Alignment.center,
                  child: Text(
                    "${events.length ?? 0}",
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                )
            ],
          ),
        );
      },
      onCellTap: (events, date) {
        setState(() {
          _search.clear();
        });
        model.searchKey = null;
        model.selectedDate = date;
        model.addEvents(events);
      },
      headerStringBuilder: (date, {secondaryDate}) =>
          "${getMonth(date.month)} ${date.year}",
      headerStyle: const HeaderStyle(
        rightIcon: Icon(
          Icons.arrow_forward_ios,
          color: Colors.white,
        ),
        leftIcon: Icon(
          Icons.arrow_back_ios,
          color: Colors.white,
        ),
        headerTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
        decoration: BoxDecoration(
          color: Colors.black,
        ),
      ),
    );
  }

  _weekView(ScanGreenHouseModel model, BuildContext context) {
    final List<String> weekTitles = ["M", "T", "W", "T", "F", "S", "S"];
    return WeekView(
      controller: _controller,
      eventTileBuilder: (date, events, boundry, start, end) {
        if (events.isNotEmpty) {
          return RoundedEventTile(
            borderRadius: BorderRadius.circular(6.0),
            title: "${events.length}",
            titleStyle: events[0].titleStyle ??
                TextStyle(
                  fontSize: 12,
                  color: events[0].color.accent,
                ),
            descriptionStyle: events[0].descriptionStyle,
            totalEvents: events.length,
            padding: const EdgeInsets.all(7.0),
            backgroundColor: events[0].color,
          );
        } else {
          return Container();
        }
      },
      // fullDayEventBuilder: (events, date) {
      //   // Return your widget to display full day event view.
      //   return Container();
      // },
      showLiveTimeLineInAllDays: false,
      // To display live time line in all pages in week view.
      liveTimeIndicatorSettings: HourIndicatorSettings(color: fromHex(yellow)),
      width: MediaQuery.of(context).size.width,
      // width of week view.
      minDay: DateTime(1990),
      maxDay: DateTime(2050),
      initialDay: DateTime.now(),
      timeLineOffset: 3,
      heightPerMinute: 0.5,
      // height occupied by 1 minute time span.// To define how simultaneous events will be arranged.
      // onEventTap: (events, date) => print(events),
      onDateTap: (date) {
        setState(() {
          _search.clear();
        });
        model.searchKey = null;
        model.selectedDate = date;
        model.setEvents(date);
      },
      startDay: WeekDays.sunday,
      backgroundColor: Colors.black,
      headerStyle: const HeaderStyle(
          headerTextStyle: TextStyle(color: Colors.white, fontSize: 17),
          rightIcon: Icon(
            Icons.arrow_forward_ios,
            color: Colors.white,
          ),
          leftIcon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          decoration: BoxDecoration(
              color: Colors.black)), // To change the first day of the week.
    );
  }


}

//number of events
//search events
