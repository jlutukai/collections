import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/scan_staff_response.dart';

import '../../utils/useful.dart';

class BedItem extends StatefulWidget {
  final BedDetails bed;

  const BedItem(this.bed, {super.key});

  @override
  State<BedItem> createState() => _BedItemState();
}

class _BedItemState extends State<BedItem> {
  late BedDetails bed;

  @override
  void initState() {
    bed = widget.bed;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Container(
              height: 50,
              width: 50,
              margin: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: CachedNetworkImageProvider(
                      '${getUrl()}/CUSTOM/Variety/picture/${bed.pictureName}'),
                ),
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Stage",
                              style: _titleStyle(),
                            ),
                            Text(bed.stageName ?? "", style: _normalStyle()),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Bed",
                              style: _titleStyle(),
                            ),
                            Text(bed.bedName ?? "", style: _normalStyle()),
                          ],
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Bed Id",
                              style: _titleStyle(),
                            ),
                            Text(bed.bedId ?? "", style: _normalStyle()),
                            Text(
                              "Variety Id",
                              style: _titleStyle(),
                            ),
                            Text(bed.varietyName ?? "", style: _normalStyle())
                          ],
                        ),
                      ),
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Start Date ", style: _titleStyle()),
                          Text(Jiffy.parse(bed.startDate??"").yMMMd,
                              style: _normalStyle()),
                          Text("End Date ", style: _titleStyle()),
                          Text(Jiffy.parse(bed.endDate??"").yMMMd, style: _normalStyle())
                        ],
                      ))
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _titleStyle() {
    return TextStyle(
        fontWeight: FontWeight.bold, fontSize: 16, color: fromHex(yellow));
  }

  _normalStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
  }
}
