import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dtp;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:path_provider/path_provider.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';
import 'package:weight_collection/core/models/complete_stage_request.dart';
import 'package:weight_collection/core/models/get_species_response.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';
import 'package:weight_collection/utils/loader.dart';

import '../../core/enums/form_response_types.dart';
import '../../core/enums/view_state.dart';
import '../../core/models/get_bed_line_items_response.dart';
import '../../core/models/get_variety_response.dart';
import '../../core/models/local/localModels.dart';
import '../../core/viewmodels/CompleteStageModel.dart';
import '../../utils/useful.dart';
import '../base_view.dart';

class CompleteStagePage extends StatefulWidget {
  final String id;

  const CompleteStagePage({super.key, required this.id});

  @override
  State<CompleteStagePage> createState() => _CompleteStagePageState();
}

class _CompleteStagePageState extends State<CompleteStagePage> {
  int? lineDataId;
  final TextEditingController _comment = TextEditingController();
  final TextEditingController _date = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _picker = ImagePicker();
  final StopWatchTimer _stopWatchTimer = StopWatchTimer();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Text(
              'Complete Stage',
              style: TextStyle(
                  color: fromHex(deepOrange), fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            automaticallyImplyLeading: false,
          ),
          body: BaseView<CompleteStageModel>(
            onModelReady: (model) async {
              //get line items
              try {
                await model.getLineItems({"bed_id": widget.id});
              } catch (e) {
                showToast("Failed to get Line Items");
                Navigator.of(context).pop();
              }
            },
            builder: (context, model, child) => model.state == ViewState.Idle
                ? SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height-120,
                      child: Column(
                        children: [
                          Expanded(
                            child: _completeStageForm(
                                model: model, context: context),
                          ),
                          _buttons(context, model)
                        ],
                      ),
                    ),
                  )
                : const Loader(),
          ),
        ),
        Positioned(
          left: 15,
          top: 40,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: Colors.white10.withOpacity(0.07),
            onPressed: () {
              Navigator.pop(context);
            },
            mini: true,
            tooltip: "go back home",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(deepOrange),
            ),
          ),
        ),
      ],
    );
  }

  _completeStageForm(
      {required CompleteStageModel model, required BuildContext context}) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: commonDropDownSearch(
                items: model.lineData,
                onChanged: (LineData ld) => {
                  setState(() {
                    model.setLineData(ld);
                    lineDataId = int.tryParse(ld.plannerStageId ?? "");
                  })
                },
              ),
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        _questionsList(context: context, model: model),
        const SizedBox(
          height: 10,
        ),
        TextFormField(
          controller: _date,
          keyboardType: TextInputType.number,
          style: const TextStyle(color: Colors.white),
          validator: (value) {
            if (value!.isEmpty) {
              return "Please Enter task's due date";
            }
            return null;
          },
          enableInteractiveSelection: false,
          autofocus: false,
          readOnly: true,
          onTap: () {
            DatePicker.showDatePicker(context,
                showTitleActions: true,
                minTime: DateTime.now().subtract(const Duration(days: 200)),
                maxTime: DateTime.now().add(const Duration(days: 94200)),
                theme: dtp.DatePickerTheme(
                  headerColor: Colors.grey.shade900,
                  backgroundColor: Colors.grey.shade900,
                  itemStyle: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  cancelStyle: const TextStyle(
                    color: Colors.deepOrange,
                    fontSize: 24,
                  ),
                  doneStyle: TextStyle(
                    color: Colors.green.shade700,
                    fontSize: 24,
                  ),
                ), onChanged: (date) {
              print('change $date in time zone ${date.timeZoneOffset.inHours}');
            }, onConfirm: (date) {
              setState(() {
                _date.text = df.format(date);
              });
            }, currentTime: DateTime.now(), locale: LocaleType.en);
          },
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white10,
            isDense: true,
            hintText: "",
            labelText: "End Date",
            labelStyle: TextStyle(color: fromHex(yellow)),
            hintStyle: TextStyle(
              color: Colors.blueGrey[400],
            ),
            border: InputBorder.none,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: fromHex(yellow)),
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        TextFormField(
          controller: _comment,
          keyboardType: TextInputType.text,
          textCapitalization: TextCapitalization.sentences,
          maxLines: 6,
          textAlignVertical: TextAlignVertical.top,
          style: const TextStyle(color: Colors.white),
          validator: (value) {
            if (value!.isEmpty) {
              return "Please Enter Your Response";
            }
            return null;
          },
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white10,
            isDense: true,
            hintText: "Enter your comments here..",
            labelText: "Comments",
            alignLabelWithHint: true,
            labelStyle: TextStyle(color: fromHex(yellow)),
            hintStyle: TextStyle(
              color: Colors.blueGrey[400],
            ),
            border: InputBorder.none,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: fromHex(yellow)),
            ),
          ),
        ),
      ],
    );
  }

  _buttons(BuildContext context, CompleteStageModel model) {
    return Row(
      children: [
        Expanded(
          child: ListTile(
            title: const Text(
              "Cancel",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.deepOrange, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        Expanded(
          child: ListTile(
            title: const Text(
              "Confirm",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.greenAccent, fontWeight: FontWeight.bold),
            ),
            onTap: () async {
              if (!await checkConnection()) {
                showToast("Check internet connection");
                return;
              }
              if (lineDataId == null) {
                showToast("Please Specify Line Item");
                return;
              }
              if (await checkIfAllHaveBeenAnswered(model)) {
                showToast("please answer all questions");
                return;
              }
              if (_date.text.isEmpty) {
                showToast("Please Specify completion date");
                return;
              }
              onCompleteStage(
                  onSuccess: () {
                    Navigator.of(context).pop();
                  },
                  model: model);
            },
          ),
        ),
      ],
    );
  }

  _questionsList(
      {required BuildContext context, required CompleteStageModel model}) {
    return model.questions.isNotEmpty
        ? ListView.builder(
            shrinkWrap: true,
            itemCount: model.questions.length,
            itemBuilder: (context, index) => Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Dismissible(
                  key: UniqueKey(),
                  direction: DismissDirection.startToEnd,
                  onDismissed: (DismissDirection direction) async {
                    _inputDialog(context, model.questions[index], model);
                    setState(() {});
                  },
                  child: InkWell(
                    onTap: () async {
                      _inputDialog(context, model.questions[index], model);
                      setState(() {});
                    },
                    child: Container(
                      color: Colors.white10.withOpacity(0.07),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 3),
                      child: Row(
                        children: [
                          Container(
                            color: model.questions[index].response!.isNotEmpty
                                ? Colors.green
                                : Colors.yellow,
                            width: 2,
                            height: 75,
                          ),
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              child: ExpansionTile(
                                leading: Text(
                                  "${index + 1}",
                                  style: TextStyle(color: fromHex(yellow)),
                                ),
                                title: Text(
                                  "${model.questions[index].question}",
                                  style: const TextStyle(color: Colors.white),
                                ),
                                children: [
                                  Text(
                                    (model.questions[index].responseName ??
                                        (model.questions[index].response ??
                                            "")),
                                    style: TextStyle(color: fromHex(yellow)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )),
            ),
          )
        : Container();
  }

  void _inputDialog(BuildContext context, FormItems checklist,
      CompleteStageModel model) async {
    if (kDebugMode) {
      print("${checklist.responseType}");
    }
    if (checklist.responseType == null) {
      showToast("Question type is undefined");
      return;
    }
    if (checklist.responseType!.isEmpty) {
      showToast("Question type is empty");
      return;
    }
    if (checklist.responseType == FormResponseTypes.YES_NO_RESPONSE.name) {
      showYesNoDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.OPEN_TEXT_RESPONSE.name) {
      showOpenTextDialog(context, checklist, model);
    } else if (checklist.responseType == FormResponseTypes.NUMBER.name) {
      // Number
      showNumberResponseDialog(context, checklist, model);
    } else if (checklist.responseType == FormResponseTypes.RECORDING.name) {
      // Video
      showRecordingResponseDialog(context, checklist, model);
    } else if (checklist.responseType == FormResponseTypes.PICTURE.name) {
      //Picture
      showPictureResponseDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.SPECIES_DROPDOWN.name) {
      showSpeciesResponseDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.STAFF_DROPDOWN.name) {
      showStaffResponseDialog(context, checklist, model);
    } else if (checklist.responseType ==
        FormResponseTypes.VARIETY_DROPDOWN.name) {
      showVarietyResponseDialog(context, checklist, model);
    } else {
      showToast("Unsupported response type");
    }
  }

  Future<File?> getVideo(ImageSource imageSource) async {
    var p = await _picker.pickVideo(source: imageSource);
    if (p != null) {
      File video = File(p.path);
      return video;
    } else {
      return null;
    }
  }

  Future<File?> getPicture(ImageSource imageSource) async {
    var p = await _picker.pickImage(source: imageSource, imageQuality: 40);
    if (p != null) {
      File picture = File(p.path);
      return picture;
    } else {
      return null;
    }
  }

  Future<File?> _getFile(List<String> s) async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: s);

    if (result != null) {
      return File(result.files.single.path!);
    } else {
      return null;
    }
  }

  Future<File?> getAudioRecording(BuildContext context) async {
    File? audioFile;
    int scanTime = 0;
    bool isRecording = false;
    var tempDir = await getTemporaryDirectory();
    String path = '${tempDir.path}/${DateTime.now()}.aac';
    await showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: _scaffoldKey.currentContext!,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, StateSetter setState) => SingleChildScrollView(
              child: Container(
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[900],
                    // color: Theme.of(context).canvasColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        SizedBox(
                          height: 100,
                          width: 100,
                          child: Stack(
                            children: [
                              Lottie.asset('assets/audio.json',
                                  animate: isRecording),
                              const Align(
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.keyboard_voice_rounded,
                                  color: Colors.black,
                                  size: 36,
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0),
                          child: StreamBuilder<int>(
                            stream: _stopWatchTimer.rawTime,
                            initialData: _stopWatchTimer.rawTime.value,
                            builder: (context, snap) {
                              final value = snap.data!;
                              final displayTime = StopWatchTimer.getDisplayTime(
                                  value,
                                  hours: true);
                              return Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      displayTime,
                                      style: TextStyle(
                                        fontSize: 26,
                                        fontFamily: 'Helvetica',
                                        color: fromHex(yellow),
                                      ),
                                    ),
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8),
                                  //   child: Text(
                                  //     value.toString(),
                                  //     style: const TextStyle(
                                  //         fontSize: 16,
                                  //         fontFamily: 'Helvetica',
                                  //         fontWeight: FontWeight.w400),
                                  //   ),
                                  // ),
                                ],
                              );
                            },
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                onTap: () async {
                                  print(path);
                                },
                                title: Center(
                                  child: Text(
                                    isRecording ? 'Pause' : 'Start',
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                            isRecording
                                ? Expanded(
                                    child: ListTile(
                                    onTap: () async {
                                      if (isRecording) {
                                        // Recording recording =
                                        //     await AudioRecorder.stop();
                                        // print(
                                        //     "Path : ${recording.path},  Format : ${recording.audioOutputFormat},  Duration : ${recording.duration},  Extension : ${recording.extension},");
                                        // _stopWatchTimer.onExecute
                                        //     .add(StopWatchExecute.stop);
                                        // setState(() {
                                        //   audioFile = File(recording.path);
                                        //   isRecording = false;
                                        // });
                                      }
                                    },
                                    title: Center(
                                      child: Text(
                                        isRecording ? 'Stop' : '',
                                        style: TextStyle(
                                            color: fromHex(deepOrange),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ))
                                : Container(),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        });
    return audioFile;
  }

  Future<bool> checkIfAllHaveBeenAnswered(CompleteStageModel model) async {
    return model.questions
        .every((element) => (element.response ?? "").isEmpty);
  }

  Future<void> showYesNoDialog(BuildContext context, FormItems checklist,
      CompleteStageModel model) async {
    String yes = 'yes';
    String no = 'no';
    String? ans = '';
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ListTile(
                  title: Text(
                    "${checklist.question}",
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          Radio(
                              value: no,
                              groupValue: ans,
                              activeColor: fromHex(yellow),
                              onChanged: (dynamic v) {
                                setState(() {
                                  ans = v;
                                });
                              }),
                          const Text('No',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      Row(
                        children: [
                          Radio(
                              value: yes,
                              activeColor: fromHex(yellow),
                              // toggleable: true,
                              groupValue: ans,
                              onChanged: (dynamic v) {
                                setState(() {
                                  ans = v;
                                });
                              }),
                          const Text('Yes',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                    ],
                  ),
                ),
                ListTile(
                  onTap: () async {
                    if (ans!.isEmpty) {
                      showToast('Please select a response');
                      return;
                    }
                    model.setResponse(
                        formItemId: checklist.formItemId,
                        type: checklist.type,
                        response: "$ans");
                    Navigator.of(context).pop();
                  },
                  title: const Center(
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> showOpenTextDialog(BuildContext context, FormItems checklist,
      CompleteStageModel model) async {
    TextEditingController comment = TextEditingController();
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  TextFormField(
                    controller: comment,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: 6,
                    textAlignVertical: TextAlignVertical.top,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Response";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "Enter your response here..",
                      labelText: "Response",
                      alignLabelWithHint: true,
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    onTap: () async {
                      if (comment.text.isEmpty) {
                        showToast("Please fill response");
                        return;
                      }
                      model.setResponse(
                          formItemId: checklist.formItemId,
                          type: checklist.type,
                          response: comment.text);
                      Navigator.of(context).pop();
                    },
                    title: const Center(
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.deepOrange),
                      ),
                    ),
                  ),
                ],
              ),
            )));
  }

  Future<void> showNumberResponseDialog(BuildContext context,
      FormItems checklist, CompleteStageModel model) async {
    TextEditingController number = TextEditingController();
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  TextFormField(
                    controller: number,
                    keyboardType: TextInputType.number,
                    textAlignVertical: TextAlignVertical.top,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Response";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "e.g. 1",
                      labelText: "Response",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    onTap: () async {
                      if (number.text.isEmpty) {
                        showToast("Please fill response");
                        return;
                      }
                      model.setResponse(
                          formItemId: checklist.formItemId,
                          type: checklist.type,
                          response: number.text);

                      Navigator.of(context).pop();
                    },
                    title: const Center(
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.deepOrange),
                      ),
                    ),
                  ),
                ],
              ),
            )));
  }

  Future<void> showRecordingResponseDialog(BuildContext context,
      FormItems checklist, CompleteStageModel model) async {
    File? video;
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  Container(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        video = await getVideo(ImageSource.camera);
                        if (video == null) {
                          showToast("File not found try again!");
                          return;
                        }
                        for (var element in model.questions) {
                          if (checklist.formItemId == element.formItemId &&
                              checklist.type == element.type) {
                            setState(() {
                              element.response = video!.path;
                              element.isFile = true;
                            });
                          }
                        }
                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.video_call_outlined,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Start Recording',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const Divider(
                    thickness: 1,
                    color: Colors.grey,
                    indent: 40,
                    endIndent: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        video = await _getFile(['mp4']);
                        if (video == null) {
                          showToast("File not found try again!");
                          return;
                        }
                        model.setResponse(
                            formItemId: checklist.formItemId,
                            type: checklist.type,
                            response: video!.path,
                            isFile: true);

                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.storage,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Pick From Storage',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }

  Future<void> showPictureResponseDialog(BuildContext context,
      FormItems checklist, CompleteStageModel model) async {
    File? image;
    await showDialog(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: Text(
                      "${checklist.question}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  Container(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        image = await getPicture(ImageSource.camera);
                        if (image == null) {
                          showToast("File not found try again!");
                          return;
                        }
                        model.setResponse(
                            formItemId: checklist.formItemId,
                            type: checklist.type,
                            response: image!.path,
                            isFile: true);

                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.video_call_outlined,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Open Camera',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const Divider(
                    thickness: 1,
                    color: Colors.grey,
                    indent: 40,
                    endIndent: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ListTile(
                      onTap: () async {
                        image = await getPicture(ImageSource.gallery);
                        if (image == null) {
                          showToast("File not found try again!");
                          return;
                        }
                        model.setResponse(
                            formItemId: checklist.formItemId,
                            type: checklist.type,
                            response: image!.path,
                            isFile: true);

                        Navigator.of(context).pop();
                      },
                      leading: Icon(
                        Icons.storage,
                        color: fromHex(yellow),
                      ),
                      title: Text(
                        'Pick From Gallery',
                        style: TextStyle(color: fromHex(yellow)),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }

  Future<void> showSpeciesResponseDialog(BuildContext context,
      FormItems checklist, CompleteStageModel model) async {
    SpeciesData? species;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<CompleteStageModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getSpecies();
          } catch (e) {
            showToast("Failed to get species");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose species Item",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSpeciesSearch(
                                items: model.species,
                                onChanged: (SpeciesData ld) => {
                                  setState(() {
                                    species = ld;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (species == null) {
                                    showToast("Please Specify Species");
                                    return;
                                  }

                                  model.setResponse(
                                    formItemId: checklist.formItemId,
                                    type: checklist.type,
                                    response: species!.id ?? "",
                                    responseName: species!.name ?? "",
                                  );

                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  // void setSaved() {
  //   if (existingForm != null) {
  //     UpdateFormData? d = existingForm!..items = questions;
  //     savedResponses.put(
  //         "${widget.type}_${widget.formData.id}_${widget.id}", d);
  //   } else {
  //     setInit();
  //     UpdateFormData? d = existingForm!..items = questions;
  //     savedResponses.put(
  //         "${widget.type}_${widget.formData.id}_${widget.id}", d);
  //   }
  //   setState(() {});
  // }

  void showStaffResponseDialog(
      BuildContext context, FormItems checklist, CompleteStageModel model) {
    showToast("WIP");
  }

  Future<void> showVarietyResponseDialog(BuildContext context,
      FormItems checklist, CompleteStageModel model) async {
    VarietyData? variety;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<CompleteStageModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getVarieties();
          } catch (e) {
            showToast("Failed to get varieties");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose variety ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownVarietySearch(
                                items: model.varieties,
                                onChanged: (VarietyData ld) => {
                                  setState(() {
                                    variety = ld;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (variety == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  }

                                  model.setResponse(
                                    formItemId: checklist.formItemId,
                                    type: checklist.type,
                                    response: variety!.plannerStageId ?? "",
                                    responseName: variety!.varietyName ?? "",
                                  );
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> onCompleteStage(
      {required Function() onSuccess,
      required CompleteStageModel model}) async {
    try {
      var response = await model.completeStage(buildRequest(model)!.toJson());
      if (response.isRight && response.right.error == false) {
        showToast(response.right.message ?? "Success", i: 1);
        onSuccess();
      } else {
        showToast(response.left.message ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Update Status", i: 1);
    }
  }

  CompleteStageRequest? buildRequest(CompleteStageModel model) {


    try {
      return CompleteStageRequest(
        formId: int.tryParse(model.formId??""),
        answeredBy: int.tryParse(getUserId()??""),
        plannerStageId: int.tryParse(model.lineItem?.plannerStageId??""),
        date: _date.text,
        items: model.questions.isEmpty
            ? null
            : model.questions
                .map((e) => Responses(
                    formItemId: e.formItemId,
                    response: e.response,
                    type: e.type))
                .toList(),
        type: "bed",
        typeId: int.tryParse(widget.id),
        endDate: _date.text,
        userId: getUserId()
      );
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return null;
    }
  }
}
