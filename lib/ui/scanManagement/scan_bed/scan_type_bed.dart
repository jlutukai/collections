import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dtp;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/complete_stage_response.dart';
import 'package:weight_collection/core/models/general_response.dart';
import 'package:weight_collection/core/models/get_available_lines.dart';
import 'package:weight_collection/core/models/get_loss_reason_response.dart';
import 'package:weight_collection/core/models/get_plants_in_suspense_response.dart';
import 'package:weight_collection/core/viewmodels/scan_type_bed.dart';
import 'package:weight_collection/ui/reportForms.dart';
import 'package:weight_collection/ui/scanManagement/complete_stage.dart';
import 'package:weight_collection/ui/scanManagement/scan_bed/un_assigned_bags.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';
import 'package:weight_collection/utils/handle_error.dart';

import '../../../core/enums/view_state.dart';
import '../../../core/models/get_bed_line_items_response.dart';
import '../../../core/models/scan_bag_qr_body.dart';
import '../../../core/models/scan_bed_response.dart';
import '../../../utils/loader.dart';
import '../../../utils/useful.dart';
import '../../base_view.dart';
import '../../taskManager/create_task.dart';

class ScanTypeBed extends StatefulWidget {
  // if (result.type == "staff" && result.id != null) {
  //   return ScanTypeEmployee(result.id!);
  // } else if (result.type == "bed" && result.id != null) {
  //   return ScanTypeBed(result.id!);
  // } else if (result.type == "bag" && result.id != null) {
  //   return ScanTypeBag(result.id!);
  // } else if (result.type == "green_house" && result.id != null) {
  static const tag = 'bed';

  final String id;

  const ScanTypeBed({required this.id, super.key});

  @override
  State<ScanTypeBed> createState() => _ScanTypeBedState();
}

class _ScanTypeBedState extends State<ScanTypeBed> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    print("@@@@@@@@@@@@@@@@ build function");
    return BaseView<ScanTypeBedModel>(onModelReady: (model) async {
      print("@@@@@@@@@@@@@@@@ onModel ready");
      if (mounted) {
        await reloadData(model);
      }
    }, builder: (context, model, child) {
      print("#################Model State : ${model.state}");
      return model.state == ViewState.Idle
          ? Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              body: SingleChildScrollView(
                // physics: const BouncingScrollPhysics(),
                child: SizedBox(
                  // height: MediaQuery.of(context).size.height-100,
                  child: Column(
                    children: [
                      model.scanResult.isEmpty
                          ? isEmptyResponse(
                              "Bed Info Not Found, Try Different Bed ID")
                          : _collectionInfo(context, model.scanResult.first),
                      const SizedBox(
                        height: 15,
                      ),
                      _buttons(context, model)
                    ],
                  ),
                ),
              ),
            )
          : const Loader();
    });
  }

  _buttons(BuildContext context, ScanTypeBedModel model) {
    return Column(
      children: [
        _sliderButton("Assign Worker", () async {
          var r = await getScanResult(context);
          if (r == null) {
            showToast("Make sure the correct QR is scanned!");
            return;
          }
          if (r.type != "staff") {
            showToast("Scan Staff Id to proceed");
            return;
          }
          getStaffInfo(r, (plannerStageId, sectionId) async {
            await assignToBed(r, model, plannerStageId, sectionId);
          });
        }, "bed_assign_worker"),
        _sliderButton("Assign Bag ", () async {
          var r = await getScanResult(context);
          if (r == null) {
            showToast("Make sure the correct QR is scanned!");
            return;
          }
          if (r.type != "bag") {
            showToast("Scan Bag Id to proceed");
            return;
          }
          getStaffInfo(r, (plannerStageId, sectionId) async {
            await assignToBed(r, model, plannerStageId, sectionId);
          });
        }, "bed_assign_sac"),
        _sliderButton("Update Loss ", () async {
          updateLossDialog((data) async {
            await updateLoss(model, data);
          });
        }, "update_loss"),
        _sliderButton("Update Scouting", () async {
          updateScoutingDialog((plannerStageId, quantity, reasonId) async {
            await updateScouting(model, plannerStageId, quantity, reasonId);
          });
        }, "scout"),
        _sliderHorizontalButton(
            leftName: "Receive",
            rightName: "Move",
            onSlideLeftAction: () async {
              movePlantsDialog((plannerStageId, quantity, type) async {
                await movePlants(model, plannerStageId, quantity, type);
              });
            },
            onSlideRightAction: () async {
              receivePlantsDialog((plannerStageId, quantity, movementId) async {
                await receivePlants(model, plannerStageId, quantity, movementId);
              });
            },
            assetName: "transfer_line_items"),
        _sliderButton("Add Plants", () async {
          await getAvailablePlantsDialog(
              (plannerStageId, quantity, plantType) async {
            await addPlant(model, plannerStageId, quantity, plantType);
          });
        }, 'receive_line_item'),
        // _sliderButton("Add Section", () async {
        //   await addSectionDialog((Map<String, dynamic> data) async {
        //     await addSection(model, data);
        //   });
        // }, 'add_section'),
        // model.sectionDetails.isNotEmpty
        //     ? _sliderButton("Remove Section", () async {
        //         await removeSectionDialog((Map<String, dynamic> data) async {
        //           await removeSection(model, data);
        //         }, (model.sectionDetails));
        //       }, 'remove_section')
        //     : Container(),
        _sliderButton("Update End Date", () async {
          await updateDateDialog((plannerStageId, date, reason) async {
            await onUpdateDate(model, plannerStageId, date, reason);
          });
        }, 'calendar'),
        _sliderButton("Complete Stage", () async {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  CompleteStagePage(id: widget.id)));
          // await upDateStatusDialog(
          //         (plannerStageId, comments, date) async {
          //       await onUpdateStatus(model, plannerStageId, comments, date);
          //     });
        }, 'checklist'),
        _sliderButton("Initialize Stage", () async {
          await startStageDialog((plannerStageId, comments, date) async {
            await onStartStage(model, plannerStageId, comments, date);
          });
        }, 'start'),
        _sliderButton("Forms", () async {
          await getPlannerStageIdDialog((plannerStageId) async {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => ReportForms(
                    id: int.parse(widget.id),
                    type: "bed",
                    plannerStageId: plannerStageId),
              ),
            );
          });
        }, 'employee_report'),
        _sliderButton("Add Task ", () async {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) =>
                  CreateTaskPage(null, type: "bed", id: widget.id),
            ),
          );
        }, "add_task"),
        model.scanResult.isNotEmpty
            ? (model.scanResult.first.staffDetails ?? []).isNotEmpty
                ? _sliderButton("Remove Staff", () async {
                    removeStaffDialog((Map<String, dynamic> data) async {
                      await clearstaff(model, data);
                    }, (model.scanResult.first.staffDetails ?? []));
                  }, "employee_reassign")
                : Container()
            : Container(),
        _sliderButton("Unassigned Bags", () async {
          Navigator.of(context).pushNamed(UnAssignedBagsPage.tag, arguments: int.parse(widget.id));
          // await upDateStatusDialog(
          //         (plannerStageId, comments, date) async {
          //       await onUpdateStatus(model, plannerStageId, comments, date);
          //     });
        }, 'assign'),
        _sliderButton("Clear Bed ", () async {
          confirmBedClearing(() async {
            await clearBed(model);
          });
        }, "clear_bed")
      ],
    );
  }

  _collectionInfo(BuildContext context, ScanBedData scanResult) {
    return Card(
      color: Colors.black,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 5, top: 5),
            decoration: BoxDecoration(
              color: Colors.white10,
              border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
              child: Column(
                children: [
                  Text(
                    '${scanResult.bedName ?? ""} (${scanResult.sectionId})',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: fromHex(yellow)),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Staff Assigned',
                        style: _titleStyle(),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: _staffList(scanResult.staffDetails ?? []),
                      )
                    ],
                  ),
                  _bedDetails(scanResult.lineDetails ?? []),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _staffList(List<StaffDetails> staffs) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: staffs.length,
      shrinkWrap: true,
      itemBuilder: (context, index) => _staffItem(context, staffs[index]),
    );
  }

  _staffItem(BuildContext context, StaffDetails staff) {
    return Row(
      children: [
        Expanded(child: Text(staff.staffName ?? "", style: _normalStyle()))
      ],
    );
  }

  _bedDetails(List<LineDetails> beds) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: beds.length,
      shrinkWrap: true,
      itemBuilder: (context, index) => _bedInfoItem(context, beds[index]),
    );
  }

  _bedInfoItem(BuildContext context, LineDetails bed) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
          const SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("${bed.contractNo ?? ""} - ${bed.stageName ?? ""}",
                        style: _titleStyle()),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Total Female Plant No.",
                            style: _titleStyle(),
                          ),
                          Text(bed.stageFemalePlantNo ?? "",
                              style: _normalStyle()),
                          Text(
                            "Total Lost Quantity",
                            style: _titleStyle(),
                          ),
                          Text("${bed.totalLostQuantity ?? 0}",
                              style: _normalStyle()),
                          Text(
                            "variety",
                            style: _titleStyle(),
                          ),
                          Text((bed.type ?? "") == 'Male'? "${bed.varietyName ?? ""} - P": "${bed.varietyName ?? ""} - F", style: _normalStyle()),
                          // Text(
                          //   "Quota(%)",
                          //   style: _titleStyle(),
                          // ),
                          // Text(
                          //     (bed.type ?? "") == 'Male'
                          //         ? "P - ${bed.stageMalePlantNo}"
                          //         : 'F - ${bed.stageFemalePlantNo}',
                          //     style: _normalStyle()),
                          Visibility(
                              visible: bed.type == 'Female',
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Quota(%)",
                                    style: _titleStyle(),
                                  ),
                                  Text(
                                      "${(bed.quotaProduced ?? 0.0).toInt()}${bed.uom ?? ''}  (${(bed.quotaProducedPercentage ?? 0.0).toPrecision(1)}%)",
                                      style: _normalStyle()),
                                ],
                              ))
                        ],
                      ),
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Total Male Plant No.",
                          style: _titleStyle(),
                        ),
                        Text(bed.stageMalePlantNo ?? "", style: _normalStyle()),
                        Text(
                          "Stage Lost Quantity",
                          style: _titleStyle(),
                        ),
                        Text("${bed.currentStageLostQuantity ?? 0}",
                            style: _normalStyle()),
                        Text("Plant No.  ", style: _titleStyle()),
                        Text(bed.currentPlantNo ?? '', style: _normalStyle()),
                        Text("End Date  ", style: _titleStyle()),
                        Text(Jiffy.parse(bed.endDate ?? "").yMMMd,
                            style: _normalStyle())
                      ],
                    ))
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _titleStyle() {
    return TextStyle(
        fontWeight: FontWeight.bold, fontSize: 16, color: fromHex(yellow));
  }

  _normalStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
  }

  _sliderButton(
    String buttonName,
    Future Function() onClickAction,
    String assetName,
  ) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        onClickAction();
      },
      child: InkWell(
        onTap: () async {
          onClickAction();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Card(
            color: Colors.white10.withOpacity(0.07),
            child: Container(
              // color: fromHex("#c19d3d"),
              height: 60,
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets1/$assetName.svg",
                        width: 36,
                        height: 36,
                        color: fromHex(yellow),
                      ),
                      const SizedBox(
                        width: 60,
                      ),
                      Text(
                        buttonName,
                        style: TextStyle(
                            color: fromHex(yellow),
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      )
                    ],
                  ),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: fromHex(yellow),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _sliderHorizontalButton({
    required String leftName,
    required String rightName,
    required Future Function() onSlideRightAction,
    required Future Function() onSlideLeftAction,
    required String assetName,
  }) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.horizontal,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        if (direction == DismissDirection.startToEnd) {
          onSlideLeftAction();
        } else {
          onSlideRightAction();
        }
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Card(
          color: Colors.white10.withOpacity(0.07),
          child: Container(
            // color: fromHex("#c19d3d"),
            height: 60,
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  Icons.arrow_back_ios_new_sharp,
                  color: fromHex(yellow),
                ),
                Text(
                  leftName,
                  style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                SvgPicture.asset(
                  "assets1/$assetName.svg",
                  width: 36,
                  height: 36,
                  color: fromHex(yellow),
                ),
                Text(
                  rightName,
                  style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getStaffInfo(
    ScanBagQRBody r,
    Future Function(String itemId, String sectionId) onClickAction,
  ) async {
    String? lineDataId;
    String? sectionId;

    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          if (model.sectionDetails.length == 1) {
            setState(() {
              sectionId = model.sectionDetails.first.id;
            });
          }
          if (r.type == "staff") {
            try {
              await model.getScanStaff({"staff_id": r.id});
            } catch (e) {
              showToast("Failed to get Staff information");
              Navigator.of(context).pop();
            }
          } else if (r.type == 'bag') {
            try {
              await model.getScanBag({"bag_id": r.id});
            } catch (e) {
              showToast("Failed to get bag information");
              Navigator.of(context).pop();
            }
          } else {
            showToast("Error type not captured");
            Navigator.of(context).pop();
          }
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ListTile(
                          title: Text(
                            "Confirm ${r.type == 'staff' ? "Staff" : "Bag"} Assignment",
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Confirm Assignment of ${r.type == 'staff' ? (model.associatedData?.name ?? "Staff") : "Bag ${r.id ?? ""}"} to this Bed..  Proceed ? ",
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId = ld.plannerStageId;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        // model.sectionDetails.length != 1
                        //     ? Row(
                        //         children: [
                        //           Expanded(
                        //             child: commonSectionDetailsDropDownSearch(
                        //               items: model.sectionDetails,
                        //               onChanged: (SectionDetails ld) => {
                        //                 setState(() {
                        //                   sectionId = ld.id;
                        //                 })
                        //               },
                        //             ),
                        //           )
                        //         ],
                        //       )
                        //     : Container(),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineDataId == null) {
                                    showToast("Choose Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onClickAction(lineDataId!, "");
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> assignToBed(ScanBagQRBody r, ScanTypeBedModel model,
      String plannerStageId, String sectionId) async {
    if (r.type == "staff") {
      try {
        var response = await model.assignStaffToBed({
          "staff_id": r.id,
          "bed_id": widget.id,
          "user_id": getUserId(),
          "section_id": sectionId,
          "planner_stage_id": plannerStageId
        });
        GeneralResponse gr = GeneralResponse.fromJson(response['response']);
        if (gr.error == false) {
          showToast(gr.message ?? "Success", i: 1);
          reloadData(model);
        } else {
          showToast(gr.erroMessage ?? "Failed", i: 1);
        }
      } catch (e) {
        showToast("Failed to assign staff to bed", i: 1);
      }
    } else if (r.type == "bag") {
      try {
        var response = await model.assignBagToBed({
          "bag_id": r.id,
          "bed_id": widget.id,
          "user_id": getUserId(),
          "section_id": sectionId,
          "planner_stage_id": plannerStageId
        });
        GeneralResponse gr = GeneralResponse.fromJson(response['response']);
        if (gr.error == false) {
          showToast(gr.message ?? "Success", i: 1);
          reloadData(model);
        } else {
          showToast(gr.erroMessage ?? "Failed", i: 1);
        }
      } catch (e) {
        showToast("Failed to assign bag to bed", i: 1);
      }
    } else {
      showToast("unknown type given", i: 1);
    }
  }

  Future<void> confirmBedClearing(Future Function() onConfirm) async {
    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              insetPadding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Confirm Bed Clearing",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      const Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Are you sure you want to proceed with clearing this Bed, all assignments to the bed will be lost ",
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                Navigator.of(context).pop();
                                onConfirm();
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  Future<void> clearstaff(
      ScanTypeBedModel model, Map<String, dynamic> data) async {
    try {
      data.addAll({"user_id": getUserId()});
      var response = await model.clearStaff(data);
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Clear Staff", i: 1);
    }
  }

  Future<void> clearBed(ScanTypeBedModel model) async {
    try {
      var response =
          await model.clearBed({"user_id": getUserId(), "bed_id": widget.id});
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Clear Bed", i: 1);
    }
  }

  Future<void> reloadData(ScanTypeBedModel model) async {
    if (await checkConnection()) {
      showToast("Check network connection");
    }

    await model.getScanBed({"bed_id": widget.id});
  }

  Future<void> updateLossDialog(
      Future<void> Function(Map<String, dynamic> data) onConfirm) async {
    int? lineDataId;
    String? plantType;
    TextEditingController quantity = TextEditingController();
    int? reasonId;
    String? type;

    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
          //get reasons
          try {
            await model.getLossReasons();
          } catch (e) {
            showToast("Failed to get reasons");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Update Loss",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonStringDropDownSearch(
                                items: ["Loss", "Count"],
                                onChanged: (String ld) => {
                                  setState(() {
                                    type = ld;
                                  })
                                },
                                labelText: "Type",
                                showSearchBox: false,
                                hint: "Select Type",
                                searchHint: "Search Type",
                                validationText: "Please Select Type",
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: quantity,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter user name";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Quantity",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        type == "Loss"
                            ? Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: commonReasonDropDown(
                                          items: model.reasons,
                                          onChanged: (ReasonData ld) => {
                                            setState(() {
                                              reasonId =
                                                  int.tryParse(ld.id ?? "");
                                            })
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                              )
                            : Container(),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    plantType = ld.type;
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if ((type ?? "").isEmpty) {
                                    showToast("Please Specify Type");
                                    return;
                                  } else if (quantity.text.isEmpty) {
                                    showToast("Please Specify Quantity");
                                    return;
                                  } else if (reasonId == null &&
                                      type == "Loss") {
                                    showToast("Please Specify Reason");
                                    return;
                                  } else if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    if (type == "Loss") {
                                      onConfirm({
                                        "planner_stage_id": lineDataId,
                                        "user_id": getUserId(),
                                        "reason_id": reasonId,
                                        "quantity": quantity.text,
                                        "bed_id": widget.id,
                                        "type": type,
                                        "plant_type": plantType
                                      });
                                    } else {
                                      onConfirm({
                                        "planner_stage_id": lineDataId,
                                        "user_id": getUserId(),
                                        "quantity": quantity.text,
                                        "bed_id": widget.id,
                                        "type": type,
                                        "plant_type": plantType
                                      });
                                    }
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> updateScoutingDialog(
      Future<void> Function(int plannerStageId, String quantity, int reasonId)
          onConfirm) async {
    int? lineDataId;
    TextEditingController quantity = TextEditingController();
    int? reasonId;

    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
          //get reasons
          try {
            await model.getScoutingReasons();
          } catch (e) {
            showToast("Failed to get reasons");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Update Scouting",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: quantity,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter user name";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Quantity",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonReasonDropDown(
                                items: model.scoutingReasons,
                                onChanged: (ReasonData ld) => {
                                  setState(() {
                                    reasonId = int.tryParse(ld.id ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (quantity.text.isEmpty) {
                                    showToast("Please Specify Quantity");
                                    return;
                                  } else if (reasonId == null) {
                                    showToast("Please Specify Reason");
                                    return;
                                  } else if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm(
                                        lineDataId!, quantity.text, reasonId!);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  updateLoss(ScanTypeBedModel model, Map<String, dynamic> data) async {
    try {
      var response = await model.updateLoss(data);
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Clear Bed", i: 1);
    }
  }

  updateScouting(ScanTypeBedModel model, int plannerStageId, String quantity,
      int reasonId) async {
    try {
      var response = await model.updateScouting({
        "planner_stage_id": plannerStageId,
        "user_id": getUserId(),
        "reason_id": reasonId,
        "quantity": quantity,
        "bed_id": widget.id
      });
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Update Scouting", i: 1);
    }
  }

  Future<void> movePlantsDialog(
      Future<void> Function(int plannerStageId, int quantity, String? type) onConfirm) async {
    int? lineDataId;
    String? type;
    TextEditingController quantity = TextEditingController();
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
          //get reasons
          try {
            await model.getLossReasons();
          } catch (e) {
            showToast("Failed to get reasons");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Move Plants",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                    type = ld.type;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: quantity,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter user name";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Quantity",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (quantity.text.isEmpty) {
                                    showToast("Please Specify Quantity");
                                    return;
                                  } else if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm(lineDataId!,
                                        int.tryParse(quantity.text) ?? 0, type);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> getPlannerStageIdDialog(
      Future<void> Function(int plannerStageId) onConfirm) async {
    int? lineDataId;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Choose Line Item",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm(lineDataId!);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> receivePlantsDialog(
      Future<void> Function(
        dynamic plannerStageId,
        dynamic quantity,
          dynamic movementId,
      ) onConfirm) async {
    int? lineDataId;
    int? movementId;
    TextEditingController quantity = TextEditingController();
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getPlantsInSuspense();
          } catch (e) {
            showToast("Failed to get plants in transfer");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Receive Plants",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonPlantsDropDownSearch(
                                items: model.plants,
                                onChanged: (PlantData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                    movementId = int.tryParse(ld.id??"");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: quantity,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter user name";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Quantity",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (quantity.text.isEmpty) {
                                    showToast("Please Specify Quantity");
                                    return;
                                  } else if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm(lineDataId!, quantity.text, movementId);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  receivePlants(ScanTypeBedModel model, plannerStageId, quantity, movementId) async {
    try {
      var response = await model.receivePlant({
        "movement_id": movementId,
        "planner_stage_id": plannerStageId,
        "received_by": getUserId(),
        "plants_received": quantity,
        "to_bed": widget.id
      });
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Receive Plants", i: 1);
    }
  }

  movePlants(ScanTypeBedModel model, int plannerStageId, int quantity, type) async {
    try {
      var response = await model.movePlant({
        "planner_stage_id": plannerStageId,
        "initiated_by": getUserId(),
        "plants_initiated": quantity,
        "from_bed": widget.id,
        "type": type
      });
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Move Plants", i: 1);
    }
  }

  Future getAvailablePlantsDialog(
      Future Function(int plannerStageId, String quantity, String plantType)
          onSubmit) async {
    int? lineDataId;
    String? plantType;
    List<String> plantTypes = ['Male', 'Female'];
    TextEditingController quantity = TextEditingController();
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getAvailablePlants();
          } catch (e) {
            showToast("Failed to get plants in transfer");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Add Plants",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonPlantsDropDownSearch(
                                items: model.availablePlants,
                                onChanged: (PlantData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonStringDropDownSearch(
                                items: plantTypes,
                                onChanged: (String ld) => {
                                  setState(() {
                                    plantType = ld;
                                  })
                                },
                                labelText: "Plant Type",
                                showSearchBox: false,
                                hint: "Select Plant Type",
                                searchHint: "Search Plant Type",
                                validationText: "Please Select Plant Type",
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: quantity,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter user name";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Quantity",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (quantity.text.isEmpty) {
                                    showToast("Please Specify Quantity");
                                    return;
                                  } else if (plantType == null) {
                                    showToast("Please Specify Plant Type");
                                    return;
                                  } else if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onSubmit(
                                        lineDataId!, quantity.text, plantType!);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  addPlant(ScanTypeBedModel model, plannerStageId, quantity,
      String plantType) async {
    try {
      var response = await model.addPlant({
        "planner_stage_id": plannerStageId,
        "created_by": getUserId(),
        "quantity": quantity,
        "plant_type": plantType,
        "bed_id": widget.id
      });
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Add Plants", i: 1);
    }
  }

  Future updateDateDialog(
      Future Function(int plannerStageId, String endDate, String reason)
          onSubmit) async {
    int? lineDataId;
    TextEditingController date = TextEditingController();
    TextEditingController comment = TextEditingController();
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Update End Date",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: date,
                          keyboardType: TextInputType.number,
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter task's due date";
                            }
                            return null;
                          },
                          enableInteractiveSelection: false,
                          autofocus: false,
                          readOnly: true,
                          onTap: () {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime.now()
                                    .subtract(const Duration(days: 200)),
                                maxTime: DateTime.now()
                                    .add(const Duration(days: 94200)),
                                theme: dtp.DatePickerTheme(
                                  headerColor: Colors.grey.shade900,
                                  backgroundColor: Colors.grey.shade900,
                                  itemStyle: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                  cancelStyle: const TextStyle(
                                    color: Colors.deepOrange,
                                    fontSize: 24,
                                  ),
                                  doneStyle: TextStyle(
                                    color: Colors.green.shade700,
                                    fontSize: 24,
                                  ),
                                ), onChanged: (date) {
                              print(
                                  'change $date in time zone ${date.timeZoneOffset.inHours}');
                            }, onConfirm: (d) {
                              setState(() {
                                date.text = df.format(d);
                              });
                            },
                                currentTime: DateTime.now(),
                                locale: LocaleType.en);
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "",
                            labelText: "End Date",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: comment,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.sentences,
                          maxLines: 6,
                          textAlignVertical: TextAlignVertical.top,
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter Your Response";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "Enter your reason here..",
                            labelText: "Reason",
                            alignLabelWithHint: true,
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (date.text.isEmpty) {
                                    showToast("Please Specify End Date");
                                    return;
                                  } else if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onSubmit(
                                        lineDataId!, date.text, comment.text);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  onUpdateDate(
      ScanTypeBedModel model, plannerStageId, date, String reason) async {
    try {
      var response = await model.updateEndDate({
        "planner_stage_id": plannerStageId,
        "user_id": getUserId(),
        "new_date": date,
        "bed_id": widget.id,
        "reason": reason
      });
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Update Date", i: 1);
    }
  }

  Future startStageDialog(
      Future Function(int plannerStageId, String comments, String date)
          onSubmit) async {
    int? lineDataId;
    TextEditingController comment = TextEditingController();
    TextEditingController date = TextEditingController();
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            var r = await model.getAvailableLines();
            if (r.isLeft) {
              handleError(r.left);
            }
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Initialize Stage",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonAvailableLineDropDownSearch(
                                items: model.availableLines,
                                onChanged: (AvailableLinesData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: date,
                          keyboardType: TextInputType.number,
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter task's start date";
                            }
                            return null;
                          },
                          enableInteractiveSelection: false,
                          autofocus: false,
                          readOnly: true,
                          onTap: () {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime.now()
                                    .subtract(const Duration(days: 200)),
                                maxTime: DateTime.now()
                                    .add(const Duration(days: 94200)),
                                theme: dtp.DatePickerTheme(
                                  headerColor: Colors.grey.shade900,
                                  backgroundColor: Colors.grey.shade900,
                                  itemStyle: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                  cancelStyle: const TextStyle(
                                    color: Colors.deepOrange,
                                    fontSize: 24,
                                  ),
                                  doneStyle: TextStyle(
                                    color: Colors.green.shade700,
                                    fontSize: 24,
                                  ),
                                ), onChanged: (date) {
                              print(
                                  'change $date in time zone ${date.timeZoneOffset.inHours}');
                            }, onConfirm: (d) {
                              setState(() {
                                date.text = df.format(d);
                              });
                            },
                                currentTime: DateTime.now(),
                                locale: LocaleType.en);
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "",
                            labelText: "Start Date",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: comment,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.sentences,
                          maxLines: 6,
                          textAlignVertical: TextAlignVertical.top,
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter Your Response";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "Enter your comments here..",
                            labelText: "Comments",
                            alignLabelWithHint: true,
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onSubmit(
                                        lineDataId!, comment.text, date.text);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  onStartStage(
      ScanTypeBedModel model, plannerStageId, comments, String date) async {
    try {
      var response = await model.startStage({
        "planner_stage_id": plannerStageId,
        "user_id": getUserId(),
        "start_date": date,
        "comment": comments
      });
      if (response.isRight) {
        CompleteStageResponse cp = response.right;
        if (cp.error == false) {
          showToast(cp.message ?? "Success", i: 1);
          reloadData(model);
        } else {
          showToast(cp.erroMessage ?? "Failed", i: 1);
        }
      } else {
        handleError(response.left);
      }
    } catch (e) {
      showToast("Failed to Update Status", i: 1);
    }
  }

  Future<void> removeStaffDialog(
      Future<void> Function(Map<String, dynamic> data) onConfirm,
      List<StaffDetails> staffDetails) async {
    int? staffId;
    int? lineDataId;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Romove Staff",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId =
                                        int.tryParse(ld.plannerStageId ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonStaffDetailsDropDownSearch(
                                items: staffDetails,
                                onChanged: (StaffDetails ld) => {
                                  setState(() {
                                    staffId = int.tryParse(ld.id ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        staffId == null
                            ? Column(
                                children: [
                                  const Text("Or"),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  _sliderButton("Scan Staff", () async {
                                    ScanBagQRBody? scanResponse =
                                        await getScanResult(context);

                                    if (scanResponse == null) {
                                      showToast(
                                          "Make sure the correct staff id is scanned!");
                                      return;
                                    }

                                    if (scanResponse.type == "staff" &&
                                        scanResponse.id != null) {
                                      showToast(
                                          "Make sure the correct staff id is scanned!");
                                      return;
                                    }
                                    staffId =
                                        int.tryParse(scanResponse.id ?? "");
                                  }, "StartScanning"),
                                ],
                              )
                            : Container(),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineDataId == null) {
                                    showToast("Please Specify Line Item");
                                    return;
                                  } else if (staffId == null) {
                                    showToast("Please Specify Staff");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm({
                                      "planner_stage_id": lineDataId,
                                      "staff_id": staffId,
                                      "bed_id": widget.id
                                    });
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  addSectionDialog(
      Future<void> Function(Map<String, dynamic> data) onConfirm) async {
    TextEditingController quantity0 = TextEditingController();
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {},
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Add Section",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: quantity0,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: const TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Quantity";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Quantity",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  var quantity = int.tryParse(quantity0.text);
                                  if (quantity == null) {
                                    showToast("Please Specify Quantity");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm({
                                      "quantity": quantity0.text,
                                      "bed_id": widget.id,
                                      "created_by": getUserId()
                                    });
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  addSection(ScanTypeBedModel model, Map<String, dynamic> data) async {
    try {
      var value = await model.addSection(data);
      if (value.isRight) {
        GeneralResponse gr = value.right;
        if (gr.error == false) {
          showToast(gr.message ?? "Success", i: 1);
          reloadData(model);
        } else {
          showToast(gr.erroMessage ?? "Failed", i: 1);
        }
      } else {
        handleError(value.left);
      }
    } catch (e) {
      showToast("Failed to Add Section");
    }
  }

  Future<void> removeSectionDialog(
      Future<void> Function(Map<String, dynamic> data) onConfirm,
      List<SectionDetails> sectionDetails) async {
    int? ssectionId;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBedModel>(
        onModelReady: (model) async {
          //get line items
          try {
            await model.getLineItems({"bed_id": widget.id});
          } catch (e) {
            showToast("Failed to get Line Items");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                insetPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Clear Section",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonSectionDetailsDropDownSearch(
                                items: sectionDetails,
                                onChanged: (SectionDetails ld) => {
                                  setState(() {
                                    ssectionId = int.tryParse(ld.id ?? "");
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (ssectionId == null) {
                                    showToast("Please Specify Section");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onConfirm({
                                      "section_id": ssectionId,
                                      "user_id": getUserId(),
                                    });
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  removeSection(ScanTypeBedModel model, Map<String, dynamic> data) async {
    try {
      var value = await model.removeSection(data);
      if (value.isRight) {
        GeneralResponse gr = value.right;
        if (gr.error == false) {
          showToast(gr.message ?? "Success", i: 1);
          reloadData(model);
        } else {
          showToast(gr.erroMessage ?? "Failed", i: 1);
        }
      } else {
        handleError(value.left);
      }
    } catch (e) {
      showToast("Failed to Remove Section");
    }
  }
}
