import 'package:collection/collection.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dtp;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/get_unassigned_bags_response.dart';
import 'package:weight_collection/ui/base_view.dart';
import 'package:weight_collection/utils/loader.dart';

import '../../../core/enums/view_state.dart';
import '../../../core/viewmodels/unassigned_bags_model.dart';
import '../../../utils/useful.dart';
import '../utils.dart';

class UnAssignedBagsPage extends StatelessWidget {
  static const tag = 'unassigned_bags_page';
  final int bedId;

  UnAssignedBagsPage({super.key, required this.bedId});

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<UnassignedBagsModel>(
      onModelReady: (model) async {
        await onGetUnAssignedBags(
            onSuccess: () {},
            model: model,
            onError: (title, message) {
              showErrorDialog(title, context, message);
            });
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              appBar: AppBar(
                backgroundColor: Colors.black,
                title: const Text("Unassigned Bags"),
                centerTitle: true,
                automaticallyImplyLeading: true,
                actions: [
                  IconButton(
                      onPressed: () {
                        onShowFilterDialog(
                            onFilter: (String startDate, String endDate) async {
                          model.startDate = startDate;
                          model.endDate = endDate;
                          await onGetUnAssignedBags(
                              onSuccess: () {},
                              model: model,
                              onError: (title, message) {
                                showErrorDialog(title, context, message);
                              });
                        });
                      },
                      icon: const Icon(Icons.filter_alt_outlined))
                ],
              ),
              body: Padding(
                padding:
                    const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                child: UnassignedDataTable(
                    unAssignedBags: model.unAssignedBags,
                    startDate: model.startDate,
                    endDate: model.endDate),
              ),
            )
          : const Loader(),
    );
  }

  Future<void> onGetUnAssignedBags(
      {required Function() onSuccess,
      required UnassignedBagsModel model,
      required Function(dynamic title, dynamic message) onError}) async {
    try {
      var response = await model.getUnassignedBags(bedId);
      if (response.isRight) {
        onSuccess();
      } else {
        onError("Error Getting Unassigned Bags", response.left.message);
      }
    } catch (e) {
      onError("Error Getting Unassigned Bags", e.toString());
    }
  }

  Future<void> onShowFilterDialog(
      {required Function(String startDate, String endDate) onFilter}) async {
    final TextEditingController _startDate = TextEditingController();
    final TextEditingController _endDate = TextEditingController();
    DateTime? startDate;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        insetPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        backgroundColor: Colors.grey[900],
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const ListTile(
                  title: Text(
                    "Filter By Date",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),

                TextFormField(
                  controller: _startDate,
                  keyboardType: TextInputType.number,
                  style: const TextStyle(color: Colors.white),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter start date";
                    }
                    return null;
                  },
                  enableInteractiveSelection: false,
                  autofocus: false,
                  readOnly: true,
                  onTap: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime:
                            DateTime.now().subtract(const Duration(days: 1000)),
                        maxTime:
                            DateTime.now(),
                        theme: dtp.DatePickerTheme(
                          headerColor: Colors.grey.shade900,
                          backgroundColor: Colors.grey.shade900,
                          itemStyle: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          cancelStyle: const TextStyle(
                            color: Colors.deepOrange,
                            fontSize: 24,
                          ),
                          doneStyle: TextStyle(
                            color: Colors.green.shade700,
                            fontSize: 24,
                          ),
                        ), onChanged: (date) {
                      print(
                          'change $date in time zone ${date.timeZoneOffset.inHours}');
                    }, onConfirm: (date) {
                      setState(() {
                        startDate = date;
                        _startDate.text = df.format(date);
                      });
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                  },
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white10,
                    isDense: true,
                    hintText: "",
                    labelText: "Start Date",
                    labelStyle: TextStyle(color: fromHex(yellow)),
                    hintStyle: TextStyle(
                      color: Colors.blueGrey[400],
                    ),
                    border: InputBorder.none,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: fromHex(yellow)),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: _endDate,
                  keyboardType: TextInputType.number,
                  style: const TextStyle(color: Colors.white),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter end date";
                    }
                    return null;
                  },
                  enableInteractiveSelection: false,
                  autofocus: false,
                  readOnly: true,
                  onTap: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: startDate??
                            DateTime.now().subtract(const Duration(days: 1000)),
                        maxTime: DateTime.now().subtract(const Duration(days: 1)),
                        theme: dtp.DatePickerTheme(
                          headerColor: Colors.grey.shade900,
                          backgroundColor: Colors.grey.shade900,
                          itemStyle: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          cancelStyle: const TextStyle(
                            color: Colors.deepOrange,
                            fontSize: 24,
                          ),
                          doneStyle: TextStyle(
                            color: Colors.green.shade700,
                            fontSize: 24,
                          ),
                        ), onChanged: (date) {
                      print(
                          'change $date in time zone ${date.timeZoneOffset.inHours}');
                    }, onConfirm: (date) {
                      setState(() {
                        _endDate.text = df.format(date);
                      });
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                  },
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white10,
                    isDense: true,
                    hintText: "",
                    labelText: "End Date",
                    labelStyle: TextStyle(color: fromHex(yellow)),
                    hintStyle: TextStyle(
                      color: Colors.blueGrey[400],
                    ),
                    border: InputBorder.none,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: fromHex(yellow)),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // model.sectionDetails.length != 1
                //     ? Row(
                //         children: [
                //           Expanded(
                //             child: commonSectionDetailsDropDownSearch(
                //               items: model.sectionDetails,
                //               onChanged: (SectionDetails ld) => {
                //                 setState(() {
                //                   sectionId = ld.id;
                //                 })
                //               },
                //             ),
                //           )
                //         ],
                //       )
                //     : Container(),
                Row(
                  children: [
                    Expanded(
                      child: ListTile(
                        title: const Text(
                          "Cancel",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.deepOrange,
                              fontWeight: FontWeight.bold),
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Expanded(
                      child: ListTile(
                        title: const Text(
                          "Confirm",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.greenAccent,
                              fontWeight: FontWeight.bold),
                        ),
                        onTap: () async {
                          if (_startDate.text.isEmpty) {
                            showToast("Please specify start date");
                            return;
                          } else if (_endDate.text.isEmpty) {
                            showToast("Please specify end date");
                            return;
                          } else {
                            Navigator.of(context).pop();
                            onFilter(_startDate.text, _endDate.text);
                          }
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class UnassignedDataTable extends StatelessWidget {
  final List<UnassignedBagData> unAssignedBags;
  final String startDate;
  final String endDate;

  const UnassignedDataTable(
      {super.key,
      required this.unAssignedBags,
      required this.endDate,
      required this.startDate});

  @override
  Widget build(BuildContext context) {
    return unAssignedBags.isNotEmpty
        ? DataTable2(
            columnSpacing: 12,
            horizontalMargin: 12,
            minWidth: 600,
            columns: const [
              DataColumn2(
                label: Text('#'),
                size: ColumnSize.S,
              ),
              DataColumn2(
                label: Align(alignment: Alignment.center, child: Text('Image')),
              ),
              DataColumn(
                label: Align(
                    alignment: Alignment.center, child: Text('Variety Name')),
              ),
              DataColumn(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Variety ID')),
              ),
              DataColumn(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Batch ID')),
              ),
              DataColumn(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Batch Date')),
              ),
            ],
            rows: unAssignedBags
                .mapIndexed((index, e) => DataRow(
                      cells: [
                        DataCell(Text('${index + 1}')),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: varietyImage(
                                pictureName: e.pictureName, h: 50, w: 50))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text('${e.varietyName}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text('${e.varietyId}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text('${e.batchId}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text(Jiffy.parse('${e.batchDate}').yMMMEd)))
                      ],
                    ))
                .toList(),
          )
        : Column(
            children: [
              Expanded(
                  child: Center(
                child: Text(
                    "No Unassigned Bags Found between ${Jiffy.parse(startDate).yMMMEd} and ${Jiffy.parse(endDate).yMMMEd}"),
              ))
            ],
          );
  }
}
