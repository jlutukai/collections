import 'package:flutter/material.dart';
import 'package:weight_collection/core/models/scan_bag_qr_body.dart';
import 'package:weight_collection/ui/scanManagement/scan_type_bag.dart';
import 'package:weight_collection/ui/scanManagement/scan_bed/scan_type_bed.dart';
import 'package:weight_collection/ui/scanManagement/scan_staff/scan_type_employee.dart';
import 'package:weight_collection/ui/scanManagement/scan_type_greenhouse.dart';

import '../../utils/useful.dart';

class ScanManagement extends StatefulWidget {
  static const tag = 'scan_management';

  final ScanBagQRBody result;

  const ScanManagement({required this.result, super.key});

  @override
  State<ScanManagement> createState() => _ScanManagementState();
}

class _ScanManagementState extends State<ScanManagement> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.black,
            appBar: AppBar(
              title: Text(
                _getTitile(widget.result),
                style: TextStyle(color: fromHex(deepOrange)),
              ),
              backgroundColor: Colors.transparent,
              automaticallyImplyLeading: false,
              centerTitle: true,
            ),
            body: Column(
              children: [
                Expanded(
                  child: _getUi(widget.result),
                ),
              ],
            ),
          ),
          Positioned(
            left: 15,
            top: 0,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: Colors.white10.withOpacity(0.07),
              onPressed: () {
                Navigator.pop(context);
              },
              mini: true,
              tooltip: "go back home",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(deepOrange),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _getUi(ScanBagQRBody result) {
    print("################# Type : ${result.type}");
    print("################# result : ${result.toJson()}");
    if (result.type == "staff" && result.id != null) {
      return ScanTypeEmployee(id: result.id!);
    } else if (result.type == "bed" && result.id != null) {
      return ScanTypeBed(id: result.id!);
    } else if (result.type == "bag" && result.id != null) {
      return ScanTypeBag(id: result.id!);
    } else if (result.type == "green_house" && result.id != null) {
      return ScanTypeGreenHouse(id: result.id!);
    } else {
      return const Center(
        child: Text(
          "Type not defined",
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      );
    }
  }

  _getTitile(ScanBagQRBody result) {
    if (result.type == "staff" && result.id != null) {
      return "Staff Details";
    } else if (result.type == "bed" && result.id != null) {
      return "Bed Details";
    } else if (result.type == "bag" && result.id != null) {
      return "Bag Details";
    }else if (result.type == "green_house" && result.id != null) {
      return "Green House Details";
    } else {
      return "";
    }
  }
}
