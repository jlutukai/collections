import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tags_x/flutter_tags_x.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/scan_bag_qr_body.dart';
import 'package:weight_collection/core/models/scan_staff_response.dart';
import 'package:weight_collection/core/viewmodels/scan_type_staff_model.dart';
import 'package:weight_collection/ui/scanManagement/scan_staff/weight_collection_distribution.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';

import '../../../core/enums/view_state.dart';
import '../../../core/models/general_response.dart';
import '../../../core/models/get_bed_line_items_response.dart';
import '../../../utils/loader.dart';
import '../../../utils/useful.dart';
import '../../base_view.dart';
import '../../reportForms.dart';

class ScanTypeEmployee extends StatefulWidget {
  static const tag = 'staff';

  final String id;

  const ScanTypeEmployee({required this.id, super.key});

  @override
  State<ScanTypeEmployee> createState() => _ScanTypeEmployeeState();
}

class _ScanTypeEmployeeState extends State<ScanTypeEmployee> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<ScanTypeStaffModel>(
      onModelReady: (model) async {
        await refreshData(model);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              body: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Center(
                        child: model.scanResult.isEmpty
                            ? isEmptyResponse(
                                "Staff Info Not Found, Try Different Staff ID")
                            : _collectionInfo(context, model.scanResult.first),
                      ),
                    ),
                  ),
                  _buttons(context, model)
                ],
              ),
            )
          : const Loader(),
    );
  }

  _buttons(BuildContext context, ScanTypeStaffModel model) {
    return Column(
      children: [
        _sliderButton("Assign To", () async {
          var r = await getScanResult(context);
          if (r == null) {
            showToast("Make sure the correct QR is scanned!");
            return;
          }
          if (r.type != "bed") {
            showToast("Scan bed Id to proceed");
            return;
          }
          getStaffInfo(r, (plannerStageId) async {
            await assignToBed(r, model, plannerStageId);
          });
        }, "employee_assign_to"),
        _sliderButton("Reassign ", () async {
          showCurrentBeds(
            bedDetails: model.scanResult.first.bedDetails,
            model: model,
            onConfirm: (data) {
              reAssignToBed(data, model);
            },
          );
        }, "employee_reassign"),
        _sliderButton("report ", () async {
          reportStaff((String description) async {
            await setReport(model, description);
          });
        }, "employee_report"),
        _sliderButton("Forms", () async {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  ReportForms(id: int.parse(widget.id), type: "staff")));
        }, 'forms'),
        model.scanResult.isNotEmpty
            ? _sliderButton("Clerk Performance", () async {
                Navigator.of(context).pushNamed(StaffCollectionPage.tag,
                    arguments: StaffPerformanceParams(
                        staffId: int.parse(widget.id),
                        staffName: model.scanResult.first.name ?? ""));
              }, 'chart-line-up')
            : Container(),
        _sliderButton("Clear Staff ", () async {
          specifyBeds((Map<String, dynamic> data) {
            confirmSatffClearing(() async {
              await clearstaff(model, data);
            });
          }, model.scanResult.first.bedDetails);
        }, "clear_staff_detail")
      ],
    );
  }

  _collectionInfo(BuildContext context, StaffScanData scanResult) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 10, left: 10),
          child: Card(
            color: Colors.black,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 25, top: 25),
                  decoration: BoxDecoration(
                    color: Colors.white10,
                    border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, top: 60, bottom: 8.0),
                    child: Column(
                      children: [
                        Text(
                          '${scanResult.name ?? ""} (${widget.id})',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: fromHex(yellow)),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Competencies',
                              style: _titleStyle(),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: _competenceList(
                                  scanResult.functionCompetency ?? []),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            _bedDetails(scanResult.bedDetails ?? []),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          left: 0,
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: CachedNetworkImageProvider(scanResult.staffPicture !=
                        null
                    ? "${getUrl()}/CUSTOM/Employees/profile_pictures/${scanResult.staffPicture}"
                    : noImage),
              ),
            ),
          ),
        )
      ],
    );
  }

  _competenceList(List<FunctionCompetency> functionCompetency) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: functionCompetency.length,
      shrinkWrap: true,
      itemBuilder: (context, index) =>
          _competenceItem(context, functionCompetency[index]),
    );
  }

  _competenceItem(BuildContext context, FunctionCompetency functionCompetency) {
    return Row(
      children: [
        Expanded(
            child: Text(functionCompetency.competencyName ?? "",
                style: _normalStyle()))
      ],
    );
  }

  _bedDetails(List<BedDetails> beds) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: beds.length,
      shrinkWrap: true,
      itemBuilder: (context, index) => _bedItems(beds[index], context),
    );
  }

  _bedItems(BedDetails bed, BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      margin: const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
      decoration: const BoxDecoration(
        color: Colors.black26,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Row(
        children: [
          varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Stage",
                            style: _titleStyle(),
                          ),
                          Text(bed.stageName ?? "", style: _normalStyle()),
                          // Text(
                          //   "Bed Id",
                          //   style: _titleStyle(),
                          // ),
                          // Text(bed.bedId ?? "", style: _normalStyle()),
                          Text(
                            "Variety Id",
                            style: _titleStyle(),
                          ),
                          Text(bed.varietyName ?? "", style: _normalStyle())
                        ],
                      ),
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Bed",
                          style: _titleStyle(),
                        ),
                        Text(bed.bedName ?? "", style: _normalStyle()),
                        Text("Start Date ", style: _titleStyle()),
                        Text(Jiffy.parse(bed.startDate ?? "").yMMMd,
                            style: _normalStyle()),
                        Text("End Date ", style: _titleStyle()),
                        Text(Jiffy.parse(bed.endDate ?? "").yMMMd,
                            style: _normalStyle())
                      ],
                    ))
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _titleStyle() {
    return TextStyle(
        fontWeight: FontWeight.bold, fontSize: 16, color: fromHex(yellow));
  }

  _normalStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
  }

  _sliderButton(
    String buttonName,
    Future Function() onClickAction,
    String assetName,
  ) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        onClickAction();
      },
      child: InkWell(
        onTap: () async {
          onClickAction();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Card(
            color: Colors.white10.withOpacity(0.07),
            child: Container(
              // color: fromHex("#c19d3d"),
              height: 60,
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets1/$assetName.svg",
                        width: 36,
                        height: 36,
                        color: fromHex(yellow),
                      ),
                      const SizedBox(
                        width: 60,
                      ),
                      Text(
                        buttonName,
                        style: TextStyle(
                            color: fromHex(yellow),
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      )
                    ],
                  ),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: fromHex(yellow),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getStaffInfo(
    ScanBagQRBody r,
    Future Function(String itemId) onClickAction,
  ) async {
    String? lineDataId;

    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeStaffModel>(
        onModelReady: (model) async {
          if (r.type == "bed") {
            try {
              await model.getScanBed({"bed_id": r.id});
            } catch (e) {
              showToast("Failed to get Bed information");
              Navigator.of(context).pop();
            }
            try {
              await model.getLineItems({"bed_id": r.id});
            } catch (e) {
              showToast("Failed to get Line Items");
              Navigator.of(context).pop();
            }
          } else {
            showToast("Error type not captured");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Confirm Bed Assignment",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Confirm Assignment of ${model.associatedData?.name ?? "bed"} to this Staff..  Proceed ? ",
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId = ld.plannerStageId;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineDataId == null) {
                                    showToast("Choose Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onClickAction(lineDataId!);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> assignToBed(
      ScanBagQRBody r, ScanTypeStaffModel model, String plannerStageId) async {
    if (r.type == "bed") {
      try {
        var response = await model.assignStaffToBed({
          "staff_id": widget.id,
          "bed_id": r.id,
          "user_id": getUserId(),
          "planner_stage_id": plannerStageId
        });
        GeneralResponse gr = GeneralResponse.fromJson(response['response']);
        if (gr.error == false) {
          showToast(gr.message ?? "Success", i: 1);
          refreshData(model);
        } else {
          showToast(gr.erroMessage ?? "Failed", i: 1);
        }
      } catch (e) {
        showToast("Failed to assign staff to bed", i: 1);
      }
    } else {
      showToast("unknown type given", i: 1);
    }
  }

  Future<void> reAssignToBed(
      Map<String, dynamic> data, ScanTypeStaffModel model) async {
    try {
      var response = await model.reAssignBed(data);
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        refreshData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to assign staff to bed", i: 1);
    }
  }

  Future<void> confirmSatffClearing(Future Function() onConfirm) async {
    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Confirm Staff Clearing",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      const Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Are you sure you want to proceed with clearing Staff Assignments, all assignments to the Staff will be lost ",
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                Navigator.of(context).pop();
                                onConfirm();
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  Future<void> clearstaff(
      ScanTypeStaffModel model, Map<String, dynamic> data) async {
    try {
      data.addAll({"user_id": getUserId(), "staff_id": widget.id});
      var response = await model.clearStaff(data);
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        refreshData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to Clear Staff", i: 1);
    }
  }

  Future<void> reportStaff(
      Future<void> Function(String description) onConfirm) async {
    final TextEditingController description = TextEditingController();

    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Report",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      TextFormField(
                        controller: description,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 6,
                        textAlignVertical: TextAlignVertical.top,
                        style: const TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please Enter Report";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white10,
                          isDense: true,
                          hintText: "eg Some Report Comment",
                          labelText: "Comment",
                          alignLabelWithHint: true,
                          labelStyle: TextStyle(color: fromHex(yellow)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                if (description.text.isEmpty) {
                                  showToast("Enter report");
                                  return;
                                }
                                Navigator.of(context).pop();
                                onConfirm(description.text.toString());
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  setReport(ScanTypeStaffModel model, String description) async {
    try {
      var response = await model.updateStaffReport({
        "description": description,
        "staff_id": widget.id,
        "user_id": getUserId() ?? "",
      });
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        refreshData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to update staff report", i: 1);
    }
  }

  Future<void> showCurrentBeds(
      {List<BedDetails>? bedDetails,
      required Function(Map<String, dynamic> data) onConfirm,
      required ScanTypeStaffModel model}) async {
    String? lineDataId;
    String? oldLineDataId;
    String? bedId;
    String? oldBedId;

    if (bedDetails == null) {
      showToast("No Bed Details");
      return;
    }
    if (bedDetails.isEmpty) {
      showToast("No Bed Details");
      return;
    }

    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Bed Re - Assignment",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: commonBedDetailsDropDownSearch(
                              items: bedDetails,
                              onChanged: (BedDetails bd) => {
                                setState(() {
                                  oldLineDataId = bd.plannerStageId;
                                  oldBedId = bd.bedId;
                                })
                              },
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Column(
                        children: [
                          _sliderButton("Scan Bed", () async {
                            bedId = null;
                            var r = await getScanResult(context);
                            if (r == null || r.id == null) {
                              showToast("Make sure the correct QR is scanned!");
                              return;
                            }
                            if (r.type != "bed") {
                              showToast("Scan bed Id to proceed");
                              return;
                            }
                            getStaffInfo(r, (p) async {
                              setState(() {
                                bedId = r.id;
                                lineDataId = p;
                              });
                            });
                          }, "bed_assign_worker"),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      // bedId != null
                      //     ? bedId!.isNotEmpty
                      //         ? Column(
                      //             children: [
                      //               Row(
                      //                 children: [
                      //                   Expanded(
                      //                     child: commonDropDownSearch(
                      //                       items: model.lineData,
                      //                       onChanged: (LineData ld) => {
                      //                         setState(() => {
                      //                               lineDataId =
                      //                                   ld.plannerStageId
                      //                             })
                      //                       },
                      //                     ),
                      //                   )
                      //                 ],
                      //               ),
                      //             ],
                      //           )
                      //         : Container()
                      //     : Container(),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                if (checkValidity(oldLineDataId, oldBedId,
                                    bedId, lineDataId)) {
                                  Navigator.of(context).pop();
                                  onConfirm({
                                    "staff_id": widget.id,
                                    "old_planner_stage_id": oldLineDataId,
                                    "old_bed_id": oldBedId,
                                    "planner_stage_id": lineDataId,
                                    "bed_id": bedId,
                                    "user_id": getUserId()
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  bool checkValidity(String? oldLineDataId, String? oldBedId, String? bedId,
      String? lineDataId) {
    if (oldBedId == null) {
      showToast("Choose Current Bed");
      return false;
    }
    if (oldBedId.isEmpty) {
      showToast("Choose Current Bed");
      return false;
    }
    if (oldLineDataId == null) {
      showToast("Choose Current Bed");
      return false;
    }
    if (oldLineDataId.isEmpty) {
      showToast("Choose Current Bed");
      return false;
    }
    if (bedId == null) {
      showToast("Choose new Bed");
      return false;
    }
    if (bedId.isEmpty) {
      showToast("Choose new Bed");
      return false;
    }
    if (lineDataId == null) {
      showToast("Choose New Line Item");
      return false;
    }
    if (lineDataId.isEmpty) {
      showToast("Choose New Line Item");
      return false;
    }
    return true;
  }

  Future<void> refreshData(ScanTypeStaffModel model) async {
    await model.getScanStaff({"staff_id": widget.id});
  }

  Future<void> specifyBeds(Function(Map<String, dynamic>) onSuccess,
      List<BedDetails>? bedDetails) async {
    Set<BedInfo> selected = {};
    List<String> clearAllList = ["Yes", "No"];
    bool clearAll = false;
    if (bedDetails == null) {
      showToast("No Bed Details");
      return;
    }
    if (bedDetails.isEmpty) {
      showToast("No Bed Details");
      return;
    }

    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Specify Which Assignment",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      commonStringDropDownSearch(
                        items: clearAllList,
                        onChanged: (value) {
                          setState(() {
                            clearAll = (value == "Yes");
                          });
                        },
                        labelText: "Clear All",
                        hint: "Clear All",
                        searchHint: "Clear All",
                        showSearchBox: false,
                        validationText: "Clear All",
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      clearAll
                          ? Container()
                          : Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Tags(
                                        itemCount: selected.length ?? 0,
                                        alignment: WrapAlignment.start,
                                        itemBuilder: (int? index) {
                                          var type =
                                              selected.elementAt(index ?? 0);
                                          return Tooltip(
                                              message:
                                                  "id : ${type.bedId ?? ""}",
                                              child: ItemTags(
                                                textStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    color: fromHex(yellow)),
                                                color: Colors.white10
                                                    .withOpacity(0.07),
                                                activeColor: Colors.white10
                                                    .withOpacity(0.07),
                                                singleItem: true,
                                                index: index ?? 0,
                                                title: type.varietyName ?? "",
                                                textColor: Colors.white,
                                                border: Border.all(
                                                    color: Colors.black),
                                                removeButton:
                                                    ItemTagsRemoveButton(
                                                  color: Colors.deepOrange,
                                                  backgroundColor:
                                                      Colors.transparent,
                                                  onRemoved: () {
                                                    setState(() {
                                                      selected.remove(type);
                                                    });
                                                    return true;
                                                  },
                                                ), // OR nu
                                              ));
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: commonBedDetailsDropDownSearch(
                                        items: bedDetails,
                                        onChanged: (BedDetails bd) {
                                          print(bd.varietyName.toString());
                                          setState(() {
                                            selected.add(BedInfo(
                                                bedId: bd.bedId,
                                                plannerStageId:
                                                    bd.plannerStageId,
                                                varietyName:
                                                    "${bd.bedName} - ${bd.varietyName} \n${bd.stageName}"));
                                          });
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                Navigator.of(context).pop();
                                onSuccess({
                                  "clear_all": clearAll,
                                  "items": selected
                                      .toList()
                                      .map((e) => e.toJson())
                                      .toList()
                                });
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }
}

class BedInfo {
  String? bedId;
  String? plannerStageId;
  String? varietyName;

  BedInfo({this.bedId, this.plannerStageId, this.varietyName});

  BedInfo.fromJson(Map<String, dynamic> json) {
    bedId = json['bed_id'];
    plannerStageId = json['planner_stage_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bed_id'] = bedId;
    data['planner_stage_id'] = plannerStageId;
    return data;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BedInfo &&
          runtimeType == other.runtimeType &&
          bedId == other.bedId &&
          plannerStageId == other.plannerStageId &&
          varietyName == other.varietyName;

  @override
  int get hashCode =>
      bedId.hashCode ^ plannerStageId.hashCode ^ varietyName.hashCode;
}
