import 'package:collection/collection.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dtp;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/get_clerk_performance_resonse.dart';

import '../../../core/enums/view_state.dart';
import '../../../core/viewmodels/clerk_performance_model.dart';
import '../../../utils/loader.dart';
import '../../../utils/useful.dart';
import '../../base_view.dart';

class StaffCollectionPage extends StatelessWidget {
  static const tag = 'staff_collection_page';
  final StaffPerformanceParams staffParams;

  StaffCollectionPage({super.key, required this.staffParams});

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<ClerkPerformanceModel>(
      onModelReady: (model) async {
        final DateTime now = DateTime.now();
        final startOfWeek = now.subtract(Duration(days: now.weekday - 1));
        model.startDate = df.format(startOfWeek.add(const Duration(days: 1)));
        await onGetPerformance(
            onSuccess: () {},
            model: model,
            onError: (title, message) {
              showErrorDialog(title, context, message);
            });
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              appBar: AppBar(
                backgroundColor: Colors.black,
                title: Text("${staffParams.staffName} Performance"),
                centerTitle: true,
                automaticallyImplyLeading: true,
                actions: [
                  IconButton(
                      onPressed: () {
                        onShowFilterDialog(
                            onFilter: (String startDate, String endDate) async {
                          model.startDate = startDate;
                          model.endDate = endDate;
                          await onGetPerformance(
                              onSuccess: () {},
                              model: model,
                              onError: (title, message) {
                                showErrorDialog(title, context, message);
                              });
                        });
                      },
                      icon: const Icon(Icons.filter_alt_outlined))
                ],
              ),
              body: Padding(
                padding:
                    const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                child: PerformanceContent(
                    performance: model.performance,
                    startDate: model.startDate,
                    endDate: model.endDate),
              ),
            )
          : const Loader(),
    );
  }

  Future<void> onGetPerformance(
      {required Function() onSuccess,
      required ClerkPerformanceModel model,
      required Function(dynamic title, dynamic message) onError}) async {
    try {
      var response = await model.getStaffPerformance(staffParams.staffId);
      if (response.isRight) {
        onSuccess();
      } else {
        onError("Error Getting Staff Performance", response.left.message);
      }
    } catch (e) {
      onError("Error Getting Staff Performance", e.toString());
    }
  }

  Future<void> onShowFilterDialog(
      {required Function(String startDate, String endDate) onFilter}) async {
    final TextEditingController _startDate = TextEditingController();
    final TextEditingController _endDate = TextEditingController();
    DateTime? startDate;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        insetPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        backgroundColor: Colors.grey[900],
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const ListTile(
                  title: Text(
                    "Filter By Date",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),

                TextFormField(
                  controller: _startDate,
                  keyboardType: TextInputType.number,
                  style: const TextStyle(color: Colors.white),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter start date";
                    }
                    return null;
                  },
                  enableInteractiveSelection: false,
                  autofocus: false,
                  readOnly: true,
                  onTap: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime:
                        DateTime.now().subtract(const Duration(days: 1000)),
                        maxTime:
                        DateTime.now(),
                        theme: dtp.DatePickerTheme(
                          headerColor: Colors.grey.shade900,
                          backgroundColor: Colors.grey.shade900,
                          itemStyle: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          cancelStyle: const TextStyle(
                            color: Colors.deepOrange,
                            fontSize: 24,
                          ),
                          doneStyle: TextStyle(
                            color: Colors.green.shade700,
                            fontSize: 24,
                          ),
                        ), onChanged: (date) {
                      print(
                          'change $date in time zone ${date.timeZoneOffset.inHours}');
                    }, onConfirm: (date) {
                      setState(() {
                        startDate = date;
                        _startDate.text = df.format(date);
                      });
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                  },
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white10,
                    isDense: true,
                    hintText: "",
                    labelText: "Start Date",
                    labelStyle: TextStyle(color: fromHex(yellow)),
                    hintStyle: TextStyle(
                      color: Colors.blueGrey[400],
                    ),
                    border: InputBorder.none,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: fromHex(yellow)),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: _endDate,
                  keyboardType: TextInputType.number,
                  style: const TextStyle(color: Colors.white),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter end date";
                    }
                    return null;
                  },
                  enableInteractiveSelection: false,
                  autofocus: false,
                  readOnly: true,
                  onTap: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: startDate??
                            DateTime.now().subtract(const Duration(days: 1000)),
                        maxTime: DateTime.now().subtract(const Duration(days: 1)),
                        theme: dtp.DatePickerTheme(
                          headerColor: Colors.grey.shade900,
                          backgroundColor: Colors.grey.shade900,
                          itemStyle: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          cancelStyle: const TextStyle(
                            color: Colors.deepOrange,
                            fontSize: 24,
                          ),
                          doneStyle: TextStyle(
                            color: Colors.green.shade700,
                            fontSize: 24,
                          ),
                        ), onChanged: (date) {
                      print(
                          'change $date in time zone ${date.timeZoneOffset.inHours}');
                    }, onConfirm: (date) {
                      setState(() {
                        _endDate.text = df.format(date);
                      });
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                  },
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white10,
                    isDense: true,
                    hintText: "",
                    labelText: "End Date",
                    labelStyle: TextStyle(color: fromHex(yellow)),
                    hintStyle: TextStyle(
                      color: Colors.blueGrey[400],
                    ),
                    border: InputBorder.none,
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: fromHex(yellow)),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // model.sectionDetails.length != 1
                //     ? Row(
                //         children: [
                //           Expanded(
                //             child: commonSectionDetailsDropDownSearch(
                //               items: model.sectionDetails,
                //               onChanged: (SectionDetails ld) => {
                //                 setState(() {
                //                   sectionId = ld.id;
                //                 })
                //               },
                //             ),
                //           )
                //         ],
                //       )
                //     : Container(),
                Row(
                  children: [
                    Expanded(
                      child: ListTile(
                        title: const Text(
                          "Cancel",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.deepOrange,
                              fontWeight: FontWeight.bold),
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Expanded(
                      child: ListTile(
                        title: const Text(
                          "Confirm",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.greenAccent,
                              fontWeight: FontWeight.bold),
                        ),
                        onTap: () async {
                          if (_startDate.text.isEmpty) {
                            showToast("Please specify start date");
                            return;
                          } else if (_endDate.text.isEmpty) {
                            showToast("Please specify end date");
                            return;
                          } else {
                            Navigator.of(context).pop();
                            onFilter(_startDate.text, _endDate.text);
                          }
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PerformanceContent extends StatelessWidget {
  final List<PerformanceData> performance;
  final String startDate;
  final String endDate;

  const PerformanceContent(
      {super.key,
      required this.startDate,
      required this.performance,
      required this.endDate});

  @override
  Widget build(BuildContext context) {
    return performance.isNotEmpty
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemCount: performance.length,
            shrinkWrap: true,
            itemBuilder: (context, index) => Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Variety",
                              style: _titleStyle(),
                            ),
                            Text(performance[index].varietyName ?? "",
                                style: _normalStyle())
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Species",
                              style: _titleStyle(),
                            ),
                            Text(performance[index].speciesName ?? "",
                                style: _normalStyle())
                          ],
                        ))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Contract",
                              style: _titleStyle(),
                            ),
                            Text(performance[index].contractNo ?? "",
                                style: _normalStyle())
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Total Weight",
                              style: _titleStyle(),
                            ),
                            Text(
                                "${double.parse(performance[index].totalWeight ?? "0").toPrecision(2)}",
                                style: _normalStyle())
                          ],
                        ))
                      ],
                    ),
                    Container(
                      constraints: BoxConstraints(
                          minHeight: MediaQuery.of(context).size.height * 0.1,
                          minWidth: MediaQuery.of(context).size.width,
                          maxHeight: MediaQuery.of(context).size.height * 0.2,
                          maxWidth: MediaQuery.of(context).size.width),
                      child: PerformanceDataTable(
                        performance: performance[index].items ?? [],
                        endDate: endDate,
                        startDate: startDate,
                      ),
                    ),
                  ],
                ))
        : Column(
            children: [
              Expanded(
                  child: Center(
                child: Text(
                    "No Data Found between ${Jiffy.parse(startDate).yMMMEd} and ${Jiffy.parse(endDate).yMMMEd}"),
              ))
            ],
          );
    ;
  }

  _titleStyle() {
    return TextStyle(
        fontWeight: FontWeight.bold, fontSize: 16, color: fromHex(yellow));
  }

  _normalStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
  }
}

class PerformanceDataTable extends StatelessWidget {
  final List<PerformanceItems> performance;
  final String startDate;
  final String endDate;

  const PerformanceDataTable(
      {super.key,
      required this.performance,
      required this.endDate,
      required this.startDate});

  @override
  Widget build(BuildContext context) {
    return performance.isNotEmpty
        ? DataTable2(
            columnSpacing: 12,
            horizontalMargin: 12,
            minWidth: 1400,
            columns: const [
              DataColumn2(
                label: Text('#'),
                size: ColumnSize.S,
              ),
              DataColumn2(
                label:
                    Align(alignment: Alignment.center, child: Text('Batch ID')),
                size: ColumnSize.S,
              ),
              DataColumn2(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Date Of Collection')),
                size: ColumnSize.L,
              ),
              DataColumn2(
                label: Align(alignment: Alignment.center, child: Text('Bed')),
                size: ColumnSize.S,
              ),
              // DataColumn2(
              //   label: Align(
              //       alignment: Alignment.center, child: Text('Associated Line/ Contract')),
              //   size: ColumnSize.L,
              // ),
              // DataColumn2(
              //   label: Align(
              //       alignment: Alignment.center, child: Text('Associated Species(Variety)')),
              //   size: ColumnSize.L,
              // ),
              DataColumn2(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Batch Dirty Weight(g)')),
                size: ColumnSize.L,
              ),
              DataColumn2(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Batch Clean Weight(g)')),
                size: ColumnSize.L,
              ),
              DataColumn2(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Clerk Weight Contribution(%)')),
                size: ColumnSize.L,
              ),
              DataColumn2(
                label: Align(
                    alignment: Alignment.center,
                    child: Text('Clean Weight(g)')),
                size: ColumnSize.L,
              ),
            ],
            rows: performance
                .mapIndexed((index, e) => DataRow(
                      cells: [
                        DataCell(Text('${index + 1}')),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text('${e.batchId}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text('${e.batchDate}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text('${e.bedName}'))),
                        // DataCell(Align(
                        //     alignment: Alignment.center,
                        //     child: Text('${e.contractNo}'))),
                        // DataCell(Align(
                        //     alignment: Alignment.center,
                        //     child: Text('${e.varietyName}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text(
                                '${double.parse(e.batchDirtyWeight ?? '0').toPrecision(2)}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text(
                                '${double.parse(e.batchCleanWeight ?? '0').toPrecision(2)}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text('${e.contributionPercentage}'))),
                        DataCell(Align(
                            alignment: Alignment.center,
                            child: Text(
                                '${double.parse(e.cleanWeight ?? '0').toPrecision(2)}'))),
                      ],
                    ))
                .toList(),
          )
        : Column(
            children: [
              Expanded(
                  child: Center(
                child: Text(
                    "No Data Found between ${Jiffy.parse(startDate).yMMMEd} and ${Jiffy.parse(endDate).yMMMEd}"),
              ))
            ],
          );
  }
}

class StaffPerformanceParams {
  String staffName;
  int staffId;

  StaffPerformanceParams({required this.staffName, required this.staffId});
}
