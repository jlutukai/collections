import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tags_x/flutter_tags_x.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/scan_bag_response.dart';
import 'package:weight_collection/core/models/scan_bed_response.dart';
import 'package:weight_collection/core/viewmodels/scan_type_bag_model.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';

import '../../core/enums/view_state.dart';
import '../../core/models/general_response.dart';
import '../../core/models/get_bed_line_items_response.dart';
import '../../core/models/scan_bag_qr_body.dart';
import '../../utils/loader.dart';
import '../../utils/useful.dart';
import '../base_view.dart';
import '../reportForms.dart';

class ScanTypeBag extends StatefulWidget {
  static const tag = 'bag';
  final String id;

  const ScanTypeBag({required this.id, super.key});

  @override
  State<ScanTypeBag> createState() => _ScanTypeBagState();
}

class _ScanTypeBagState extends State<ScanTypeBag> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<ScanTypeBagModel>(
      onModelReady: (model) async {
        await reloadData(model);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              body: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Center(
                        child: model.scanResult.isEmpty
                            ? isEmptyResponse(
                                "Bag Info Not Found, Try Different Bag")
                            : _collectionInfo(context, model.scanResult.first),
                      ),
                    ),
                  ),
                  _buttons(context, model)
                ],
              ),
            )
          : const Loader(),
    );
  }

  _buttons(BuildContext context, ScanTypeBagModel model) {
    return Column(
      children: [
        _sliderButton("Assign to", () async {
          var r = await getScanResult(context);
          if (r == null) {
            showToast("Make sure the correct QR is scanned!");
            return;
          }
          if (r.type != "bed") {
            showToast("Scan Bed Id to proceed");
            return;
          }
          getStaffInfo(r, (plannerStageId) async {
            await assignToBed(r, model, plannerStageId);
          });
        }, "sack_assign_to"),
        _sliderButton("Forms", () async {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  ReportForms(id: int.parse(widget.id), type: "bag")));
        }, 'employee_report'),
        model.scanResult.isNotEmpty
            ? model.scanResult.first.bagStatus == "Empty"
                ? _sliderButton("Weighing ", () async {
                    confirmBagClearing(onConfirm: (staff) async {
                      await setBagForWeighing(model, staff);
                    }, title:  "Confirm Weighing",
                      desc:   "Are you sure you want to set this bag for weighing?", bedId:  model.scanResult.first.bedId);
                  }, "sack_weighing")
                : _sliderButton("Clear Bag ", () async {
                    confirmBagClearing( onConfirm: (staff) async {
                      await clearBag(model);
                    }, title:  "Confirm Bag Clearing",
                      desc:   "Are you sure you want to proceed with clearing this Bag, all assignments to the bag will be lost ");
                  }, "clear_sacks")
            : Container()
      ],
    );
  }

  Future<void> getStaffInfo(
    ScanBagQRBody r,
    Future Function(String itemId) onClickAction,
  ) async {
    String? lineDataId;
    await showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (context) => BaseView<ScanTypeBagModel>(
        onModelReady: (model) async {
          if (r.type == "bed") {
            try {
              await model.getScanBed({"bed_id": r.id});
            } catch (e) {
              showToast("Failed to get Bed information");
              Navigator.of(context).pop();
            }
            try {
              await model.getLineItems({"bed_id": r.id});
            } catch (e) {
              showToast("Failed to get Line Items");
              Navigator.of(context).pop();
            }
          } else {
            showToast("Error type not captured");
            Navigator.of(context).pop();
          }
        },
        builder: (context, model, child) => model.state == ViewState.Idle
            ? Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const ListTile(
                          title: Text(
                            "Confirm Bed Assignment",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Confirm Assignment of ${model.associatedData?.name ?? "Bed"}  to this Bag..  Proceed ? ",
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: commonDropDownSearch(
                                items: model.lineData,
                                onChanged: (LineData ld) => {
                                  setState(() {
                                    lineDataId = ld.plannerStageId;
                                  })
                                },
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if (lineDataId == null) {
                                    showToast("Choose Line Item");
                                    return;
                                  } else {
                                    Navigator.of(context).pop();
                                    onClickAction(lineDataId!);
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : const Loader(),
      ),
    );
  }

  Future<void> assignToBed(
      ScanBagQRBody r, ScanTypeBagModel model, String plannerStageId) async {
    if (r.type == "bed") {
      try {
        var response = await model.assignBagToBed({
          "bag_id": widget.id,
          "bed_id": r.id,
          "user_id": getUserId(),
          "planner_stage_id": plannerStageId
        });
        GeneralResponse gr = GeneralResponse.fromJson(response['response']);
        if (gr.error == false) {
          showToast(gr.message ?? "Success", i: 1);
          await reloadData(model);
        } else {
          showToast(gr.erroMessage ?? "Failed", i: 1);
        }
      } catch (e) {
        showToast("Failed to assign bag to bed", i: 1);
      }
    } else {
      showToast("unknown type given", i: 1);
    }
  }

  _collectionInfo(BuildContext context, ScanBagData scanResult) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 10, left: 10),
          child: Card(
            color: Colors.black,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 25, top: 25),
                  decoration: BoxDecoration(
                    color: Colors.white10,
                    border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, top: 60.0, bottom: 8.0),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Bag",
                                    style: _titleStyle(),
                                  ),
                                  Text(scanResult.bagId ?? "",
                                      style: _normalStyle()),
                                  Text(
                                    "variety",
                                    style: _titleStyle(),
                                  ),
                                  Text(scanResult.varietyName ?? "",
                                      style: _normalStyle()),
                                  Text(
                                    "Bed",
                                    style: _titleStyle(),
                                  ),
                                  Text(scanResult.bedName ?? "",
                                      style: _normalStyle()),
                                  Text(
                                    "Status",
                                    style: _titleStyle(),
                                  ),
                                  Text(scanResult.bagStatus ?? "",
                                      style: _normalStyle())
                                ],
                              ),
                            ),
                            Expanded(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Staff Assigned",
                                  style: _titleStyle(),
                                ),
                                Text(scanResult.staffName ?? "",
                                    style: _normalStyle()),
                                Text("Stage  ", style: _titleStyle()),
                                Text(scanResult.stageName ?? '',
                                    style: _normalStyle()),
                                Text("End Date  ", style: _titleStyle()),
                                Text(Jiffy.parse(scanResult.endDate??"").yMMMd,
                                    style: _normalStyle())
                              ],
                            ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: varietyImage(pictureName: scanResult.pictureName))
      ],
    );
  }

  _titleStyle() {
    return TextStyle(
        fontWeight: FontWeight.bold, fontSize: 16, color: fromHex(yellow));
  }

  _normalStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
  }

  _sliderButton(
    String buttonName,
    Future Function() onClickAction,
    String assetName,
  ) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        onClickAction();
      },
      child: InkWell(
        onTap: () async {
          onClickAction();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Card(
            color: Colors.white10.withOpacity(0.07),
            child: Container(
              // color: fromHex("#c19d3d"),
              height: 60,
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets1/$assetName.svg",
                        width: 36,
                        height: 36,
                        color: fromHex(yellow),
                      ),
                      const SizedBox(
                        width: 60,
                      ),
                      Text(
                        buttonName,
                        style: TextStyle(
                            color: fromHex(yellow),
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      )
                    ],
                  ),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: fromHex(yellow),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> confirmBagClearing({
      required Future Function(List<String>? staff) onConfirm, required String title, required String desc, String? bedId}) async {
    Set<StaffDetails> selected = {};
    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => BaseView<ScanTypeBagModel>(
          onModelReady: (model) async {
            try {
              await model.getScanBed({"bed_id": bedId});
              selected.addAll(model.staffDetails);
            } catch (e) {
              showToast("Failed to get Bed information");
              Navigator.of(context).pop();
            }
          },
          builder: (context, model, child) => model.state == ViewState.Idle
              ?  Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                elevation: 0.0,
                backgroundColor: Colors.grey[900],
                child: StatefulBuilder(
                  builder: (context, StateSetter setState) => Container(
                    color: Colors.transparent,
                    padding:
                        const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ListTile(
                          title: Text(
                            title,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                desc,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        Visibility(
                          visible: bedId != null,
                          child: Column(
                            children: [
                              const SizedBox(height: 10,),
                              const Text(
                                "Confirm Assigned Staff",
                                textAlign: TextAlign.center,
                              ),
                              const SizedBox(height: 10,),
                              Row(
                                children: [
                                  Expanded(
                                    child: Tags(
                                      itemCount: selected.length ?? 0,
                                      alignment: WrapAlignment.start,
                                      itemBuilder: (int? index) {
                                        var type =
                                        selected.elementAt(index ?? 0);
                                        return Tooltip(
                                            message:
                                            "id : ${type.staffName ?? ""}",
                                            child: ItemTags(
                                              textStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight:
                                                  FontWeight.normal,
                                                  color: fromHex(yellow)),
                                              color: Colors.white10
                                                  .withOpacity(0.07),
                                              activeColor: Colors.white10
                                                  .withOpacity(0.07),
                                              singleItem: true,
                                              index: index ?? 0,
                                              title: type.staffName ?? "",
                                              textColor: Colors.white,
                                              border: Border.all(
                                                  color: Colors.black),
                                              removeButton:
                                              ItemTagsRemoveButton(
                                                color: Colors.deepOrange,
                                                backgroundColor:
                                                Colors.transparent,
                                                onRemoved: () {
                                                  setState(() {
                                                    selected.remove(type);
                                                  });
                                                  return true;
                                                },
                                              ), // OR nu
                                            ));
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: commonStaffDetailsDropDownSearch(
                                      items: model.pollinatedStaff,
                                      onChanged: (StaffDetails bd) {
                                        setState(() {
                                          selected.add(bd);
                                        });
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Cancel",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.deepOrange,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text(
                                  "Confirm",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.greenAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  if((bedId??"").isNotEmpty && selected.isEmpty){
                                    showToast("Please specify assigned staff");
                                    return;
                                  }
                                  Navigator.of(context).pop();
                                  onConfirm(selected.map((e) => e.id??"").toList());
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ): const Loader(),
        ));
  }

  Future<void> clearBag(ScanTypeBagModel model) async {
    try {
      var response =
          await model.clearBag({"user_id": getUserId(), "bag_id": widget.id});
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        await reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to clear bag", i: 1);
    }
  }

  setBagForWeighing(ScanTypeBagModel model, List<String>? staff) async {
    try {
      var response = await model
          .updateBagWeighment({"user_id": getUserId(), "bag_id": widget.id, "staff": staff});
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
        await reloadData(model);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to clear bag", i: 1);
    }
  }

  Future<void> reloadData(ScanTypeBagModel model) async {
    await model.getScanBag({"bag_id": widget.id});
  }
}
