import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/scan_greenhouse_response.dart';
import 'package:weight_collection/core/viewmodels/scan_greenhouse_model.dart';
import 'package:weight_collection/ui/base_view.dart';
import 'package:weight_collection/ui/scanManagement/utils.dart';

import '../../core/enums/view_state.dart';
import '../../core/models/general_response.dart';
import '../../core/models/get_scan_greenhouse_response.dart';
import '../../core/models/scan_bed_response.dart';
import '../../utils/loader.dart';
import '../../utils/useful.dart';
import '../reportForms.dart';

class ScanTypeGreenHouse extends StatefulWidget {
  static const tag = 'green_house';
  final String id;

  const ScanTypeGreenHouse({required this.id, super.key});

  @override
  State<ScanTypeGreenHouse> createState() => _ScanTypeGreenHouseState();
}

class _ScanTypeGreenHouseState extends State<ScanTypeGreenHouse> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<ScanGreenHouseModel>(
      onModelReady: (model) async {
        await onGetData(model);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              body: Column(
                children: [
                  Expanded(
                    child: Center(
                      child: model.scanResult != null
                          ? _greenHouseInfo(context, model.scanResult!)
                          : isEmptyResponse(
                              "Green House Info Not Found, Try Different Green House ID"),
                    ),
                  ),
                  _buttons(context, model),
                  const SizedBox(
                    height: 30,
                  )
                ],
              ),
            )
          : const Loader(),
    );
  }

  _greenHouseInfo(BuildContext context, GreenHouseData data) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 10, left: 10),
          child: Card(
            color: Colors.black,
            child: Container(
              margin: const EdgeInsets.only(bottom: 25, top: 25),
              decoration: BoxDecoration(
                color: Colors.white10,
                border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 10, left: 16, right: 16, bottom: 20),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "${data.greenhouseName}",
                          style: TextStyle(
                            fontSize: 24,
                            color: fromHex(yellow),
                          ),
                        )
                      ],
                    ),

                    Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Area : ",
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "${data.greenhouseArea}",
                              style: TextStyle(
                                color: fromHex(yellow),
                              ),
                            )
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Used : ",
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "${data.totalUsedArea}",
                              style: TextStyle(
                                color: fromHex(yellow),
                              ),
                            )
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Rate : ",
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "${data.usedAreaRate??0.0} %",
                              style: TextStyle(
                                color: fromHex(yellow),
                              ),
                            )
                          ],
                        ))
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Beds : ",
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "${data.greenhouseBedsNo}",
                              style: TextStyle(
                                color: fromHex(yellow),
                              ),
                            )
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Used : ",
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "${data.usedBeds??0.0}",
                              style: TextStyle(
                                color: fromHex(yellow),
                              ),
                            )
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: [
                            Text(
                              "Rate : ",
                              style: TextStyle(
                                color: fromHex(yellow),
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "${data.usedBedsRate} %",
                              style: TextStyle(
                                color: fromHex(yellow),
                              ),
                            )
                          ],
                        ))
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Expanded(child:
                    _bedDetails(data.lineDetails ?? []))
                  ],
                ),
              ),
            ),
          ),
        ),
        // Positioned(
        //   top: 0,
        //   right: 0,
        //   left: 0,
        //   child: Container(
        //     height: 100,
        //     width: 100,
        //     decoration: BoxDecoration(
        //       shape: BoxShape.circle,
        //       image: DecorationImage(
        //         image: CachedNetworkImageProvider(data.staffPicture != null
        //             ? "${getUrl()}/CUSTOM/Employees/profile_pictures/${data.staffPicture}"
        //             : noImage),
        //       ),
        //     ),
        //   ),
        // )
      ],
    );
  }

  _buttons(BuildContext context, ScanGreenHouseModel model) {
    return Column(
      children: [
        _sliderButton("Maintenance ", () async {
          setUpGHMaintenance((String description) async {
            await setGHMaintenance(model, description);
          });
        }, Icons.construction),
        _sliderButton("Forms", () async {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  ReportForms(id: int.parse(widget.id), type: "green_house")));
        }, Icons.auto_graph),
      ],
    );
  }

  _sliderButton(
    String buttonName,
    Function() onClickAction,
    IconData iconData,
  ) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        onClickAction();
      },
      child: InkWell(
        onTap: () async {
          onClickAction();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Card(
            color: Colors.white10.withOpacity(0.07),
            child: Container(
              // color: fromHex("#c19d3d"),
              height: 60,
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        iconData,
                        size: 36,
                        color: fromHex(yellow),
                      ),
                      const SizedBox(
                        width: 60,
                      ),
                      Text(
                        buttonName,
                        style: TextStyle(
                            color: fromHex(yellow),
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      )
                    ],
                  ),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: fromHex(yellow),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _bedDetails(List<LineDetails> beds) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: beds.length,
      shrinkWrap: true,
      itemBuilder: (context, index) => _bedInfoItem(context, beds[index]),
    );
  }

  _bedInfoItem(BuildContext context, LineDetails bed) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
          const SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("${bed.contractNo ?? ""} - ${bed.stageName ?? ""}",
                        style: _titleStyle()),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Total Female Plant No.",
                            style: _titleStyle(),
                          ),
                          Text(bed.stageFemalePlantNo ?? "",
                              style: _normalStyle()),
                          Text(
                            "Total Lost Quantity",
                            style: _titleStyle(),
                          ),
                          Text("${bed.totalLostQuantity ?? 0}",
                              style: _normalStyle()),
                          Text(
                            "variety",
                            style: _titleStyle(),
                          ),
                          Text((bed.type ?? "") == 'Male'? "${bed.varietyName ?? ""} - P": "${bed.varietyName ?? ""} - F", style: _normalStyle()),
                          // Text(
                          //   "Quota(%)",
                          //   style: _titleStyle(),
                          // ),
                          // Text(
                          //     (bed.type ?? "") == 'Male'
                          //         ? "P - ${bed.stageMalePlantNo}"
                          //         : 'F - ${bed.stageFemalePlantNo}',
                          //     style: _normalStyle()),
                          Visibility(
                              visible: bed.type == 'Female',
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Quota(%)",
                                    style: _titleStyle(),
                                  ),
                                  Text(
                                      "${(bed.quotaProduced ?? 0.0).toInt()}${bed.uom ?? ''}  (${(bed.quotaProducedPercentage ?? 0.0).toPrecision(1)}%)",
                                      style: _normalStyle()),
                                ],
                              ))
                        ],
                      ),
                    ),
                    Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Total Male Plant No.",
                              style: _titleStyle(),
                            ),
                            Text(bed.stageMalePlantNo ?? "", style: _normalStyle()),
                            Text(
                              "Stage Lost Quantity",
                              style: _titleStyle(),
                            ),
                            Text("${bed.currentStageLostQuantity ?? 0}",
                                style: _normalStyle()),
                            Text("Plant No.  ", style: _titleStyle()),
                            Text(bed.currentPlantNo ?? '', style: _normalStyle()),
                            // Text("End Date  ", style: _titleStyle()),
                            // Text(Jiffy.parse(bed.endDate ?? "").yMMMd,
                            //     style: _normalStyle())
                          ],
                        ))
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _titleStyle() {
    return TextStyle(
        fontWeight: FontWeight.bold, fontSize: 16, color: fromHex(yellow));
  }

  _normalStyle() {
    return TextStyle(
        fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
  }

  // _lineItems(List<LineDetails> lineDetails) {
  //   return DataTable(
  //     columns: <DataColumn>[
  //       DataColumn(
  //         label: Text(
  //           'Image',
  //           style: TextStyle(
  //               fontWeight: FontWeight.w800,
  //               fontSize: 16,
  //               color: fromHex(yellow)),
  //         ),
  //       ),
  //       DataColumn(
  //         label: Text(
  //           'Line',
  //           style: TextStyle(
  //               fontWeight: FontWeight.w800,
  //               fontSize: 16,
  //               color: fromHex(yellow)),
  //         ),
  //       ),
  //       DataColumn(
  //         label: Text(
  //           'Stage',
  //           style: TextStyle(
  //               fontWeight: FontWeight.w800,
  //               fontSize: 16,
  //               color: fromHex(yellow)),
  //         ),
  //       ),
  //       DataColumn(
  //         label: Text(
  //           'Beds',
  //           style: TextStyle(
  //               fontWeight: FontWeight.w800,
  //               fontSize: 16,
  //               color: fromHex(yellow)),
  //         ),
  //       ),
  //       DataColumn(
  //         label: Text(
  //           'End Date',
  //           style: TextStyle(
  //               fontWeight: FontWeight.w800,
  //               fontSize: 16,
  //               color: fromHex(yellow)),
  //         ),
  //       ),
  //     ],
  //     rows: lineDetails
  //         .map(
  //           (e) => DataRow(
  //             cells: [
  //               DataCell(
  //                 Container(
  //                   height: 50,
  //                   width: 50,
  //                   margin: const EdgeInsets.all(8),
  //                   decoration: BoxDecoration(
  //                     shape: BoxShape.circle,
  //                     image: DecorationImage(
  //                       image: CachedNetworkImageProvider(
  //                           '${getUrl()}/CUSTOM/Variety/picture/${e.pictureName}'),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //               DataCell(
  //                 Text(
  //                   "${e.varietyName}",
  //                   style: TextStyle(
  //                     color: fromHex(yellow),
  //                   ),
  //                 ),
  //               ),
  //               DataCell(
  //                 Text(
  //                   "${e.stageName}",
  //                   style: TextStyle(
  //                     color: fromHex(yellow),
  //                   ),
  //                 ),
  //               ),
  //               DataCell(
  //                 Text(
  //                   "${e.stageBedsUsed}",
  //                   style: TextStyle(
  //                     color: fromHex(yellow),
  //                   ),
  //                 ),
  //               ),
  //               DataCell(
  //                 Text(
  //                   "${e.stageEndDate}",
  //                   style: TextStyle(
  //                     color: fromHex(yellow),
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         )
  //         .toList(),
  //   );
  // }

  setGHMaintenance(ScanGreenHouseModel model, String description) async {
    try {
      var response = await model.updateMaintenance({
        "description": description,
        "greenhouse_id": widget.id,
        "user_id": getUserId()
      });
      GeneralResponse gr = GeneralResponse.fromJson(response['response']);
      if (gr.error == false) {
        showToast(gr.message ?? "Success", i: 1);
      } else {
        showToast(gr.erroMessage ?? "Failed", i: 1);
      }
    } catch (e) {
      showToast("Failed to update maintenance description", i: 1);
    }
  }

  setUpGHMaintenance(Future<void> Function(String) onConfirm) async {
    final TextEditingController description = TextEditingController();

    await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) => Container(
                  color: Colors.transparent,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ListTile(
                        title: Text(
                          "Maintenance Status",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      TextFormField(
                        controller: description,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 6,
                        textAlignVertical: TextAlignVertical.top,
                        style: const TextStyle(color: Colors.white),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please Enter Maintenance Comment";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white10,
                          isDense: true,
                          hintText: "eg Some Comment",
                          labelText: "Comment",
                          alignLabelWithHint: true,
                          labelStyle: TextStyle(color: fromHex(yellow)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text(
                                "Confirm",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.greenAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                if (description.text.isEmpty) {
                                  showToast("Enter maintenance description");
                                  return;
                                }
                                Navigator.of(context).pop();
                                onConfirm(description.text.toString());
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ));
  }

  onGetData(ScanGreenHouseModel model) async {
    await onGetGreenHouseData(
        onSuccess: () {},
        model: model,
        onError: (title, message) {
          showErrorDialog(title, context, message);
        });
  }

  Future<void> onGetGreenHouseData(
      {required void Function() onSuccess,
      required ScanGreenHouseModel model,
      required void Function(dynamic title, dynamic message) onError}) async {
    try {
      Map<String, dynamic> data = {'greenhouse_id': widget.id};
      var response = await model.scanGreenHouse(data);
      if (response.isRight) {
        onSuccess();
      } else {
        onError("Error Getting Green House Data", response.left.message);
      }
    } catch (e) {
      onError("Error Getting Green House Data", e.toString());
    }
  }
}
