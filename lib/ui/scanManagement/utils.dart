import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:weight_collection/core/models/get_available_lines.dart';
import 'package:weight_collection/core/models/get_loss_reason_response.dart';
import 'package:weight_collection/core/models/get_plants_in_suspense_response.dart';
import 'package:weight_collection/core/models/get_species_response.dart';
import 'package:weight_collection/core/models/get_staff_response.dart';

import '../../core/models/get_bed_line_items_response.dart';
import '../../core/models/get_variety_response.dart';
import '../../core/models/scan_bag_qr_body.dart';
import '../../core/models/scan_bed_response.dart';
import '../../core/models/scan_staff_response.dart';
import '../../utils/QrScanner.dart';
import '../../utils/useful.dart';
import '../design_system/DropDownSearch.dart';

Future<ScanBagQRBody?> getScanResult(BuildContext context) async {
  ScanBagQRBody? scanResponse;
  var result = await Navigator.of(context).pushNamed(QRScanner.tag) as String;

  if ((result??"").isEmpty) {
    showToast("Scan again, no code captured");
    return null;
  }
  if (kDebugMode) {
    print("####################$result");
    print(jsonDecode(result) is Map<String, dynamic>);
  }
  try {
    if (jsonDecode(result) is Map<String, dynamic>) {
      if (kDebugMode) {
        print(result);
      }
      scanResponse = ScanBagQRBody.fromJson(jsonDecode(result));
    }
  } catch (e) {
    FirebaseCrashlytics.instance.log(e.toString());
  }

  return scanResponse;
}

Widget isEmptyResponse(String s) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text(
        s,
        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      )
    ],
  );
}

Widget commonDropDownSearch({
  required List<LineData> items,
  required Function(LineData ld) onChanged,
}) {
  return CommonDropDownSearch<LineData>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: "Line Item",
    hint: "Select Line Item",
    items: items,
    validationText: "Please Select Line Item",
    showSearchBox: true,
    searchHint: "Search Line Item",
    itemWidget: (u) => _infoLineItem(u),
  );
}

Widget commonLineItemDrop(BuildContext context, LineData? item) {
  return item != null
      ? _summaryInfoLineItem(item)
      : Text(
          "Select Line Item",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonLineItemPopUp(
    BuildContext context, LineData item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: _infoLineItem(item),
  );
}

_summaryInfoLineItem(LineData bed) {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 5),
    decoration: const BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(Radius.circular(16))),
    child: Row(
      children: [
        varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
        const SizedBox(
          width: 5,
        ),
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Stage",
                          style: _titleStyle(),
                        ),
                        Text(bed.stageName ?? "", style: _normalStyle()),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Variety ", style: _titleStyle()),
                      Text("${bed.varietyName} - ${bed.type}",
                          style: _normalStyle()),
                    ],
                  ))
                ],
              )
            ],
          ),
        ),
      ],
    ),
  );
}

Widget commonBedDetailsDropDownSearch({
  required List<BedDetails> items,
  required Function(BedDetails ld) onChanged,
}) {
  return CommonDropDownSearch<BedDetails>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: "Bed",
    hint: "Select Bed",
    items: items,
    validationText: "Please Select Bed",
    showSearchBox: true,
    searchHint: "Search Bed",
    itemWidget: (u) => _infoBedItem(u),
  );
}

Widget commonBedItemDrop(BuildContext context, BedDetails? item) {
  return item != null
      ? _summaryInfoBedItem(item)
      : Text(
          "Select Bed",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

_summaryInfoBedItem(BedDetails bed) {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 5),
    decoration: const BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(Radius.circular(16))),
    child: Row(
      children: [
        varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
        const SizedBox(
          width: 5,
        ),
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Bed",
                          style: _titleStyle(),
                        ),
                        Text(bed.bedName ?? "", style: _normalStyle()),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Variety ", style: _titleStyle()),
                      Text("${bed.varietyName}", style: _normalStyle()),
                    ],
                  ))
                ],
              ),
              Column(
                children: [
                  Text(
                    "Stage",
                    style: _titleStyle(),
                  ),
                  Text(bed.stageName ?? "", style: _normalStyle())
                ],
              )
            ],
          ),
        ),
      ],
    ),
  );
}

Widget commonBedItemPopUp(
    BuildContext context, BedDetails item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: _infoBedItem(item),
  );
}

_infoBedItem(BedDetails? bed) {
  return bed != null
      ? Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          decoration: const BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            children: [
              varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
              const SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Bed",
                                style: _titleStyle(),
                              ),
                              Text(bed.bedName ?? "", style: _normalStyle()),
                              Text(
                                "Bed Id",
                                style: _titleStyle(),
                              ),
                              Text(bed.bedId ?? "", style: _normalStyle()),
                              Text(
                                "Variety Id",
                                style: _titleStyle(),
                              ),
                              Text(bed.varietyName ?? "", style: _normalStyle())
                            ],
                          ),
                        ),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Variety",
                              style: _titleStyle(),
                            ),
                            Text(bed.varietyName ?? "", style: _normalStyle()),
                            Text("Start Date : ", style: _titleStyle()),
                            Text(Jiffy.parse(bed.startDate ?? "").yMMMd,
                                style: _normalStyle()),
                            Text("End Date : ", style: _titleStyle()),
                            Text(Jiffy.parse(bed.endDate ?? "").yMMMd,
                                style: _normalStyle())
                          ],
                        ))
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      : Container();
}

_infoLineItem(LineData? bed) {
  return bed != null
      ? Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: const BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
              const SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  children: [
                    const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Stage ",
                                style: _titleStyle(),
                              ),
                              Text(bed.stageName ?? "", style: _normalStyle()),
                              Text(
                                "variety  ",
                                style: _titleStyle(),
                              ),
                              Text("${bed.varietyName ?? " "} - ${bed.type}",
                                  style: _normalStyle()),
                            ],
                          ),
                        ),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("End Date ", style: _titleStyle()),
                            Text(Jiffy.parse(bed.endDate ?? "").yMMMd,
                                style: _normalStyle())
                          ],
                        ))
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      : Container();
}

_titleStyle() {
  return TextStyle(
      fontWeight: FontWeight.bold, fontSize: 16, color: fromHex(yellow));
}

_normalStyle() {
  return TextStyle(
      fontWeight: FontWeight.w300, fontSize: 14, color: fromHex(yellow));
}

Widget commonReasonDropDown({
  required List<ReasonData> items,
  required Function(ReasonData ld) onChanged,
}) {
  return CommonDropDownSearch<ReasonData>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: "Reason",
    hint: "Select Reason",
    items: items,
    validationText: "Please Select Reason",
    showSearchBox: true,
    searchHint: "Search Reason",
  );
}

Widget commonReasonDrop(BuildContext context, ReasonData? item) {
  return item != null
      ? Text(
          item.name ?? "",
          style: TextStyle(color: fromHex(deepOrange), fontSize: 15),
        )
      : Text(
          "Select Reason",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonReasonPopUp(
    BuildContext context, ReasonData item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: ListTile(
      title: Text(item.name ?? "",
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w100,
            fontSize: 20,
          )),
    ),
  );
}

Widget commonPlantsDropDownSearch({
  required List<PlantData> items,
  required Function(PlantData ld) onChanged,
}) {
  return CommonDropDownSearch<PlantData>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: "Plant",
    hint: "Select Plant",
    items: items,
    validationText: "Please Select Plant",
    showSearchBox: true,
    searchHint: "Search Plant",
    itemWidget: (u) => _summaryInfoPlantItem(u),
  );
}

Widget commonPlantItemDrop(BuildContext context, PlantData? item) {
  return item != null
      ? _summaryInfoPlantItem(item)
      : Text(
          "Select Plant",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonPlantItemPopUp(
    BuildContext context, PlantData item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: _summaryInfoPlantItem(item),
  );
}

_summaryInfoPlantItem(PlantData? bed) {
  return bed != null
      ? Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: const BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            children: [
              varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
              const SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Stage",
                                style: _titleStyle(),
                              ),
                              Text(bed.stageName ?? "", style: _normalStyle()),
                            ],
                          ),
                        ),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Variety ", style: _titleStyle()),
                            Text("${bed.varietyName}", style: _normalStyle()),
                          ],
                        ))
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        )
      : Container();
}

Widget varietyImage({String? pictureName, double? h, double? w}) {
  return Container(
    padding: const EdgeInsets.all(5),
    decoration:
        const BoxDecoration(shape: BoxShape.circle, color: Colors.white),
    child: Container(
      height: h ?? 100,
      width: w ?? 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        image: DecorationImage(
          image: CachedNetworkImageProvider(pictureName != null
              ? "${getUrl()}/CUSTOM/Variety/picture/$pictureName"
              : noImage),
        ),
      ),
    ),
  );
}

Widget commonStringDropDownSearch({
  required List<String> items,
  required Function(String s) onChanged,
  required String hint,
  required String labelText,
  required String validationText,
  required String searchHint,
  required bool showSearchBox,
}) {
  return CommonDropDownSearch<String>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: labelText,
    hint: hint,
    items: items,
    validationText: validationText,
    showSearchBox: showSearchBox,
    searchHint: searchHint,
  );
}

Widget commonStringDrop(BuildContext context, String? item) {
  return Text(
    item ?? "Select Batch Type",
    style: TextStyle(
      color: item == null ? fromHex(yellow) : Colors.white,
      fontSize: 17,
    ),
  );
}

Widget commonStringPopUp(BuildContext context, String item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: ListTile(
      title: Text(item,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w100,
            fontSize: 20,
          )),
    ),
  );
}

Widget commonDropDownVarietySearch({
  required List<VarietyData> items,
  required Function(VarietyData ld) onChanged,
}) {
  return CommonDropDownSearch<VarietyData>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: "Variety",
    hint: "Select Variety",
    items: items,
    validationText: "Please Select Variety",
    showSearchBox: true,
    searchHint: "Search Variety",
    itemWidget: (u) => _varietyItem(u),
  );
}

Widget commonVarietyItemDrop(BuildContext context, VarietyData? item) {
  return item != null
      ? _summaryVarietyInfo(item)
      : Text(
          "Select Variety",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonVarietyItemPopUp(
    BuildContext context, VarietyData item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: _varietyItem(item),
  );
}

_summaryVarietyInfo(VarietyData bed) {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 5),
    decoration: const BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(Radius.circular(16))),
    child: Row(
      children: [
        varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
        const SizedBox(
          width: 5,
        ),
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Stage",
                          style: _titleStyle(),
                        ),
                        Text(bed.stageName ?? "", style: _normalStyle()),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Variety ", style: _titleStyle()),
                      Text("${bed.varietyName}", style: _normalStyle()),
                    ],
                  ))
                ],
              )
            ],
          ),
        ),
      ],
    ),
  );
}

_varietyItem(VarietyData? bed) {
  return bed != null
      ? Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: const BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
              const SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Stage ",
                                style: _titleStyle(),
                              ),
                              Text(bed.stageName ?? "", style: _normalStyle()),
                              Text(
                                "variety  ",
                                style: _titleStyle(),
                              ),
                              Text(bed.varietyName ?? "",
                                  style: _normalStyle()),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      : Container();
}

Widget commonDropDownSpeciesSearch({
  required List<SpeciesData> items,
  required Function(SpeciesData ld) onChanged,
}) {
  return CommonDropDownSearch<SpeciesData>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: "Species",
    hint: "Select Species",
    items: items,
    validationText: "Please Select Species",
    showSearchBox: true,
    searchHint: "Search Species",
    itemWidget: (u) => _speciesItem(u),
  );
}

Widget commonSpeciesItemDrop(BuildContext context, SpeciesData? item) {
  return item != null
      ? _summarySpeciesInfo(item)
      : Text(
          "Select Species",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonSpeciesItemPopUp(
    BuildContext context, SpeciesData item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: _speciesItem(item),
  );
}

_summarySpeciesInfo(SpeciesData bed) {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 5),
    decoration: const BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(Radius.circular(16))),
    child: Row(
      children: [
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Name",
                          style: _titleStyle(),
                        ),
                        Text(bed.name ?? "", style: _normalStyle()),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Type ", style: _titleStyle()),
                      Text("${bed.type}", style: _normalStyle()),
                    ],
                  ))
                ],
              )
            ],
          ),
        ),
      ],
    ),
  );
}

_speciesItem(SpeciesData? bed) {
  return bed != null
      ? Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: const BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Name ",
                                style: _titleStyle(),
                              ),
                              Text(bed.name ?? "", style: _normalStyle()),
                              Text(
                                "Type  ",
                                style: _titleStyle(),
                              ),
                              Text(bed.type ?? "", style: _normalStyle()),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      : Container();
}

Widget commonAvailableLineDropDownSearch({
  required List<AvailableLinesData> items,
  required Function(AvailableLinesData ld) onChanged,
}) {
  return CommonDropDownSearch<AvailableLinesData>(
    isRequired: true,
    showAsterisk: true,
    onChanged: (u) {
      if (u != null) {
        onChanged(u);
      }
    },
    labelText: "Line Item",
    hint: "Select Line Item",
    items: items,
    validationText: "Please Select Line Item",
    showSearchBox: true,
    searchHint: "Search Line Item",
    itemWidget: (u) => _infoAvailableLinesDataItem(u),
  );
}

Widget commonAvailableLinesDataItemDrop(
    BuildContext context, AvailableLinesData? item) {
  return item != null
      ? _summaryInfoAvailableLinesDataItem(item)
      : Text(
          "Select Line Item",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonAvailableLinesDataItemPopUp(
    BuildContext context, AvailableLinesData item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: _infoAvailableLinesDataItem(item),
  );
}

_summaryInfoAvailableLinesDataItem(AvailableLinesData bed) {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 5),
    decoration: const BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(Radius.circular(16))),
    child: Row(
      children: [
        varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
        const SizedBox(
          width: 5,
        ),
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Stage",
                          style: _titleStyle(),
                        ),
                        Text(bed.stageName ?? "", style: _normalStyle()),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Variety ", style: _titleStyle()),
                      Text("${bed.varietyName}", style: _normalStyle()),
                    ],
                  ))
                ],
              )
            ],
          ),
        ),
      ],
    ),
  );
}

_infoAvailableLinesDataItem(AvailableLinesData? bed) {
  return bed != null
      ? Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          decoration: const BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              varietyImage(pictureName: bed.pictureName, h: 50, w: 50),
              const SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  children: [
                    const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Stage ",
                                style: _titleStyle(),
                              ),
                              Text(bed.stageName ?? "", style: _normalStyle()),
                              Text(
                                "variety  ",
                                style: _titleStyle(),
                              ),
                              Text(bed.varietyName ?? "",
                                  style: _normalStyle()),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      : Container();
}

Widget commonStaffDropDownSearch({
  required List<StaffData> items,
  required Function(StaffData s) onChanged,
}) {
  return CommonDropDownSearch<StaffData>(
      isRequired: true,
      showAsterisk: true,
      onChanged: (u) {
        if (u != null) {
          onChanged(u);
        }
      },
      labelText: "Staff",
      hint: "Select Staff",
      items: items,
      validationText: "Please Select Staff",
      showSearchBox: true,
      searchHint: "Search Staff");
}

Widget commonStaffDrop(BuildContext context, StaffData? item) {
  return Text(
    item?.name ?? "Select Staff",
    style: TextStyle(
      color: item == null ? fromHex(yellow) : Colors.white,
      fontSize: 17,
    ),
  );
}

Widget commonStaffPopUp(BuildContext context, StaffData item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: ListTile(
      title: Text(item.name ?? "Name Not Set",
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w100,
            fontSize: 20,
          )),
    ),
  );
}

Widget commonYESNODrop(BuildContext context, String? item) {
  return Text(
    item ?? "Clear All ?",
    style: TextStyle(
      color: item == null ? fromHex(yellow) : Colors.white,
      fontSize: 17,
    ),
  );
}

Widget commonYESNOPopUp(BuildContext context, String item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: ListTile(
      title: Text(item,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w100,
            fontSize: 20,
          )),
    ),
  );
}

Widget commonStaffDetailsDropDownSearch({
  required List<StaffDetails> items,
  required Function(StaffDetails ld) onChanged,
}) {
  return CommonDropDownSearch<StaffDetails>(
      isRequired: true,
      showAsterisk: true,
      onChanged: (u) {
        if (u != null) {
          onChanged(u);
        }
      },
      labelText: "Staff",
      hint: "Select Staff",
      items: items,
      validationText: "Please Select Staff",
      showSearchBox: true,
      searchHint: "Search Staff");
}

Widget commonStaffDetailsItemDrop(BuildContext context, StaffDetails? item) {
  return item != null
      ? Text(
          "${item.staffName}",
          style: const TextStyle(
            color: Colors.white,
            fontSize: 17,
          ),
        )
      : Text(
          "Select Staff",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonStaffDetailsItemPopUp(
    BuildContext context, StaffDetails item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: Text(
      "${item.staffName}",
      style: TextStyle(
        color: item == null ? fromHex(yellow) : Colors.white,
        fontSize: 17,
      ),
    ),
  );
}

Widget commonSectionDetailsDropDownSearch({
  required List<SectionDetails> items,
  required Function(SectionDetails ld) onChanged,
}) {
  return CommonDropDownSearch<SectionDetails>(
      isRequired: true,
      showAsterisk: true,
      onChanged: (u) {
        if (u != null) {
          onChanged(u);
        }
      },
      labelText: "Section",
      hint: "Select Section",
      items: items,
      validationText: "Please Select Section",
      showSearchBox: true,
      searchHint: "Search Section");
}

Widget commonSectionDetailsItemDrop(
    BuildContext context, SectionDetails? item) {
  return item != null
      ? Text(
          "${item.sectionName}",
          style: const TextStyle(
            color: Colors.white,
            fontSize: 17,
          ),
        )
      : Text(
          "Select Section",
          style: TextStyle(
            color: fromHex(yellow),
            fontSize: 17,
          ),
        );
}

Widget commonSectionDetailsItemPopUp(
    BuildContext context, SectionDetails item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: Text(
      "${item.sectionName}",
      style: TextStyle(
        color: item == null ? fromHex(yellow) : Colors.white,
        fontSize: 17,
      ),
    ),
  );
}

Widget commonLossTypeDrop(BuildContext context, String? item) {
  return Text(
    item ?? "Select Type",
    style: TextStyle(
      color: item == null ? fromHex(yellow) : Colors.white,
      fontSize: 17,
    ),
  );
}

Widget commonLossTypePopUp(BuildContext context, String item, bool isSelected) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8),
    decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
    child: ListTile(
      title: Text(item,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w100,
            fontSize: 20,
          )),
    ),
  );
}

void logToCrushlytics(String s) {
  FirebaseCrashlytics.instance.log(s);
}
