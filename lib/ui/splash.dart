import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:weight_collection/core/services/push_notifications_service.dart';
import 'package:weight_collection/ui/url_page.dart';

import '../locator.dart';
import 'home_page.dart';
import 'login_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  PushNotificationService fcm = locator<PushNotificationService>();

  @override
  void initState() {
    // Timer(
    //     const Duration(seconds: 5),
    //     () => Navigator.pushNamedAndRemoveUntil(
    //         context, HomePage.tag, (Route<dynamic> route) => false));
    initializeStuff();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text(
                  'Welcome',
                  style: TextStyle(color: Colors.grey, fontSize: 36),
                ),
                const SizedBox(
                  height: 30,
                ),
                SvgPicture.asset(
                  "assets/mainlogo.svg",
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.center,
                  width: 150,
                  height: 200,
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SvgPicture.asset(
              "assets/footer.svg",
              width: MediaQuery.of(context).size.width,
            ),
          )
        ],
      ),
    );
  }

  Future<void> initializeStuff() async {
    await fcm.initialise();
    // if (kDebugMode) {
    //   print(fcm.getFCMToken());
    // }
    var url = accountInfo.get('url');
    var name = accountInfo.get('userId');
    if (url != null) {
      if (url.isNotEmpty) {
        if (name != null && name.isNotEmpty) {
          Timer(
              const Duration(seconds: 5),
              () => Navigator.pushNamedAndRemoveUntil(
                  context, HomePage.tag, (Route<dynamic> route) => false));
        } else {
          Timer(
              const Duration(seconds: 5),
              () => Navigator.pushNamedAndRemoveUntil(
                  context, LoginPage.tag, (Route<dynamic> route) => false));
        }
      }
      if (url.isEmpty) {
        Timer(
            const Duration(seconds: 5),
            () => Navigator.pushNamedAndRemoveUntil(
                context, UrlPage.tag, (Route<dynamic> route) => false));
      }
    }
    if (url == null) {
      Timer(
          const Duration(seconds: 5),
          () => Navigator.pushNamedAndRemoveUntil(
              context, UrlPage.tag, (Route<dynamic> route) => false));
    }
  }
}
