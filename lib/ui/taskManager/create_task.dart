import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dtp;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_tags_x/flutter_tags_x.dart';
import 'package:hive/hive.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/get_function_response.dart';
import 'package:weight_collection/core/models/get_staff_task_response.dart';
import 'package:weight_collection/core/models/get_tasks_types_response.dart';
import 'package:weight_collection/core/models/local/localModels.dart';
import 'package:weight_collection/core/viewmodels/task_model.dart';
import 'package:weight_collection/utils/loader.dart';
import 'package:weight_collection/utils/useful.dart';
import '../base_view.dart';
import '../design_system/DropDownSearch.dart';
import 'task_manager.dart';

class CreateTaskPage extends StatefulWidget {
  final bool? goBack;
  final String? type;
  final String? id;
  const CreateTaskPage(this.goBack, {super.key, this.type, this.id});

  @override
  _CreateTaskPageState createState() => _CreateTaskPageState();
}

class _CreateTaskPageState extends State<CreateTaskPage> {
  final TextEditingController _taskDescription = TextEditingController();
  final TextEditingController _date = TextEditingController();
  final TextEditingController _title = TextEditingController();

  String? priority = "";
  String? taskTypeName = "";
  String? taskTypeId = "";
  String? visibility = "";
  String? association = "";
  String? functionName = "";
  String? functionId = "";
  String? staffName = "";
  String? staffId = "";
  bool isTaskSet = false;
  List<String> priorities = [
    'Urgent & Important',
    'Urgent',
    'Important',
    'Wishlist'
  ];
  List<String> visibilityList = ['Private', 'Public'];
  List<String> associations = ['Function', 'Individual'];

  final _formKey = GlobalKey<FormState>();
  late bool hasConnection;
  late CreateTaskData? _taskData;
  final _createTaskData = Hive.box('created_task_data');

  @override
  void initState() {
    super.initState();
    _taskData = _createTaskData.get("current_task", defaultValue: null);
    if (_taskData == null) {
      initializeData();
    } else {
      setData(_taskData!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<TaskModel>(
      onModelReady: (model) async {
        await model.getTaskTypes();
        await model.getStaff();
        await model.getFunctions();
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? SafeArea(
              child: Stack(
                children: [
                  Scaffold(
                    backgroundColor: Colors.black,
                    // key: _scaffoldKey,
                    appBar: AppBar(
                      backgroundColor: Colors.black,
                      automaticallyImplyLeading: false,
                      centerTitle: true,
                      title: Text(
                        "Create Task",
                        style: TextStyle(color: fromHex(deepOrange)),
                      ),
                    ),
                    body: Column(
                      children: [
                        Expanded(
                          child: SingleChildScrollView(
                            physics: const BouncingScrollPhysics(),
                            child: Column(
                              children: [
                                isTaskSet
                                    ? _filledDetails(context)
                                    : Container(),
                                _createTaskForm(context, model),
                                const SizedBox(
                                  height: 30,
                                ),
                              ],
                            ),
                          ),
                        ),
                        _submitBtn(context, model),
                        const SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 15,
                    top: 7,
                    child: FloatingActionButton(
                      foregroundColor: Colors.grey,
                      backgroundColor: Colors.white10.withOpacity(0.07),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      mini: true,
                      tooltip: "go back home",
                      child: Icon(
                        Icons.keyboard_backspace_sharp,
                        color: fromHex(yellow),
                      ),
                    ),
                  ),
                ],
              ),
            )
          : const Loader(),
    );
  }

  _createTaskForm(BuildContext context, TaskModel model) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            const SizedBox(
              height: 15,
            ),
            TextFormField(
              controller: _title,
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.words,
              style: const TextStyle(color: Colors.white),
              validator: (value) {
                if (value!.isEmpty) {
                  return "Please Enter task's title";
                }
                return null;
              },
              onChanged: (value) {
                if (_taskData != null) {
                  CreateTaskData t = _taskData!..title = value;
                  isTaskSet = true;
                  _saveTask(t);
                }
              },
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white10,
                isDense: true,
                hintText: "eg Task A",
                labelText: "Title",
                labelStyle: TextStyle(color: fromHex(yellow)),
                hintStyle: TextStyle(
                  color: Colors.blueGrey[400],
                ),
                border: InputBorder.none,
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: fromHex(yellow)),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            CommonDropDownSearch<String>(
              isRequired: true,
              showAsterisk: true,
              onChanged: (u) {
                setState(() {
                  priority = u;
                });
                if (_taskData != null) {
                  isTaskSet = true;
                  CreateTaskData t = _taskData!..priority = u;
                  _saveTask(t);
                }
              },
              labelText: "Priority",
              hint: "Select Priority",
              selectedItem: priority,
              items: priorities,
              validationText: "Please Select Priority",
            ),
            const SizedBox(
              height: 10,
            ),
            CommonDropDownSearch<TaskType>(
              isRequired: true,
              showAsterisk: true,
              onChanged: (s) {
                setState(() {
                  taskTypeId = s?.id ?? "";
                  taskTypeName = s?.name ?? "";
                });
                if (_taskData != null) {
                  isTaskSet = true;
                  CreateTaskData t = _taskData!
                    ..taskType = s?.id ?? ""
                    ..taskTypeName = s?.name ?? "";
                  _saveTask(t);
                }
              },
              labelText: "Task Type",
              hint: "Select Task Type",
              items: model.taskTypes ?? [],
              validationText: "Please Task Type",
              showSearchBox: true,
              searchHint: "Search Task Type",
            ),
            const SizedBox(
              height: 10,
            ),
            CommonDropDownSearch<String>(
              isRequired: true,
              showAsterisk: true,
              onChanged: (s) {
                setState(() {
                  visibility = s;
                });
                if (_taskData != null) {
                  CreateTaskData t = _taskData!..taskVisibility = s;
                  _saveTask(t);
                }
              },
              labelText: "Task Visibility",
              hint: "Select Task Visibility",
              items: visibilityList,
              validationText: "Please Task Visibility"
            ),
            const SizedBox(
              height: 10,
            ),
            CommonDropDownSearch<String>(
                isRequired: true,
                showAsterisk: true,
                onChanged: (s) {
                  if (s != association) {
                    setState(() {
                      association = s;
                      functionId = "";
                      functionName = "";
                      staffId = "";
                      staffName = "";
                    });

                    if (_taskData != null) {
                      CreateTaskData t = _taskData!
                        ..associatedBy = s
                        ..assignTo = [];
                      _saveTask(t);
                    }
                  }
                },
                labelText: "Task Assigned to",
                hint: "Select Task Assigned to",
                items: visibilityList,
                validationText: "Please Specify Task Assigned to"
            ),
            const SizedBox(
              height: 10,
            ),
            association == "Individual"
                ? Column(
                    children: [
                      CommonDropDownSearch<StaffTask>(
                          isRequired: true,
                          showAsterisk: true,
                          onChanged: (s) {
                            setStaff(s);
                          },
                          labelText: "Assignee",
                          hint: "Select Assignee",
                          items: model.staff ?? [],
                          validationText: "Please Specify Assignee",
                          showSearchBox: true,
                          searchHint: "Search Assignee",
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  )
                : Container(),
            association == "Function"
                ? Column(
                    children: [
                      CommonDropDownSearch<FunctionData>(
                        isRequired: true,
                        showAsterisk: true,
                        onChanged: (s) {
                          setFunction(s);
                        },
                        labelText: "Function",
                        hint: "Select Function",
                        items: model.functions ?? [],
                        validationText: "Please Specify Function",
                        showSearchBox: true,
                        searchHint: "Search Function",
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  )
                : Container(),
            TextFormField(
              controller: _date,
              keyboardType: TextInputType.number,
              style: const TextStyle(color: Colors.white),
              validator: (value) {
                if (value!.isEmpty) {
                  return "Please Enter task's due date";
                }
                return null;
              },
              enableInteractiveSelection: false,
              autofocus: false,
              readOnly: true,
              onTap: () {
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    minTime: DateTime.now(),
                    maxTime: DateTime.now().add(const Duration(days: 94200)),
                    theme: dtp.DatePickerTheme(
                      headerColor: Colors.grey.shade900,
                      backgroundColor: Colors.grey.shade900,
                      itemStyle: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                      cancelStyle: const TextStyle(
                        color: Colors.deepOrange,
                        fontSize: 24,
                      ),
                      doneStyle: TextStyle(
                        color: Colors.green.shade700,
                        fontSize: 24,
                      ),
                    ), onChanged: (date) {
                  print('change $date in time zone ${date.timeZoneOffset.inHours}');
                }, onConfirm: (date) {
                  setState(() {
                    _date.text = df.format(date);
                  });
                  if (_taskData != null) {
                    CreateTaskData t = _taskData!..deadline = df.format(date);
                    _saveTask(t);
                  }
                }, currentTime: DateTime.now(), locale: LocaleType.en);
              },
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white10,
                isDense: true,
                hintText: "",
                labelText: "Due Date",
                labelStyle: TextStyle(color: fromHex(yellow)),
                hintStyle: TextStyle(
                  color: Colors.blueGrey[400],
                ),
                border: InputBorder.none,
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: fromHex(yellow)),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              child: TextFormField(
                controller: _taskDescription,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.sentences,
                maxLines: 5,
                textAlignVertical: TextAlignVertical.top,
                style: const TextStyle(color: Colors.white),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter task's description";
                  }
                  return null;
                },
                onChanged: (value) {
                  if (_taskData != null) {
                    CreateTaskData t = _taskData!..taskDescription = value;
                    _saveTask(t);
                  }
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "eg Task Description",
                  labelText: "Task Description",
                  labelStyle: TextStyle(color: fromHex(yellow)),
                  hintStyle: TextStyle(
                    color: Colors.blueGrey[400],
                  ),
                  border: InputBorder.none,
                  alignLabelWithHint: true,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _priorityDrop(BuildContext context, String? item) {
    return Container(
      child: Text(
        priority!.isNotEmpty ? "$priority" : "Select Priority",
        style: TextStyle(
          color: priority!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _priorityPopUp(BuildContext context, String item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _taskTypeDrop(BuildContext context, TaskType? item) {
    return Container(
      child: Text(
        taskTypeId!.isNotEmpty ? "$taskTypeName" : "Select Task Type",
        style: TextStyle(
          color: taskTypeId!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _taskTypePopUp(BuildContext context, TaskType item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("${item.name}",
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _visibilityDrop(BuildContext context, String? item) {
    return Container(
      child: Text(
        visibility!.isNotEmpty ? "$visibility" : "Select Task Visibility",
        style: TextStyle(
          color: visibility!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _visibilityPopUp(BuildContext context, String item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _associationDrop(BuildContext context, String? item) {
    return Container(
      child: Text(
        association!.isNotEmpty ? "$association" : "Specify Task Assigned to",
        style: TextStyle(
          color: association!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _associationPopUp(BuildContext context, String item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _staffDrop(BuildContext context, StaffTask? item) {
    return Text(
      "Select Assignee",
      style: TextStyle(
        color: fromHex(yellow),
        fontSize: 17,
      ),
    );
  }

  Widget _staffPopUp(BuildContext context, StaffTask item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("${item.name}",
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _functionDrop(BuildContext context, FunctionData? item) {
    return Container(
      child: Text(
        "Select Function",
        style: TextStyle(
          color: fromHex(yellow),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _functionPopUp(
      BuildContext context, FunctionData item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("${item.jobTitle}",
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  void _saveTask(CreateTaskData taskData) {
    _createTaskData.put("current_task", taskData);
    setState(() {
      _taskData = taskData;
    });
  }

  void initializeData() {
    setState(() {
      _taskData = CreateTaskData(
          assignedBy: getUserId(),
          taskDescription: "",
          deadline: "",
          assignTo: [],
          taskItem: [],
          title: "",
          taskType: "",
          taskVisibility: "",
          priority: "",
          associatedBy: "",
          taskTypeName: "");
      _title.text = "";
      priority = '';
      taskTypeId = '';
      taskTypeName = '';
      visibility = '';
      association = '';
      _date.text = '';
      _taskDescription.text = '';
      isTaskSet = false;
      _saveTask(_taskData!);
    });
  }

  _filledDetails(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Card(
        color: Colors.black,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Task Details",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  Container(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            _taskData!.title!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${_taskData!.title}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Task Title",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _taskData!.priority!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${_taskData!.priority}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          'Task Priority',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                          ],
                        ),
                        Row(
                          children: [
                            _taskData!.taskTypeName!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.taskTypeName}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Task Type",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _taskData!.taskVisibility!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.taskVisibility}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          'Task Visibility',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                          ],
                        ),
                        Row(
                          children: [
                            _taskData!.associatedBy!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.associatedBy}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Assigned to",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _taskData!.deadline!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.deadline}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Due By",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                          ],
                        ),
                        _taskData?.assignTo == null
                            ? Container()
                            : _taskData!.assignTo!.isEmpty
                                ? Container()
                                : Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 8.0),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Tags(
                                                itemCount: _taskData
                                                        ?.assignTo?.length ??
                                                    0,
                                                alignment: WrapAlignment.start,
                                                itemBuilder: (int? index) {
                                                  var type = _taskData
                                                      ?.assignTo?[index ?? 0];
                                                  return Tooltip(
                                                      message:
                                                          "Associated by : ${type?.type ?? ""}",
                                                      child: ItemTags(
                                                        textStyle: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: fromHex(
                                                                yellow)),
                                                        color: Colors.white10
                                                            .withOpacity(0.07),
                                                        activeColor: Colors
                                                            .white10
                                                            .withOpacity(0.07),
                                                        singleItem: true,
                                                        index: index ?? 0,
                                                        title: type?.name ?? "",
                                                        textColor: Colors.white,
                                                        border: Border.all(
                                                            color:
                                                                Colors.black),
                                                        removeButton:
                                                            ItemTagsRemoveButton(
                                                          color:
                                                              Colors.deepOrange,
                                                          backgroundColor:
                                                              Colors
                                                                  .transparent,
                                                          onRemoved: () {
                                                            setState(() {
                                                              // _taskData!.assignTo!
                                                              //     .removeAt(
                                                              //         index ?? 0);
                                                            });
                                                            _saveTask(
                                                                _taskData!);
                                                            return true;
                                                          },
                                                        ), // OR nu
                                                      ));
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const Padding(
                                        padding:
                                            EdgeInsets.only(left: 10, top: 2),
                                        child: Text(
                                          'Assignees',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                        Row(
                          children: [
                            _taskData!.taskDescription!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.taskDescription}",
                                          style: const TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        const Text(
                                          "Task Description",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                          ],
                        ),
                        _taskData!.taskItem!.isEmpty
                            ? Container()
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Tags(
                                            itemCount:
                                                _taskData!.taskItem!.length,
                                            alignment: WrapAlignment.start,
                                            itemBuilder: (int index) {
                                              var type =
                                                  _taskData!.taskItem![index];
                                              return InkWell(
                                                onLongPress: () {
                                                  _addTaskItemDialog(context,
                                                      type?.description ?? "");
                                                },
                                                child: ItemTags(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      color: fromHex(yellow)),
                                                  color: Colors.white10
                                                      .withOpacity(0.07),
                                                  activeColor: Colors.white10
                                                      .withOpacity(0.07),
                                                  singleItem: true,
                                                  index: index,
                                                  title:
                                                      type?.description ?? "",
                                                  textColor: Colors.white,
                                                  border: Border.all(
                                                      color: Colors.black),
                                                  removeButton:
                                                      ItemTagsRemoveButton(
                                                    color: Colors.deepOrange,
                                                    backgroundColor:
                                                        Colors.transparent,
                                                    onRemoved: () {
                                                      setState(() {
                                                        _taskData!.taskItem!
                                                            .removeAt(index);
                                                      });
                                                      _saveTask(_taskData!);
                                                      return true;
                                                    },
                                                  ), // OR nu
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const Padding(
                                    padding: EdgeInsets.only(left: 10, top: 2),
                                    child: Text(
                                      'Assignees',
                                      style: TextStyle(
                                        fontSize: 11,
                                        fontStyle: FontStyle.italic,
                                        color: Colors.white54,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _submitBtn(BuildContext context, TaskModel model) {
    return Column(
      children: [
        Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) async {
            setState(() {});
            _addTaskItemDialog(context, "");
          },
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white10.withOpacity(0.07),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(
                  Icons.add,
                  size: 36,
                  color: fromHex(yellow),
                ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    'Add Subtask',
                    style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.w100,
                      fontSize: 20,
                    ),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
        Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) async {
            setState(() {});
            onSubmitTask(model);
            setState(() {});
          },
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white10.withOpacity(0.07),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(
                  Icons.done,
                  size: 36,
                  color: fromHex(yellow),
                ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    'Submit Response',
                    style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.w100,
                      fontSize: 20,
                    ),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  void setData(CreateTaskData t) {
    _title.text = t.title ?? "";
    priority = t.priority ?? "";
    taskTypeId = t.taskType ?? "";
    taskTypeName = t.taskTypeName ?? "";
    visibility = t.taskVisibility ?? "";
    association = t.associatedBy ?? "";
    _date.text = t.deadline ?? "";
    _taskDescription.text = t.taskDescription ?? "";
    isTaskSet = true;
  }

  Future<void> _addTaskItemDialog(
      BuildContext context, String description) async {
    List<SubTask?> subtasks = _taskData?.taskItem ?? [];
    TextEditingController desc = TextEditingController(text: description);
    return await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ListTile(
                  title: Center(
                    child: Text(
                      description.isEmpty ? 'Add Sub Task' : "Edit Sub Task",
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w800),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: desc,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: 5,
                    textAlignVertical: TextAlignVertical.top,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter task's description";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg Sub Task Description",
                      labelText: "Sub Task Description",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      alignLabelWithHint: true,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                ListTile(
                  onTap: () async {
                    if (desc.text.isNotEmpty) {
                      if (_taskData != null) {
                        SubTask sub = SubTask()..description = desc.text;

                        subtasks.add(sub);

                        CreateTaskData t = _taskData!..taskItem = subtasks;
                        isTaskSet = true;
                        _saveTask(t);
                      }
                    }
                    Navigator.of(context).pop();
                  },
                  title: const Center(
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onSubmitTask(TaskModel model) async {
    if (_taskData!.assignTo!.isEmpty) {
      showToast("Please Select Assignee(s)");
      return;
    }
    if (_formKey.currentState!.validate()) {
      if (_taskData!.taskItem!.isEmpty) {
        SubTask sub = SubTask()..description = _title.text;
        CreateTaskData t = _taskData!
          ..taskItem = [sub]
          ..associatedId = widget.id
          ..associatedType = widget.type;
        isTaskSet = true;
        _saveTask(t);
      }

      bool hasConnection = await checkConnection();
      if (hasConnection) {
        print(_taskData!.toJson());
        try {
          var r = await model.createTask(_taskData!.toJson());
          if (r['success']) {
            initializeData();
            showToast("Success");
            if (widget.goBack != null) {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => const TaskManagerPage()),
                  (Route<dynamic> route) => false);
            } else {
              Navigator.of(context).pop();
            }
          }
        } catch (e) {
          showToast(e.toString());
        }
      } else {
        // offlineResponses.add(data);
        showToast("Check your internet connection");
        // Navigator.of(context).pop();
      }
    }
  }

  void setStaff(StaffTask? s) {
    setState(() {
      staffId = s?.Id ?? "";
      staffName = s?.name ?? "";
    });
    if (_taskData != null) {
      if (_taskData!.assignTo != null) {
        if (_taskData!.assignTo!.isNotEmpty) {
          var contain = _taskData!.assignTo!.where(
                  (element) => element.assignToId! == s!.Id);
          if (kDebugMode) {
            print(contain.toString());
          }
          if (contain.isEmpty) {
            showToast("Added ${s?.name ?? ""}");
            AssignedTo a = AssignedTo(
                assignToId: s!.Id,
                name: s.name,
                type: association);
            CreateTaskData t = _taskData!
              ..assignTo!.add(a);
            _saveTask(t);
          } else {
            if (kDebugMode) {
              print('already added');
            }
          }
          if (contain.isNotEmpty) {
            showToast("Already added ${s?.name ?? ""}");
          }
        } else {
          showToast("Added ${s?.name ?? ""}");
          AssignedTo a = AssignedTo(
              assignToId: s!.Id,
              name: s.name,
              type: association);
          CreateTaskData t = _taskData!..assignTo!.add(a);
          _saveTask(t);
        }
      }
    }
  }

  void setFunction(FunctionData? s) {
    setState(() {
      functionId = s?.id ?? "";
      functionName = s?.jobTitle ?? "";
    });
    if (_taskData != null) {
      if (_taskData!.assignTo != null) {
        if (_taskData!.assignTo!.isNotEmpty) {
          var contain = _taskData!.assignTo!.where(
                  (element) => element.assignToId! == s!.id);
          print(contain.toString());
          if (contain.isEmpty) {
            showToast("Added ${s?.jobTitle ?? ""}");
            AssignedTo a = AssignedTo(
                assignToId: s!.id,
                name: s.jobTitle,
                type: association);
            CreateTaskData t = _taskData!
              ..assignTo!.add(a);
            _saveTask(t);
          } else {
            print('already added');
          }
          if (contain.isNotEmpty) {
            showToast(
                "Already added ${s?.jobTitle ?? ""}");
          }
        } else {
          showToast("Added ${s?.jobTitle ?? ""}");
          AssignedTo a = AssignedTo(
              assignToId: s!.id,
              name: s.jobTitle,
              type: association);
          CreateTaskData t = _taskData!..assignTo!.add(a);
          _saveTask(t);
        }
      }
    }
  }
}
