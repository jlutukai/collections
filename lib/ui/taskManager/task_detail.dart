import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dtp;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/models/get_task_comments_reponse.dart';
import 'package:weight_collection/core/models/get_tasks_response.dart';
import 'package:weight_collection/core/viewmodels/task_model.dart';
import 'package:weight_collection/ui/taskManager/task_manager.dart';
import 'package:weight_collection/utils/loader.dart';
import 'package:weight_collection/utils/useful.dart';

import '../base_view.dart';

class TaskDetailPage extends StatefulWidget {
  final TaskData task;

  const TaskDetailPage(this.task, {super.key});

  @override
  _TaskDetailPageState createState() => _TaskDetailPageState();
}

class _TaskDetailPageState extends State<TaskDetailPage> {
  late TaskData _task;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _task = widget.task;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      onPopInvoked: (b) async {
        if(b) {
          Navigator.pushNamedAndRemoveUntil(
              context, TaskManagerPage.tag, (Route<dynamic> route) => false);
        }
      },
      child: BaseView<TaskModel>(
        onModelReady: (model) async {
          Map<String, String> data = {
            'task_id': "${_task.id}",
          };
          await model.getTaskComment(data);
          await model.getStaff();
        },
        builder: (context, model, child) =>
        model.state == ViewState.Idle
            ? Stack(
          children: [
            Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.black,
              // key: _scaffoldKey,
              appBar: AppBar(
                backgroundColor: Colors.black,
                automaticallyImplyLeading: false,
                centerTitle: true,
                title: Text(
                  "Task Details",
                  style: TextStyle(color: fromHex(deepOrange)),
                ),
              ),
              body: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          _taskDetails(context, model),
                          const SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  _btnsOptions(context, model),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            Positioned(
              left: 15,
              top: 27,
              child: FloatingActionButton(
                foregroundColor: Colors.grey,
                backgroundColor: Colors.white10.withOpacity(0.07),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, TaskManagerPage.tag, (Route<dynamic> route) => false);
                },
                mini: true,
                tooltip: "go back home",
                child: Icon(
                  Icons.keyboard_backspace_sharp,
                  color: fromHex(yellow),
                ),
              ),
            ),
          ],
        )
            : const Loader(),
      ),
    );
  }

  _taskDetails(BuildContext context, TaskModel model) {
    if (kDebugMode) {
      print(
        "########################## ####################### - ${_task.subTask
            ?.length ?? 0}");
    }
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Card(
        color: Colors.black,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                color: Colors.white10,
                border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  CircularPercentIndicator(
                    radius: 70.0,
                    lineWidth: 5.0,
                    percent:
                    (double.tryParse(_task.progress ?? "0.0") ?? 0.0) / 100,
                    center: Text("${_task.progress} %", style: const TextStyle(color: Colors.white),),
                    progressColor: Colors.green[600],
                  ),
                  Container(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            _task.title!.isEmpty
                                ? Container()
                                : Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${_task.title}",
                                    style: const TextStyle(
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  const Text(
                                    "Task Title",
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            _task.status!.isEmpty
                                ? Container()
                                : Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${_task.status}",
                                    style: const TextStyle(
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  const Text(
                                    'Task Status',
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            _task.priority!.isEmpty
                                ? Container()
                                : Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "${_task.priority}",
                                    style: const TextStyle(
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  const Text(
                                    "Task Priority",
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            _task.taskVisibility!.isEmpty
                                ? Container()
                                : Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "${_task.taskVisibility}",
                                    style: const TextStyle(
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  const Text(
                                    'Task Visibility',
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            _task.associatedBy!.isEmpty
                                ? Container()
                                : Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "${_task.associatedBy}",
                                    style: const TextStyle(
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  const Text(
                                    "Assigned to",
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            _task.deadline!.isEmpty
                                ? Container()
                                : Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "${_task.deadline}",
                                    style: const TextStyle(
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  const Text(
                                    "Due By",
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        // _taskData!.assignTo!.isEmpty
                        //     ? Container()
                        //     : Column(
                        //   crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: [
                        //     Padding(
                        //       padding: const EdgeInsets.only(top: 8.0),
                        //       child: Row(
                        //         children: [
                        //           Expanded(
                        //             child: Tags(
                        //               itemCount:
                        //               _taskData!.assignTo!.length,
                        //               alignment: WrapAlignment.start,
                        //               itemBuilder: (int index) {
                        //                 var type =
                        //                 _taskData!.assignTo![index];
                        //                 return Tooltip(
                        //                     message:
                        //                     "Associated by : ${type?.type ?? ""}",
                        //                     child: ItemTags(
                        //                       textStyle: TextStyle(
                        //                           fontSize: 14,
                        //                           fontWeight:
                        //                           FontWeight.normal,
                        //                           color: fromHex(yellow)),
                        //                       color: Colors.white10
                        //                           .withOpacity(0.07),
                        //                       activeColor: Colors.white10
                        //                           .withOpacity(0.07),
                        //                       singleItem: true,
                        //                       index: index,
                        //                       title: type?.name ?? "",
                        //                       textColor: Colors.white,
                        //                       border: Border.all(
                        //                           color: Colors.black),
                        //                       removeButton:
                        //                       ItemTagsRemoveButton(
                        //                         color: Colors.deepOrange,
                        //                         backgroundColor:
                        //                         Colors.transparent,
                        //                         onRemoved: () {
                        //                           setState(() {
                        //                             _taskData!.assignTo!
                        //                                 .removeAt(index);
                        //                           });
                        //                           _saveTask(_taskData!);
                        //                           return true;
                        //                         },
                        //                       ), // OR nu
                        //                     ));
                        //               },
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //     Padding(
                        //       padding:
                        //       const EdgeInsets.only(left: 10, top: 2),
                        //       child: Text(
                        //         'Assignees',
                        //         style: TextStyle(
                        //           fontSize: 11,
                        //           fontStyle: FontStyle.italic,
                        //           color: Colors.white54,
                        //         ),
                        //       ),
                        //     ),
                        //   ],
                        // ),
                        Row(
                          children: [
                            _task.taskDescription!.isEmpty
                                ? Container()
                                : Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "${_task.taskDescription}",
                                    style: const TextStyle(
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  const Text(
                                    "Task Description",
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.white54,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),

                        _task.subTask != null
                            ? _task.subTask!.isNotEmpty
                            ? Column(
                          children: [
                            const SizedBox(
                              height: 15,
                            ),
                            const Row(
                              children: [
                                Text(
                                  "Sub Task Items",
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontStyle: FontStyle.italic,
                                    color: Colors.white54,
                                  ),
                                ),
                              ],
                            ),
                            _subTasksList(
                                context, _task.subTask, model)
                          ],
                        )
                            : Container()
                            : Container(),

                        model.comments != null
                            ? model.comments!.isNotEmpty
                            ? Column(
                          children: [
                            const SizedBox(
                              height: 15,
                            ),
                            const Row(
                              children: [
                                Text(
                                  "Comments",
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontStyle: FontStyle.italic,
                                    color: Colors.white54,
                                  ),
                                ),
                              ],
                            ),
                            _commentsList(
                                context, model.comments, model)
                          ],
                        )
                            : Container()
                            : Container(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _btnsOptions(BuildContext context, TaskModel model) {
    return Column(
      children: [
        Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) async {
            setState(() {});
            _editTaskDialog(context, model);
          },
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white10.withOpacity(0.07),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(
                  Icons.edit,
                  size: 36,
                  color: fromHex(yellow),
                ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    'Edit Task Deadline',
                    style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.w100,
                      fontSize: 20,
                    ),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
        Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) async {
            setState(() {});
            _addCommentDialog(context, model);
          },
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white10.withOpacity(0.07),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(
                  Icons.add,
                  size: 36,
                  color: fromHex(yellow),
                ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    'Add Comments',
                    style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.w100,
                      fontSize: 20,
                    ),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _addCommentDialog(BuildContext context, TaskModel model) async {
    TextEditingController desc = TextEditingController();
    return await showDialog(
      context: context,
      builder: (context) =>
          Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const ListTile(
                    title: Center(
                      child: Text(
                        "Add Comment",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: desc,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: 5,
                    textAlignVertical: TextAlignVertical.top,
                    style: const TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter task's description";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg Enter Comment Here...",
                      labelText: "Comment",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      alignLabelWithHint: true,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    onTap: () async {
                      if (desc.text.isEmpty) {
                        showToast("Please Provide Comment First");
                        return;
                      }
                      Map<String, String> data = {
                        'user_id': "${getUserId()}",
                        'task_id': "${_task.id}",
                        'comment': desc.text
                      };
                      bool hasConnection = await checkConnection();
                      if (hasConnection) {
                        if (kDebugMode) {
                          print(data);
                        }
                        Navigator.of(context).pop();
                        _sendCommentData(data, model);
                      } else {
                        // offlineResponses.add(data);
                        showToast("Check your internet connection");
                        Navigator.of(context).pop();
                      }
                    },
                    title: const Center(
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.deepOrange),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
    );
  }

  Future<void> _editTaskDialog(BuildContext context, TaskModel model) async {
    TextEditingController desc = TextEditingController();
    TextEditingController date =
    TextEditingController(text: _task.deadline ?? "");
    DateTime dueDate =
    DateFormat("yyyy-MM-dd").parse(_task.deadline ?? "2020-10-08");
    return await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) =>
            Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: StatefulBuilder(
                builder: (context, StateSetter setState) =>
                    Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const ListTile(
                            title: Center(
                              child: Text(
                                'Change Due Date',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          TextFormField(
                            controller: date,
                            keyboardType: TextInputType.number,
                            style: const TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter task's due date";
                              }
                              return null;
                            },
                            enableInteractiveSelection: false,
                            autofocus: false,
                            readOnly: true,
                            onTap: () {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime.now(),
                                  maxTime:
                                  DateTime.now().add(const Duration(days: 94200)),
                                  theme: dtp.DatePickerTheme(
                                    headerColor: Colors.grey.shade900,
                                    backgroundColor: Colors.grey.shade900,
                                    itemStyle: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    ),
                                    cancelStyle: const TextStyle(
                                      color: Colors.deepOrange,
                                      fontSize: 24,
                                    ),
                                    doneStyle: TextStyle(
                                      color: Colors.green.shade700,
                                      fontSize: 24,
                                    ),
                                  ),
                                  onChanged: (date) {
                                    print('change $date in time zone ${date.timeZoneOffset.inHours}');
                                  },
                                  onConfirm: (d) {
                                    setState(() {
                                      date.text = df.format(d);
                                    });
                                  },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.en);
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "",
                              labelText: "Due Date",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: fromHex(yellow)),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: desc,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization
                                .sentences,
                            maxLines: 5,
                            textAlignVertical: TextAlignVertical.top,
                            style: const TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter task's description";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg Enter Reason Here...",
                              labelText: "Reason",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              alignLabelWithHint: true,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: fromHex(yellow)),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            onTap: () async {
                              if (desc.text.isEmpty && date.text != _task.deadline) {
                                showToast("Please Provide Reason First");
                                return;
                              }
                              Map<String, String> data = {
                                'user_id': "${getUserId()}", //Todo
                                'task_id': "${_task.id}",
                                'deadline': date.text,
                                'reason': desc.text
                              };

                              bool hasConnection = await checkConnection();
                              if (hasConnection) {
                                if (kDebugMode) {
                                  print(data);
                                }
                                Navigator.of(context).pop();
                                _sendEditData(data, model, date.text);
                              } else {
                                // offlineResponses.add(data);
                                showToast("Check your internet connection");
                                Navigator.of(context).pop();
                              }
                            },
                            title: const Center(
                              child: Text(
                                'Submit',
                                style: TextStyle(color: Colors.deepOrange),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
              ),
            ));
  }

  _subTasksList(BuildContext context, List<TaskDataSubTask?>? subTask,
      TaskModel model) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      shrinkWrap: true,
      itemCount: subTask?.length ?? 0,
      itemBuilder: (context, index) =>
          _subTaskListItem(
            context,
            subTask![index]!,
            model,
          ),
    );
  }

  _subTaskListItem(BuildContext context, TaskDataSubTask subTask,
      TaskModel model) {
    bool isChecked = subTask.status == "Completed";
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 1),
      decoration: BoxDecoration(
        color: Colors.white10.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Expanded(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          "${subTask.itemDescription}",
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Checkbox(
                          value: isChecked,
                          activeColor: Colors.green[600],
                          onChanged: (newValue) {
                            if (kDebugMode) {
                              print("$newValue");
                            }
                            if (newValue == true) {
                              subTask.status = "Completed";
                              Map<String, String> data = {
                                'user_id': "${getUserId()}", //Todo
                                'task_id': "${_task.id}",
                                'task_item_id': "${subTask.Id}"
                              };
                              _updateProgress(data, model);
                            } else {
                              subTask.status = "Pending";
                            }

                            setState(() {
                              isChecked = newValue ?? false;
                            });
                          }),
                    ],
                  ),
                ],
              ))
        ],
      ),
    );
  }

  _commentsList(BuildContext context, List<CommentData?>? comments,
      TaskModel model) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      shrinkWrap: true,
      itemCount: comments?.length ?? 0,
      itemBuilder: (context, index) =>
          _commentsListItem(
            context,
            comments![index]!,
            model,
          ),
    );
  }

  _commentsListItem(BuildContext context, CommentData commentData,
      TaskModel model) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white10.withOpacity(0.07),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Expanded(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${getStaffName(commentData.userId, model)}", style: TextStyle(color: fromHex(deepOrange), fontWeight: FontWeight.bold),),
                      const SizedBox(width: 5,),
                      const Text("-"),
                      const SizedBox(width: 5,),
                      Expanded(
                        child: Text(
                          "${commentData.comment} ",
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        Jiffy.parse(commentData.date??"1970-01-01").fromNow(),
                        style: const TextStyle(fontWeight: FontWeight.w100,
                            fontStyle: FontStyle.italic,
                            fontSize: 11),)
                    ],
                  )
                ],
              ))
        ],
      ),
    );
  }

  String? getStaffName(String? userId, TaskModel model) {
    String name = "";
    if (userId != null) {
      if (userId.isNotEmpty) {
        if (model.staff != null) {
          for (var element in model.staff!) {
            if (element.Id == userId) {
              name = element.name ?? "";
            }
          }
        }
      }
    }
    return name;
  }


  Future<void> _sendCommentData(Map<String, String> data,
      TaskModel model) async {
    try {
      var r = await model.createTaskComment(data);
      if (r['success']) {
        await reload(model);
        showToast("Success");
      }
    } catch (e) {
      showToast(e.toString());
    }
  }

  reload(TaskModel model) async {
    Map<String, String> data = {
      'task_id': "${_task.id}",
    };
    await model.getTaskComment(data);
  }


  Future<void> _sendEditData(Map<String, String> data, TaskModel model, String date) async {
    try {
      var r = await model.editTaskDate(data);
      if (r['success']) {
        setState(() {
          _task.deadline = date;
        });
        showToast("Success");
      }
    } catch (e) {
      showToast(e.toString());
    }
  }

  Future<void> _updateProgress(Map<String, String> data, TaskModel model) async {
    try {
      var r = await model.updateTaskProgress(data);
      if (r['success']) {
        showToast("Success");
      }
    } catch (e) {
      showToast(e.toString());
    }
  }
}

