import 'package:calendar_view/calendar_view.dart';
import 'package:flutter/material.dart';
import 'package:weight_collection/core/viewmodels/scan_greenhouse_model.dart';
import 'package:weight_collection/utils/loader.dart';

import '../../core/enums/view_state.dart';
import '../base_view.dart';

class TestEvent extends StatefulWidget {
  const TestEvent({super.key});

  @override
  State<TestEvent> createState() => _TestEventState();
}

class _TestEventState extends State<TestEvent> {
  final _controller = EventController();
  @override
  Widget build(BuildContext context) {
    return BaseView<ScanGreenHouseModel>(
      onModelReady: (model) async {
        final event = CalendarEventData(
          date: DateTime(2024, 1, 20),
          event: "Event 1",
          title: '',
          titleStyle: const TextStyle(color: Colors.black),
        );

        CalendarControllerProvider.of(context).controller.add(event);
        _controller.add(event);
        print(_controller.events.length);
        // await model.scanGreenHouse({"greenhouse_id": widget.id});
      },
      builder: (context, model, child)=> model.state == ViewState.Idle?Scaffold(
        backgroundColor: Colors.black,
        body: MonthView(
          controller:_controller,
          // to provide custom UI for month cells.
          // cellBuilder: (date, events, isToday, isInMonth) {
          //   // Return your widget to display as month cell.
          //   return Container();
          // },
          minMonth: DateTime(1990),
          maxMonth: DateTime(2050),
          initialMonth: DateTime(2024),
          cellAspectRatio: 1,
          headerStyle: const HeaderStyle(
            rightIcon: Icon(
              Icons.arrow_forward_ios,
              color: Colors.white,
            ),
            leftIcon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            headerTextStyle: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            decoration: BoxDecoration(
              color: Colors.black,
            ),
          ),
          onPageChange: (date, pageIndex) => print("$date, $pageIndex"),
          onCellTap: (events, date) {
            // Implement callback when user taps on a cell.
            print(events);
          },
          startDay: WeekDays.sunday, // To change the first day of the week.
          // This callback will only work if cellBuilder is null.
          onEventTap: (event, date) => print(event),
          onDateLongPress: (date) => print(date),
        ),
      ): const Loader(),
    );
  }
}
