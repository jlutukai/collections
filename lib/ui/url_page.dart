import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:weight_collection/core/enums/view_state.dart';
import 'package:weight_collection/core/viewmodels/url_model.dart';
import 'package:weight_collection/utils/buttons.dart';
import 'package:weight_collection/utils/useful.dart';

import '../utils/handle_error.dart';
import 'base_view.dart';
import 'login_page.dart';

class UrlPage extends StatefulWidget {
  static const tag = 'url';

  const UrlPage({super.key});

  @override
  _UrlPageState createState() => _UrlPageState();
}

class _UrlPageState extends State<UrlPage> {
  final TextEditingController _url = TextEditingController();
  final _formUrlKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<UrlModel>(
      builder: (context, model, child) => Scaffold(
        body: Stack(
          children: [
            // SvgPicture.asset("assets/bg.svg",
            //   fit: BoxFit.fitHeight,
            //   alignment: Alignment.center,
            //   width: MediaQuery.of(context).size.width,
            //   height: MediaQuery.of(context).size.height,
            // ),
            Container(
              decoration: const BoxDecoration(
                color: Colors.black,
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 120,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Form(
                        key: _formUrlKey,
                        child: Container(
                          height: MediaQuery.of(context).size.height - 120,
                          decoration: const BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  topRight: Radius.circular(30))),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const SizedBox(
                                height: 25,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    "assets/mainlogo.svg",
                                    width: 150,
                                    height: 200,
                                  ),
                                  // Text("Scio Mobile 2.0", style: TextStyle(color: fromHex("#255B73").withOpacity(0.5), fontSize: 18, fontWeight: FontWeight.bold),),
                                ],
                              ),
                              Flexible(
                                fit: FlexFit.loose,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 25),
                                      child: TextFormField(
                                        controller: _url,
                                        keyboardType: TextInputType.url,
                                        style: const TextStyle(
                                            color: Colors.white),
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return "Please Enter url";
                                          }
                                          return null;
                                        },
                                        decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white10,
                                          isDense: true,
                                          hintText: "example.com",
                                          labelText: "Organization Url",
                                          labelStyle:
                                              TextStyle(color: fromHex(yellow)),
                                          hintStyle: TextStyle(
                                            color: Colors.blueGrey[400],
                                          ),
                                          border: InputBorder.none,
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: fromHex(yellow)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 20),
                                      padding: const EdgeInsets.only(top: 20),
                                      child: model.state == ViewState.Idle
                                          ? commonButton(
                                              buttonName: "Verify",
                                              onClickAction: () async {
                                                if (await checkConnection()) {
                                                  onVerify(model);
                                                } else {
                                                  showToast(
                                                      "check internet connection");
                                                }
                                              })
                                          : const CupertinoActivityIndicator(),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> onVerify(UrlModel model) async {
    if (_formUrlKey.currentState!.validate()) {
      FormData data = FormData.fromMap({
        'url': _url.text,
      });
      var result = await model.veryUrl(data);
      if (result.isRight) {
        var r = result.right;
        if (r.scioUrl != null) {
          saveUrl(r.scioUrl?.data?.url??"");
          if (r.scioUrl!.data!.status == "Active") {
            showToast(r.message!);
            Navigator.pushNamedAndRemoveUntil(
                context, LoginPage.tag, (Route<dynamic> route) => false);
          } else {
            showToast(
                "Sorry Account is currently ${r.scioUrl?.data?.status ?? 'In-Active'}");
          }
        } else {
          showToast(r.message ?? "Failed");
        }
      } else {
        handleError(result.left);
      }
    }
  }
}
