import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class QRScanner extends StatefulWidget {
  static const tag = 'qr_scanner';

  const QRScanner({super.key});

  @override
  State<QRScanner> createState() => _QRScannerState();
}

class _QRScannerState extends State<QRScanner> {
  final controller = MobileScannerController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('QR Scanner')),
      body: MobileScanner(
          controller: controller,
          onDetect: (capture) {
            try {
              final List<Barcode> barcodes = capture.barcodes;
              final Uint8List? image = capture.image;
              final Barcode barcode = barcodes.first;
              final String code = barcode.rawValue!;
              Navigator.of(context).pop(code);
              controller.dispose();
            }catch(e){
              Navigator.of(context).pop();
              controller.dispose();
            }
          }),
    );
  }
}
