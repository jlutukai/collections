import 'package:flutter/material.dart';

import '../../utils/useful.dart';

commonButton({
  required String buttonName,
  required Future Function() onClickAction,
}) {
  return ElevatedButton(
    onPressed: () async {
      // await fcm.getFCMToken();
      onClickAction();
    },
    style: ElevatedButton.styleFrom(
      backgroundColor: fromHex(deepOrange),
      padding: const EdgeInsets.symmetric(
        horizontal: 50,
      ),
      textStyle: const TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
    ),
    child: Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Text(
          buttonName,
          style: const TextStyle(color: Colors.black, fontSize: 18),
        ),
      ),
    ),
  );
}
