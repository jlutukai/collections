import 'package:weight_collection/utils/useful.dart';

import '../core/models/error.dart';

void handleError(CustomError error) {
  if(error.key == AppError.ValidationError){
    showToast(error.message??"An error occurred");
  }else{
    showToast(error.message??"An error occurred");
  }
}