import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

const String noImage =
    "https://748073e22e8db794416a-cc51ef6b37841580002827d4d94d19b6.ssl.cf3.rackcdn.com/not-found.png";

String deepOrange = '#f48100';
String yellow = '#f4b600';
const String grey = '#aca793';
const String orange = '#f49600';
const String red = '#FF3232';
const String green = '#4eff4e';
var currencyFormat =
    NumberFormat.currency(locale: "en_KE", symbol: 'Ksh. ', decimalDigits: 2);

var df = DateFormat("yyyy-MM-dd"); // 2021-05-23
var dF = DateFormat("yyyy-MM-dd HH:mm:ss");
var tf = DateFormat("HH:mm");
var cf =
    NumberFormat.currency(locale: "en_KE", symbol: 'Ksh. ', decimalDigits: 2);

var wf = NumberFormat.currency(decimalDigits: 2);

var nf = NumberFormat.compact();

const String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

showToast(String msg, {int? i}) {
  Fluttertoast.showToast(
      msg: msg, toastLength: i == 1 ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
}

extension Ex on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

extension StringExtension on String {
  String capitalize() {
    if (isNotEmpty) {
      return "${this[0].toUpperCase()}${substring(1)}";
    } else {
      return "";
    }
  }
}

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
}

// setPlanner(PlannerData? data) {
//   Box currentPlanner = Hive.box("planner");
//   currentPlanner.put("current_planner", data);
// }
//
// PlannerData? getPlanner() {
//   Box currentPlanner = Hive.box("planner");
//   return currentPlanner.get("current_planner");
// }
//
// setShiftID(String data) {
//   Box<String> currentPlanner = Hive.box("accountInfo");
//   currentPlanner.put("shift_id", data);
// }
//
// String? getShift() {
//   Box<String> currentPlanner = Hive.box("accountInfo");
//   return currentPlanner.get("shift_id");
// }

setUserId(String id) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("userId", id);
}

String? getUserId() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("userId");
}

setTokenId(String id) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("fcm_token", id);
}

String? getTokenId() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("fcm_token");
}

String? getToken() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("sessionToken");
}

saveToken(String token) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("sessionToken", token);
}

saveUrl(String? url) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("url", "https://$url");
}

String? getUrl() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("url");
}

bool getTheme() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("theme") == "true";
}

setTheme(bool isDark) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.put("theme", isDark.toString());
}

saveUser(String userId, String userName) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("userId", userId);
  accountInfo.put("userName", userName);
}

clearAll() async {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  await accountInfo.clear();
}

const String hillaryPhone = "0722984495";
const String aliciaPhone = "0715219666";
const String chelimoPhone = "0700316962";

void showErrorDialog(String? r, BuildContext context, String s, [bool? bool]) {
  showModalBottomSheet(
      backgroundColor: Colors.transparent,
      clipBehavior: Clip.antiAlias,
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          child: Container(
            color: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[900],
                // color: Theme.of(context).canvasColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
              ),
              child: Padding(
                padding:
                    const EdgeInsets.only(right: 20.0, left: 20.0, top: 20),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.error_outline_rounded,
                          size: 74,
                          color: fromHex(deepOrange),
                        )
                      ],
                    ),
                    ListTile(
                      title: Center(
                        child: Text(
                          r!,
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w800,
                              fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          s,
                          style:
                              TextStyle(color: fromHex(yellow), fontSize: 14),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Row(
                      children: [
                        Text('Please contact for more help :-'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  launch("tel://$hillaryPhone");
                                },
                                child: const Row(
                                  children: [
                                    Text('Jesse - '),
                                    Text(hillaryPhone)
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  launch("tel://$aliciaPhone");
                                },
                                child: const Row(
                                  children: [
                                    Text('Alice - '),
                                    Text(aliciaPhone)
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  launch("tel://$chelimoPhone");
                                },
                                child: const Row(
                                  children: [
                                    Text('Purity - '),
                                    Text(chelimoPhone)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Expanded(child: Container()),
                        Expanded(
                          child: ListTile(
                            onTap: () async {
                              Navigator.pop(context);
                            },
                            title: const Center(
                              child: Text(
                                'Close',
                                style: TextStyle(color: Colors.deepOrange),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      });
}
